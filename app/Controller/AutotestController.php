<?php
App::uses('AppController', 'Controller');

/**
 * Created by PhpStorm.
 * User: Phan Minh
 * Date: 7/20/2016
 * Time: 2:28 PM
 */
class AutotestController extends AppController {

    public $uses = array('User', 'Team', 'Message', "OfUser");
    public $helpers = array('Paginator', 'Html', 'Form');
    public $components = array('Upload', 'Session', 'Paginator');

    public function beforeFilter() {
        $this->layout = 'test';
    }

    public function index() {
        $this->set('title_for_layout', 'List of users');
        $search = array();
        $query_datetime = '';
        $joins = [[
            'table' => 'teams',
            'alias' => 'Team',
            'type' => 'LEFT',
            'conditions' => ['Team.id = User.team_id']
        ]];
        if (isset($this->params['url']['submit_search'])) {
            $time = $this->params['url']['filter'];
            $search['filter'] = $time;
            $query_datetime = array();
            foreach ($time as $kt => $vt) {
                $datetime_from = $datetime_to = '';
                ($vt['day_from'] != '') ? ($datetime_from = strtotime($vt['day_from'] . ' ' . $vt['hour_from'])) : '';
                ($vt['day_to'] != '') ? ($datetime_to = strtotime($vt['day_to'] . ' ' . $vt['hour_to'])) : '';
                ($datetime_from != '') ? array_push($query_datetime, "User." . $kt . ">=" . $datetime_from) : '';
                ($datetime_to != '') ? array_push($query_datetime, "User." . $kt . "<=" . $datetime_to) : '';
            }
        }
        $this->paginate = array(
            'fields' => 'User.* , Team.title',
            'order' => array(
                'created_datetime' => 'DESC'
            ),
            'joins' => $joins,
            'conditions' => [$query_datetime],
            'limit' => 15,
            'page' => 1,
        );
        $users = $this->paginate('User');
        $this->set('users', $users);
        $this->set('param_search', $search);
    }

    public function team() {
        
    }

    public function create_user() {
        if ($this->request->is('post')) {
            $params = $this->request->data;
            for ($i = 1; $i <= $params['number_user']; $i++) {
                $k = $i;
                $username = $params['username_root'] . $i;
                while (true) {
                    if (!$this->User->check_exists_user($username)) {
                        break;
                    } else {
                        $k++;
                        $username = $params['username_root'] . $k;
                    }
                }
                $data_user = array(
                    'username' => $username,
                    'password' => $params['password'],
                    'toeic_level_id' => 1,
                    'word_category' => 1,
                    'account_status' => 1
                );
                if (!$this->User->save($data_user)) { // error register with
                    break;
                } else {
                    if (!$this->OfUser->save(array('username' => $this->User->getInsertID()))) { // error register with
                        break;
                    }
                }
                $this->OfUser->clear();
                $this->User->clear();
            }
        }
    }

    public function create_team_2() {
        
    }

    public function create_team_3() {
        
    }

}
