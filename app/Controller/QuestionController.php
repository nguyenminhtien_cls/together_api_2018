<?php
class QuestionController extends AppController {
	public $uses = array (
			'Question',
			'Answer',
			'Audio',
			'Sentence', 
	);
	public $helpers = array (
			'Paginator',
			'Html',
			'Form' 
	);
	public $components = array (
			'Session',
			'Paginator' 
	);
	public function index() {
		$page_header = 'List questions';

		
		if(isset($this->request->data['search']['txt_search']) && isset($this->request->data['search']['stt'])){
			
			$key = $this->request->data['search']['txt_search'];
			$stt = $this->request->data['search']['stt'];
			// pr($key); pr($stt); 
			if($stt == ''){
				$sql = "select tg_question.id_part, tg_question.id_ques, tg_question.images, tg_answer.iscorrect, tg_answer.answer, tg_audio.audio, tg_audio.tape_script, tg_audio.status from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen
						where tape_script like '%".$key."%' and id_part = 1 order by tg_question.id_ques";
			}
			elseif($key == ''){
				$sql = "select  tg_question.id_part, tg_question.id_ques, tg_question.images, tg_answer.iscorrect, tg_answer.answer, tg_audio.audio, tg_audio.tape_script, tg_audio.status  from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen
						where tg_audio.status = '".$stt."' and id_part = 1 order by tg_question.id_ques";
			}
			else{
				$sql = "select  tg_question.id_part, tg_question.id_ques, tg_question.images, tg_answer.iscorrect, tg_answer.answer, tg_audio.audio, tg_audio.tape_script, tg_audio.status  from ((tg_question 
							left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
							left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
							left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen
							where tape_script like '%".$key."%' and tg_audio.status = '".$stt."' and id_part = 1 order by tg_question.id_ques";
			}
			// pr($sql); die;
		}
	
		else{
		$sql = "select  tg_question.id_part, tg_question.id_ques, tg_question.images, tg_answer.iscorrect, tg_answer.answer, tg_audio.audio, tg_audio.tape_script, tg_audio.status  from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen where id_part = 1 order by tg_question.id_ques";
		}
		$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where id_part = 1";
		$sql2 = "select id_part from tg_question";
		$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where id_part = 1";
		// $data = $this->Question->find('all');
		
		$data = $this->Question->query($sql);
		$data1 = $this->Question->query($sql1);
		$data2 = $this->Question->query($sql2);
		$data6 = $this->Question->query($sql6);

		$this->paginate = array(
		    'conditions' => array('query' => $sql),
		    'limit' => 40,
		    'page' => 1,
		);
		$data= $this->paginate('Question');
		// pr($data2); die;
		// pr($data);die;
		// pr($data1);die;
		// $this->set('data', $data);
		$this->set('data', $data);
		$this->set('data1', $data1);
		$this->set('data2', $data2);
		$this->set('data6', $data6);
	}

	public function index_test(){
		$page_header = 'List questions';
		// $sql = "select * from ((tg_question 
		// 				left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
		// 				left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
		// 				left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen";
		// $data = $this->Question->find('all');
		
		// pr($dataA);die;
		// pr($data);die;
		// $this->set('data', $data);
		
		// $data = $this->Question->query($sql);

		// $this->set('data', $data);

		$this->paginate = array (
				'limit' => 4,
				'order' => array (
						'id' => 'desc' 
				),
				
		);
		// $data = $this->Question->find("all");
		// pr($data);die;
		$data1 = $this->paginate("Question");
		// $this->set ( compact ( 'data', 'page_header' ) );
		// pr('data1'); die;
		$this->set("data1", $data1);
	}

	public function add() {
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if($this->request->is('post')){
			// pr($this->request->data);
			//Upload Images
			if(!empty($this->request->data['Question']['images']['name'])){
				$Image=$this->request->data['Question']['images']['name'];
				$Image=sha1($this->request->data['Question']['images']['name'].rand(0,100)).'-'.$Image;
				$filename = Configure::read('upload_question_bank'). 'img'.DS.$Image; 
				$tmp_name=$this->request->data['Question']['images']['tmp_name'];
				$this->request->data['Question']['images']=$Image;
			}
			else{
				unset($this->request->data['Question']['images']);
			}
			
			move_uploaded_file($tmp_name,$filename);

			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($item['answer'] == $this->request->data['Question']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++;
			}
			pr($this->request->data);die;
			// $this->Question->saveAssociated($this->request->data);
			// $this->redirect(array('action'=>'index'));
			
		}
		// pr($this->request->data);
	}

	public function add_p1(){
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if($this->request->is('post')){
			// pr($this->request->data);
			//Upload Images
			if(!empty($this->request->data['Question']['images']['name'])){
				$Image=$this->request->data['Question']['images']['name'];
				$Image=sha1($this->request->data['Question']['images']['name'].rand(0,100)).'-'.$Image;
				$filename = Configure::read('upload_question_bank'). 'img'.DS.$Image; 
				$tmp_name=$this->request->data['Question']['images']['tmp_name'];
				$this->request->data['Question']['images']=$Image;
			}
			else{
				unset($this->request->data['Question']['images']);
			}
			
			move_uploaded_file($tmp_name,$filename);

			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Question']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++;
			}
			// pr($this->request->data);die;
			$this->Question->saveAssociated($this->request->data);
			$this->redirect(array('action'=>'index'));
			
		}
	}

	public function add_p2(){
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if($this->request->is('post')){
			// pr($this->request->data);
			
			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Question']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++;
			}
			// pr($this->request->data);die;

			$this->Question->saveAssociated($this->request->data);
			$this->redirect(array('action'=>'part2'));
			
		}
	}

	public function add_p3(){
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if($this->request->is('post')){
			// pr($this->request->data);die;
			
			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			/*$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Audio']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++; 
			}*/
			// pr($this->request->data); die; 

			$data_audi['audio']=$Audio;	
			// pr($data_audi['audio']); die;
			$this->Audio->save($data_audi);	
			// pr($this->Audio->id);
			$id_audio = $this->Audio->id;
			$tape = $this->request->data['Audio']['tape_script'];
			$status = $this->request->data['Audio']['status'];
			$this->Audio->saveField('tape_script', $tape);
			$this->Audio->saveField('status', $status);
			$data=$this->request->data;
			// $dataT = $this->request->data['Audio']['tape_script'];
			// $this->Audio->save($dataT);
			// pr($dataT);
			foreach ($data['Question'] as $item) {
				$item['id_part']=3;
				$item['id_audio']=$id_audio;

				// $i=0;
				// foreach ($item['Answer'] as $value) {
				// 	if($i == $item['abc']){
				// 		$item['Answer'][$i]['iscorrect'] = 1;
				// 	}
				// 	$i++;
				// }die;
				foreach ($item['Answer'] as $key => $value) {
					if($key == $item['abc']){
						$item['Answer'][$key]['iscorrect'] = 1;
					}
				}
				// pr($item);die;
				$this->Question->saveAssociated($item);

			}
			
			$this->redirect(array('action'=>'part3'));
		
		}
	}

	public function add_p4(){
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if($this->request->is('post')){
			// pr($this->request->data);die;
			
			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			/*$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Audio']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++; 
			}*/
			// pr($this->request->data); die; 

			$data_audi['audio']=$Audio;	
			// pr($data_audi); die;
			$this->Audio->save($data_audi);	
			// pr($this->Audio->id);
			$id_audio = $this->Audio->id;
			$tape = $this->request->data['Audio']['tape_script'];
			$status = $this->request->data['Audio']['status'];
			$this->Audio->saveField('tape_script', $tape);
			$this->Audio->saveField('status', $status);
			$data=$this->request->data;
			
			 // pr($data);die;
			foreach ($data['Question'] as $item) {
				$item['id_part']=4;
				$item['id_audio']=$id_audio;

				// $i=0;
				// foreach ($item['Answer'] as $value) {
				// 	if($i == $item['abc']){
				// 		$item['Answer'][$i]['iscorrect'] = 1;
				// 	}
				// 	$i++;
				// }die;
				foreach ($item['Answer'] as $key => $value) {
					if($key == $item['abc']){
						$item['Answer'][$key]['iscorrect'] = 1;
					}
				}
				// pr($item);die;
				$this->Question->saveAssociated($item);

			}
			
			$this->redirect(array('action'=>'part4'));
		
		}
	}

	public function add_p5(){
		if($this->request->is('post')){
			$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Question']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++;
			}
			// pr($this->request->data);die;
			$this->Question->saveAssociated($this->request->data);
			$this->redirect(array('action'=>'part5'));
		}
		
	}

	public function add_p6(){
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if($this->request->is('post')){
			// pr($this->request->data);die;
			
			//Upload Audio
			// if(!empty($this->request->data['Audio']['audio']['name'])){
			// 	$Audio=$this->request->data['Audio']['audio']['name'];
			// 	$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
			// 	$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
			// 	$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
			// 	$this->request->data['Audio']['audio']=$Audio;
			// }
			// else{
			// 	unset($this->request->data['Audio']['audio']);
			// }
			// move_uploaded_file($tmp_name,$filename2);
			

			// $data_audi['audio']=$Audio;	
			// pr($data_audi['audio']); die;
			// $this->Audio->save($data_audi);	
			// pr($this->Audio->id);
			// $id_audio = $this->Audio->id;
			// $tape = $this->request->data['Audio']['tape_script'];
			$sen = $this->request->data['Sentence']['sentence'];
			// pr($sen); die;
			$this->Sentence->saveField('sentence',$sen);
			$id_sen = $this->Sentence->id;
			// pr($this->Sentence->id);die;
			$status = $this->request->data['Sentence']['status'];
			$this->Sentence->saveField('status', $status);
			$data=$this->request->data;
			// pr($data); die;
			// pr($dataT);
			foreach ($data['Question'] as $item) {
				$item['id_part']=6;
				$item['id_sen']=$id_sen;

				
				foreach ($item['Answer'] as $key => $value) {
					if($key == $item['abc']){
						$item['Answer'][$key]['iscorrect'] = 1;
					}
				}
				// pr($item);die;
				$this->Question->saveAssociated($item);

			}
			
			$this->redirect(array('action'=>'part6'));
		
		}
	}

	public function add_p7(){
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if($this->Session->check('num')){
			$num = $this->Session->read('num');
			$this->set('num', $num);
		}
		$this->Session->delete('num');
		// $num = $this->Session->consume('num');
		
		if($this->request->is('post')){
			// pr($this->request->data);die;
			
			//Upload Audio
			// if(!empty($this->request->data['Audio']['audio']['name'])){
			// 	$Audio=$this->request->data['Audio']['audio']['name'];
			// 	$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
			// 	$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
			// 	$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
			// 	$this->request->data['Audio']['audio']=$Audio;
			// }
			// else{
			// 	unset($this->request->data['Audio']['audio']);
			// }
			// move_uploaded_file($tmp_name,$filename2);
			

			// $data_audi['audio']=$Audio;	
			// pr($data_audi['audio']); die;
			// $this->Audio->save($data_audi);	
			// pr($this->Audio->id);
			// $id_audio = $this->Audio->id;
			// $tape = $this->request->data['Audio']['tape_script'];
			$sen = $this->request->data['Sentence']['sentence'];
			// pr($sen); die;
			$this->Sentence->saveField('sentence',$sen);
			$id_sen = $this->Sentence->id;
			// pr($this->Sentence->id);die;
			$status = $this->request->data['Sentence']['status'];
			$this->Sentence->saveField('status', $status);
			$data=$this->request->data;
			// pr($data); die;
			// pr($dataT);
			foreach ($data['Question'] as $item) {
				$item['id_part']=7;
				$item['id_sen']=$id_sen;

				
				foreach ($item['Answer'] as $key => $value) {
					if($key == $item['abc']){
						$item['Answer'][$key]['iscorrect'] = 1;
					}
				}
				// pr($item);die;
				$this->Question->saveAssociated($item);

			}
			
			$this->redirect(array('action'=>'part7'));
		
		}
	}

	public function edit_p1($id = null){
		$page_header = "Edit";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if(empty($this->data)) {
    		$this->request->data = $this->Question->read(null, $id);//Lấy thông tin 
    		// pr($this->request->data);die;
    		$this->set('data',$this->request->data);
		}
		else{
			$this->request->data['Question']['id_ques'] = $id;
			//Upload Images
			if(!empty($this->request->data['Question']['images']['name'])){
				$Image=$this->request->data['Question']['images']['name'];
				$Image=sha1($this->request->data['Question']['images']['name'].rand(0,100)).'-'.$Image;
				$filename = Configure::read('upload_question_bank'). 'img'.DS.$Image; 
				$tmp_name=$this->request->data['Question']['images']['tmp_name'];
				$this->request->data['Question']['images']=$Image;
			}
			else{
				unset($this->request->data['Question']['images']);
			}
			
			move_uploaded_file($tmp_name,$filename);

			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Question']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++;
			}
			// pr($this->request->data);die;
			$this->Answer->deleteAll(array('Answer.id_ques'=>$id));
			$this->Question->saveAssociated($this->request->data);
			$this->redirect(array('action'=>'index'));
 
		}
	}

	public function edit_p2($id = null){
		$page_header = "Edit";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if(empty($this->data)) {
    		$this->request->data = $this->Question->read(null, $id);//Lấy thông tin 
    		// pr($this->request->data);die;
    		$this->set('data',$this->request->data);
		}
		else{
			$this->request->data['Question']['id_ques'] = $id;
			//Upload Images
			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Question']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++;
			}
			// pr($this->request->data);die;
			$this->Answer->deleteAll(array('Answer.id_ques'=>$id));
			$this->Question->saveAssociated($this->request->data);
			$this->redirect(array('action'=>'part2'));
 
		}
	}

	public function edit_p3($id = null){
		$page_header = "Edit part 3";
		$page_header = "Edit";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
    		// $data = $this->Audio->find('first',array(
    		// 	'conditions'=> array('Audio.id_audio' => $id),
    		// 	'contain' => array(
    		// 		'Question','Answer')
    		// 	));//Lấy thông tin 
		$data = $this->Question->find('all',array(
    			'conditions'=> array('Question.id_audio' => $id),
    			// 'contain' => array(
    			// 	'Question','Answer')
    			));

		$this->set('data', $data);
		if($this->request->is('post')){
			// pr($this->request->data); die;
			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			/*$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Audio']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++; 
			}*/
			// pr($this->request->data); die; 

			$data_audi['audio']=$Audio;	
			// pr($data_audi['audio']); die;
			// pr($this->Audio->id);

			$id_audio = $id;
			$tape = $this->request->data['Audio']['tape_script'];
			$status = $this->request->data['Audio']['status'];
			$this->Audio->id=$id;
			$this->Audio->saveField('tape_script', $tape);
			$this->Audio->saveField('status', $status);
			$this->Audio->saveField('audio',$Audio);	
			$datasubmit=$this->request->data;
			// $dataT = $this->request->data['Audio']['tape_script'];
			// $this->Audio->save($dataT);
			//pr($datasubmit);die;
			foreach ($data as $keyq => $itemques) {
				$this->Question->id=$itemques['Question']['id_ques'];
				$this->Question->saveField('question',$datasubmit['Question'][$keyq]['question']);
				foreach ($itemques['Answer'] as $keya => $answer) {
					$this->Answer->id=$answer['id_ans'];
					$answer['answer']=$datasubmit['Question'][$keyq]['Answer'][$keya]['answer'];
					if($datasubmit['Question'][$keyq]['abc']==$keya){
						$answer['iscorrect']=1;
					}
					else{
						$answer['iscorrect']=0;
					}
					$this->Answer->save($answer);
				}
			}
			
			$this->redirect(array('action'=>'part3'));
		}
	}

	public function edit_p4($id = null){
		$page_header = "Edit part 4";
		$page_header = "Edit";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
    		// $data = $this->Audio->find('first',array(
    		// 	'conditions'=> array('Audio.id_audio' => $id),
    		// 	'contain' => array(
    		// 		'Question','Answer')
    		// 	));//Lấy thông tin 
		$data = $this->Question->find('all',array(
    			'conditions'=> array('Question.id_audio' => $id),
    			// 'contain' => array(
    			// 	'Question','Answer')
    			));

		$this->set('data', $data);
		if($this->request->is('post')){
			// pr($this->request->data); die;
			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			/*$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Audio']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++; 
			}*/
			// pr($this->request->data); die; 

			$data_audi['audio']=$Audio;	
			// pr($data_audi['audio']); die;
			// pr($this->Audio->id);

			$id_audio = $id;
			$tape = $this->request->data['Audio']['tape_script'];
			$status = $this->request->data['Audio']['status'];
			$this->Audio->id=$id;
			$this->Audio->saveField('tape_script', $tape);
			$this->Audio->saveField('status', $status);
			$this->Audio->saveField('audio',$Audio);	
			$datasubmit=$this->request->data;
			// $dataT = $this->request->data['Audio']['tape_script'];
			// $this->Audio->save($dataT);
			//pr($datasubmit);die;
			foreach ($data as $keyq => $itemques) {
				$this->Question->id=$itemques['Question']['id_ques'];
				$this->Question->saveField('question',$datasubmit['Question'][$keyq]['question']);
				foreach ($itemques['Answer'] as $keya => $answer) {
					$this->Answer->id=$answer['id_ans'];
					$answer['answer']=$datasubmit['Question'][$keyq]['Answer'][$keya]['answer'];
					if($datasubmit['Question'][$keyq]['abc']==$keya){
						$answer['iscorrect']=1;
					}
					else{
						$answer['iscorrect']=0;
					}
					$this->Answer->save($answer);
				}
			}
			
			$this->redirect(array('action'=>'part4'));
		}
	}

	public function edit_p5($id = null){
		$page_header = "Edit";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if(empty($this->data)) {
    		$this->request->data = $this->Question->read(null, $id);//Lấy thông tin 
    		// pr($this->request->data);die;
    		$this->set('data',$this->request->data);
		}
		else{
			$this->request->data['Question']['id_ques'] = $id;
			$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Question']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++;
			
			}
			// pr($this->request->data);die;
			$this->Answer->deleteAll(array('Answer.id_ques'=>$id));
			$this->Question->saveAssociated($this->request->data);
			$this->redirect(array('action'=>'part5'));
 
		}
	}
	
	public function edit_p6($id = null){
		$page_header = "Edit part 6";
		$data = $this->Question->find('all',array(
    			'conditions'=> array('Question.id_sen' => $id),
    			));
		// pr($data); die;
		$this->set('data', $data);
		if($this->request->is('post')){
			// pr($this->request->data); die;
			//Upload Audio
			
			

			$sen = $this->request->data['Sentence']['sentence'];
			// pr($sen); die;
			
			$id_sen = $id;

			
			// $tape = $this->request->data['Audio']['tape_script'];
			$status = $this->request->data['Sentence']['status'];
			$this->Sentence->id=$id;
			
			$this->Sentence->saveField('status', $status);
			$this->Sentence->saveField('sentence',$sen);	
			$datasubmit=$this->request->data;
			// $dataT = $this->request->data['Audio']['tape_script'];
			// $this->Audio->save($dataT);
			//pr($datasubmit);die;
			foreach ($data as $keyq => $itemques) {
				// $this->Question->id=$itemques['Question']['id_ques'];
				// $this->Question->saveField('question',$datasubmit['Question'][$keyq]['question']);
				foreach ($itemques['Answer'] as $keya => $answer) {
					$this->Answer->id=$answer['id_ans'];
					$answer['answer']=$datasubmit['Question'][$keyq]['Answer'][$keya]['answer'];
					if($datasubmit['Question'][$keyq]['abc']==$keya){
						$answer['iscorrect']=1;
					}
					else{
						$answer['iscorrect']=0;
					}
					$this->Answer->save($answer);
				}
			}
			
			$this->redirect(array('action'=>'part6'));
		}
	}

	public function edit_p7($id = null){
		$page_header = "Edit part 7";
		$data = $this->Question->find('all',array(
    			'conditions'=> array('Question.id_sen' => $id),
    			));
		// pr($data); die;
		$this->set('data', $data);
		if($this->request->is('post')){
			// pr($this->request->data); die;
			//Upload Audio
			
			

			$sen = $this->request->data['Sentence']['sentence'];
			// pr($sen); die;
			
			$id_sen = $id;

			
			// $tape = $this->request->data['Audio']['tape_script'];
			$status = $this->request->data['Sentence']['status'];
			$this->Sentence->id=$id;
			
			$this->Sentence->saveField('status', $status);
			$this->Sentence->saveField('sentence',$sen);	
			$datasubmit=$this->request->data;
			// $dataT = $this->request->data['Audio']['tape_script'];
			// $this->Audio->save($dataT);
			//pr($datasubmit);die;
			foreach ($data as $keyq => $itemques) {
				$this->Question->id=$itemques['Question']['id_ques'];
				$this->Question->saveField('question',$datasubmit['Question'][$keyq]['question']);
				foreach ($itemques['Answer'] as $keya => $answer) {
					$this->Answer->id=$answer['id_ans'];
					$answer['answer']=$datasubmit['Question'][$keyq]['Answer'][$keya]['answer'];
					if($datasubmit['Question'][$keyq]['abc']==$keya){
						$answer['iscorrect']=1;
					}
					else{
						$answer['iscorrect']=0;
					}
					$this->Answer->save($answer);
				}
			}
			
			$this->redirect(array('action'=>'part7'));
		}
	}


	public function view_p1($id = null){
		$page_header = "Edit";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if(empty($this->data)) {
    		$this->request->data = $this->Question->read(null, $id);//Lấy thông tin 
    		// pr($this->request->data);die;
    		$this->set('data',$this->request->data);
		}
	}

	public function view_p2($id = null){
		$page_header = "Edit";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if(empty($this->data)) {
    		$this->request->data = $this->Question->read(null, $id);//Lấy thông tin 
    		// pr($this->request->data);die;
    		$this->set('data',$this->request->data);
		}
	}

	public function view_p3($id = null){
		$page_header = "View part 3";
		$page_header = "View";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
    		// $data = $this->Audio->find('first',array(
    		// 	'conditions'=> array('Audio.id_audio' => $id),
    		// 	'contain' => array(
    		// 		'Question','Answer')
    		// 	));//Lấy thông tin 
		$data = $this->Question->find('all',array(
    			'conditions'=> array('Question.id_audio' => $id),
    			// 'contain' => array(
    			// 	'Question','Answer')
    			));

		$this->set('data', $data);
		if($this->request->is('post')){
			// pr($this->request->data); die;
			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			/*$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Audio']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++; 
			}*/
			// pr($this->request->data); die; 

			$data_audi['audio']=$Audio;	
			// pr($data_audi['audio']); die;
			$this->Audio->save($data_audi);	
			// pr($this->Audio->id);
			$id_audio = $id;
			$tape = $this->request->data['Audio']['tape_script'];
			$status = $this->request->data['Audio']['status'];
			$this->Audio->saveField('tape_script', $tape);
			$this->Audio->saveField('status', $status);
			$data=$this->request->data;
			// $dataT = $this->request->data['Audio']['tape_script'];
			// $this->Audio->save($dataT);
			// pr($dataT);
			foreach ($data['Question'] as $item) {
				$item['id_part']=3;
				$item['id_audio']=$id_audio;

				// $i=0;
				// foreach ($item['Answer'] as $value) {
				// 	if($i == $item['abc']){
				// 		$item['Answer'][$i]['iscorrect'] = 1;
				// 	}
				// 	$i++;
				// }die;
				foreach ($item['Answer'] as $key => $value) {
					if($key == $item['abc']){
						$item['Answer'][$key]['iscorrect'] = 1;
					}
				}
				// pr($item);die;
				$this->Question->saveAssociated($item);

			}
			
			$this->redirect(array('action'=>'index'));
		}
	}

	public function view_p4($id = null){
		$page_header = "View part 4";
		$page_header = "View";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
    		// $data = $this->Audio->find('first',array(
    		// 	'conditions'=> array('Audio.id_audio' => $id),
    		// 	'contain' => array(
    		// 		'Question','Answer')
    		// 	));//Lấy thông tin 
		$data = $this->Question->find('all',array(
    			'conditions'=> array('Question.id_audio' => $id),
    			// 'contain' => array(
    			// 	'Question','Answer')
    			));
		// pr($data); die;
		$this->set('data', $data);
		if($this->request->is('post')){
			// pr($this->request->data); die;
			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = Configure::read('upload_question_bank'). 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			/*$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Audio']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++; 
			}*/
			// pr($this->request->data); die; 

			$data_audi['audio']=$Audio;	
			// pr($data_audi['audio']); die;
			$this->Audio->save($data_audi);	
			// pr($this->Audio->id);
			$id_audio = $id;
			$tape = $this->request->data['Audio']['tape_script'];
			$status = $this->request->data['Audio']['status'];
			$this->Audio->saveField('tape_script', $tape);
			$this->Audio->saveField('status', $status);
			$data=$this->request->data;
			// $dataT = $this->request->data['Audio']['tape_script'];
			// $this->Audio->save($dataT);
			// pr($dataT);
			foreach ($data['Question'] as $item) {
				$item['id_part']=4;
				$item['id_audio']=$id_audio;

				// $i=0;
				// foreach ($item['Answer'] as $value) {
				// 	if($i == $item['abc']){
				// 		$item['Answer'][$i]['iscorrect'] = 1;
				// 	}
				// 	$i++;
				// }die;
				foreach ($item['Answer'] as $key => $value) {
					if($key == $item['abc']){
						$item['Answer'][$key]['iscorrect'] = 1;
					}
				}
				// pr($item);die;
				$this->Question->saveAssociated($item);

			}
			
			$this->redirect(array('action'=>'index'));
		}
	}

	public function view_p5($id = null){
		$page_header = "Edit";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
		if(empty($this->data)) {
    		$this->request->data = $this->Question->read(null, $id);//Lấy thông tin 
    		// pr($this->request->data);die;
    		$this->set('data',$this->request->data);
		}
	}
	public function view_p6($id = null){
		$page_header = "View";
		$data = $this->Question->find('all',array(
    			'conditions'=> array('Question.id_sen' => $id),
    			));
		// pr($data); die;
		$this->set('data', $data);
	}
	public function view_p7($id = null){
		$page_header = "View";
		$data = $this->Question->find('all',array(
    			'conditions'=> array('Question.id_sen' => $id),
    			));
		// pr($data); die;
		$this->set('data', $data);
	}

	public function delete($id = null){
		// $this->request->data['Question']['id_ques'] = $id;
		if (isset($id) && !empty($id)){
			$this->Answer->deleteAll(array('Answer.id_ques'=>$id));
			$this->Question->deleteAll(array('Question.id_ques'=>$id));
			$this->redirect(array('action'=>'index'));
		}
		
	}

	public function delete_p($id = null){
		
		if (isset($id) && !empty($id)){

			$question=$this->Question->find('all',array('conditions'=>array('Question.id_audio'=>$id)));

			//pr($question);die;
			foreach ($question as $item) {
				foreach ($item['Answer'] as $answer) {
					$this->Answer->delete(array('Answer.id_ans'=>$answer['id_ans']));
				}
				//pr($item);die;
				$this->Question->delete(array('Question.id_ques'=>$item['Question']['id_ques']));
			}

//			$this->Question->deleteAll(array('Question.id_audio'=>$id));
			$this->Audio->delete(array('Audio.id_audio'=>$id));
			$this->redirect(array('action'=>'index'));
		}
		
	}

	public function delete_p6($id = null){
		
		if (isset($id) && !empty($id)){

			$question=$this->Question->find('all',array('conditions'=>array('Question.id_sen'=>$id)));

			//pr($question);die;
			foreach ($question as $item) {
				foreach ($item['Answer'] as $answer) {
					$this->Answer->delete(array('Answer.id_ans'=>$answer['id_ans']));
				}
				//pr($item);die;
				$this->Question->delete(array('Question.id_ques'=>$item['Question']['id_ques']));
			}

//			$this->Question->deleteAll(array('Question.id_audio'=>$id));
			$this->Sentence->delete(array('Sentence.id_sen'=>$id));
			$this->redirect(array('action'=>'index'));
		}
		
	}
	public function part2(){

		$page_header = 'List questions';

		
		if(isset($this->request->data['search']['txt_search']) && isset($this->request->data['search']['stt'])){
			
			$key = $this->request->data['search']['txt_search'];
			$stt = $this->request->data['search']['stt'];
			// pr($key); pr($stt); 
			if($stt == ''){
				$sql = "select tg_question.id_part, tg_answer.iscorrect, tg_question.id_ques, tg_audio.audio, tg_answer.answer, tg_audio.tape_script, tg_audio.status from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen
						where tape_script like '%".$key."%' and id_part = 2 order by tg_question.id_ques";
			}
			elseif($key == ''){
				$sql = "select tg_question.id_part, tg_answer.iscorrect, tg_question.id_ques, tg_audio.audio, tg_answer.answer, tg_audio.tape_script, tg_audio.status from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen
						where tg_audio.status = '".$stt."' and id_part = 2 order by tg_question.id_ques";
			}
			else{
				$sql = "select tg_question.id_part, tg_answer.iscorrect, tg_question.id_ques, tg_audio.audio, tg_answer.answer, tg_audio.tape_script, tg_audio.status from ((tg_question 
							left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
							left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
							left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen
							where tape_script like '%".$key."%' and tg_audio.status = '".$stt."' and id_part = 2 order by tg_question.id_ques";
			}
			// pr($sql); die;
		}
	
		else{
		$sql = "select tg_question.id_part, tg_answer.iscorrect, tg_question.id_ques, tg_audio.audio, tg_answer.answer, tg_audio.tape_script, tg_audio.status from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen where id_part = 2 order by tg_question.id_ques";
		}
		$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where id_part = 2";
		$sql2 = "select id_part from tg_question";
		$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where id_part = 2";
		// $data = $this->Question->find('all');
		
		$data = $this->Question->query($sql);
		$data1 = $this->Question->query($sql1);
		$data2 = $this->Question->query($sql2);
		$data6 = $this->Question->query($sql6);
		$this->paginate = array(
		    'conditions' => array('query' => $sql),
		    'limit' => 40,
		    'page' => 1,
		);
		$data= $this->paginate('Question');
		// pr($data2); die;
		// pr($data);die;
		// pr($data1);die;
		// $this->set('data', $data);
		$this->set('data', $data);
		$this->set('data1', $data1);
		$this->set('data2', $data2);
		$this->set('data6', $data6);
	}

	public function part3(){
		$page_header = 'List questions';

		$sql = "select * from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen where id_part = 3";
		
		if(isset($this->request->data['search']['txt_search']) && isset($this->request->data['search']['stt'])){
			
			$key = $this->request->data['search']['txt_search'];
			$stt = $this->request->data['search']['stt'];
			// pr($key); pr($stt); die;
			if($stt == ''){
				$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where tape_script like '%".$key."%' and id_part = 3  order by id_audio";
			}

			elseif($key == ''){
				$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where tg_audio.status = '".$stt."' and id_part = 3  order by id_audio";
			}
			else{
			$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where tape_script like '%".$key."%' and tg_audio.status = '".$stt."' and id_part = 3  order by id_audio";
			}
			// pr($sql1); die;
			// pr($key); pr($stt); die;
		}
		else{
			$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where id_part = 3 order by id_audio";
		}
		
		$sql2 = "select id_part from tg_question";
		$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where id_part = 3";
		// $data = $this->Question->find('all');
		
		$data = $this->Question->query($sql);
		$data1 = $this->Question->query($sql1);
		$data2 = $this->Question->query($sql2);
		$data6 = $this->Question->query($sql6);
		$this->paginate = array(
		    'conditions' => array('query' => $sql1),
		    'limit' => 10,
		    'page' => 1,
		);
		$data1= $this->paginate('Question');
		// pr($data2); die;
		// pr($data);die;
		// pr($data1);die;
		// $this->set('data', $data);
		$this->set('data', $data);
		$this->set('data1', $data1);
		$this->set('data2', $data2);
		$this->set('data6', $data6);
	}

	public function part4(){
		$page_header = 'List questions';

		$sql = "select * from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen where id_part = 4";
		
		if(isset($this->request->data['search']['txt_search']) && isset($this->request->data['search']['stt'])){
			
			$key = $this->request->data['search']['txt_search'];
			$stt = $this->request->data['search']['stt'];
			// pr($key); pr($stt); die;
			if($stt == ''){
				$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where tape_script like '%".$key."%' and id_part = 4 order by id_audio";
			}
			elseif($key == ''){
				$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where tg_audio.status = '".$stt."' and id_part = 4 order by id_audio";
			}
			else{
			$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where tape_script like '%".$key."%' and tg_audio.status = '".$stt."' and id_part = 4 order by id_audio";
			}
			// pr($sql1); die;
			// pr($key); pr($stt); die;
		}
		
		else{
			$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where id_part = 4 order by id_audio";
		}
		
		$sql2 = "select id_part from tg_question";
		$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where id_part = 4";
		// $data = $this->Question->find('all');
		
		$data = $this->Question->query($sql);
		$data1 = $this->Question->query($sql1);
		$data2 = $this->Question->query($sql2);
		$data6 = $this->Question->query($sql6);
		$this->paginate = array(
		    'conditions' => array('query' => $sql1),
		    'limit' => 10,
		    'page' => 1,
		);
		$data1= $this->paginate('Question');
		// pr($data2); die;
		// pr($data);die;
		// pr($data1);die;
		// $this->set('data', $data);
		$this->set('data', $data);
		$this->set('data1', $data1);
		$this->set('data2', $data2);
		$this->set('data6', $data6);
	}

	public function part5(){
		$page_header = 'List questions';

		
		if(isset($this->request->data['search']['txt_search']) && isset($this->request->data['search']['stt'])){
			
			$key = $this->request->data['search']['txt_search'];
			$stt = $this->request->data['search']['stt'];
			// pr($key); pr($stt); 
			if($stt == ''){
				$sql = "select tg_question.id_part, tg_answer.iscorrect, tg_question.id_ques, tg_sentence.sentence, tg_answer.answer, tg_sentence.status from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen
						where sentence like '%".$key."%' and id_part = 5 order by tg_question.id_ques";
			}
			elseif($key == ''){
				$sql = "select tg_question.id_part, tg_answer.iscorrect, tg_question.id_ques, tg_sentence.sentence, tg_answer.answer, tg_sentence.status from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen
						where tg_sentence.status = '".$stt."' and id_part = 5 order by tg_question.id_ques";
			}
			else{
				$sql = "select tg_question.id_part, tg_answer.iscorrect, tg_question.id_ques, tg_sentence.sentence, tg_answer.answer, tg_sentence.status from ((tg_question 
							left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
							left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
							left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen
							where sentence like '%".$key."%' and tg_sentence.status = '".$stt."' and id_part = 5 order by tg_question.id_ques";
			}
			// pr($sql); die;
		}
	
		else{
		$sql = "select tg_question.id_part, tg_answer.iscorrect, tg_question.id_ques, tg_sentence.sentence, tg_answer.answer, tg_sentence.status from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen where id_part = 5 order by tg_question.id_ques";
		}
		$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where id_part = 5";
		$sql2 = "select id_part from tg_question";
		$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where id_part = 5";
		// $data = $this->Question->find('all');
		
		$data = $this->Question->query($sql);
		$data1 = $this->Question->query($sql1);
		$data2 = $this->Question->query($sql2);
		$data6 = $this->Question->query($sql6);
		$this->paginate = array(
		    'conditions' => array('query' => $sql),
		    'limit' => 40,
		    'page' => 1,
		);
		$data= $this->paginate('Question');
		// pr($data2); die;
		// pr($data);die;
		// pr($data1);die;
		// $this->set('data', $data);
		$this->set('data', $data);
		$this->set('data1', $data1);
		$this->set('data2', $data2);
		$this->set('data6', $data6);
	}

	public function part6(){
		$page_header = 'List questions';
		$sql = "select * from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen where id_part = 6 order by tg_sentence.id_sen";

		if(isset($this->request->data['search']['txt_search']) && isset($this->request->data['search']['stt'])){
			$key = $this->request->data['search']['txt_search'];
			$stt = $this->request->data['search']['stt'];
			if($stt == ''){
				$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where sentence like '%".$key."%' and id_part = 6 order by tg_sentence.id_sen";
			}
			elseif($key == ''){
				$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where tg_sentence.status = '".$stt."' and id_part = 6 order by tg_sentence.id_sen";
			}
			else{
				$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where sentence like '%".$key."%' and tg_sentence.status = '".$stt."' and id_part = 6 order by tg_sentence.id_sen";
			}
		}
		else{
			$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where id_part = 6 order by tg_sentence.id_sen";
		}
		$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where id_part = 6";
		$sql2 = "select id_part from tg_question";
		
		// $data = $this->Question->find('all');
		
		$data = $this->Question->query($sql);
		$data1 = $this->Question->query($sql1);
		$data2 = $this->Question->query($sql2);
		$data6 = $this->Question->query($sql6);
		// pr($data2); die;
		
		// pr($data1);die;
		// $this->set('data', $data);

		$this->paginate = array (
		    'conditions' => array('query' => $sql6),
			'limit' => 5,
			'page' => 1
		);
		$data6= $this->paginate('Question');
		$this->set ( compact ( 'records', 'page_header' ) );
		$this->set('data', $data);
		$this->set('data1', $data1);
		$this->set('data2', $data2);
		$this->set('data6', $data6);
	}

	public function part7(){
		$page_header = 'List questions';
		$sql = "select * from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen where id_part = 7";

		if(isset($this->request->data['search']['txt_search']) && isset($this->request->data['search']['stt'])){
			$key = $this->request->data['search']['txt_search'];
			$stt = $this->request->data['search']['stt'];
			if($stt == ''){
				$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where sentence like '%".$key."%' and id_part = 7 order by tg_sentence.id_sen";
			}
			elseif($key == ''){
				$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where tg_sentence.status = '".$stt."' and id_part = 7 order by tg_sentence.id_sen";
			}
			else{
				$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where sentence like '%".$key."%' and tg_sentence.status = '".$stt."' and id_part = 7 order by tg_sentence.id_sen";
			}
		}
		else{
			$sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where id_part = 7 order by tg_sentence.id_sen";
		}
		$sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where id_part = 7";
		$sql2 = "select id_part from tg_question";
		
		// $data = $this->Question->find('all');
		
		$data = $this->Question->query($sql);
		$data1 = $this->Question->query($sql1);
		$data2 = $this->Question->query($sql2);
		$data6 = $this->Question->query($sql6);
		// pr($data2); die;
		
		// pr($data1);die;
		// $this->set('data', $data);

		$this->paginate = array (
		    'conditions' => array('query' => $sql6),
		    'limit' => 5,
		    'page' => 1
		);
		$data6= $this->paginate('Question');
		$this->set ( compact ( 'records', 'page_header' ) );
		$this->set('data', $data);
		$this->set('data1', $data1);
		$this->set('data2', $data2);
		$this->set('data6', $data6);
	}

	public function number_ques($num){
		$this->autoRender = false;
		if($this->request->is('post')){
			$num = $this->request->data['Question']['num'];
			$this->Session->write('num', $num);
			$this->redirect(array('action'=>'add_p7'));
		}
	}
}