<?php

App::uses('AppController', 'Controller');

/**
 * User: Nguyen Thai
 * Date: 12/23/2016
 */
class MentorController extends AppController {

    public $uses = array('User', 'Team', 'Message', 'OfUser', "Question", "OfMucMember", 'OfMucRoom');
    //public $uses = array('ofUser', 'ofMucRoom', 'Message');
    public $helpers = array('Paginator', 'Html', 'Form');
    public $components = array('Upload', 'Session', 'Paginator');

    public function list_class() {
        $this->set('title_for_layout', 'List of class');
        $search = array();
        $conditions = [];
        if (isset($this->params->query['filters'])) {
            $filters = $this->params->query['filters'];
            if (trim($filters['class_name']) != '') {
                $conditions[] = "OfMucRoom.naturalName LIKE '%" . trim($filters['class_name']) . "%'";
            }
            if (isset($filters['class_id']) && $filters['class_id'] != '') {
                $conditions[] = "OfMucRoom.roomID = " . $filters['class_id'];
            }
        }
        $this->paginate = array(
            'fields' => 'OfMucRoom.*',
            'order' => array(
                'id' => 'DESC'
            ),
            'conditions' => $conditions,
            'limit' => 15,
            'page' => 1,
        );
        $class_data = $this->paginate('OfMucRoom');
        $this->set('class_data', $class_data);
        $this->set('page_header', 'class list');
        if (isset($filters)) {
            $this->set('filters', $filters);
        }
    }

    public function get_class_info() {
        
    }


    public function list_member_of_class() {
        $class_code = $this->params->query['class_code'];
        $conditions = [];
        $conditions['OfMucMember.roomID'] = $class_code;
        $this->paginate = array(
            'fields' => array('OfMucMember.*', 'OfUser.*'),
            'conditions' => $conditions,
            'joins' => array(
                array(
                    'table' => 'ofUser',
                    'alias' => 'OfUser',
                    'type' => 'inner',
                    'conditions' => array(
                        'OfUser.username = OfMucMember.nickname'
                    )
                )
            )
        );
        $member_data = $this->paginate('OfMucMember');
        $this->set('member_data', $member_data);
        $this->set('users', $member_data);
    }

    public function questions() {
        $page_header = 'List questions';

        $sql = "select * from ((tg_question 
						left join tg_answer on tg_question.id_ques = tg_answer.id_ques)
						left join tg_audio on tg_question.id_audio = tg_audio.id_audio)
						left join tg_sentence on tg_question.id_sen = tg_sentence.id_sen where id_part = 4";

        if (isset($this->request->data['search']['txt_search']) && isset($this->request->data['search']['stt'])) {

            $key = $this->request->data['search']['txt_search'];
            $stt = $this->request->data['search']['stt'];
            // pr($key); pr($stt); die;
            if ($stt == '') {
                $sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where tape_script like '%" . $key . "%' and id_part = 4 order by id_audio";
            } elseif ($key == '') {
                $sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where tg_audio.status = '" . $stt . "' and id_part = 4 order by id_audio";
            } else {
                $sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where tape_script like '%" . $key . "%' and tg_audio.status = '" . $stt . "' and id_part = 4 order by id_audio";
            }
            // pr($sql1); die;
            // pr($key); pr($stt); die;
        } else {
            $sql1 = "select DISTINCT tg_audio.id_audio, audio, tape_script, tg_audio.status, id_part from tg_audio inner join tg_question on tg_audio.id_audio = tg_question.id_audio where id_part = 4 order by id_audio";
        }

        $sql2 = "select id_part from tg_question";
        $sql6 = "select DISTINCT tg_sentence.id_sen, sentence, tg_sentence.status, id_part from tg_sentence inner join tg_question on tg_sentence.id_sen = tg_question.id_sen where id_part = 4";
        // $data = $this->Question->find('all');

        $data = $this->Question->query($sql);
        $data1 = $this->Question->query($sql1);
        $data2 = $this->Question->query($sql2);
        $data6 = $this->Question->query($sql6);
        $this->paginate = array(
            'conditions' => array('query' => $sql1),
            'limit' => 10,
            'page' => 1,
        );
        $data1 = $this->paginate('Question');
        // pr($data2); die;
        // pr($data);die;
        // pr($data1);die;
        // $this->set('data', $data);
        $this->set('data', $data);
        $this->set('data1', $data1);
        $this->set('data2', $data2);
        $this->set('data6', $data6);
    }

}
