<?php

App::uses('Controller', 'Controller');

class AppController extends Controller {

    public $uses = array('VUser');
    public $components = array(
        'Session',
        'Auth' => array(
            'loginAction' => array(
                'controller' => 'auth',
                'action' => 'login'
            ),
            'authError' => 'You must be logged in to view this page!!!',
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'VUser',
                    'fields' => array('username' => 'username'),
                    'passwordHasher' => array(
                        'className' => 'Simple',
                        'hashType' => 'md5'
                    ),
                //authenticate for only user have type and id as declared
//                    'scope' => array('type' => 0, )
                )
            )
        ),
        'Common'
    );

    // call when data user changed
    // when change password, profile
    function refreshAuthData() {
        $user_data = $this->User->findById($this->Auth->User('id'));
        if ($user_data) {
            $auth_data = $user_data['User'];
            $this->Session->write('Auth.User', $auth_data);
        } else {  // logout
            $this->redirect($this->Auth->logout());
        }
    }

    function beforeFilter() {
        // auto detect layout
        if ($this->request->is('ajax')) {
            $this->layout = 'ajax';
        } else {
            $this->layout = 'sakama';
        }

        // for auth login
        if (strpos($this->request->controller, 'auth') !== false && $this->action == 'login') {
            $this->layout = 'sakama_simple';
        }
    }

    // for AJAX

    function responseApi($code = 'OK', $message = '', $data = array(), $header_type = 'json') {
        if ($header_type == 'json') {
            header('Content-Type: application/json');
        } else {
            header('Content-Type: text/html');
        }

        $this->setHeader($code);

        echo json_encode(array(
            'code' => $code,
            'message' => $message,
            'data' => $data
                )
        );
        die;
    }

}
