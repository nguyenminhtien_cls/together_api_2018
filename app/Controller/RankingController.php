<?php
/**
 * ranking
 * -------------------------------
 * Author : ndhung.dev@gmail.com
 * Created : 13/01/2017
 */
App::uses('AppController', 'Controller');

class RankingController extends AppController
{
    public $uses = array('User', 'Team', 'Ranking');
    public $helpers = array('Paginator', 'Html', 'Form');
    public $components = array('Session', 'Paginator');

    public function index() {
    	$conditions = [];
    	$order = ['id' => 'asc'];
    	if (isset($this->params->query['filters'])) {
    		$filters = $this->params->query['filters'];
    		if(isset($filters['type_top'])) {
    			$date = $this->convertTimeTop($filters['type_top']);
    			if ($date) {
    				$conditions['week'] = $date['week'];
    				$conditions['month'] = $date['month'];
    				$conditions['year'] = $date['year'];
    				$order = ['score' => 'desc'];
    			}
    		}

    		if (isset($filters['group_name']) && trim($filters['group_name'] != '')) {
    			$groups = $this->Team->find('list', [
    				'conditions' => [
    					'title LIKE' => "%".strip_tags($filters['group_name'])."%" 
    				],
    				'limit' => 100
    			]);	
    			if (!empty($groups)) {
    				foreach ($groups as $k => $v) {
    					$list_group_ids[] = $k;
    				}
    				$conditions['team_id'] = $list_group_ids;
    			}
    		}

    		if (isset($filters['week']) && is_numeric($filters['week'])) {
    			$conditions['week'] = $filters['week'];
    		}

    		if (isset($filters['level_bonus']) && trim($filters['level_bonus']) != '') {
    			$conditions['NOT'] =  ['level_bonus' => ['0', 'null']];
    			$order = ['level_bonus' => 'desc'];
    		}
    	}
    	$this->Paginator->settings = array(
    		'Ranking' => [ 
	    		'limit' => 20,
	            'conditions' => $conditions,
	            'order' => $order
	        ],
	        'contain' => ['Team']
		);
    	$ranking = $this->Paginator->paginate('Ranking');
    	$data = array(
    		'ranking' => $ranking
    	);
    	$this->set($data);
    	if (isset($filters)) {
            $this->set('filters', $filters);
        }
    	$this->set('page_header', 'Ranking list');
    }

    public function edit($id='') {
    	$ranking = $this->Ranking->find('first',array(
            'conditions' => array('Ranking.id' => $id)
            ));
    	if (empty($ranking)) {
    		throw new Exception("Record is not exist", 1);
    	}
    	if ($this->request->is('post') || $this->request->is('put')) {
    		if ($this->Ranking->save($this->request->data['Ranking'])) {
    			$ranking = $this->Ranking->find('first',array(
	            	'conditions' => array('Ranking.id' => $id)
	            ));
	            $this->Session->setFlash('Update is successlly', 'Flashs/success', array(), 'success');
    		} else {
    			$this->Session->setFlash('Something bad, please review data', 'Flashs/success', array(), 'success');
    		}
    	}
    	$data = [
    		'ranking' => $ranking
    	];
    	$this->set($data);
    	$this->set('page_header', 'Edit Ranking');	
    }

    private function convertTimeTop($type_top=null) {
  		$current_time = time();
    	switch ($type_top) {
    		case 'ON WEEKEND':
    			$get_time = $current_time;
    			break;
    		case 'LAST WEEKEND':
    			$get_time = $current_time - 7*24*60*60;
    			break;
    		case 'TWO WEEKEND AGO':
    			$get_time = $current_time - 14*24*60*60;
    			break;
    		default:
    			break;
    	}
    	if (isset($get_time)) {
    		$date = new DateTime('@'.$get_time);
    		return ['week' => $date->format("W"), 'month' =>$date->format("m"), 'year' =>$date->format("Y")];	
    	}
    	return null;
    }
}