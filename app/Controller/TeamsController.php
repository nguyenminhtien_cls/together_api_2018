<?php

App::uses('AppController', 'Controller');

/**
 * User: Nguyen Thai
 * Date: 12/23/2016
 */
class TeamsController extends AppController {

    public $uses = array('User', 'Team', 'Message');
    //public $uses = array('ofUser', 'ofMucRoom', 'Message');
    public $helpers = array('Paginator', 'Html', 'Form');
    public $components = array('Upload', 'Session', 'Paginator');

    public function add() {
        if ($this->request->is('post')) {
            $results = [
                'error' => 1,
                'msg' => 'Cant not add new team, please review input data'
            ];

            $Form_Data = $this->request->data;
            $this->Team->set($Form_Data);
            if (!$this->Team->validates()) {
                if ($Form_Data['Team']['title'] == '') {
                    $Form_Data['Team']['title'] = 'New team';
                }
                // process image
                if (!empty($Form_Data['Team']['avatar']['tmp_name'])) {
                    $image = $Form_Data['Team']['avatar'];
                    $upload_config = array(
                        'file' => $image,
                        'path' => Configure::read('file_dir'),
                    );
                    $file_name = $this->Upload->copy($upload_config);
                    $Form_Data['Team']['avatar'] = $file_name;
                } else {
                    $Form_Data['Team']['avatar'] = null;
                }
                if (!empty($Form_Data['Team']['list_member'])) {
                    $i = 1;
                    foreach ($Form_Data['Team']['list_member'] as $value) {
                        $Form_Data['Team']['member' . $i . '_id'] = $value;
                        $Form_Data['User'][$i] = array('id' => $value);
                        $i++;
                    }
                }
                unset($Form_Data['list_member']);
                if ($this->Team->saveAll($Form_Data)) {
                    $results = [
                        'error' => 0,
                        'msg' => 'Successfully',
                        'data' => ['id' => $this->Team->id]
                    ];
                }
            }
            exit(json_encode($results));
        }
        $User_list = $this->User->find('list', array(
            'fields' => array('id', 'username'),
            'order' => array('id' => 'desc'),
            'conditions' => array('team_id' => 0)
        ));
        $data = array(
            'User_list' => $User_list,
        );
        $this->set($data);
    }

    public function edit($id = null) {
        $team = $this->Team->find('first', array(
            'contain' => ['User' => ['fields' => ['User.id', 'User.username']]],
            'fields' => ['id', 'avatar', 'title', 'member1_id', 'member2_id', 'member3_id'],
            'conditions' => array('Team.id' => $id)
        ));
        if (empty($team)) {
            throw new Exception("Team is not exist", 1);
        }
        $list_id = [];
        $list_member = [];

        if (!empty($team['User'])) {
            foreach ($team['User'] as $k => $v) {
                $list_member[] = [
                    $v['id'] => $v['username']
                ];
                $list_id[] = (int) $v['id'];
            }
        }
        $group_info = [
            'id' => $team['Team']['id'],
            'title' => $team['Team']['title'],
            'avatar' => $team['Team']['avatar']
        ];
        if ($this->request->is('post')) {
            $results = [
                'error' => 1,
                'msg' => 'Cant not update, please review input data'
            ];
            $Form_Data = $this->request->data;
            $this->Team->set($Form_Data);
            if (!$this->Team->validates()) {
                if (!empty($Form_Data['Team']['avatar']['tmp_name'])) {
                    $image = $Form_Data['Team']['avatar'];
                    $upload_config = array(
                        'file' => $image,
                        'path' => Configure::read('file_dir'),
                    );
                    $file_name = $this->Upload->copy($upload_config);
                    $Form_Data['Team']['avatar'] = $file_name;
                } else {
                    unset($Form_Data['Team']['avatar']);
                }
                if (!empty($Form_Data['Team']['list_member'])) {
                    $i = 1;
                    foreach ($Form_Data['Team']['list_member'] as $value) {
                        $Form_Data['Team']['member' . $i . '_id'] = $value;
                        $Form_Data['User'][$i] = array('id' => $value);
                        $i++;
                    }
                }
                for ($i = 1; $i <= 3; $i++) {
                    if ($team['Team']['member' . $i . '_id'] != null) {
                        $this->User->id = $team['Team']['member' . $i . '_id'];
                        $this->User->save(['team_id' => null]);
                    }
                    $Form_Data['Team']['member' . $i . '_id'] = null;
                    if (!empty($Form_Data['Team']['list_member'])) {
                        if (isset($Form_Data['Team']['list_member'][$i - 1])) {
                            $Form_Data['Team']['member' . $i . '_id'] = $Form_Data['Team']['list_member'][$i - 1];
                        }
                    }
                }
                unset($Form_Data['list_member']);
                if ($this->Team->saveAll($Form_Data)) {
                    $results = [
                        'error' => 0,
                        'msg' => 'Successfully',
                        'data' => ['id' => $id]
                    ];
                }
            }
            exit(json_encode($results));
        }
        $this->set('group_info', $group_info);
        $this->set('list_member', $list_member);
        $this->set('list_id', $list_id);
        $this->set('page_header', 'Edit team');
    }

    public function index() {
        $conditions = [];
        // set conditions
        if (isset($this->params->query['filters'])) {
            $filters = $this->params->query['filters'];
            if (trim($filters['title']) != '') {
                $conditions[] = "Team.title LIKE '%" . trim($filters['title']) . "%'";
            }
            if (isset($filters['member']) && $filters['member'] != '') {
                // find member on table user
                $users = $this->User->find('list', [
                    'fields' => ['id'],
                    'conditions' => [
                        'username LIKE' => "%" . trim($filters['member']) . "%",
                    ]
                ]);
                if ($users) {
                    foreach ($users as $k => $u) {
                        $ids[] = $k;
                    }
                    $conditions[] = [
                        'OR' => ['Team.member1_id' => $ids, 'Team.member2_id' => $ids, 'Team.member3_id' => $ids]
                    ];
                }
            }
            if (isset($filters['status']) && in_array($filters['status'], ['ACTIVATED', 'DEACTIVATED'])) {
                if ($filters['status'] == 'ACTIVATED') {
                    $status = 1;
                } else {
                    $status = 0;
                }
                $conditions[] = "Team.is_published = '" . $status . "'";
            }
        }
        $this->Paginator->settings = array(
            'limit' => 20,
            'order' => array('id' => 'desc'),
            'conditions' => $conditions,
        );
        $team_all = $this->paginate('Team');
        $data = array(
            'team_all' => $team_all
        );
        $this->set($data);
        if (isset($filters)) {
            $this->set('filters', $filters);
        }
        $this->set('page_header', 'Teams list');
    }

    /**
     * change status for team
     */
    public function changestatus($team_id = null, $status = null) {
        if (in_array($status, [0, 1]) && is_numeric($team_id)) {
            $sql = "Update teams SET is_published=" . $status . " WHERE id = " . $team_id;
            if ($this->Team->query($sql)) {
                exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not change status item')));
    }

    public function remove() {
        $id = $this->request->data['id'];
        if (is_array($id) && !empty($id)) {
            foreach ($id as $item) {
                $this->Team->delete($id);
            }
        } else {
            $this->Team->delete($id);
        }
        exit(json_encode(array('status' => true, 'message' => 'Successfully',)));
    }

    public function searchmembers() {
        $name = $this->request->query('q');
        $users = $this->User->find('all', [
            'fields' => ['id', 'username', 'nickname', 'avatar'],
            'conditions' => [
                'OR' => [
                    'username LIKE' => "%" . $name . "%",
                    'nickname LIKE' => "%" . $name . "%"
                ]
            ],
            "limit" => 50
        ]);
        $results = [];
        if (!empty($users)) {
            foreach ($users as $k => $r) {
                $results['items'][] = [
                    'id' => $r['User']['id'],
                    'username' => $r['User']['username'],
                    'avatar' => ($r['User']['avatar']) ? $this->webroot . 'uploads/files/' . $r['User']['avatar'] : $this->webroot . 'img/no-avatar.jpg',
                    'nickname' => $r['User']['nickname']
                ];
            }
        }
        exit(json_encode($results));
    }

    public function checkExistTeamUser() {
        $this->autoRender = false;
        $list_member = $this->request->data['Team']['list_member'];
        $records = $this->User->find('list', [
            'fields' => ['username'],
            'conditions' => [
                'id' => $list_member,
                'NOT' => [
                    'team_id' => ["null", "0"]
                ]
            ]
        ]);
        if (!empty($records)) {
            $msg = '';
            foreach ($records as $r => $v) {
                $msg .= $v . ';';
            }
            $msg = trim($msg, ';');
            $msg .= ' existed in other group, are you sure change?';
            exit($msg);
        } else {
            exit(0);
        }
    }

    public function invite_bylink() {
        $this->autoLayout = FALSE;
        $param['departure_type'] = $this->request->query('departure_type');
        $param['title'] = $this->request->query('title');
        $param['user_type'] = $this->request->query('user_type');
        $param['scree_type'] = $this->request->query('scree_type');
        $param['user_id'] = $this->request->query('user_id');
        $param['other_user_id'] = $this->request->query('other_user_id');
        $param['travel_id'] = $this->request->query('travel_id');
        $param['campaign_id'] = $this->request->query('campaign_id');
        $param['trip_id'] = $this->request->query('trip_id');
        $param['sourceCoordinate'] = $this->request->query('sourceCoordinate');
        $param['destCoordinate'] = $this->request->query('destCoordinate');
        $ios = "https://itunes.apple.com/jp/app/%E5%89%B2%E3%82%8A%E5%8B%98%E3%81%A7%E5%BE%97%E3%81%99%E3%82%8B%E7%9B%B8%E4%B9%97%E3%82%8A%E3%82%A2%E3%83%97%E3%83%AA-nori-na-%E3%83%8E%E3%83%AA%E3%83%BC%E3%83%8A/id1133036671";
        $android = "https://play.google.com/store/apps/details?id=jp.co.zerotoone.norina";
        $app_url = 'norina://home/' . $param['scree_type'] . '?user_type=' . $param['user_type'];
        if (!empty($param['user_id'])) {
            $app_url = $app_url . '&user_id=' . $param['user_id'];
        }
        if (!empty($param['other_user_id'])) {
            $app_url = $app_url . '&other_user_id=' . $param['other_user_id'];
        }
        if (!empty($param['travel_id'])) {
            $app_url = $app_url . '&travel_id=' . $param['travel_id'];
        }
        if (!empty($param['campaign_id'])) {
            $app_url = $app_url . '&campaign_id=' . $param['campaign_id'];
        }
        if (!empty($param['trip_id'])) {
            $app_url = $app_url . '&trip_id=' . $param['trip_id'];
        }
        if (!empty($param['sourceCoordinate'])) {
            $app_url = $app_url . '&sourceCoordinate=' . $param['sourceCoordinate'];
        }
        if (!empty($param['destCoordinate'])) {
            $app_url = $app_url . '&destCoordinate=' . $param['destCoordinate'];
        }
        if (!empty($param['title'])) {
            $app_url = $app_url . '&title=' . $param['title'];
        }
        if (!empty($param['departure_type'])) {
            $app_url = $app_url . '&departure_type=' . $param['departure_type'];
        }
        $this->set('ios', $ios);
        $this->set('android', $android);
        $this->set('app_url', $app_url);
    }

}
