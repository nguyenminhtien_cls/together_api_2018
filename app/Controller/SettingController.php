<?php

App::uses('AppController', 'Controller');


/**
 * Author: ndhung.dev@gmail.com
 * Date: 19/01/2017
 */
class SettingController extends AppController
{
	public $helpers = array('Paginator', 'Html', 'Form');
	public $components = array('Upload', 'Session', 'Paginator');

	/**
	 * General setting
	 * @return [type] [description]
	 */
	public function index() {
		$data = array(
	      'page_header' => " General Settings",
	     );
	    $this->set($data);
	}

	public function staticpage() {
		$data = array(
	      'page_header' => " Static page settings",
	     );
	    $this->set($data);	
	}
}