<?php

App::uses('AppController', 'Controller');


/**
 * User: Nguyen Thai
 * Date: 12/23/2016
 */
class GamesController extends AppController
{
	public $uses = array('Game', 'GameScheduler', 'GameDataSentence', 'GameDataPronounce');
  public $helpers = array('Paginator', 'Html', 'Form');
  public $components = array('Upload', 'Session', 'Paginator', 'DynamicModel');

  /**
   * [index description]
   * -----------------------------
   * Author : ndhung.dev@gmail.com
   * Date : 13/01/2016
   */
  public function index(){
    $conditions = [];
    $filters = [];
    if ($this->request->is('post')) {
        $filters = $this->request->data;
        if (trim($filters['game_name']) != '') {
           $conditions['name LIKE '] = "%{$filters['game_name']}%";
        }
    }
    $games = $this->Game->find('all', [
      'conditions' => $conditions
    ]);  

    $data = array(
      'games' => $games,
      'filters' => $filters,
      'page_header' => "Games",
      );
    $this->set($data);
  }

  /**
   * [add description]
   * -----------------------------
   * Author : ndhung.dev@gmail.com
   * Date : 13/01/2016
   */
  public function add() {
    die('stop');
    if ($this->request->is('post')) {
      $form_data = $this->request->data;

      // process image
      if (!empty($form_data['Game']['avatar']['tmp_name'])) {
        $image = $form_data['Game']['avatar'];
        $upload_config = array(
            'file' => $image,
            'path' => Configure::read('file_dir'),
        );
        $file_name = $this->Upload->copy($upload_config);
        $form_data['Game']['avatar'] = $file_name;
      } else {
        $form_data['Game']['avatar'] = '';
      }

      // process save data to db
      try {
        if ($this->Game->save($form_data)) {
          $this->Session->setFlash('Game has been created', 'Flashs/success', [], 'flashlist');
          $this->redirect(
            ['action' => 'index']
          );  
        } else {
          $this->Session->setFlash(($string= $this->Common->convertErrorsToString($this->User->validationErrors))?$string:'Can not save data', 'errors', [], 'game');
        }
      } catch (Exception $e) {
        throw new Exception("Error Processing Request", 1);
      }
      $this->set('Game', $this->request->data['Game']);
    }
    $data = [
      'page_header' => 'Create new game'
    ];
    $this->set($data);
  }

  public function gamedata($shortcode=null) {
    $page_header = "Edit game";
    $title_for_layout = 'Game data '.$shortcode;
    $tab_type = "game_data";
    // get detail game 
    $detailGame = $this->Game->find('first', array(
      'conditions' => array(
        'short_code' => $shortcode
      )
    ));
    $today = date('Y-m-d'); 
    $date = new DateTime($today);
    $on_week = $date->format("W");
    $day_in_week = date('w', time());
    $days = ['monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday', 'sunday']; 

    $filters = array(
      'week' => (int)$on_week, 
      'day_in_week' => $days[$day_in_week-1],
      'year' => date('Y')
    );

    $conditions = array(
      'week' => (int)$on_week,
      'day_in_week' => $day_in_week,
      'year' => date('Y')
    );

    if (isset($this->params->query['filters'])) {
        $filters = $this->params->query['filters'];
        if (isset($filters['day_in_week'])) {
          $filters['day_in_week'] = strtolower($filters['day_in_week']);
        } else {
          $filters['day_in_week'] = $days[$day_in_week-1];
        }

        if (isset($filters['day_in_week']) && trim($filters['day_in_week']) != '') {
            $conditions['day_in_week'] = array_search($filters['day_in_week'], $days)+1;
        }
        if (isset($filters['year']) && trim($filters['year']) != '') {
            $conditions['year'] = $filters['year'];
        }
        if (isset($filters['week']) && trim($filters['week']) != '') {
            $conditions['week'] = $filters['week'];
        }
        if (trim($filters['team_or_member']) != '') {
            $conditions['Team.title LIKE '] = "%{$filters['team_or_member']}%";
        }
    }

    $this->paginate = array(
        'fields' => '*',
        'order' => array(
            'created_datetime' => 'DESC'
        ),
        'conditions' => $conditions,
        'limit' => 30
    );
    $words = $this->paginate('GameData'.ucfirst($shortcode));
    $data = compact('page_header', 'title_for_layout', 'shortcode', 'tab_type', 'on_week','filters','detailGame','words');
    $this->set($data);
    $this->render('/Games/gamedata/GameData'.ucfirst($shortcode));
  }

  /**
   * [edit description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   * ----------------------------------
   * Author : ndhung.dev@gmail.com
   * Date : 13/01/2016
   */
  public function edit($id=null) {
    $page_header = "Edit game";
    $title_for_layout = 'Edit game';
    $tab_type = "game_edit";
    $Game = $this->Game->find('first', [
      'conditions' => [
        'id' => $id
      ]
    ]);
    if (empty($Game)) {
      throw new Exception("Game is not exist", 1);
    }
    
    if ($this->request->is('post') || $this->request->is('put')) {
      $form_data = $this->request->data;
      
      // process image
      if (!empty($form_data['Game']['avatar']['tmp_name'])) {
        $image = $form_data['Game']['avatar'];
        $upload_config = array(
            'file' => $image,
            'path' => Configure::read('file_dir'),
        );
        $file_name = $this->Upload->copy($upload_config);
        $form_data['Game']['avatar'] = $file_name;
      } else {
        unset($form_data['Game']['avatar']);
      }
      $form_data['Game']['updated_datetime'] = date('Y-m-d H:i:s');
      $this->Game->save($form_data);
      // process updated data to db
      try {
        if ($this->Game->save($form_data)) {
          $this->Session->setFlash('Game has been updated', 'Flashs/success', [], 'game');
          $this->redirect(
            ['action' => 'index']
          );  
        } else {
          $this->Session->setFlash(($string= $this->Common->convertErrorsToString($this->User->validationErrors))?$string:'Can not update data', 'errors', [], 'game');
        }
      } catch (Exception $e) {
        throw new Exception("Error Processing Request", 1);
      }
      $this->set('Game', $this->request->data['Game']);
    }

    $data = compact('page_header','title_for_layout', 'tab_type', 'Game');
    $this->set($data);
  }

  /**
   *  Game calendar crontab
   */
  public function gamecalendar($game_id=null) {
    $page_header = "Edit game";
    $title_for_layout = 'Edit game';
    $tab_type = "game_edit";
    $Game = $this->Game->find('first', [
      'conditions' => [
        'id' => $game_id
      ]
    ]);
    if (empty($Game)) {
      throw new Exception("Game is not exist", 1);
    } 
    $data = compact('page_header','title_for_layout', 'tab_type', 'Game');
    $this->set($data);
  } 

  /**
   * [setting description]
   * @return [type] [description]
   * --------------------------------
   * Author : ndhung.dev@gmail.com
   * Date : 13/01/2017
   */
  public function setting(){
    $conditions = [];
    $filters = [];
    $list_apps = array();
    $apps = $this->Game->find('all', array('contain' => 'GameScheduler'));
    if (!empty($apps)) {
      foreach ($apps as $k => $app) {
        $gamescheduler = $this->GameScheduler->find('first', array(
            'contain' => array('Game'),
            'conditions' => array('game_id' => $app['Game']['id'])
        ));
        $gameinfo = array(
          'game_id' => $app['Game']['id'],
          'game_name' => $app['Game']['name'],
          'scheduler' => []
        ); 
        if (!empty($app['GameScheduler'])) {
          foreach ($app['GameScheduler'] as $m => $s) {
            $points[$s['day']] = array(
              'id' => $s['id'],
              'bash_timepoints' => json_decode($s['bash_timepoints']),
              'push_timepoints' => json_decode($s['push_timepoints']),
              'status_bash_timepoints' => $s['status_bash_timepoints'],
              'status_push_timepoints' => $s['status_push_timepoints']
            );
          }
          $gameinfo['scheduler'] = $points;
        }
        $list_apps[] = $gameinfo;
      }
    }
    $data = array(
      'list_apps' => $list_apps,
      'filters' => $filters,
      'page_header' => "Game Settings",
      'type_scheduler' => 'push_timepoints'
      );
    $this->set($data);
  } 

  /**
   * [setting_edit description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   * Author : ndhung.dev@gmail.com
   * Date : 13/01/2017
   */
  public function setting_edit($id = null) {
    $data = [
      'page_header' => 'View Setting'
    ];
    $this->set($data);
  }

  /**
   * [setting_edit description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   * Author : ndhung.dev@gmail.com
   * Date : 13/01/2017
   */
  public function setting_view($id = null) {
    $data = [
      'page_header' => 'Edit Setting'
    ];
    $this->set($data);
  }  

  public function cronsetting() {
    $conditions = [];
    $filters = [];
    $list_apps = array();

    $apps = $this->Game->find('all', array('contain' => 'GameScheduler'));
    if (!empty($apps)) {
      foreach ($apps as $k => $app) {
        $gamescheduler = $this->GameScheduler->find('first', array(
            'contain' => array('Game'),
            'conditions' => array('game_id' => $app['Game']['id'])
        ));
        $gameinfo = array(
          'game_id' => $app['Game']['id'],
          'game_name' => $app['Game']['name'],
          'scheduler' => []
        ); 
        if (!empty($app['GameScheduler'])) {
          foreach ($app['GameScheduler'] as $m => $s) {
            $points[$s['day']] = array(
              'id' => $s['id'],
              'bash_timepoints' => json_decode($s['bash_timepoints']),
              'push_timepoints' => json_decode($s['push_timepoints']),
              'status_bash_timepoints' => $s['status_bash_timepoints'],
              'status_push_timepoints' => $s['status_push_timepoints']
            );
          }
          $gameinfo['scheduler'] = $points;
        }
        $list_apps[] = $gameinfo;
      }
    }

    $data = array(
      'list_apps' => $list_apps,
      'filters' => $filters,
      'page_header' => "Crontab Settings",
      'type_scheduler' => 'bash_timepoints'
      );
    $this->set($data);
  }

  public function searchapp() {
    exit(json_encode([]));
  }

  public function update_word_in_gamedata($game_data_id=null,  $shortcode=null) {
    if (is_numeric($game_data_id)) {
      $game_data = 'GameData'.ucfirst($shortcode);
      if ($this->request->isAjax()) {
          if ($this->request->is('post')) {
              $this->{$game_data}->id = $game_data_id;
              if ($this->{$game_data}->exists()) {
                  $data_update = [
                      'word1' => $this->request->data('dataPost')[0],
                      'word2' => $this->request->data('dataPost')[1],
                      'word3' => $this->request->data('dataPost')[2],
                      'word4' => $this->request->data('dataPost')[3],
                      'word5' => $this->request->data('dataPost')[4],
                  ]; 
                  try {
                      if ($this->{$game_data}->save($data_update)) {
                          exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                      }
                  } catch (Exception $e) {
                      exit(json_encode(array('error' => 1, 'message' => 'Can not update item',)));  
                  }
              }
          }
      }
    }
    
    exit(json_encode(array('error' => 1, 'message' => 'Can not update item',)));  
  }

  public function delete_word_in_gamedata($game_data_id=null, $shortcode=null) {
    if (is_numeric($game_data_id)) {
      $game_data = 'GameData'.ucfirst($shortcode);
      if($this->request->isAjax()) {
          if ($this->request->is('post')) {
              $this->{$game_data}->id = $game_data_id;
              if ($this->{$game_data}->exists()) {
                  if ($this->{$game_data}->delete()) {
                      exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                  }
              }
          }
      }
    }
    exit(json_encode(array('error' => 1, 'message' => 'Can not delete item',))); 
  }

  public function save_setting($type_scheduler = null) {
    if ($this->request->isAjax()) {
      $data = $this->request->data;
      unset($data['name']);
      foreach ($data as $key => $value) {
        $post = array(
          'game_id' => $value['game_id'],
          'day' => $value['day'],
          $value['type_scheduler'] => (isset($value[$value['type_scheduler']]))?json_encode($value[$value['type_scheduler']]):json_encode([])
        );

        $check_scheduler = $this->GameScheduler->find('first', array(
          'conditions' => array(
            'game_id' => $post['game_id'],
            'day' => $post['day']
          )
        ));
        if ($check_scheduler && !empty($check_scheduler)) {
          $post['id'] = $check_scheduler['GameScheduler']['id'];
        } else {
          $post['status_'.$value['type_scheduler']] = 1;
        }
        $this->GameScheduler->create();
        $this->GameScheduler->save($post);
      }
      exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
    }
    exit(json_encode(array('error' => 1, 'message' => 'Can not update item',)));  
  }


  public function changestatus_scheduler($scheduler_id=null, $type_scheduler=null, $status=null) {
    if($this->request->isAjax()) {
        if ($this->request->is('post')) {
            $this->GameScheduler->id = $scheduler_id;
            if ($this->GameScheduler->exists()) {
                $data_update = ['status_'.$type_scheduler => $status];
                if ($this->GameScheduler->save($data_update)) {
                    exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                }
            }
        }
    }
    exit(json_encode(array('error' => 1, 'message' => 'Can not change status item',)));  
  }
}
