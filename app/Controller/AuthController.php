<?php
App::uses('AppController', 'Controller');

/**
 * Auth Controller
 */
class AuthController extends AppController
{

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('logout');
       
    }

    public function login()
    {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect($this->Auth->redirectUrl('/home/'));
            }
            $this->Session->setFlash('Email or password is incorrect', 'default', null, 'auth');
        } else {
            if ($this->Auth->User()) {
                $this->redirect('/home/');
            }
            $this->set('title_for_layout', 'ログイン');
        }
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

}
