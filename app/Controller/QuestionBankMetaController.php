<?php 
Class QuestionBankMetaController extends Controller {
	public $uses = array();
	public $helpers = array();
	public $components = array();

	public function index() {
		$data = array();
		$this->set(compact('data'));
	}

	public function add() {
		$data = array();
		$this->set(compact('data'));
	}

	public function edit($id=null) {
		$data = array();
		$this->set(compact('data'));
	}

	public function remove($id=null) {
		$data = array();
		$this->set(compact('data'));
	} 
}