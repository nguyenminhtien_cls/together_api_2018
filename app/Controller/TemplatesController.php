<?php

App::uses('AppController', 'Controller');


/**
 * User: Nguyen Thai
 * Date: 12/23/2016
 */
class TemplatesController extends AppController
{
	public $uses = array('Template');
    public $helpers = array('Paginator', 'Html', 'Form');
    public $components = array('Upload', 'Session', 'Paginator');
   function add(){
        if ($this->request->is('post')) {
        	$this->Template->save($this->request->data);
        	$this->Session->setFlash(__('Create Template is susscess'),'default',array('class' => 'alert alert-success'));
        	$this->redirect(
                array('action' => 'edit',$this->Template->id)
            );
        }
   }

   function edit($id = null){
   		if ($this->request->is('post')) {
   			$this->request->data['Template']['id'] = $id;
        	$this->Template->save($this->request->data);
        	$this->Session->setFlash(__('Edit Template is susscess'),'default',array('class' => 'alert alert-success col-md-12'));
        	$this->redirect(
                array('action' => 'edit',$id)
            );
        }
   		$template = $this->Template->find('first',array(
   			'conditions' => array('id' => $id)
   			));
   		$data = array(
   			'template' => $template,
   			);
   		$this->set($data);
   }

   function index(){
      if ($this->request->is('post')) {
        if(isset($this->request->data['Template']['action'])){
          if($this->request->data['Template']['action'] == 'delete'){
            $this->Template->delete($this->request->data['checkbox_id']);
            $this->Session->setFlash(__('Delete Template is susscess'),'default',array('class' => 'alert alert-success'));
            $this->redirect(
                array('action' => 'index')
            );
          }
        }
      }
   		$this->Paginator->settings = array(
	        'limit' => 10,
	        'order' => array('id' => 'desc')
	    );
    	$template_all = $this->paginate('Template');
    	$data = array(
    		'template_all' => $template_all,
        'page_header' => "List Templates",
    		);
    	$this->set($data);
   }
}