<?php  
App::uses('AppController', 'Controller');
class ViewTermController extends AppController{
	public $uses = array(
		'ViewTerm',
	);
	public $helpers = array (
		'Paginator',
		'Html',
		'Form',
		'Session' 
	);
	public $components = array (
		'Session' 
	);
	public function beforeFilter(){
		$this->Auth->allow('term');
		$this->Auth->allow('policy');
	}
	public function term(){
		$this->layout = false;
		$data = $this->ViewTerm->find("all");
		$this->set('data', $data);
	}
	public function policy(){
		$this->layout = false;
		$data = $this->ViewTerm->find("all");
		$this->set('data', $data);
	}
}
?>
