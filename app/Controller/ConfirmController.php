<?php

App::uses('AppController', 'Controller');

/**
 * Home Controller
 *
 */
class ConfirmController extends AppController {

    public $uses = array('User', 'OfUser');

    public function beforeFilter() {
        $this->Auth->allow();
        $this->autoLayout = false;
    }

    public function index() {
        $param = $this->request->query('params');
        $param = explode('AAAAAAAAAAAAAAAAAAAAAAAAAAAA', $param);
        $inactive = 0;
        if ($param[0] < time()) {
            $inactive = 1;
        }
        if ($this->request->is('POST')) {
            $data = $this->request->data;
            if ($data['password'] != $data['re_password']) {
                echo "Password and repassword isn't same";
                die;
            }
            $data_user = $this->User->get_user_by_email($data['email']);
            $data_user = $data_user['User'];
            $data_user['password'] = $data['password'];
            if ($this->User->save($data_user)) {
                echo 'Change password susscess';
                die;
            } else {
                echo 'Error in change password. Try again !';
                die;
            }
        }
        $this->set('email', $param[1]);
        $this->set('inactive', $inactive);
    }

}
