<?php

App::uses('AppController', 'Controller');

class GetmeanController extends AppController {

    public $uses = array('Word');

    public function beforeFilter() {
        $this->Auth->allow();
        $this->autoLayout = FALSE;
    }

    public function run() {
        $word_data = $this->Word->find('all', array(
            'conditions' => array(
//                'Word.id >= 15000'
                'Word.id >= 10000 AND Word.id < 15000'
            ),
        ));
        foreach ($word_data as $index => $value) {
            $mean_data = $this->getContentTest($value['Word']['word']);
            $value['Word']['meaning_en'] = json_encode($mean_data);
            $value['Word']['pronounce'] = $mean_data['pronounce'];
            $this->Word->save($value['Word']);
            $this->Word->clear();
        }
        die('done');
    }

    public function getResultFromYQL($word) {
        $yql_query_url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url=%22http://thefreedictionary.com/{$word}%22%20and%20xpath%20=%27//div[contains(@id,%22Definition%22)]%27&format=json&callback=";
//        $session = curl_init($yql_query_url);
//        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
//        $json = curl_exec($session);
//        curl_close($session);
        $json = file_get_contents($yql_query_url);
        return $json;
    }

    public function getContentTest($word) {
        $rep_array = array();
        $rep_array['word'] = $word;
        $json = $this->getResultFromYQL($word);
        $jsonD = json_decode($json, true);
        if (is_array($jsonD['query']['results']['div']['section'])) {
            $result = $jsonD['query']['results']['div']['section'][0]['div'];
        } else {
            $result = $jsonD['query']['results']['div']['section']['div'];
        }
        $rep_array['pronounce'] = (isset($jsonD['query']['results']['div']['section'][1]['span'][0])) ? $jsonD['query']['results']['div']['section'][1]['span'][0]['content'] : $jsonD['query']['results']['div']['section'][1]['span']['content'];
        foreach ($result as $k => $v) {
            if (isset($v['class']) && $v['class'] == 'pseg') {
                if (is_array($v['i'])) {
                    $type = implode('|', $v['i']);
                } else {
                    $type = $v['i'];
                }
                if (!empty($v['div']['content'])) {
                    $rep_array['type'][$type . ''][1] = $this->get_mean_and_ex($v['div']);
                } elseif (isset($v['div'])) {
                    foreach ($v['div'] as $key => $value) {
                        if (isset($value['content'])) {
                            $rep_array['type'][$type][$key + 1] = $this->get_mean_and_ex($value);
                        } else {
                            foreach ($value['div'] as $key1 => $value1) {
                                $rep_array['type'][$type][$key1 + 1] = $this->get_mean_and_ex($value1);
                            }
                        }
                    }
                }
            }
        }
        return $rep_array;
    }

    public function get_mean_and_ex($data) {
        $rep = array();
        $rep['mean'] = $data['content'];
        if (isset($data['span'])) {
            if (isset($data['span']['content'])) {
                $rep['ex'] = $data['span']['content'];
            } else {
                $rep['ex'] = $data['span'][0]['content'];
            }
        } else {
            $rep['ex'] = '';
        }
        return $rep;
    }

}
