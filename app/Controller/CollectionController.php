<?php
/**
 * Author : ndhung.dev@gmail.com
 * Date : 12/02/2017
 */
App::uses('AppController', 'Controller');

class CollectionController extends AppController
{
    public $uses = array('Word', 'WordCollection', 'Collection', 'Dynamic');
    public $helpers = array('Html', 'Form', 'Paginator');
    public $components = array('Paginator');

    public function index()
    {
        $page_header = 'Collections';
        $title_for_layout = 'Collections list';
        $filters = [];
        $conditions = [];
        if (isset($this->params->query['filters'])) {
            $filters = $this->params->query['filters'];
            if (trim($filters['keysearch']) != '') {
                $conditions[] = "Collection.collection_title LIKE '%".trim($filters['keysearch'])."%'";
            }
        }
        $this->paginate = array(
            'fields' => '*',
            'order' => array(
                'created_datetime' => 'DESC'
            ),
            'conditions' => $conditions,
            'limit' => 30
        );
        $collections = $this->paginate('Collection');
        $data = compact('title_for_layout','page_header','filters', 'conditions', 'collections');
        $this->set($data);
        $this->render('/Collections/index');
    }

    public function words_in_collection ($collection_id = null) {
        $page_header = 'Collections';
        $title_for_layout = 'Words in collections';
        $filters = [];
        $conditions['Collection.id'] = $collection_id;
        if (isset($this->params->query['filters'])) {
            $filters = $this->params->query['filters'];
            if (trim($filters['keysearch']) != '') {
                $conditions[] = "Word.word LIKE '%".trim($filters['keysearch'])."%'";
            }
        }
        $this->paginate = array(
            'fields' => 'WordCollection.id, Word.*, Collection.collection_title',
            'contain' => array(
                'Word',
                'Collection'
            ),
            'conditions' => $conditions,
            'order' => array('Word.id' => 'desc'),
            'limit' => 25
        );
        $words = $this->paginate('WordCollection');
        $data = compact('title_for_layout','page_header','filters', 'conditions', 'filters', 'collection_id', 'words');
        $this->set($data);
        $this->render('/Collections/list_words');
    }

    /**
     * add new collection
     */
    public function add()
    {
        $this->set('title_for_layout', 'Add collectio');
        $this->Collection->create();
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if ($this->Collection->save($data)) {
                $this->Session->setFlash(__('Your Collection has been saved.'));
                return $this->redirect(array('action' => 'Collection'));
            }
            $this->Session->setFlash(__('Unable to add your Collection.'));
        }
    }

    /**
     * update collection use ajax
     */
    public function ajaxupdate($collection_id=null) {
        if ($this->request->isAjax()) {
            if ($this->request->is('post')) {
                $this->Collection->id = $collection_id;
                if ($this->Collection->exists()) {
                    $data_update = [
                        'collection_title' => $this->request->data('dataPost')[0],
                        'collection_description' => $this->request->data('dataPost')[1]
                    ]; 
                    try {
                        if ($this->Collection->save($data_update)) {
                            exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                        }
                    } catch (Exception $e) {
                        exit(json_encode(array('error' => 1, 'message' => 'Can not update item',)));  
                    }
                }
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not update item',)));  
    }

    /**
     *  Change status collection 
     */
    public function changestatus($collection_id = null, $status = null) {
        if($this->request->isAjax() && $this->request->is('post')) {
            $this->Collection->id = $collection_id;
            if ($this->Collection->exists()) {
                $data_update = ['status' => $status];
                if ($this->Collection->save($data_update)) {
                    exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                }
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not change status item',)));  
    }

    /**
     * Delete collection
     */
    public function delete($collection_id = null)
    {
        if (!is_numeric($collection_id)) {
            die('Invalid');
        }
        if($this->request->isAjax()) {
            if ($this->request->is('post')) {
                $this->Collection->id = $collection_id;
                if ($this->Collection->exists()) {
                    if ($this->Collection->delete()) {
                        exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                    }
                }
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not delete item',))); 
    }

    /**
     * remove word out collection
     */
    public function remove_word_out_collection($word_collection_id=null) {
        if (!is_numeric($word_collection_id)) {
            die('Invalid');
        }
        if($this->request->isAjax()) {
            if ($this->request->is('post')) {
                $this->WordCollection->id = $word_collection_id;
                if ($this->WordCollection->exists()) {
                    if ($this->WordCollection->delete()) {
                        exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                    }
                }
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not delete item',))); 
    }
}