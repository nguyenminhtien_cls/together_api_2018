<?php

App::uses('Component', 'Controller', 'AppController', 'CakeEmail', 'Network/Email');

class UserComponent extends Component {

    // the other component your component uses
    public $components = array(
        'Response',
        'Common'
    );
    protected $requests = [];

    function initialize(Controller $controller) {
        $this->Controller = $controller;
        $this->User = ClassRegistry::init('User');
        $this->ToeicLevel = ClassRegistry::init('ToeicLevel');
    }

    public function connectModule($module_name = null) {
        $this->requests = $this->Controller->send_request;
        $this->userinfo = $this->Controller->userinfo;
        $this->{'_' . $module_name}();
    }

    private function _normal_register() {
        $email = $this->requests ['email'];
        $password = isset($this->requests['password']) ? $this->requests['password'] : '';
        if ($password == '') {
            $this->Response->responseApi(PASSWORD_WRONG, null, null);
            return;
        }
        if ($this->Common->validate_email($email) != 1) {
            $this->Response->responseApi(EMAIL_INVALID, null, null);
            return;
        }
        $username = $this->Common->format_email($email);
        if (isset($username)) {
            $name = explode("_", $username);
        }
        $data = $this->requests;
        $params = array(
            'email' => $email,
            'name' => isset($name['0']) ? $name['0'] : '',
            'username' => $username,
            'password' => $this->requests['password']
        );
        $user = json_encode($params, true);
        $data = $this->Common->create_user($user);
        if ($data == '201') {
            $params['properties'] = $this->Common->_generate_properties($this->requests);
            $this->_create_profile($params);
            $token = array(
                'token' => $this->Common->_get_value('token', $params['properties']['property'])
            );
            $this->_get_user_info();
//            $this->Response->responseApi(SUCCESS, null, $token);
        } elseif ($data == '500') {
            $this->Response->responseApi(EMAIL_ALREADY_EXIST, null, null);
        } else {
            $this->Response->responseApi(SYSTEM_ERROR, null, null);
        }
    }

    private function _create_profile($userEntity) {
        if (!empty($userEntity)) {
            $link = OPENSMART_USER_API;
            $userEntity = json_encode($userEntity, true);
            $data = $this->Common->postdata_openfire($link, $userEntity);
        }
    }

    /*  get all user paging */

    private function _get_all_user() {
        $page = isset($this->requests['page']) ? $this->requests['page'] : '0';
        $access_token = isset($this->requests['_token']) ? $this->requests['_token'] : '';
        $link = API_GET_ALL_USER . '?page=' . $page . '&token=' . $access_token;
        $this->log('API_GET_ALL_USER :' . $link, 'debug');
        $user = $this->Common->getdata_openfire($link);
        $user = $this->Common->conver_xml($user);
        $this->log('USER :' . json_encode($user), 'debug');
        if (!empty($user)) {
            foreach ($user['user'] as $key => $value) {
                if (gettype($key) == "integer") {
                    if (!empty($user['user'][$key]['properties'])) {
                        foreach ($user['user'][$key]['properties']['property'] as $property) {
                            if ($property['name'] == 'is_online')
                                $user['user'][$key]['is_online'] = ($property['value'] == 'true') ? true : false;
                            if (($property['name'] == 'nick_name') && isset($property['value']))
                                $user['user'][$key]['name'] = $property['value'];
                            if ($property['name'] == 'toeic_level_id')
                                $user['user'][$key]['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                        }
                    }
                    else {
                        $user['user'][$key]['is_online'] = false;
                    }
                    $user['user'][$key]['is_favorited'] = isset($user['user'][$key]['isFavorited']) ? $user['user'][$key]['isFavorited'] : 'false';
                    unset($user['user'][$key]['isFavorited']);
                    unset($user['user'][$key]['properties']);
                } else {
                    if (!empty($user['user']['properties']))
                        foreach ($user['user']['properties']['property'] as $property) {
                            if ($property['name'] == 'is_online')
                                $user['user']['is_online'] = ($property['value'] == 'true') ? true : false;
                            if (($property['name'] == 'nick_name') && isset($property['value']))
                                $user['user']['name'] = $property['value'];
                            if ($property['name'] == 'toeic_level_id')
                                $user['user']['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                        }
                    else {
                        $user['user']['is_online'] = false;
                    }
                    $user['user']['is_favorited'] = isset($user['user']['isFavorited']) ? $user['user']['isFavorited'] : 'false';
                }
            }
            unset($user['user']['isFavorited']);
            unset($user['user']['properties']);
            $this->Response->responseApi(SUCCESS, null, $user['user']);
        } else {
            $this->Response->responseApi(SUCCESS, null, $user);
        }
    }

    /* get user noteam */

    private function _get_users_participants() {
        $page = isset($this->requests['page']) ? $this->requests['page'] : '0';
        $access_token = isset($this->requests['_token']) ? $this->requests['_token'] : '';
        $link = API_GET_USER_NOTEAM . '?page=' . $page . '&token=' . $access_token;
        $user = $this->Common->getdata_openfire($link);
        $user = $this->Common->conver_xml($user);
        if (!empty($user)) {
            foreach ($user['user'] as $key => $value) {
                if (gettype($key) == "integer") {
                    if (!empty($user['user'][$key]['properties'])) {
                        foreach ($user['user'][$key]['properties']['property'] as $property) {
                            if ($property['name'] == 'is_online')
                                $user['user'][$key]['is_online'] = ($property['value'] == 'true') ? true : false;
                            if (($property['name'] == 'nick_name') && isset($property['value']))
                                $user['user'][$key]['name'] = $property['value'];
                            if ($property['name'] == 'claps')
                                $user['user'][$key]['claps'] = isset($property['value']) ? $property['value'] : '';
                            if ($property['name'] == 'toeic_level_id')
                                $user['user'][$key]['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                        }
                    }
                    else {
                        $user['user'][$key]['is_online'] = false;
                        $user['user'][$key]['claps'] = '';
                    }
                    $user['user'][$key]['is_favorited'] = isset($user['user'][$key]['isFavorited']) ? $user['user'][$key]['isFavorited'] : 'false';
                    unset($user['user'][$key]['isFavorited']);
                    unset($user['user'][$key]['properties']);
                } else {
                    if (!empty($user['user']['properties'])) {
                        foreach ($user['user']['properties']['property'] as $property) {
                            if ($property['name'] == 'is_online')
                                $user['user']['is_online'] = ($property['value'] == 'true') ? true : false;
                            if (($property['name'] == 'nick_name') && isset($property['value']))
                                $user['user']['name'] = $property['value'];
                            if ($property['name'] == 'claps')
                                $user['user']['claps'] = isset($property['value']) ? $property['value'] : '';
                            if ($property['name'] == 'toeic_level_id')
                                $user['user']['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                        }
                    }
                    else {
                        $user['user']['is_online'] = false;
                        $user['user']['claps'] = '';
                    }
                    $user['user']['is_favorited'] = isset($user['user']['isFavorited']) ? $user['user']['isFavorited'] : 'false';
                }
            }
            unset($user['user']['isFavorited']);
            unset($user['user']['properties']);
            $this->Response->responseApi(SUCCESS, null, $user['user']);
        } else {
            $this->Response->responseApi(SUCCESS, null, $user);
        }
    }

    private function _search_users_participants() {
        $page = isset($this->requests['page']) ? $this->requests['page'] : '0';
        $access_token = isset($this->requests['_token']) ? $this->requests['_token'] : '0';
        $condition = isset($this->requests['condition']) ? $this->requests['condition'] : '';
        $is_favorited = isset($this->requests['is_favorited']) ? $this->requests['is_favorited'] : 'all';
        $invited = isset($this->requests['invited']) ? $this->requests['invited'] : 'all';
        $link_search = API_GET_USER_NOTEAM . '/search?page=' . $page . '&token=' . $access_token;
        $con_entity = array(
            'condition' => $condition,
            'favorited' => $is_favorited,
            'invited' => $invited
        );
        $con_entity = json_encode($con_entity, true);
        $user = $this->Common->searchdata_openfire($link_search, $con_entity);
        $user = $this->Common->conver_xml($user);
        if (!empty($user)) {
            foreach ($user['user'] as $key => $value) {
                if (gettype($key) == "integer") {
                    if (!empty($user['user'][$key]['properties'])) {
                        foreach ($user['user'][$key]['properties']['property'] as $property) {
                            if ($property['name'] == 'is_online')
                                $user['user'][$key]['is_online'] = ($property['value'] == 'true') ? true : false;
                            if (($property['name'] == 'nick_name') && isset($property['value']))
                                $user['user'][$key]['name'] = $property['value'];
                            if ($property['name'] == 'claps')
                                $user['user'][$key]['claps'] = isset($property['value']) ? $property['value'] : '';
                            if ($property['name'] == 'toeic_level_id')
                                $user['user'][$key]['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                        }
                    }
                    else {
                        $user['user'][$key]['is_online'] = false;
                        $user['user'][$key]['claps'] = '';
                    }

                    $user['user'][$key]['is_favorited'] = isset($user['user'][$key]['isFavorited']) ? $user['user'][$key]['isFavorited'] : 'false';
                    unset($user['user'][$key]['isFavorited']);
                    unset($user['user'][$key]['properties']);
                } else {
                    if ($key == 'properties') {
                        if (!empty($value)) {
                            foreach ($value['property'] as $key1 => $property) {
                                if ($property['name'] == 'is_online')
                                    $tmpuser['is_online'] = ($property['value'] == 'true') ? true : false;
                                if (($property['name'] == 'nick_name') && isset($property['value']))
                                    $tmpuser['name'] = $property['value'];
                                if ($property['name'] == 'claps')
                                    $tmpuser['claps'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'toeic_level_id')
                                    $tmpuser['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                            }
                        }
                        else {
                            $tmpuser['is_online'] = false;
                            $tmpuser['claps'] = '';
                        }
                    } elseif ($key == 'isFavorited') {
                        $tmpuser['is_favorited'] = isset($user['user']['isFavorited']) ? $user['user']['isFavorited'] : 'false';
                        unset($user['user'][$key]);
                    } else {
                        $tmpuser[$key] = $value;
                        unset($user['user'][$key]);
                    }
                    $user['user'] = array($tmpuser);

                    /* if(!empty($user['user']['properties']))
                      foreach($user['user']['properties']['property'] as $property)
                      {
                      if($property['name'] == 'is_online')
                      $user['user']['is_online'] = ($property['value']=='true')?true:false;
                      if(($property['name'] == 'nick_name')&&isset($property['value']))
                      $user['user']['name'] = $property['value'];
                      if($property['name'] == 'claps')
                      $user['user']['claps'] = isset($property['value'])?$property['value']:'';
                      if($property['name'] == 'toeic_level_id')
                      $user['user']['toeic_level_id'] = isset($property['value'])?$property['value']:'';
                      }
                      else
                      {
                      $user['user']['is_online'] = false;
                      $user['user']['claps'] = '';
                      }

                      $user['user']['is_favorited'] = isset($user['user']['isFavorited'])?$user['user']['isFavorited']:'false';
                     */
                }
            }
            unset($user['user']['isFavorited']);
            unset($user['user']['properties']);
            $this->Response->responseApi(SUCCESS, null, $user['user']);
        } else {
            $this->Response->responseApi(SUCCESS, null, $user);
        }
    }

    /* Check user by opensmart */

    private function _check_user() {
        $email = $this->requests ['email'];

        if ($this->Common->validate_email($email) != 1) {
            $this->Response->responseApi(EMAIL_INVALID, null, null);
            return;
        }
        $username = $this->Common->format_email($email);
        $link = OPENSMART_USER_API . '?page=0&username=' . $username;
        $data = $this->Common->getdata_openfire($link);
        $data = $this->Common->conver_xml($data);
        if (!empty($data)) {
            $this->Response->responseApi(SUCCESS, null, 1);
        } else {
            $this->Response->responseApi(SUCCESS, null, 0);
        }
    }

    /* get toeic  */

    private function _get_toeic() {
        $data = $this->ToeicLevel->getToeicLevel();
        if (!empty($data)) {
            $this->Response->responseApi(SUCCESS, null, $data);
        } else {
            $this->Response->responseApi(SYSTEM_ERROR, null, null);
        }
    }

    /* search user by user name  */

    private function _search_user() {
        $name = isset($this->requests['username']) ? $this->requests['username'] : '';
        $access_token = isset($this->requests['_token']) ? $this->requests['_token'] : '';
        $page = isset($this->requests['page']) ? $this->requests['page'] : '0';
        $link_search = OPENSMART_USER_API . '?page=' . $page . '&username=' . $name . '&token=' . $access_token;
        $user = $this->Common->getdata_openfire($link_search);
        $user = $this->Common->conver_xml($user);
        if (!empty($user)) {
            foreach ($user['user'] as $key => $value) {
                if (gettype($key) == "integer") {
                    if (!empty($user['user'][$key]['properties'])) {
                        foreach ($user['user'][$key]['properties']['property'] as $property) {
                            if ($property['name'] == 'is_online')
                                $user['user'][$key]['is_online'] = ($property['value'] == 'true') ? true : false;
                            if (($property['name'] == 'nick_name') && isset($property['value']))
                                $user['user'][$key]['name'] = $property['value'];
                            if ($property['name'] == 'claps')
                                $user['user'][$key]['claps'] = isset($property['value']) ? $property['value'] : '';
                            if ($property['name'] == 'toeic_level_id')
                                $user['user'][$key]['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                        }
                    }
                    else {
                        $user['user'][$key]['is_online'] = false;
                        $user['user'][$key]['claps'] = '';
                    }
                    $user['user'][$key]['is_favorited'] = isset($user['user'][$key]['isFavorited']) ? $user['user'][$key]['isFavorited'] : 'false';
                    unset($user['user'][$key]['isFavorited']);
                    unset($user['user'][$key]['properties']);
                } else {
                    if (!empty($user['user']['properties']))
                        foreach ($user['user']['properties']['property'] as $property) {
                            if ($property['name'] == 'is_online')
                                $user['user']['is_online'] = ($property['value'] == 'true') ? true : false;
                            if (($property['name'] == 'nick_name') && isset($property['value']))
                                $user['user']['name'] = $property['value'];
                            if ($property['name'] == 'claps')
                                $user['user']['claps'] = isset($property['value']) ? $property['value'] : '';
                            if ($property['name'] == 'toeic_level_id')
                                $user['user']['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                        }
                    else {
                        $user['user']['is_online'] = false;
                        $user['user']['claps'] = '';
                    }

                    $user['user']['is_favorited'] = isset($user['user']['isFavorited']) ? $user['user']['isFavorited'] : 'false';
                }
            }
            unset($user['user']['isFavorited']);
            unset($user['user']['properties']);
            $this->Response->responseApi(SUCCESS, null, $user);
        } else
            $this->Response->responseApi(SUCCESS, null, $user);
    }

    /* get user info */

    private function _get_user_info_() {
        $email = $this->requests ['email'];
        $push_token = isset($this->requests ['push_token']) ? $this->requests ['push_token'] : null;
        $username = $this->Common->format_email($email);
        $salt = Configure::read('Security.salt');
        $pass = md5($salt . $this->requests ['password']);
        $link_search = OPENSMART_USER_API . '?page=0&username=' . $username;
        $data = $this->Common->getdata_openfire($link_search);
        $data = $this->Common->conver_xml($data);

        if (!empty($data['user']['properties']['property'])) {
            foreach ($data['user']['properties']['property'] as $value) {
                if ($value['name'] == 'pass_word') {
                    $cf_pass = $value['value'];
                }
            }
            if ($cf_pass == $pass) {
                $new_access_token = md5(uniqid() . time());
                if (isset($push_token)) {
                    $this->update_push_token($username, $push_token);
                }
                $this->update_session_token($username, $new_access_token);
                $return_data = $this->_filter_user_info_for_return($data, $new_access_token);
                $this->Response->responseApi(SUCCESS, null, $return_data);
            } else {
                $this->Response->responseApi(PASSWORD_WRONG, 100206, 1);
            }
        } else {
            $this->Response->responseApi(EMAIL_INVALID, 100204, 2);
        }
    }

    private function _get_user_info() {
        $this->log('-------------------------', 'debug');

        $email = $this->requests ['email'];
        $push_token = isset($this->requests ['push_token']) ? $this->requests ['push_token'] : null;
        $this->log('-------------------------1111', 'debug');
        $username = $this->Common->format_email($email);
        $salt = Configure::read('Security.salt');
        $pass = md5($salt . $this->requests ['password']);
        $_token = isset($this->requests ['_token']) ? $this->requests ['_token'] : '';
        $this->log($username . '---' . $pass, 'debug');

        $link_search = OPENSMART_USER_API . '/checkuser?username=' . $username;
        $this->log($link_search, 'debug');
        $data = $this->Common->getdata_openfire($link_search);
        $this->log($data, 'debug');
        $data = $this->Common->conver_xml($data);
        $this->log('-------------------------1112', 'debug');
        if (!empty($data['user']['properties']['property'])) {
            $this->log('-------------------------1115', 'debug');
            foreach ($data['user']['properties']['property'] as $value) {
                if ($value['name'] == 'pass_word') {
                    $cf_pass = $value['value'];
                }
            }
            $this->log($cf_pass . '----' . $pass, 'debug');
            if ($cf_pass == $pass) {
                if ($pass != "") {
                    if ($cf_pass == $pass) {
                        $this->log('-------------------------1113', 'debug');
                        $new_access_token = md5(uniqid() . time());
                        if (isset($push_token)) {
                            $this->update_push_token($username, $push_token);
                        }
                        $this->update_session_token($username, $new_access_token);
                        $return_data = $this->_filter_user_info_for_return($data, $new_access_token);
                        $this->Response->responseApi(SUCCESS, null, $return_data);
                    } else {
                        $this->Response->responseApi(PASSWORD_WRONG, 100206, 1);
                    }
                }
            } else {
                $return_data = $this->_filter_user_info_for_return($data);
                if (($_token == $return_data['token']) && ($_token != '')) {
                    $this->Response->responseApi(SUCCESS, null, $return_data);
                } else {
                    $this->Response->responseApi(TOKEN_INVALID, 100001, 1);
                }
            }
        } else {
            $this->Response->responseApi(EMAIL_INVALID, 100204, 2);
        }
    }

    /* get user email */

    public function get_user_email($username) {
        $link_search = OPENSMART_USER_API . '?page=0&username=' . $username;
        $data = $this->Common->getdata_openfire($link_search);
        $data = $this->Common->conver_xml($data);
        if (!empty($data['user'])) {
            return $data['user']['email'];
        } else {
            return null;
        }
    }

    public function get_user_info_by_token($access_token) {
        $link_search = OPENSMART_USER_API . '/userInfor/' . $access_token;
        $data = $this->Common->getdata_openfire($link_search);
        $data = $this->Common->conver_xml($data);
        if (empty($data))
            return null;
        $data['user'] = $data;
        return $this->_filter_user_info_for_return($data, $access_token);
    }

    private function _get_facebookid() {
        $facebook_id = $this->requests['facebook_id'];
        if ($facebook_id != '') {
            $link_search = OPENSMART_USER_API . '/facebook/' . $facebook_id;
            $data = $this->Common->getdata_openfire($link_search);
            $data = $this->Common->conver_xml($data);
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    if (gettype($key) == 'integer') {
                        $this->Response->responseApi(FACEBOOK_ID_NOT_EXIST, null, null);
                        return false;
                    } else {
                        if ($key == 'properties') {
                            $new_access_token = md5(uniqid() . time());
                            $this->update_session_token($data['username'], $new_access_token);
                            $userData = array(
                                'username' => $data['username'],
                                'email' => $data['email'],
                                'teamName' => isset($data['teamName']) ? $data['teamName'] : '',
                                'invited' => isset($data['invited']) ? $data['invited'] : 'false',
                                'is_favorited' => isset($data['isFavorited']) ? $data['isFavorited'] : 'false',
                                'team_id' => isset($data['roomID']) ? $data['roomID'] : '',
                                'token' => $new_access_token,
                                'openfire_server' => XMPP_HOST,
                                'openfire_port' => XMPP_PORT
                            );
                            if (!empty($value)) {
                                foreach ($value['property'] as $property) {
                                    if ($property['name'] == 'toeic_level_id')
                                        $userData['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'quote')
                                        $userData['quote'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'avatar_url')
                                        $userData['avarta_url'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'goal')
                                        $userData['goal'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'gender')
                                        $userData['gender'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'nick_name')
                                        $userData['nick_name'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'claps')
                                        $userData['claps'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'together_id')
                                        $userData['together_id'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'native')
                                        $userData['native'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'description')
                                        $userData['description'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'full_name')
                                        $userData['full_name'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'birthday')
                                        $userData['birthday'] = isset($property['value']) ? $property['value'] : '';
                                    if ($property['name'] == 'facebook_id')
                                        $userData['facebook_id'] = isset($property['value']) ? $property['value'] : '';
                                }
                            }
                            else {
                                $userData['toeic_level_id'] = '';
                                $userData['quote'] = '';
                                $userData['avarta_url'] = '';
                                $userData['goal'] = '';
                                $userData['gender'] = '';
                                $userData['nick_name'] = '';
                                $userData['team_id'] = '';
                                $userData['claps'] = '';
                            }
                            $this->Response->responseApi(SUCCESS, null, $userData);
                            return false;
                        }
                    }
                }
            } else {
                $this->Response->responseApi(FACEBOOK_ID_NOT_EXIST, null, null);
            }
        } else {
            $this->Response->responseApi(FACEBOOK_ID_EMPTY, null, null);
        }
    }

    private function _update_push_token() {
        $email = $this->requests['email'];
        $username = $this->Common->format_email($email);
        $push_token = isset($this->requests['push_token']) ? $this->requests['push_token'] : '';
        $userdata = $this->Common->get_user_info($username);
        if (!empty($userdata)) {
            $property = array('name' => 'push_token',
                'value' => isset($push_token) ? $push_token : ''
            );
            $param = json_encode($property, true);
            $link = OPENSMART_USER_API . '/' . $username;
            $return = $this->Common->putdata_openfire($link, $param);
            $this->Response->responseApi(SUCCESS, null, $property);
        } else {
            $this->Response->responseApi(EMAIL_INVALID, null, null);
        }
    }

    private function update_push_token($username, $push_token) {
        $userdata = $this->Common->get_user_info($username);
        if (!empty($userdata)) {
            $property = array('name' => 'push_token',
                'value' => isset($push_token) ? $push_token : ''
            );
            $param = json_encode($property, true);
            $link = OPENSMART_USER_API . '/' . $username;
            $return = $this->Common->putdata_openfire($link, $param);
            return true;
        } else {
            return false;
        }
    }

    /* get user info other */

    private function _get_info_other() {
        $username = $this->requests['username'];
        $access_token = isset($this->requests['_token']) ? $this->requests['_token'] : '0';
        $link = OPENSMART_USER_API . '?page=0&username=' . $username . '&token=' . $access_token;
        $data = $this->Common->getdata_openfire($link);
        $data = $this->Common->conver_xml($data);
        $this->log('_get_info_other: ' . json_encode($data), 'debug');
        if (!empty($data)) {
            foreach ($data['user'] as $key => $value) {
                if (gettype($key) == 'integer') {
                    if ($value['username'] == $username) {
                        $userData = array(
                            'username' => $value['username'],
                            'email' => $value['email'],
                            'teamName' => isset($value['teamName']) ? $value['teamName'] : '',
                            'invited' => isset($value['invited']) ? $value['invited'] : 'false',
                            'is_favorited' => isset($value['isFavorited']) ? $value['isFavorited'] : 'false',
                            'team_id' => isset($value['roomID']) ? $value['roomID'] : '',
                        );
                        if (!empty($value['properties'])) {
                            $value1 = $value['properties'];
                            foreach ($value1['property'] as $property) {
                                if ($property['name'] == 'toeic_level_id')
                                    $userData['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'quote')
                                    $userData['quote'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'avatar_url')
                                    $userData['avarta_url'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'goal')
                                    $userData['goal'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'gender')
                                    $userData['gender'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'nick_name')
                                    $userData['nick_name'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'claps')
                                    $userData['claps'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'together_id')
                                    $userData['together_id'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'native')
                                    $userData['native'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'description')
                                    $userData['description'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'full_name')
                                    $userData['full_name'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'birthday')
                                    $userData['birthday'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'facebook_id')
                                    $userData['facebook_id'] = isset($property['value']) ? $property['value'] : '';
                            }
                        }
                        $this->Response->responseApi(SUCCESS, null, $userData);
                    }
                }
                else {
                    if ($key == 'properties') {
                        $userData = array(
                            'username' => $data['user']['username'],
                            'email' => $data['user']['email'],
                            'teamName' => isset($data['user']['teamName']) ? $data['user']['teamName'] : '',
                            'invited' => isset($data['user']['invited']) ? $data['user']['invited'] : 'false',
                            'is_favorited' => isset($data['user']['isFavorited']) ? $data['user']['isFavorited'] : 'false',
                            'team_id' => isset($data['user']['roomID']) ? $data['user']['roomID'] : '',
                        );
                        if (!empty($value)) {
                            foreach ($value['property'] as $property) {
                                if ($property['name'] == 'toeic_level_id')
                                    $userData['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'quote')
                                    $userData['quote'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'avatar_url')
                                    $userData['avarta_url'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'goal')
                                    $userData['goal'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'gender')
                                    $userData['gender'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'nick_name')
                                    $userData['nick_name'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'claps')
                                    $userData['claps'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'together_id')
                                    $userData['together_id'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'native')
                                    $userData['native'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'description')
                                    $userData['description'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'full_name')
                                    $userData['full_name'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'birthday')
                                    $userData['birthday'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'facebook_id')
                                    $userData['facebook_id'] = isset($property['value']) ? $property['value'] : '';
                            }
                        }
                        else {
                            $userData['toeic_level_id'] = '';
                            $userData['quote'] = '';
                            $userData['avarta_url'] = '';
                            $userData['goal'] = '';
                            $userData['gender'] = '';
                            $userData['nick_name'] = '';
                            $userData['team_id'] = '';
                            $userData['claps'] = '';
                        }
                        $this->Response->responseApi(SUCCESS, null, $userData);
                    }
                }
            }
        } else {
            $this->Response->responseApi(USER_OR_EMAIL_INVALID, null, null);
        }
    }

    /* update token user */

    private function _update_token($openfireUserData, $token, $device_token) {
        $username = $openfireUserData ['username'];
        $email = $openfireUserData ['email'];
        $name = isset($openfireUserData ['name']) ? $openfireUserData ['name'] : '';
        $property = $openfireUserData ['properties'] ['property'];
        $return_user = $this->Common->change_info($property);
        $param = $this->Common->get_param($return_user);
        foreach ($param ['property'] as $key => $value) {
            if ($value ['@key'] == 'token') {
                $param ['property'] [$key] ['@value'] = $token;
            }
            if ($value ['@key'] == 'device_token') {
                $param ['property'] [$key] ['@value'] = $device_token;
            }
        }
        $param_update = array(
            'email' => $email,
            'username' => $username,
            'name' => $name,
            'properties' => $param
        );
        $param_update = json_encode($param_update, true);
        $data = $this->Common->update_user($username, $param_update);
        return json_decode($param_update, true);
    }

    private function update_session_token($username, $token) {
        $userdata = $this->Common->get_user_info($username);
        if (!empty($userdata)) {
            $property = array(
                'name' => 'token',
                'value' => isset($token) ? $token : ''
            );
            $param = json_encode($property, true);
            $link = OPENSMART_USER_API . '/' . $username;
            $return = $this->Common->putdata_openfire($link, $param);
            return true;
        } else {
            return false;
        }
    }

    private function _forgot_password() {
        $to_email = $this->requests ['email'];
        $username = $this->Common->format_email($to_email);
        $link = OPENSMART_USER_API . '/' . $username;
        $link_check = OPENSMART_USER_API . '?page=0&username=' . $username;
        $data = $this->Common->getdata_openfire($link_check);
        $data = $this->Common->conver_xml($data);
        if (!empty($data)) {
            $token_reset = md5(uniqid() . time());
            $property = array(
                'name' => 'token_reset',
                'value' => $token_reset
            );
            $param = json_encode($property, true);
            $link = OPENSMART_USER_API . '/' . $username;
            $link_reset = DOMAIN . '/ForgotPassword?email=' . $to_email . '&code=' . $token_reset;
            $Email = new CakeEmail('gmail');
            $Email->from(array(
                SEND_EMAIL => 'Together'
            ))->to($to_email)->subject('Forgot Password')->send('Please click the link below to change your password:' . $link_reset);

            $return = $this->Common->putdata_openfire($link, $param);

            $this->Response->responseApi(SUCCESS, null, $return);
        } else {
            $this->Response->responseApi(USER_NOT_AVAI, null, null);
        }
    }

    /**
     * Filter all user info for returns
     *
     * @param unknown $openFireData        	
     * @return array|unknown[ ]
     */
    public function _filter_user_info_for_return($openFireData, $token = '') {
        $userData = array(
            'username' => $openFireData ['user']['username'],
            'email' => isset($openFireData ['user']['email']) ? $openFireData['user']['email'] : '',
            'name' => isset($openFireData['user']['name']) ? $openFireData['user']['name'] : '',
            'team_mems' => isset($openFireData['user']['teamMems']) ? $openFireData['user']['teamMems'] : '',
            'team_id' => isset($openFireData['user']['roomID']) ? $openFireData['user']['roomID'] : '',
            'teamName' => isset($openFireData['user']['teamName']) ? $openFireData['user']['teamName'] : '',
            'roomName' => isset($openFireData['user']['roomName']) ? $openFireData['user']['roomName'] : '',
            'is_teacher' => isset($openFireData['user']['is_teacher']) ? $openFireData['user']['is_teacher'] : 0,
        );
        $property = $openFireData ['user']['properties']['property'];
        foreach ($property as $value) {
            if ($value ['name'] == 'toeic_level_id') {
                $userData ['toeic_level_id'] = $value ['value'];
            }
            if ($value ['name'] == 'birthday') {
                $userData ['birthday'] = $value ['value'];
            }
            if ($value ['name'] == 'gender') {
                $userData['gender'] = $value ['value'];
            }
            if ($value ['name'] == 'token') {
//                $userData ['token'] = $token;
                $userData ['token'] = $token == '' ? $value ['value'] : $token;
            }
            if ($value ['name'] == 'claps') {
                $userData ['claps'] = $value ['value'];
            }
            if ($value ['name'] == 'goal') {
                $userData ['goal'] = $value ['value'];
            }
            if ($value ['name'] == 'quote') {
                $userData ['quote'] = $value ['value'];
            }
            if ($value ['name'] == 'nick_name') {
                $userData ['nick_name'] = $value ['value'];
            }
            if ($value ['name'] == 'together_id') {
                $userData['together_id'] = $value['value'];
            }
            if ($value ['name'] == 'native') {
                $userData['native'] = $value['value'];
            }
            if ($value ['name'] == 'description') {
                $userData['description'] = $value['value'];
            }
            if ($value ['name'] == 'full_name') {
                $userData['full_name'] = $value['value'];
            }
            if ($value ['name'] == 'facebook_id') {
                $userData['facebook_id'] = $value['value'];
            }
        }
        // Open Fire config (Client will use this for connecting to XMPP)
        $userData ['openfire_server'] = XMPP_HOST;
        $userData ['openfire_port'] = XMPP_PORT;

        return $userData;
    }

    /**
     * Module facbook_login
     *
     * @return json
     */
    private function _facebook_login() {
        
    }

    /**
     * Module google_login
     *
     * @return json
     */
    private function _google_login() {
        
    }

    /**
     * Module twitter_login
     *
     * @return json
     */
    private function _twitter_login() {
        
    }

    /**
     * Module logout
     *
     * @return json
     */
    private function _logout() {
        try {
            $email = $this->requests['email'];
            $username = $this->Common->format_email($email);
            $this->update_push_token($username, '');
            $this->update_session_token($username, '');
            $this->Response->responseApi(SUCCESS, null, null);
        } catch (Exception $ex) {
            $this->Response->responseApi(SYSTEM_ERROR, null, null);
        }
    }

    /**
     * Module update_profile
     *
     * @return Json
     */
    private function _update_profile() {
        $data = $this->requests;
        if (isset($this->requests ['name']) && $this->requests ['name'] != '') {
            $name = $this->requests ['name'];
        }
        if (isset($this->requests ['email']) && (string) $this->requests ['email'] != '') {
            $email = $this->requests['email'];
            $username = $this->Common->format_email($email);
            $link_check = OPENSMART_USER_API . '?page=0&username=' . $username;
            $data_check = $this->Common->getdata_openfire($link_check);
            $data_check = $this->Common->conver_xml($data_check);
            if (empty($data_check)) {
                $this->Response->responseApi(EMAIL_INVALID, null, null);
                return false;
            }
        } else {
            $this->Response->responseApi(EMAIL_INVALID, null, null);
            return false;
        }
        if (isset($data['together_id'])) {
            if (isset($data_check['user']['properties']['property'])) {
                $hadfield = false;
                foreach ($data_check['user']['properties']['property'] as $value) {
                    if ($value['name'] == 'together_id') {
                        $hadfield = true;
                        if (!empty($value['value'])) {
                            if ($value['value'] != $data['together_id']) {
                                $this->Response->responseApi(TOGETHER_ID_WAS_SET, null, null);
                                return false;
                            }
                        } else {
                            $link_check = OPENSMART_USER_API . '/checkid?togetherid=' . $data['together_id'];
                            $togetherid = $this->Common->getdata_openfire($link_check);
                            if ($togetherid == isset($data['together_id'])) {
                                $this->Response->responseApi(TOGETHER_ID_WAS_EXIST, null, null);
                                return false;
                            }
                        }
                    }
                }
                if (!$hadfield) {
                    $link_check = OPENSMART_USER_API . '/checkid?togetherid=' . $data['together_id'];
                    $togetherid = $this->Common->getdata_openfire($link_check);
                    if ($togetherid == isset($data['together_id'])) {
                        $this->Response->responseApi(TOGETHER_ID_WAS_EXIST, null, null);
                        return false;
                    }
                }
            }
        }

        if (isset($this->requests['pass_word']) && (string) $this->requests['pass_word'] != '') {
            $pass_openfire = $this->requests['pass_word'];
            $param = array(
                'email' => $email,
                'username' => $username,
                'password' => $pass_openfire,
                'name' => isset($name) ? $name : (isset($data_check['user']['name']) ? $data_check['user']['name'] : '')
            );
        } else {
            $param = array(
                'email' => $email,
                'username' => $username,
                'name' => isset($name) ? $name : (isset($data_check['user']['name']) ? $data_check['user']['name'] : '')
            );
        }
        $properties = $this->Common->_generate_properties($data, isset($data_check['user']['properties']['property']) ? $data_check['user']['properties']['property'] : null);
        $param['properties'] = $properties;
        $link_update = OPENSMART_USER_API . '/updateProfile/' . $username;
        $userEntity = json_encode($param, true);
        $ret = $this->Common->putdata_openfire($link_update, $userEntity);
        unset($param['password']);
        if ($ret == 200) {
            $this->Response->responseApi(SUCCESS, null, $param);
        } else {
            $this->Response->responseApi(SUCCESS, 'Error', $param);
            //$this->Response->responseApi ( SAVE_DATA_FALSE, null, $param );
        }
    }

    private function _leave_team() {
        $data = $this->requests;
        $team_id = isset($this->requests ['team_id']) ? $this->requests['team_id'] : '';
        if (isset($this->requests ['email']) && (string) $this->requests ['email'] != '') {
            $email = $this->requests['email'];
            $username = $this->Common->format_email($email);
            $link_check = OPENSMART_USER_API . '?page=0&username=' . $username;
            $data_check = $this->Common->getdata_openfire($link_check);
            $data_check = $this->Common->conver_xml($data_check);
            if (empty($data_check)) {
                $this->Response->responseApi(EMAIL_INVALID, null, null);
                return false;
            }
        } else {
            $this->Response->responseApi(EMAIL_INVALID, null, null);
            return false;
        }
        $team = array(
            'roomID' => $team_id
        );
        $link_update = OPENSMART_USER_API . '/leaveteam/' . $username;
        $team = json_encode($team, true);
        $ret = $this->Common->putdata_openfire($link_update, $team);
        if ($ret == 200) {
            $this->Response->responseApi(SUCCESS, null, null);
        } else {
            $this->Response->responseApi(LEAVE_TEAM_FAIL, null, null);
        }
    }

    private function _update_favorite() {
        $username = isset($this->requests['username']) ? $this->requests['username'] : '';
        $favoriteduser = isset($this->requests['favoriteduser']) ? $this->requests['favoriteduser'] : '';
        $status = isset($this->requests['status']) ? $this->requests['status'] : '0';

        if (($username != '') && ($favoriteduser != '')) {
            $favoriteEntity = array(
                'username' => $username,
                'favoriteduser' => $favoriteduser,
                'status' => $status
            );
            $favoriteEntity = array(
                'name' => $favoriteduser,
                'value' => $status
            );
            $favoriteEntity = json_encode($favoriteEntity, true);
            $link = OPENSMART_USER_API . '/updatefavorite/' . $username;
            $return = $this->Common->putdata_openfire($link, $favoriteEntity);
            if ($return == 200) {
                $this->Response->responseApi(SUCCESS, null, null);
            } else {
                $this->Response->responseApi(UPDATE_FAVORITE_FAIL, null, null);
            }
        } else {
            $this->Response->responseApi(USERNAME_OR_FAVORITEDUSER_EMPTY, null, null);
        }
    }

    private function _search_individuals_old() {
        $condition = isset($this->requests['condition']) ? $this->requests['condition'] : '';
        $page = isset($this->requests['page']) ? $this->requests['page'] : '0';
        $access_token = isset($this->requests['_token']) ? $this->requests['_token'] : '0';
        $is_favorited = isset($this->requests['is_favorited']) ? $this->requests['is_favorited'] : 'false';
        $link_search = OPENSMART_USER_API . '/searchusers/' . $page . '/' . $access_token;
        $con_entity = array(
            'condition' => $condition,
            'favorited' => $is_favorited
        );
        $con_entity = json_encode($con_entity, true);
        $user = $this->Common->searchdata_openfire($link_search, $con_entity);
        $user = $this->Common->conver_xml($user);
        if (!empty($user)) {
            foreach ($user['user'] as $key => $value) {
                if (gettype($key) == "integer") {
                    if (!empty($user['user'][$key]['properties'])) {
                        foreach ($user['user'][$key]['properties']['property'] as $property) {
                            if ($property['name'] == 'is_online')
                                $user['user'][$key]['is_online'] = ($property['value'] == 'true') ? true : false;
                            if (($property['name'] == 'nick_name') && isset($property['value']))
                                $user['user'][$key]['name'] = $property['value'];
                            if ($property['name'] == 'claps')
                                $user['user'][$key]['claps'] = isset($property['value']) ? $property['value'] : '';
                            if ($property['name'] == 'toeic_level_id')
                                $user['user'][$key]['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                        }
                    }
                    else {
                        $user['user'][$key]['is_online'] = false;
                        $user['user'][$key]['claps'] = '';
                    }

                    $user['user'][$key]['is_favorited'] = isset($user['user'][$key]['isFavorited']) ? $user['user'][$key]['isFavorited'] : 'false';
                    unset($user['user'][$key]['isFavorited']);
                    unset($user['user'][$key]['properties']);
                } else {
                    if (!empty($user['user']['properties']))
                        foreach ($user['user']['properties']['property'] as $property) {
                            if ($property['name'] == 'is_online')
                                $user['user']['is_online'] = ($property['value'] == 'true') ? true : false;
                            if (($property['name'] == 'nick_name') && isset($property['value']))
                                $user['user']['name'] = $property['value'];
                            if ($property['name'] == 'claps')
                                $user['user']['claps'] = isset($property['value']) ? $property['value'] : '';
                            if ($property['name'] == 'toeic_level_id')
                                $user['user']['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                        }
                    else {
                        $user['user']['is_online'] = false;
                        $user['user']['claps'] = '';
                    }

                    $user['user']['is_favorited'] = isset($user['user']['isFavorited']) ? $user['user']['isFavorited'] : 'false';
                }
            }
            unset($user['user']['isFavorited']);
            unset($user['user']['properties']);
            $this->Response->responseApi(SUCCESS, null, $user);
        } else
            $this->Response->responseApi(SUCCESS, null, $user);
    }

    private function _search_individuals() {
        $condition = isset($this->requests['condition']) ? $this->requests['condition'] : '';
        $page = isset($this->requests['page']) ? $this->requests['page'] : '0';
        $access_token = isset($this->requests['_token']) ? $this->requests['_token'] : '0';
        $is_favorited = isset($this->requests['is_favorited']) ? $this->requests['is_favorited'] : 'all';
        $invited = isset($this->requests['invited']) ? $this->requests['invited'] : 'all';
        $link_search = OPENSMART_USER_API . '/searchusers/' . $page . '/' . $access_token;
        $con_entity = array(
            'condition' => $condition,
            'favorited' => $is_favorited,
            'invited' => $invited
        );
        $con_entity = json_encode($con_entity, true);
        $user = $this->Common->searchdata_openfire($link_search, $con_entity);
        $user = $this->Common->conver_xml($user);
        if (!empty($user)) {
            foreach ($user['user'] as $key => $value) {
                if (gettype($key) == "integer") {
                    if (!empty($user['user'][$key]['properties'])) {
                        foreach ($user['user'][$key]['properties']['property'] as $property) {
                            if ($property['name'] == 'is_online')
                                $user['user'][$key]['is_online'] = ($property['value'] == 'true') ? true : false;
                            if (($property['name'] == 'nick_name') && isset($property['value']))
                                $user['user'][$key]['name'] = $property['value'];
                            if ($property['name'] == 'claps')
                                $user['user'][$key]['claps'] = isset($property['value']) ? $property['value'] : '';
                            if ($property['name'] == 'toeic_level_id')
                                $user['user'][$key]['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                        }
                    }
                    else {
                        $user['user'][$key]['is_online'] = false;
                        $user['user'][$key]['claps'] = '';
                    }

                    $user['user'][$key]['is_favorited'] = isset($user['user'][$key]['isFavorited']) ? $user['user'][$key]['isFavorited'] : 'false';
                    unset($user['user'][$key]['isFavorited']);
                    unset($user['user'][$key]['properties']);
                } else {
                    if ($key == 'properties') {
                        if (!empty($value)) {
                            foreach ($value['property'] as $key1 => $property) {
                                if ($property['name'] == 'is_online')
                                    $tmpuser['is_online'] = ($property['value'] == 'true') ? true : false;
                                if (($property['name'] == 'nick_name') && isset($property['value']))
                                    $tmpuser['name'] = $property['value'];
                                if ($property['name'] == 'claps')
                                    $tmpuser['claps'] = isset($property['value']) ? $property['value'] : '';
                                if ($property['name'] == 'toeic_level_id')
                                    $tmpuser['toeic_level_id'] = isset($property['value']) ? $property['value'] : '';
                            }
                        }
                        else {
                            $tmpuser['is_online'] = false;
                            $tmpuser['claps'] = '';
                        }
                    } elseif ($key == 'isFavorited') {
                        $tmpuser['is_favorited'] = isset($value) ? $value : 'false';
                        unset($user['user'][$key]);
                    } else {
                        $tmpuser[$key] = $value;
                        unset($user['user'][$key]);
                    }
                    $user['user'] = array($tmpuser);
                }
            }
            unset($user['user']['isFavorited']);
            unset($user['user']['properties']);
            $this->Response->responseApi(SUCCESS, null, $user);
        } else
            $this->Response->responseApi(SUCCESS, null, $user);
    }

    private function _get_iq_messages() {
        $username = isset($this->requests['username']) ? $this->requests['username'] : '';
        $link_search = OPENSMART_MESSAGE_API . '/iq/' . $username;
        $messages = $this->Common->getdata_openfire($link_search);
        $messages = $this->Common->conver_xml($messages);
        $this->Response->responseApi(SUCCESS, null, $messages);
    }

    private function _get_last_chat_messages() {
        $username = isset($this->requests['username']) ? $this->requests['username'] : '';
        $link_search = OPENSMART_MESSAGE_API . '/chat/last/' . $username;
        $messages = $this->Common->getdata_openfire($link_search);
        $messages = $this->Common->conver_xml($messages);
        $this->Response->responseApi(SUCCESS, null, $messages);
    }

    private function _update_privacy() {
        $username = isset($this->requests['username']) ? $this->requests['username'] : '';
        $attribute = isset($this->requests['name']) ? $this->requests['name'] : '';
        $show = isset($this->requests['show']) ? $this->requests['show'] : 'false';
        $privacy = array('name' => $attribute,
            'show' => $show
        );
        $param = json_encode($privacy, true);
        $link = OPENSMART_USER_API . '/privacy/' . $username;
        $return = $this->Common->putdata_openfire($link, $param);
        $privacy['username'] = $username;
        if ($return == 200) {
            $this->Response->responseApi(SUCCESS, null, $privacy);
        } else {
            $this->Response->responseApi(UPDATE_PRIVACY_FAIL, null, $privacy);
        }
    }

    private function _get_privacies() {
        $username = isset($this->requests['username']) ? $this->requests['username'] : '';
        $link_get = OPENSMART_USER_API . '/privacy?username=' . $username;
        $privacies = $this->Common->getdata_openfire($link_get);
        $privacies = $this->Common->conver_xml($privacies);
        $this->Response->responseApi(SUCCESS, null, $privacies);
    }

    private function _get_chat_type() {
        $username = isset($this->requests['username']) ? $this->requests['username'] : '';
        $partner = isset($this->requests['partner']) ? $this->requests['partner'] : '';
        $link_get = OPENSMART_USER_API . '/chattype?username=' . $username . '&partner=' . $partner;
        $result = $this->Common->getdata_openfire($link_get);
        $params = explode('|', $result);
        $data = array(
            'chat_type' => $params[0],
            'team_id' => $params[1],
            'teamName' => $params[2]
        );
        $this->Response->responseApi(SUCCESS, null, $data);
    }

    private function _get_list_pairchat() {
        $username = isset($this->requests['username']) ? $this->requests['username'] : '';
        $link_get = OPENSMART_MESSAGE_API . '/chat/listpairchat/' . $username;
        $data = $this->Common->getdata_openfire($link_get);
        $data = $this->Common->conver_xml($data);
        if (!empty($data)) {
            foreach ($data['PairChat'] as $key => $value) {
                if (gettype($key) == 'integer') {
                    $this->Response->responseApi(SUCCESS, null, $data);
                    return;
                } else {
                    $tmp = $data['PairChat'];
                    unset($data['PairChat']);
                    $data['PairChat'] = array($tmp);
                    $this->Response->responseApi(SUCCESS, null, $data);
                    return;
                }
            }
        } else {
            $this->Response->responseApi(SUCCESS, null, null);
        }
    }

    private function _get_list_teamchat() {
        $username = isset($this->requests['username']) ? $this->requests['username'] : '';
        $link_get = OPENSMART_MESSAGE_API . '/chat/listteamchat/' . $username;
        $data = $this->Common->getdata_openfire($link_get);
        $data = $this->Common->conver_xml($data);
        if (!empty($data)) {
            foreach ($data['TeamChat'] as $key => $value) {
                if (gettype($key) == 'integer') {
                    $this->Response->responseApi(SUCCESS, null, $data);
                    return;
                } else {
                    $tmp = $data['TeamChat'];
                    unset($data['TeamChat']);
                    $data['TeamChat'] = array($tmp);
                    $this->Response->responseApi(SUCCESS, null, $data);
                    return;
                }
            }
        } else {
            $this->Response->responseApi(SUCCESS, null, null);
        }
    }

    private function _get_chatmessages() {
        $username = isset($this->requests['username']) ? $this->requests['username'] : '';
        $page = isset($this->requests['page']) ? $this->requests['page'] : '0';
        $lastmessageid = isset($this->requests['lastmessageid']) ? $this->requests['lastmessageid'] : '0';
        $partner = isset($this->requests['partner']) ? $this->requests['partner'] : '';
        $link_get = OPENSMART_MESSAGE_API . '/chat/listchatmessages/' . $username . '?partner=' . $partner . '&page=' . $page . '&lastmessageid=' . $lastmessageid;
        $data = $this->Common->getdata_openfire($link_get);
        $data = $this->Common->conver_xml($data);
        if (!empty($data['Message'])) {
            $flag = false;
            foreach ($data['Message'] as $key => $value) {
                if (gettype($key) == 'integer') {
                    $flag = true;
                    break;
                }
            }
            if ($flag == false) {
                $data['Message'] = array($data['Message']);
            }
        }
        $this->Response->responseApi(SUCCESS, null, $data);
    }

    private function _get_teamchatmessages() {
        $username = isset($this->requests['username']) ? $this->requests['username'] : '';
        $roomname = isset($this->requests['roomname']) ? $this->requests['roomname'] : '';
        $page = isset($this->requests['page']) ? $this->requests['page'] : '0';
        $lastmessageid = isset($this->requests['lastmessageid']) ? $this->requests['lastmessageid'] : '0';
        $link_get = OPENSMART_MESSAGE_API . '/chat/listteamchatmessages/' . $username . '?roomname=' . $roomname . '&page=' . $page . '&lastmessageid=' . $lastmessageid;
        $data = $this->Common->getdata_openfire($link_get);
        $data = $this->Common->conver_xml($data);
        if (!empty($data['Message'])) {
            $flag = false;
            foreach ($data['Message'] as $key => $value) {
                if (gettype($key) == 'integer') {
                    $flag = true;
                    break;
                }
            }
            if ($flag == false) {
                $data['Message'] = array($data['Message']);
            }
        }

        $this->Response->responseApi(SUCCESS, null, $data);
    }

    private function _test01() {
        $data = array(
            'chat_type' => 'chat',
            'type' => array(
                array('type1' => 'type1'),
                array('type1' => 'type2')
            )
        );
        $this->Response->responseApi(SUCCESS, null, $data);
    }

    private function _test02() {
        $data = array(
            'chat_type' => 'chat',
            'type' => array(
                array('type1' => 'type2')
            )
        );
        $this->Response->responseApi(SUCCESS, null, $data);
    }

}
