<?php 
App::uses('Component', 'Controller');
class PronounceGameComponent extends Component {
    // the other component your component uses
    public $components = array('Response', 'Team', 'Common');
    protected $requests = [];
    function initialize(Controller $controller) {
        $this->Controller = $controller;
        $this->Game = ClassRegistry::init('Game');
        $this->GameTimes = ClassRegistry::init('GameTimes');
        $this->GameDates = ClassRegistry::init('GameDates');
        $this->GameStubs = ClassRegistry::init('GameStubs');
        $this->GameWords = ClassRegistry::init('GameWords');
        $this->Word = ClassRegistry::init('Word');
        $this->GameResultTemp = ClassRegistry::init('GameResultTemp');
        $this->OfMucRoom = ClassRegistry::init('OfMucRoom');
    }

    public function connectModule($module_name=null) {
        $this->requests = $this->Controller->send_request;
        $this->userinfo = $this->Controller->userinfo;
        $this->time_today = $this->Controller->time_today;
        $this->{'_'.$module_name}();
    }
    
    private function _create_game_data()
    {
        MakePronounceGameDataShell::main();
    }
    
    private function _get_current_stubs()
    {
        $game_code = $this->requests['game_code'];
        $team_id = $this->requests['team_id'];
        $username = $this->requests['user_name'];
        
        $gameEnt = $this->Game->find('first', array(
            'conditions' => array(
                'short_code' => $game_code
            )
        ));
        
        if(!$gameEnt)
        {
            $this->Response->responseApi(GAME_STUB_EMPTY, null, null);
            return;
        }
        
        $game_id = $gameEnt['Game']['id'];
        
        $current_day = date('w');
        $game_date = date('Ymd');
        $current_hour = date('H:m');
        
        $gameTimesEnt = $this->GameTimes->find('first', array(
            'conditions' => array(
                'game_id' => $game_id,
                'start_point <= ' => $current_hour,
                'end_point >= ' => $current_hour
            )
        ));
        
        if(!$gameTimesEnt)
        {
            $this->Response->responseApi(GAME_STUB_EMPTY, null, null);
            return;
        }
        $game_time_id = $gameTimesEnt['GameTimes']['id'];
        
        $gameDatesEnt = $this->GameDates->find('first', array(
            'conditions' => array(
                'game_id' => $game_id,
                'date' => $current_day
            )
        ));
        
        
        if(!$gameDatesEnt)
        {
            $this->Response->responseApi(GAME_STUB_EMPTY, null, null);
            return;
        }
        
        $game_date_id = $gameDatesEnt['GameDates']['id'];
        
        $gameStubsEnt = $this->GameStubs->find('all', array(
            'conditions' => array(
                'game_date_id' => $game_date_id,
                //'game_time_id' => $game_time_id,
                'username' => $username,
                'team_id' => $team_id,
                'game_date' => $game_date,
                'status' => true,
                'start_point <= ' => $current_hour,
                'end_point >= ' => $current_hour
            )
        ));
        
        if(!$gameStubsEnt)
        {
            $this->makeGameData($team_id, $username, $game_code);
            $gameStubsEnt = $this->GameStubs->find('all', array(
                'conditions' => array(
                    'game_date_id' => $game_date_id,
                    //'game_time_id' => $game_time_id,
                    'username' => $username,
                    'team_id' => $team_id,
                    'game_date' => $game_date,
                    'status' => true,
                    'start_point <= ' => $current_hour,
                    'end_point >= ' => $current_hour
                )
            ));
            if(!$gameStubsEnt)
            {
                $this->Response->responseApi(GAME_DATA_EMPTY, null, null);
                return;
            }
        }
        
        $this->Response->responseApi(SUCCESS, null, $gameStubsEnt);
    }
    
    private function _get_pronounce_data()
    {
        $game_code = $this->requests['game_code'];
        $team_id = $this->requests['team_id'];
        $username = $this->requests['user_name'];
        $game_stub_id = empty($this->requests['stub_id'])? '0' : $this->requests['stub_id'];
        
        if($game_stub_id == '0')
        {
            $gameEnt = $this->Game->find('first', array(
                'conditions' => array(
                    'short_code' => $game_code 
                )
            ));
            
            if(!$gameEnt)
            {
                $this->Response->responseApi(GAME_DATA_EMPTY, null, null);
                return;
            }
                
            $game_id = $gameEnt['Game']['id'];
            
            $current_day = date('w');
            $game_date = date('Y-m-d');
            $current_hour = date('H:m');
            
            $gameTimesEnt = $this->GameTimes->find('first', array(
                'conditions' => array(
                    'game_id' => $game_id,
                    'start_point <= ' => $current_hour,
                    'end_point >= ' => $current_hour
                )
            ));
    
            if(!$gameTimesEnt)
            {
                $this->Response->responseApi(GAME_DATA_EMPTY, null, null);
                return;
            }
            $game_time_id = $gameTimesEnt['GameTimes']['id'];
            
            $gameDatesEnt = $this->GameDates->find('first', array(
                'conditions' => array(
                    'game_id' => $game_id,
                    'date' => $current_day
                )
            ));
            
           
            if(!$gameDatesEnt)
            {
                $this->Response->responseApi(GAME_DATA_EMPTY, null, null);
                return;
            }
            
            $game_date_id = $gameDatesEnt['GameDates']['id'];
            
            $gameStubsEnt = $this->GameStubs->find('first', array(
                'conditions' => array(
                    'game_date_id' => $game_date_id,
                    'game_time_id' => $game_time_id,
                    'username' => $username,
                    'team_id' => $team_id,
                    'game_date' => $game_date,
                    'status' => true
                )
            ));
            
            if(!$gameStubsEnt)
            {
                $this->makeGameData($team_id, $username, $game_code);
                $gameStubsEnt = $this->GameStubs->find('first', array(
                    'conditions' => array(
                        'game_date_id' => $game_date_id,
                        'game_time_id' => $game_time_id,
                        'username' => $username,
                        'team_id' => $team_id,
                        'game_date' => $game_date,
                        'status' => true
                    )
                ));
                if(!$gameStubsEnt)
                {
                    $this->Response->responseApi(GAME_DATA_EMPTY, null, null);
                    return;
                }
            }
            
            $game_stub_id = $gameStubsEnt['GameStubs']['id'];
            
            $gameWordsEnt = $this->GameWords->find('all', array(
                'conditions' => array(
                    'stub_id' => $game_stub_id
                )
            ));
            
            $resultSql = 'select if(sum(result * 3 * (4-submit_time)) is null, 0, sum(result * 3 * (4-submit_time)) is null) as score, count(*) as played from game_words where stub_id = '.$game_stub_id.' and submit_time > 0';
            $result = $this->GameWords->query($resultSql);
            
            $datas['Words'] = $gameWordsEnt;
            $datas['Results'] = array(
                'Played' => $result[0][0]['played'].'/5',
                'Scored' => $result[0][0]['score'].'/45'
            );
            
            $datas['Expired'] = date('d-m-Y').' '. $gameTimesEnt['GameTimes']['end_point'] .':00';
            
            $this->Response->responseApi(SUCCESS, null, $datas);
        }
        else 
        {
            $gameStubsEnt = $this->GameStubs->find('first', array(
                'conditions' => array(
                    'GameStubs.id' => $game_stub_id
                )
            ));
            
            $gameTimesEnt = $this->GameTimes->find('first', array(
                'conditions' => array(
                    'id' => $gameStubsEnt['GameStubs']['game_time_id']
                )
            ));
            
            $gameWordsEnt = $this->GameWords->find('all', array(
                'conditions' => array(
                    'stub_id' => $game_stub_id
                )
            ));
            
            $resultSql = 'select if(sum(result * 3 * (4-submit_time)) is null, 0, sum(result * 3 * (4-submit_time)) is null) as score, count(*) as played from game_words where stub_id = '.$game_stub_id.' and submit_time > 0';
            $result = $this->GameWords->query($resultSql);
            
            $datas['Words'] = $gameWordsEnt;
            $datas['Results'] = array(
                'Played' => $result[0][0]['played'].'/5',
                'Scored' => $result[0][0]['score'].'/45'
            );
            
            $datas['Expired'] = date('d-m-Y').' '. $gameTimesEnt['GameTimes']['end_point'] .':00';
            $endTime = DateTime::createFromFormat('d-m-Y H:i:s', date('d-m-Y').' '. $gameTimesEnt['GameTimes']['end_point'] .':00');
            $datas['Remaining'] = $endTime->diff(new DateTime())->format('%H:%i:%s');
            
            $this->Response->responseApi(SUCCESS, null, $datas);
        }
    }
    
    private function makeGameData($team_id, $username, $game_code)
    {
        
        $game_id = $this->getGameID($game_code);
        if($game_id == null) return;
        
        $gameDates = $this->getGameDates($game_id);
        if($gameDates== null) return;
        
        $gameTimes = $this->getGameTimes($game_id);
        if($gameTimes== null) return;
        
        foreach($gameDates as $keyDate=>$date)
        {
            foreach($gameTimes as $keyTime=>$time)
            {
                //Tao ban ghi stub
                $stub['game_date_id'] = $date['GameDates']['id'];
                $stub['game_time_id'] = $time['GameTimes']['id'];
                $stub['username'] = $username;
                $stub['team_id'] = $team_id;
                $stub['game_date'] = $this->genGameDate($date['GameDates']['date']);
                $stub['create_date'] = date('Ymd');
                $stub['status'] = true;
                $condition = array(
                    'game_date_id' => $date['GameDates']['id'],
                    'game_time_id' => $time['GameTimes']['id'],
                    'username' => $username,
                    'team_id' => $team_id,
                    'game_date' => $this->genGameDate($date['GameDates']['date'])
                );
                //Xoa cac stub cu
                $this->GameStubs->deleteAll($condition , false);
                //Them ban ghi stub
                $this->GameStubs->saveAssociated($stub);
                $stub_id = $this->GameStubs->find('first', array(
                    'conditions' => $condition,
                    'fields' => array('id')
                ))['GameStubs']['id'];
                $words = $this->Word->getRandanWord($username);
                for($i = 1; $i <= 5; $i++)
                {
                    //Tao du lieu word cho game
                    $word = $words[array_rand($words)]["words"]['id'];
                    
                    $gameWord['stub_id'] = $stub_id;
                    $gameWord['word_id'] = $word;
                    $gameWord['create_time'] = date('Ymd');
                    $this->GameWords->saveAssociated($gameWord);
                }
            }
        }
    }
    
    private  function _get_pronounce_file(){
        $l_directory = Configure::read('upload_pronounce_audio_dir');
        $url = Configure::read('upload_pronounce_audio_path');
        $audio_id= $_GET['audio_id'];
        $this->getPronounceAudio($l_directory,$audio_id,$url);
    }
    
    
    private  function _get_speech2text_file(){
        $l_directory = Configure::read('upload_pronounce_audio_wav_dir');
        $url = Configure::read('upload_pronounce_audio_wav_path');
        $audio_id= $_GET['audio_id'];
        $this->getPronounceAudio($l_directory,$audio_id,$url);
    }
    
    /**
     * upload File for Pronounce Game, UP in folder: upload/folder/games/pronounce/zip/
     * variable : access_token && user_name && file
     * type zip : zip , tar
     **/
    private function _submit_pronounce_file(){
        $dir = Configure::read('upload_pronounce_audio_dir');
        $audio_id= $_POST['user_name'] . 'pronounce_audio'.uniqid().time();
        $game_word_id = $_POST['game_word_id'];
        $game_code= $_POST['game_code'];
        $username = $_POST['user_name'];
        $teamname = $_POST['team_name'];
        
        $gameWordEnt = $this->GameWords->find('first', array(
            'conditions' => array(
                'GameWords.id' => $game_word_id
            )
        ));        
        
        $data = array(
            'file' => $_FILES['file'],
            'idModified' => $audio_id,
            'path' => $dir,
            'type_allow' => array('tar', 'zip', 'mp3')
        );
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $data = $this->uploadFile($data);
        
        //$link = API_GAME.'/'.$game_code.'?username='.$username.'&teamname='.$teamname.'&iddata='.md5($audio_id);
        //$return = $this->Common->putdata_openfire($link, null);
        
        if($data['response'] == SUCCESS){
            $score = $this->check_pronounce($game_code, $username, $teamname, $game_word_id, md5($audio_id));
            $res_data = array(
                'messages'=>$data['message'],
                'audio_id' => md5($audio_id),
                'result' => $score == 0 ? false : true,
                'score' => $score
            );
            $this->Response->responseApi(SUCCESS, null,$res_data);
        }
        else{
            $this->Response->responseApi(METHOD_NOT_ALLOW);
        }
    }
    
    //Phai cai dat ham kiem tra
    private function check_pronounce($game_code, $username, $teamname, $game_word_id, $audio_id)
    {
        
        $mp3_dir = Configure::read('upload_pronounce_audio_dir');
        $wav_dir = Configure::read('upload_pronounce_audio_wav_dir');
        
        foreach(glob($mp3_dir.'/*.*') as $file) {
            if(basename($file, '.'.pathinfo($file, PATHINFO_EXTENSION)) == $audio_id){
                $data['path'] = $file;
                $data['file_name'] = basename($file);
                break;
            }
        }        
        
        $wav_cmd = "ffmpeg -i " . $data['path'] . " -acodec pcm_s16le -ac 1 -ar 16000 " . $wav_dir . $audio_id . ".wav";
        $last_line = exec($wav_cmd);
                
        $gameWordEnt = $this->GameWords->find('first', array(
            'conditions' => array(
                'GameWords.id' => $game_word_id
            )
        ));
        
        $word =  $gameWordEnt['Meaning']['word'];
        
        //$link = API_GAME.'/'.$game_code.'/'.$word.'?filepath='.$wav_dir. $audio_id . ".wav";
        //$return = $this->Common->getdata_openfire($link);

        $link = API_SPEECH2TEXT . '/' . $game_code . '/' . $word . '?filepath=' . $audio_id;
        
        $return = $this->Common->getdata_speech2text($link) == 'true' ? 1 : 0;
        $score = 0;
        
        if($gameWordEnt)
        {
            if($gameWordEnt['GameWords']['submit_time'] == 0)
            {
                $update = array(
                    'submit_time' => 1,
                    'audio1_id' => $audio_id,
                    'score' => 9*$return,
                    'result' => $return,
                    'update_time' => (new DateTime())->format('Y-m-d H:i:s')
                );
                $this->GameWords->id = $game_word_id;
                $this->GameWords->save($update);
                $score = 9*$return;
            }
            elseif ($gameWordEnt['GameWords']['submit_time'] == 1)
            {
                $update = array(
                    'submit_time' => 2,
                    'audio2_id' => $audio_id,
                    'score' => 6*$return,
                    'result' => $return,
                    'update_time' => (new DateTime())->format('Y-m-d H:i:s')
                );
                $this->GameWords->id = $game_word_id;
                $this->GameWords->save($update);
                $score = 6*$return;
            }
            elseif ($gameWordEnt['GameWords']['submit_time'] == 2)
            {
                $update = array(
                    'submit_time' => 3,
                    'audio3_id' => $audio_id,
                    'score' => 3*$return,
                    'result' => $return,
                    'update_time' => (new DateTime())->format('Y-m-d H:i:s')
                );
                $this->GameWords->id = $game_word_id;
                $this->GameWords->save($update);
                $score = 3*$return;
            }
            elseif ($gameWordEnt['GameWords']['submit_time'] == 3)
            {
                $score = 0;
            }
            
            $gameResult = $this->GameResultTemp->find('first', array(
                'conditions' => array(
                    'id' => $gameWordEnt['GameWords']['stub_id']
                )
            ));
            
            date_default_timezone_set('Asia/Saigon');
            $playedTime = date('H')*3600 + date('i')*60 + date('sa');
                        
            if($gameResult){
                $updateResult = array(
                    'score' => $gameResult['GameResultTemp']['score'] + $score,
                    'played' => $gameResult['GameResultTemp']['played'] + ($gameWordEnt['GameWords']['submit_time'] == 0 ? 1 : 0),
                    'first_times' => $gameResult['GameResultTemp']['first_times'] + $return * ($gameWordEnt['GameWords']['submit_time'] == 0 ? 1 : 0),
                    'second_times' => $gameResult['GameResultTemp']['second_times'] + $return * ($gameWordEnt['GameWords']['submit_time'] == 1 ? 1 : 0),
                    'third_times' => $gameResult['GameResultTemp']['third_times'] + $return * ($gameWordEnt['GameWords']['submit_time'] == 2 ? 1 : 0),
                    'total_time_first' => $gameResult['GameResultTemp']['total_time_first'] + $return * ($gameWordEnt['GameWords']['submit_time'] == 0 ? $playedTime : 0),
                    'total_time_second' => $gameResult['GameResultTemp']['total_time_second'] + $return * ($gameWordEnt['GameWords']['submit_time'] == 1 ? $playedTime : 0),
                    'total_time_thirt' => $gameResult['GameResultTemp']['total_time_thirt'] + $return * ($gameWordEnt['GameWords']['submit_time'] == 2 ? $playedTime : 0)
                );
                $this->GameResultTemp->id = $gameResult['GameResultTemp']['id'];
                $this->GameResultTemp->save($updateResult);
            }
            else{
                $abc = $this->OfMucRoom->find('first', array(
                    'conditions' => array(
                        'name' => $teamname
                    )
                ));
                
                $newResult['stub_id'] = $gameWordEnt['GameWords']['stub_id'];
                $newResult['username'] = $username;
                $newResult['team_id'] = $abc['OfMucRoom']['roomID'];
                $newResult['score'] = $score;
                $newResult['max_score'] = 45;
                $newResult['played'] = 1;
                $newResult['total'] = 5;
                
                $newResult['first_times'] = $return * ($gameWordEnt['GameWords']['submit_time'] == 0 ? 1 : 0);
                $newResult['second_times'] = $return * ($gameWordEnt['GameWords']['submit_time'] == 1 ? 1 : 0);
                $newResult['third_times'] = $return * ($gameWordEnt['GameWords']['submit_time'] == 2 ? 1 : 0);
                $newResult['total_time_first'] = $return * ($gameWordEnt['GameWords']['submit_time'] == 0 ? $playedTime : 0);
                $newResult['total_time_second'] = $return * ($gameWordEnt['GameWords']['submit_time'] == 1 ? $playedTime : 0);
                $newResult['total_time_thirt'] = $return * ($gameWordEnt['GameWords']['submit_time'] == 2 ? $playedTime : 0);
                
                $newResult['year'] = date('Y');
                $newResult['week'] = date('W');
                
                $this->GameResultTemp->saveAssociated($newResult);
            } 
            return $score;
        }
        else {
            return 0;
        }
    }
    /**
     * Upload Audio File
     * Like function copy but just upload
     **/
    private function uploadFile($data){
        $idModified = $data['idModified'];
        $file = $data['file'];
        $path = $data['path'];
        $type_allow = $data['type_allow'];
        $response = METHOD_NOT_ALLOW;
        // Return data
        $dataReturn = array(
            'response' => $response,
            'message' => $this->errors,
        );
        
        // Verify file and path
        if (!isset($file) || !isset($path) || !is_array($file)){
            $this->errors[] = 'Name or path not found!';
            $dataReturn['message'] = $this->errors;
            return $dataReturn;
        }
        
        if (!is_writable($path)){
            $dataReturn['message'] = $path;//$this->errors;
            return $dataReturn;
        }
        
        $filename = md5($idModified);
        $filename = $filename.'.'.pathinfo($file['name'], PATHINFO_EXTENSION);
        
        
        $fullPath = $path . $filename;
        
        if (is_uploaded_file($file['tmp_name'])) {
            if (!move_uploaded_file($file['tmp_name'], $fullPath)){
                $this->errors[] = 'Error while upload the image!';
                $dataReturn['message'] = $this->errors;
                return $dataReturn;
            } else {
                $dataReturn['response'] = SUCCESS;
                $dataReturn['url'] = $fullPath;
                $dataReturn['type'] = $file['type'];
                chmod($fullPath, 0644);
                
            }
        } else {
            $this->errors[] = 'The uploaded file exceeds the upload_max_filesize directive ' . ini_get('upload_max_filesize');
            $dataReturn['message'] = $this->errors;
            return $dataReturn;
        }
        
        /*$zip = new ZipArchive;
        $res = $zip->open($fullPath);
        if ($res === true) {
            $audioFile = $zip->getNameIndex(0);
            $zip->extractTo($path, $audioFile);
            $destFile = $path.md5($idModified).'.'.pathinfo($audioFile, PATHINFO_EXTENSION);
            rename($path.$audioFile, $destFile);
            $zip->close();
        } else {
        }*/
        
        return $dataReturn;
    }
    
    private function getPronounceAudio($l_directory, $name, $url){
        $data =array();
        foreach(glob($l_directory.'/*.*') as $file) {
            if(basename($file, '.'.pathinfo($file, PATHINFO_EXTENSION)) == $name){
                $data['path'] = $file;
                $data['file_name'] = basename($file);
                break;
            }
        }
        if(isset($data['file_name']))
        {
            $url = $url.$data['file_name'];
        }
        else{
            $url ='';
        }
        if(@fopen($url,"r")){
            $type = pathinfo($url, PATHINFO_EXTENSION);
            $img = file_get_contents($url);
            header('content-type: audio/'. $type );
            //header('content-type: audio/'. 'm4a' );
            echo $img;
        }
    }
    
    /**
     * Module today game
     * @return json
     */
    private function _today_game() {
        $teaminfo = $this->Team->_exist_team_with_member($this->userinfo['id']);
        if (!$teaminfo || $teaminfo['id'] != $this->requests['team_id']) {
            $this->Response->responseApi(YOU_NOT_BELONG_TEAM);
        }
        $conditions = array(
            'team_id' => $this->requests['team_id'],
            'user_id' => $this->userinfo['id'],
            'week' => $this->time_today['week'],
            'month' => $this->time_today['month'],
            'day_in_week' => $this->time_today['day_in_week'],
            'year' => $this->time_today['year']
        ); 
        $item = array();
        $questions = $this->_get_game_data($conditions);
        if ($questions) {
            for ($i=1; $i<=5; $i++) {
                $item['word'.$i] = $questions['word'.$i];
                $item['word'.$i.'_answer_status'] = ($questions['word_answer'.$i] !=null)?true:false;
            }
        }
        $this->Response->responseApi(SUCCESS, null, $item);
    }
    
    private  function _get_result_this_week()
    {    
        $team_id = $this->requests['team_id'];
        $game_code = isset($this->requests['game_code'])?$this->requests['game_code']:'pronounce';
        
        $year = date('Y', strtotime("+0 week"));
        $week = date('W', strtotime("+0 week"));
        $stard_date = date('Ymd', strtotime( $year . "W". $week . 1));
        $end_date = date('Ymd', strtotime( $year . "W". $week . 7));
        
        $gameEnt = $this->Game->find('first', array(
            'conditions' => array(
                'short_code' => $game_code
            )
        ));
        
        if(!$gameEnt)
        {
            $this->Response->responseApi(GAME_DATA_EMPTY, null, null);
            return;
        }
        
        $game_id = $gameEnt['Game']['id'];
        
        $gameDatesEnt = $this->GameDates->find('all', array(
            'conditions' => array(
                'game_id' => $game_id,
            )
        ));
        
        $data_result = array();
        $total = array();
        
        foreach($gameDatesEnt as $keyDate=>$date)
        {
            $game_date = date('Ymd', strtotime( $year . "W". $week . $date['GameDates']['date']));
            $query = "select * from (select game_stubs.username, sum(score) as score, email, propValue as nick_name from game_stubs inner join game_words on game_stubs.id = game_words.stub_id"
               ." left join ofUser on ofUser.username = game_stubs.username"
               ." left join ofUserProp on ofUserProp.username = game_stubs.username and ofUserProp.name = 'nick_name'"
               ." where team_id = ".$team_id 
               ." and game_date = '" .$game_date."' group by username, email, nick_name) result";
            $results = $this->GameWords->query($query);
            
            $data_result['details']['day_'.$date['GameDates']['date']] = array();
            foreach($results as $keyWord=>$user)
            {
                array_push($data_result['details']['day_'.$date['GameDates']['date']], $user['result']);
            }
        }
        $query = "select * from (select game_stubs.username, sum(score) as score, email, propValue as nick_name from game_stubs inner join game_words on game_stubs.id = game_words.stub_id "
            ." left join ofUser on ofUser.username = game_stubs.username"
            ." left join ofUserProp on ofUserProp.username = game_stubs.username and ofUserProp.name = 'nick_name'"
            ." where team_id = ". $team_id ." and game_date >='". $stard_date ."' and game_date <='". $end_date ."' group by username, email, nick_name) result";
        
        $totalScore = $this->GameWords->query($query);
        
        foreach($totalScore as $keyScore=>$score)
        {
            array_push($total, $score['result']);
        }
        
        $data_result['total'] = $total;
        
        $this->Response->responseApi(SUCCESS, null, $data_result);
    }
    
    private function _get_ranking_last_week()
    {
        $game_code = isset($this->requests['game_code'])?$this->requests['game_code']:'pronounce';
        $page = isset($this->requests['page'])?$this->requests['page']:0;
        
        $year = date('Y', strtotime("-1 week"));
        $week = date('W', strtotime("-1 week"));
        $stard_date = date('Ymd', strtotime( $year . "W". $week . 1));
        $end_date = date('Ymd', strtotime( $year . "W". $week . 7));
        
        $sql = "select * from (SELECT team_id, room_name as roomName, team_name as teamName, SUM( score ) AS total_score, SUM( IF( score =9, 1, 0 ) ) AS first_times, SUM( IF( score =6, 1, 0 ) ) AS second_times, SUM( IF( score =3, 1, 0 ) ) AS third_times,"
                ." sum(IF( score =9, hour(update_time)*3600 + minute(update_time)*60 + second(update_time) - hour(start_point)*3600 - minute(start_point)*60, 0 )) as total_time_first,"
                ." sum(IF( score =6, hour(update_time)*3600 + minute(update_time)*60 + second(update_time) - hour(start_point)*3600 - minute(start_point)*60, 0 )) as total_time_second,"
                ." sum(IF( score =3, hour(update_time)*3600 + minute(update_time)*60 + second(update_time) - hour(start_point)*3600 - minute(start_point)*60, 0 )) as total_time_thirt"
                ." FROM (SELECT team_id, name as room_name, naturalName as team_name, game_words.id, game_words.update_time, submit_time, score, start_point"
                ." FROM (SELECT * FROM game_stubs WHERE game_date >=  '".$stard_date."' AND game_date <=  '".$end_date."' )stubs"
                ." INNER JOIN game_words ON stubs.id = game_words.stub_id"
                ." INNER JOIN game_times ON game_times.id = stubs.game_time_id INNER JOIN ofMucRoom ON ofMucRoom.roomID = stubs.team_id) RAW_DATA"
                ." GROUP BY team_id, room_name, team_name order by total_score desc, first_times desc, second_times desc, third_times desc, total_time_first asc, total_time_second asc, total_time_thirt asc) DATA limit 10 offset ".$page*10;
        
        $datas = $this->GameWords->query($sql);
        
        $results = array();
        
        foreach($datas as $key=>$data)
        {
            array_push($results, $data['DATA']);
        }
        
        $this->Response->responseApi(SUCCESS, null, $results);
    }
    
    private function _get_ranking()
    {
        $game_code = isset($this->requests['game_code'])?$this->requests['game_code']:'pronounce';
        $page = isset($this->requests['page'])?$this->requests['page']:0;
        $week_offset = isset($this->requests['week_offset'])?$this->requests['week_offset']:0;
        
        $year = date('Y', strtotime("-". $week_offset ." week"));
        $week = date('W', strtotime("-". $week_offset ." week"));
        $stard_date = date('Ymd', strtotime( $year . "W". $week . 1));
        $end_date = date('Ymd', strtotime( $year . "W". $week . 7));
        
        $sql = "select * from (SELECT team_id, name as roomName, naturalName as teamName, SUM(score) AS total_score, SUM(first_times) AS first_times, SUM(second_times) AS second_times, SUM(third_times) AS third_times,"
            ." sum(total_time_first) as total_time_first,"
            ." sum(total_time_second) as total_time_second,"
            ." sum(total_time_thirt) as total_time_thirt"
            ." FROM game_result_temp INNER JOIN ofMucRoom ON ofMucRoom.roomID = game_result_temp.team_id"
            ." WHERE game_result_temp.year = " . $year . " and game_result_temp.week= " . $week . " and status = 1 "
            ." GROUP BY team_id, name, naturalName order by total_score desc, first_times desc, second_times desc, third_times desc, total_time_first asc, total_time_second asc, total_time_thirt asc) DATA limit 10 offset ".$page*10;
                                        
        $datas = $this->GameWords->query($sql);
                                        
        $results = array();
                                        
        foreach($datas as $key=>$data)
        {
            array_push($results, $data['DATA']);
        }
                                        
        $this->Response->responseApi(SUCCESS, null, $results);
    }
    
    private function getGameID($game_code)
    {
        $gameEnt = $this->Game->find('first', array(
            'conditions' => array(
                'short_code' => $game_code
            )
        ));
        
        if(!$gameEnt)
        {
            return null;
        }
        
        return $gameEnt['Game']['id'];
    }
    
    private function getGameTimes($game_id)
    {
        $gameTimesEnt = $this->GameTimes->find('all', array(
            'conditions' => array(
                'game_id' => $game_id
            )
        ));
        
        if(!$gameTimesEnt)
        {
            return null;
        }
        return $gameTimesEnt;
    }
    
    private function getGameDates($game_id)
    {
        $gameDatesEnt = $this->GameDates->find('all', array(
            'conditions' => array(
                'game_id' => $game_id
            )
        ));
        
        if(!$gameDatesEnt)
        {
            return null;
        }
        return $gameDatesEnt;
    }
    
    private function genGameDate($day)
    {
        $year = date('Y', strtotime("+0 week"));
        $week = date('W', strtotime("+0 week"));
        $game_date = date('Ymd', strtotime( $year . "W". $week . $day));
        return $game_date;
    }
}

