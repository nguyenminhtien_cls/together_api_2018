<?php

App::uses('Component', 'Controller');

class ClassComponent extends Component {

    // the other component your component uses
    public $components = array('Response', 'Common');
    protected $requests = [];

    function initialize(Controller $controller) {
        $this->Controller = $controller;
        $this->User = ClassRegistry::init('User');
        $this->Team = ClassRegistry::init('Team');
        $this->Ranking = ClassRegistry::init('Ranking');
        $this->OfMucRoom = ClassRegistry::init('OfMucRoom');
        $this->OfMucMember = ClassRegistry::init('OfMucMember');
    }

    public function connectModule($module_name = null) {
        $this->requests = $this->Controller->send_request;
        $this->userinfo = $this->Controller->userinfo;
        $this->{'_' . $module_name}();
    }

    private function _create_class() {
        $link = "plugins/restapi/v1/chatrooms";
        $class_name = isset($this->requests ['class_name']) ? $this->requests ['class_name'] : "default";
        $description = isset($this->requests ['description']) ? $this->requests ['description'] : "default";
        $avatar = isset($this->requests ['avatar']) ? $this->requests ['avatar'] : "";
        $room_uniqid = uniqid();
        $params = array(
            "roomName" => $room_uniqid,
            "naturalName" => $class_name,
            "description" => $description,
            "subject" => $avatar,
            "maxUsers" => 100,
            "persistent" => "true",
            "publicRoom" => "true",
            "registrationEnabled" => "false",
            "canAnyoneDiscoverJID" => "true",
            "canOccupantsChangeSubject" => "false",
            "canOccupantsInvite" => "false",
            "canChangeNickname" => "false",
            "logEnabled" => "true",
            //"loginRestrictedToNickname" => "true", bo cai nay se join room dc bang api
            "membersOnly" => "false",
            "moderated" => "false",
        );
        $params = json_encode($params, true);
        //create class
        $create_result = $this->Common->postdata_openfire($link, $params);
        if ($create_result == "201") {
            //update member
            $add_room_link = "plugins/restapi/v1/chatrooms/" . $room_uniqid . "/members/" . $this->userinfo['username'];
            $this->Common->postdata_openfire($add_room_link, NULL);
            $res = $this->OfMucRoom->find('first', array(
                'fields' => array(
                    'roomID as class_id',
                    'naturalName as class_name',
                    'subject as avatar',
                    'description',
                    'name as class_code',
                    'creationDate'
                ),
                'conditions' => array(
                    'name' => $room_uniqid
                ),
            ));
            $res = Set::extract('/OfMucRoom/.', $res);
            $this->Response->responseApi(SUCCESS, null, $res);
        }

        $this->Response->responseApi(SYSTEM_ERROR, null, null);
    }

    private function _add_leaner_class() {
        $link = "plugins/restapi/v1/chatrooms";
        $room_uniqid = isset($this->requests ['class_id']) ? $this->requests ['class_name'] : "class_id";
        //update member
        $add_room_link = "plugins/restapi/v1/chatrooms/" . $room_uniqid . "/members/" . $this->userinfo['username'];
        $this->Common->postdata_openfire($add_room_link, NULL);
        $this->Response->responseApi(SUCCESS, null, null);
    }

    private function _remove_leaner_class() {
        $link = "plugins/restapi/v1/chatrooms";
        $room_uniqid = isset($this->requests ['class_id']) ? $this->requests ['class_name'] : "class_id";
        //update member
        $add_room_link = "plugins/restapi/v1/chatrooms/" . $room_uniqid . "/members/" . $this->userinfo['username'];
        $this->Common->deletedata_openfire($add_room_link, NULL);
        $this->Response->responseApi(SUCCESS, null, null);
    }

    /* Get all class  */

    private function _get_all_class() {
//        print_r($this->userinfo);die;
        $params = $this->requests;
        $limit = isset($params ['limit']) ? $params ['limit'] : 10;
        $page = isset($params ['page']) ? $params ['page'] : 0;
        $data = $this->OfMucRoom->find('all', array(
            'fields' => array(
                'roomID as class_id',
                'naturalName as class_name',
                'subject as avatar',
                'description',
                'class_member',
                'name as class_code',
                'creationDate'
            ),
            'limit' => $limit,
            'page' => $page
        ));
        $data = Set::extract('/OfMucRoom/.', $data);
        $this->Response->responseApi(SUCCESS, null, $data);
    }

}
