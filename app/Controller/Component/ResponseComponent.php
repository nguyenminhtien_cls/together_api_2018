<?php
App::uses('Component', 'Controller');
class ResponseComponent extends Component {
    public function initialize(Controller $controller)
    {
    }

	public function responseApi($code = 0, $message = null, $data = null) {
        http_response_code(200);

        if (!isset(Configure::read('api_error')[$code])) {
            $code = 130001;
        }
        if ($message) {
            $message = Configure::read('api_error')[$code];
        }

        if ($code != 0) {
            //http_response_code(400);
            http_response_code(200);
            $data = null;
        } else {
        	$message = Configure::read('api_error')[$code];
        }

        exit(json_encode(array(
            'error_code' => $code,
            'error_msg' => $message,
            'data' => $data
            )
        ));
	}
}