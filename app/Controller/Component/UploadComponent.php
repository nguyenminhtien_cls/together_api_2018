<?php
	
	App::uses('Component', 'Controller');
	App::uses('CakeFolder', 'Lib/');

	/**
	 *
	 * CakePHP (version 2) component to upload, resize, crop and
	 * add watermark to images.
	 *
	 * @author Angelito M. Goulart <ange.sap@hotmail.com>
	 *
	 */
	class UploadComponent extends Component{
        public $components = array('Response');
        protected $requests = [];

        public function connectModule($module_name=null) {
            $this->{'_'.$module_name}();
        }
		
		/**
		 * Watermark image file (must be png)
		 *
		 * @var string
		 */
		public $watermarkImage;		


		/**
		 * Jpeg image quality (0 - 100)
		 *
		 * @var int
		 */
		public $jpgQuality;

		
		/**
		 * Property that will contain execution errors
		 *
		 * @var array
		 */
		private $errors;
	
		
		/**
		 * Initialize method. Initialize class properties.
		 *
		 * @param Controller $controller
		 */
		public function initialize(Controller $controller) {
			$this->watermarkImage = "img" . DIRECTORY_SEPARATOR . "watermark.png";
			$this->jpgQuality = 100;
			$this->errors = array();
		}
	
		
		/**
		 * Copy an uploaded image to the destination path
		 *
		 * $data['file'] 	-> array with image data (found in $_FILES)
		 * $data['path'] 	-> destination path
		 * $data['name'] 	-> name file
		 *
		 * @param array $data
		 * @return mixed
		 */
		public function copy($data, $type_allow = null){
			
			// Verify file and path
			if (!isset($data['file']) || !isset($data['path']) || !is_array($data['file'])){
				$this->errors[] = 'Name or path not found!';
				return false;
			}

			if (!is_writable($data['path'])){
				$this->errors[] = 'Destination path is not writable!';
				return false;
			}
			
			if (!$this->_verifyMime($data['file']['name'], $type_allow)){
				$this->errors[] = 'The file type not allow!';
				return false;
			}
			
			// Generate filename and move file to destination path
			$filename_array = explode('.', $data['file']['name']);
			$ext = end($filename_array);
			$ext = strtolower($ext);

			if ( ! empty($data['name']))
			{
				$name = $data['name'] . '.' . $ext;
			}
			else
			{
				$name = date('Y/m/d') . '/' . uniqid() . '.' . $ext;
			}

			$complete_path = $data['path'] . $name;
			
			// create folder
			$folder = explode('/', $name);
			if (count($folder) > 1)	// if filename in subfolder -> create folder
			{
				array_pop($folder);
				$folder = implode('/', $folder);
				$CakeFolder = new CakeFolder();
				$CakeFolder->create($data['path'] . $folder);
			}

			// -- it's been uploaded with php
            if (is_uploaded_file($data['file']['tmp_name'])) {
				if ( ! move_uploaded_file($data['file']['tmp_name'], $complete_path)){
					$this->errors[] = 'Error while upload the image!';
					return false;
				}
				else
				{
					chmod($data['path'] . $name, 0644);
				}
			} else {
                $this->errors[] = 'The uploaded file exceeds the upload_max_filesize directive ' . ini_get('upload_max_filesize');
                return false;
            }

			// Return image filename
			return $name;
		}
		
		
		
		/**
		 * Adds a watermark on footer of an image.
		 * The watermark image file must be informed in public $watermarkImage.
		 *
		 * $data['file'] -> image path
		 *
		 * @param array $data
		 * @return bool
		 */
		public function watermark($data){
			
			// Verify files
			if (!is_file($this->watermarkImage)){
				$this->errors[] = 'Invalid watermark file!';
				return false;
			}
			
			if (!is_file($data['file'])){
				$this->errors[] = 'Invalid file!';
				return false;
			}
			
			if(!$this->_verifyMime($data['file'])){
				$this->errors[] = 'Invalid file type!';
				return false;
			}
			
			// Get image info
			$img = getimagesize($data['file']);

			// Get watermark image info
			$wimg = getimagesize($this->watermarkImage);
			if ($wimg['mime'] !== 'image/png') {
				$this->errors[] = 'Watermark image must be png!';
				return false;
			}

			$watermark = imagecreatefrompng($this->watermarkImage);
			$watermark_width = imagesx($watermark);
			$watermark_height = imagesy($watermark);
			
			// Defines watermark margin
			$margin_right = $img[0] - $watermark_width - 15;
			$margin_bottom = $img[1] - $watermark_height - 15;
			
			$createFunction = $this->_getCreateFunction($img['mime']);
			$finishFunction = $this->_getFinishFunction($img['mime']);
			if (false === $createFunction || false === $finishFunction) {
				$this->errors[] = 'Invalid file type!';
				return false;
			}
			
			// Generate image with watermark
			$image = $createFunction($data['file']);
			imagecopy($image, $watermark, $margin_right, $margin_bottom, 0, 0, $watermark_width, $watermark_height);
			
			// Replace the original image with the new image with watermark
			if ($img['mime'] == 'image/jpeg' || $img['mime'] == 'image/pjpeg'){
				$finishFunction($image, $data['file'], (int) $this->jpgQuality);
			} else {
				$finishFunction($image, $data['file']);
			}
			
			return true;
		
		}
		
		
		/**
		 * Method responsible for resize an image. Return false on error.
		 *
		 * $data['file']                -> complete path of original image file
		 * $data['width']               -> new width
		 * $data['height']              -> new height
		 * $data['output']              -> output path where resized image will be saved
		 * $data['proportional']        -> (true or false). If true, the image will be resized 
		 * only if its dimensions are larger than the values reported in width and height 
		 * parameters. Default: true.
		 *
		 * If only the width or height is given, the function will automatically calculate 
		 * whether the image is horizontal or vertical and will automatically apply the informed 
		 * size in the correct property (width or height).
		 *
		 * @param array $data
		 * @return bool
		 */
		public function resize($data)
		{
			
			// Verify parameters
			if (!isset($data['file']) || (!isset($data['width']) && !isset($data['height']))){
				$this->errors[] = 'Invalid filename or width/height!';
				return false;
			}
			
			if (!isset($data['output']) || !is_dir($data['output'])){
				$this->errors[] = 'Invalid output dir!';
				return false;
			}
			
			$data['proportional'] = (isset($data['proportional'])) ? $data['proportional'] : true;

			$data['height'] = (isset($data['height'])) ? $data['height'] : 0;
			$data['width']  = (isset($data['width']))  ? $data['width']  : 0;
			
			if (!is_writable($data['output'])){
				$this->errors[] = 'Output dir is not writable!';
				return false;
			}
			
			if (!is_file($data['file'])){
				$this->errors[] = 'Invalid file!';
				return false;
			}
		
			if(!$this->_verifyMime($data['file'])){
				$this->errors[] = 'Invalid file type!';
				return false;
			}
				
			if (!isset($data['proportional']))
			{
				$data['proportional'] = true;
			}
			
			// Validates width and height
			$width  = (isset($data['width']))  ? (int) $data['width']  : 0;
			$height = (isset($data['height'])) ? (int) $data['height'] : 0;		
			
			// Get attributes of image
			$img = getimagesize($data['file']);
			$original_width = $img[0];
			$original_height = $img[1];
			$mime = $img['mime'];
					 			
			$source = ($mime == 'image/png') ? imagecreatefrompng($data['file']) : imagecreatefromstring(file_get_contents($data['file']));
			$filename = basename($data['file']);
			$output = $data['output'] . $filename;
			
			// Verify if resize it's necessary
			if (($width > $original_width || $height > $original_height) && $data['proportional'] === true){
				$width = $original_width;
				$height = $original_height;
			} else {
				// If width or height not defined, it's necessary calculate proportional size
				if (!($width > 0 && $height > 0)){
					// Verify if image is horizontal or vertical
					if ($original_height > $original_width){
						$height = ($data['width'] > 0) ? $data['width'] : $data['height'];
						$width  = ($height / $original_height) * $original_width;
					} else {
						$width = ($data['height'] > 0) ? $data['height'] : $data['width'];
						$height = ($width / $original_width) * $original_height;
					}
				}
			}
			// Generate thumb
			$thumb = imagecreatetruecolor($width, $height);
			
			// Add transparency if image is png
			if ($mime == 'image/png') {
				imagealphablending($thumb, false);
				imagesavealpha($thumb,true);
				$transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
				imagefilledrectangle($thumb, 0, 0, $width, $height, $transparent);
			} 
			
			// Finish image
			imagecopyresampled($thumb, $source, 0, 0, 0, 0, $width, $height, $original_width, $original_height);
			$finishFunction = $this->_getFinishFunction($mime); 
						
			if (false === $finishFunction) { 
				$this->errors[] = 'Invalid file type.';
				return false;	
			} elseif ($mime == 'image/jpeg' || $mime == 'image/pjpeg') {
				$finishFunction($thumb, $output, (int) $this->jpgQuality);
			} else {
				$finishFunction($thumb, $output);
			} 
			return true;
		}


		/**
		 * Method to crop an image
		 * 
		 * $data['file']        -> complete path of original image file
		 * $data['w']           -> width of crop area
		 * $data['h']           -> height of crop area
		 * $data['x']           -> x coordinate of source point
		 * $data['y']           -> y coordinate of source point
		 * $data['output']      -> output path where cropped image will be saved
		 *
		 * @param array $data
		 * @return bool
		 */
		public function crop($data = array()){

			// Validates params
			if (!isset($data['file']) ||
			    !isset($data['w']) ||
			    !isset($data['h']) ||
			    !isset($data['x']) ||
			    !isset($data['y']) ||
			    !isset($data['output'])) {
				 $this->errors[] = 'Params missing!';
			}
			
			if (!is_file($data['file'])) {
				$this->errors[] = 'Invalid image!';
				return false;
			}

			if (!is_dir($data['output']) || !is_writable($data['output'])) {
				$this->errors[] = 'Output dir is not a dir or not writeable!';
				return false;
			}

			// Output path
			$path = $data['output'] . DIRECTORY_SEPARATOR . basename($data['file']);

			// Get image data
			$img = getimagesize($data['file']);

			$createFunction = $this->_getCreateFunction($img['mime']);
			$finishFunction = $this->_getFinishFunction($img['mime']);

			// Create source and destination image
			$src_img = $createFunction($data['file']);
			$dst_img = imagecreatetruecolor($data['w'], $data['h']);

			// Crop image
			imagecopyresampled($dst_img, 
					   $src_img, 
					   0, 
					   0, 
					   (int) $data['x'], 
					   (int) $data['y'],
	        			   (int) $data['w'],
	        			   (int) $data['h'],
	        			   (int) $data['w'], 
	        			   (int) $data['h']);

			// Finish image
			if ($img['mime'] == 'image/jpeg' || $img['mime'] == 'image/pjpeg'){
				$finishFunction($dst_img, $path, (int) $this->jpgQuality);
			} else {
				$finishFunction($dst_img, $path);
			}

		}
		
		
		/** 
		 * Verifies the mime-type of a file
		 *
		 * @param string $file
		 * @return bool
		 */
		private function _verifyMime($file, $type_allow = null){
			
			$filename_array = explode('.',$file);

			$extension = end($filename_array);
			
			$extension = strtolower($extension);
			
			if (empty($type_allow))	// default image
			{
				$mimes = array('jpeg', 'jpg', 'png', 'gif');
			}
			else
			{
				$mimes = $type_allow;
			}

			if (in_array($extension, $mimes)){
				return true;
			} else {
				return false;
			}
			
		}


		/**
		 * Method to get the specific function to create an image
		 * 
		 * @param string $mime
		 * @return string
		 */
		private function _getCreateFunction($mime){
		    if ($mime == 'image/jpeg' || $mime == 'image/pjpeg'){
	            	return 'imagecreatefromjpeg';
		    } elseif ($mime == 'image/gif') {
		        return 'imagecreatefromgif';
		    } elseif ($mime == 'image/png') {
		        return 'imagecreatefrompng';
		    } else {
		        $this->errors[] = 'Invalid file type!';
		        return false;
		    }
		}


		/**
		 * Method to get the specific function to finish an image
		 *
		 * @param string $mime
		 * @return string
		 */
		private function _getFinishFunction($mime) {
			if ($mime == 'image/jpeg' || $mime == 'image/pjpeg'){
				return 'imagejpeg';
			} elseif ($mime == 'image/gif') {
				return 'imagegif';
			} elseif ($mime == 'image/png') {
				return 'imagepng';
			} else {
				$this->errors[] = 'Invalid file type.';
				return false;	
			}
		}


		/**
		 * Get errors
		 *
		 * @return array
		 */
		public function getErrors() {
			return $this->errors;
		}


		/**
		 * Upload avatar
		 * Like function copy but just upload
		 **/
		public function uploadAvatar($data){
            $idModified = $data['idModified'];
            $file = $data['file'];
            $path = $data['path'];
            $type_allow = $data['type_allow'];
            $response = METHOD_NOT_ALLOW;
			// Return data
            $dataReturn = array(
                'response' => $response,
                'message' => $this->errors,
            );

			// Verify file and path
            if (!isset($file) || !isset($path) || !is_array($file)){
                $this->errors[] = 'Name or path not found!';
                $dataReturn['message'] = $this->errors;
                return $dataReturn;
            }

            if (!is_writable($path)){
                $this->errors[] = 'Destination path is not writable!';
                $dataReturn['message'] = $this->errors;
                return $dataReturn;
            }

            if (!$this->_verifyMime($file['name'], $type_allow)){
                $this->errors[] = 'The file type not allow!';
                $dataReturn['message'] = $this->errors;
                return $dataReturn;
            }

            // Generate filename and move file to destination path
//            $filename = basename($file['name'], '.'.pathinfo($file['name'], PATHINFO_EXTENSION));
            $filename = md5($idModified);
            $filename = $filename.'.'.pathinfo($file['name'], PATHINFO_EXTENSION);


            $fullPath = $path . $filename;

            if (is_uploaded_file($file['tmp_name'])) {
                if ( ! move_uploaded_file($file['tmp_name'], $fullPath)){
                    $this->errors[] = 'Error while upload the image!';
                    $dataReturn['message'] = $this->errors;
                    return $dataReturn;
                } else {
                    $dataReturn['response'] = SUCCESS;
                    $dataReturn['url'] = $fullPath;
                    $dataReturn['type'] = $file['type'];
                    chmod($fullPath, 0644);
                    
                }
            } else {
                $this->errors[] = 'The uploaded file exceeds the upload_max_filesize directive ' . ini_get('upload_max_filesize');
                $dataReturn['message'] = $this->errors;
                return $dataReturn;
            }

            return $dataReturn;
		}

		/**
         * upload Avatar for user, UP in folder : upload/folder/users/
         * variable : access_token && user_name && file
         * type img : png , jpg
		 **/
		public  function _upload_avatar_users(){
            $dir = Configure::read('upload_image_dir').'users/';
            $data = array(
                'file' => $_FILES['file'],
				'idModified' => $_POST['user_name'] . 'avatar_user',
				'path' => $dir,
				'type_allow' => array('jpg', 'png')
			);
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $data = $this->uploadAvatar($data);

            if($data['response'] == SUCCESS){
                $res_data = array(
                    'messages'=>$data['message'],
                    'url'=>$data['url']);
                $this->Response->responseApi(SUCCESS, null,$res_data);
			}
            else{
                $this->Response->responseApi(METHOD_NOT_ALLOW);
			}
		}
		
		/**
		 * get image for chat, in folder : upload/folder/chat/images/
		 * variable :  image_id (in message)
		 **/
		
		public  function _get_chat_images(){
		    $l_directory = Configure::read('upload_chat_images_dir');
		    $url = Configure::read('upload_chat_images_path');
		    $userName = $_GET['image_id'];
		    $this->getAvatar($l_directory,$userName,$url);
		}
		
		public  function _get_chat_videos(){
		    $l_directory = Configure::read('upload_chat_videos_dir');
		    $url = Configure::read('upload_chat_videos_path');
		    $userName = $_GET['video_id'];
		    $this->getVideo($l_directory, $userName, $url);
		    //$this->Response->responseApi(SUCCESS, null,$url.'/'.$userName.'.'.$result);
		}
		
		public  function _get_chat_thumbnail(){
		    $l_directory = Configure::read('upload_chat_thumbnail_dir');
		    $url = Configure::read('upload_chat_thumbnail_path');
		    $userName = $_GET['video_id'];
		    //Comment lai sau khi sinh duoc thumbnail
		    //$userName = 'thumbnail';
		    $this->getAvatar($l_directory, $userName, $url);
		}
		
		/**
		 * upload Image for chat, UP in folder : upload/folder/chat/images/
		 * variable : access_token && user_name && file
		 * type img : png , jpg
		 **/
		public  function _upload_chat_images(){
		    $dir = Configure::read('upload_chat_images_dir');
		    $image_id= $_POST['user_name'] . 'chat_images'.uniqid().time(); 
		    $data = array(
		        'file' => $_FILES['file'],
		        'idModified' => $image_id,
		        'path' => $dir,
		        'type_allow' => array('jpg', 'png')
		    );
		    if (!file_exists($dir)) {
		        mkdir($dir, 0777, true);
		    }
		    $data = $this->uploadAvatar($data);
		    
		    if($data['response'] == SUCCESS){
		        $res_data = array(
		            'messages'=>$data['message'],
		            'image_id' => md5($image_id)
		            );
		        $this->Response->responseApi(SUCCESS, null,$res_data);
		    }
		    else{
		        $this->Response->responseApi(METHOD_NOT_ALLOW);
		    }
		}
		
		public  function _upload_chat_videos(){
		    $dir = Configure::read('upload_chat_videos_dir');
		    $video_id= $_POST['user_name'] . 'chat_videos'.uniqid().time();
		    $data = array(
		        'file' => $_FILES['file'],
		        'idModified' => $video_id,
		        'path' => $dir,
		        'type_allow' => array('mov', 'mp4')
		    );
		    if (!file_exists($dir)) {
		        mkdir($dir, 0777, true);
		    }
		    $data = $this->uploadAvatar($data);
		    if($data['response'] == SUCCESS){
		        $res_data = array(
		            'messages'=>$data['message'],
		            'video_id' => md5($video_id)
		        );
		        
		        $thumbnail_dir = Configure::read('upload_chat_thumbnail_dir');
		        $video_dir = $data['url'];
		        $thumbnail_cmd = 'ffmpeg -i ' .$video_dir. ' -deinterlace -an -ss 1 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg ' .$thumbnail_dir.md5($video_id).'.jpg 2>&1';
		        $this->log('Command create thumbnail: '.$thumbnail_cmd, 'debug');
		        //exec('mkdir /home/ubuntu/www/together-project/together-api/app/webroot/assets/uploads/files/chat/thumbnail');
		        $last_line = exec($thumbnail_cmd);
		        $this->Response->responseApi(SUCCESS, null,$res_data);
		    }
		    else{
		        $this->Response->responseApi(METHOD_NOT_ALLOW);
		    }
		}
		
        /**
         * upload Avatar for group, UP in folder : upload/folder/group/
         * variable : access_token && group_name && file
         * type img : png , jpg
         **/
        public  function _upload_avatar_groups(){
        	$dir = Configure::read('upload_image_dir').'groups/';
            $data = array(
                'file' => $_FILES['file'],
                'idModified' => $_POST['group_name'].'avatar_group',
                'path' => $dir,
                'type_allow' => array('jpg', 'png')
            );
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
            $data = $this->uploadAvatar($data);
            if($data['response'] == SUCCESS){
                $res_data = array(
                    'messages'=>$data['message'],
                    'url'=>$data['url']);
                $this->Response->responseApi(SUCCESS, null,$res_data);
			} else{
                $this->Response->responseApi(METHOD_NOT_ALLOW);
			}
        }

        /**
         * get Avatar for user, in folder : upload/folder/user/
         * variable :  _token_users = md5(user_name+'avatar_user')
         **/
        
        public  function _get_avatar_users(){
            $l_directory = Configure::read('upload_image_dir').'users/';
            $url = Configure::read('upload_image_path').'users/';
            $userName = $_GET['_token_users'];
            $token = $_GET['_token'];
            $this->getAvatar($l_directory,$userName,$url);
        }
        
        
        public  function __get_avatar_users(){
            $l_directory = Configure::read('upload_image_dir').'users/';
            $url = Configure::read('upload_image_path').'users/';
            $groupName = $_GET['_token_users'];
            $email = $this->get_user_email($groupName);
            $groupName = $email.'avatar_user';
            $groupName = md5($groupName);
            $this->getAvatar($l_directory,$groupName,$url);
        }
        
        private function getdata_openfire($link) {
            $ch = curl_init ();
            curl_setopt_array ( $ch, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => OPENFIRE_HOST . '/' . $link,
                CURLOPT_HTTPHEADER => [
                    'Content-Type:application/json',
                    'Authorization:3QaqHOlLCF2tOG5D'
                ]
            ] );
            $response = curl_exec ( $ch );
            curl_close ( $ch );
            $jsonDecoded = json_decode ( $response ); // Returns an array
            return $response;
        }
        
        /* get user email */
        private function get_user_email($username) {
            $link_search = OPENSMART_USER_API.'?page=0&username='.$username;
            $data = $this->getdata_openfire($link_search);
            $data = $this->conver_xml($data);
            
            $this->log(json_encode($data), 'debug');
            if (!empty($data['user'])) {
                return $data['user']['email'];
            } else {
                return null;
            }
        }
        
        public function conver_xml($data){
            $xml = simplexml_load_string ( $data, "SimpleXMLElement", LIBXML_NOCDATA );
            $json = json_encode ( $xml );
            $array = json_decode ( $json, TRUE );
            return $array;
        }
        
        /**
         * get Avatar for user, in folder : upload/folder/group/
         * variable : _token_groups = md5(group_name+'avatar_group')
         **/
        public  function _get_avatar_groups(){
            $l_directory = Configure::read('upload_image_dir').'groups/';
            $url = Configure::read('upload_image_path').'groups/';
            $userName = $_GET['_token_groups'];
            $this->getAvatar($l_directory,$userName,$url);
        }
        
        private function getAvatar($l_directory,$name,$url){
            $data =array();
            foreach(glob($l_directory.'/*.*') as $file) {
                if(basename($file, '.'.pathinfo($file, PATHINFO_EXTENSION)) == $name){
                    $data['path'] = $file;
                    $data['file_name'] = basename($file);
                    break;
                }
            }
            if(isset($data['file_name']))
            {
            	$url = $url.$data['file_name'];
            }
            else{
                $url ='';
			}
            if(@fopen($url,"r")){
                $type = pathinfo($url, PATHINFO_EXTENSION);
                $img = file_get_contents($url);
                //header('content-type: image/'. $type );
                header('content-type: image/'. 'jpeg' );
                echo $img;
			}
		}
		
		private function getVideo($l_directory,$name,$url){
		    $data =array();
		    foreach(glob($l_directory.'/*.*') as $file) {
		        if(basename($file, '.'.pathinfo($file, PATHINFO_EXTENSION)) == $name){
		            $data['path'] = $file;
		            $data['file_name'] = basename($file);
		            break;
		        }
		    }
		    
		    if(isset($data['file_name']))
		    {
		        $url = $url.$data['file_name'];
		    }
		    else{
		        $url ='';
		    }
		    if(@fopen($url,"r")){
		        $type = pathinfo($url, PATHINFO_EXTENSION);
		        $video = file_get_contents($url);
		        if(strtoupper($type) == 'MOV')
		        {
                    header('content-type: video/quicktime');
		        }
		        else 
		        {
		            header('content-type: video/'. $type);
		        }
		        echo $video;
		    }
		}

 	}

