<?php

App::uses('Component', 'Controller');

class TeamComponent extends Component {

    // the other component your component uses
    public $components = array('Response', 'Common');
    protected $requests = [];

    function initialize(Controller $controller) {
        $this->Controller = $controller;
        $this->User = ClassRegistry::init('User');
        $this->Team = ClassRegistry::init('Team');
        $this->Ranking = ClassRegistry::init('Ranking');
    }

    public function connectModule($module_name = null) {
        $this->requests = $this->Controller->send_request;
        $this->userinfo = $this->Controller->userinfo;
        $this->{'_' . $module_name}();
    }

    /* Get all team  */

    private function _get_all_team() {
        $page = isset($this->requests ['page']) ? $this->requests ['page'] : '0';
        $access_token = isset($this->requests ['_token']) ? $this->requests ['_token'] : '';
        $link = API_TEAM . '?page=' . $page . '&token=' . $access_token;
        $data = $this->Common->getdata_openfire($link);
        $return = $this->Common->conver_xml($data);
        if (!empty($return)) {
            foreach ($return['chatRoom'] as $key => $value) {
                $this->log('Key: ' . $key, 'debug');
                if (gettype($key) == "integer") {
                    if (!empty($value['roomMembers'])) {
                        $arr = array();
                        foreach ($value['roomMembers']['roomMember'] as $key1 => $value1) {
                            if (gettype($key1) == "integer") {
                                $arr[] = array(
                                    'username' => isset($value1['username']) ? $value1['username'] : '',
                                    'name' => isset($value1['nickname']) ? $value1['nickname'] : '',
                                    'nick_name' => isset($value1['nickname']) ? $value1['nickname'] : '',
                                    'email' => isset($value1['email']) ? $value1['email'] : ''
                                );
                            } else {
                                if ($key1 == "username") {
                                    $arr[0]['username'] = isset($value1) ? $value1 : '';
                                }
                                if ($key1 == "nickname") {
                                    $arr[0]['name'] = isset($value1) ? $value1 : '';
                                    $arr[0]['nick_name'] = isset($value1) ? $value1 : '';
                                }
                                if ($key1 == "email") {
                                    $arr[0]['email'] = isset($value1) ? $value1 : '';
                                }
                            }
                        }
                        unset($return['chatRoom'][$key]['roomMembers']);
                        $return['chatRoom'][$key]['roomMembers'] = $arr;
                    }
                } else {
                    if ($key == 'roomMembers') {
                        $arr = array();
                        foreach ($value['roomMember'] as $key1 => $value1) {
                            if (gettype($key1) == "integer") {
                                $arr[] = array(
                                    'username' => isset($value1['username']) ? $value1['username'] : '',
                                    'name' => isset($value1['nickname']) ? $value1['nickname'] : '',
                                    'nick_name' => isset($value1['nickname']) ? $value1['nickname'] : '',
                                    'email' => isset($value1['email']) ? $value1['email'] : ''
                                );
                            } else {
                                if ($key1 == "username") {
                                    $arr[0]['username'] = isset($value1) ? $value1 : '';
                                }
                                if ($key1 == "nickname") {
                                    $arr[0]['name'] = isset($value1) ? $value1 : '';
                                    $arr[0]['nick_name'] = isset($value1) ? $value1 : '';
                                }
                                if ($key1 == "email") {
                                    $arr[0]['email'] = isset($value1) ? $value1 : '';
                                }
                            }
                        }
                        unset($return['chatRoom'][$key]);
                        $return['chatRoom'][$key] = $arr;
                    }
                }
            }
            $this->Response->responseApi(SUCCESS, null, $return);
        } else {
            $return = array();
            $this->Response->responseApi(SUCCESS, null, $return);
        }
    }

    /**
     * Module get_users
     * @return json
     */
    private function _get_users() {
        $limit = 20;
        $page = (isset($this->requests['page']) && is_numeric($this->requests['page'])) ? $this->requests['page'] : 1;
        $conds = array();
        $records = $this->User->find('all', array(
            'fields' => 'username, email, type, gender, avatar,name, nickname, goal, created_datetime, updated_datetime',
            'conditions' => $conds,
            'page' => $page,
            'limit' => $limit
        ));
        $users = array();
        if (!empty($records)) {
            foreach ($records as $r) {
                $users[] = $r['User'];
            }
            // caching
        }
        $this->Response->responseApi(SUCCESS, null, $users);
    }

    /**
     * Module create_team
     * @return json
     */
    private function _create_team() {
        
    }

    /**
     * Module join_team
     * @return json
     */
    private function _join_team() {
        $params = array(
            'team_id' => $this->requests['team_id']
        );
        $teaminfo = $this->_get_team_info($params['team_id'], true);
        if (!empty($teaminfo)) {
            $current_team_for_member = $this->_exist_team_with_member($this->userinfo['id']);
            if ($current_team_for_member && $current_team_for_member['team_id'] == $params['team_id']) { // you are already here. 
                $this->Response->responseApi(YOU_ARE_THIS_TEAM);
            }
            $member_arr = array(
                'member1_id' => $teaminfo['member1_id'],
                'member2_id' => $teaminfo['member2_id'],
                'member3_id' => $teaminfo['member3_id'],
            );

            if (count($member_arr) == count(array_filter($member_arr))) { // this team is full
                $this->Response->responseApi(TEAM_IS_FULL);
            }

            if (!in_array($this->userinfo['id'], $member_arr)) { // user not join this team
                $i = 0;
                foreach ($member_arr as $key_mem => $member) {
                    $i++;
                    if ($member == null || $member == '') {
                        $member_arr[$key_mem] = $this->userinfo['id'];
                        $member_arr['member' . $i . '_join_date'] = date('Y-m-d H:i:s');
                        break;
                    }
                }
                $data = $this->_set_update_team($params['team_id'], $member_arr);
                if (!empty($data)) {
                    $this->Response->responseApi(SUCCESS, null, $data);
                }
                $this->Response->responseApi(SAVE_DATA_FALSE);
            } else {
                $this->Response->responseApi(YOU_ARE_THIS_TEAM);
            }
        }
        $this->Response->responseApi(TEAM_NOT_EXIST);
    }

    /**
     * Module get team info
     * @return json
     */
    private function _get_detail_team() {
        $id_group = isset($this->requests['room_id']) ? $this->requests['room_id'] : '';
        $access_token = isset($this->requests['_token']) ? $this->requests['_token'] : '0';
        if ($id_group == '') {
            $this->Response->responseApi(TEAM_NOT_EXIST);
        } else {
            $link = API_TEAM . '/' . $id_group . '?token=' . $access_token;
            $this->log('Link get detail team :' . json_encode($link), 'debug');
            $data = $this->Common->getdata_openfire($link);
            $return = $this->Common->conver_xml($data);
            $this->log('RESULT TEAM API :' . json_encode($return), 'debug');
            $arr = array();
            $properties = array();
            if (!empty($return['roomMembers']['roomMember'])) {
                foreach ($return['roomMembers']['roomMember'] as $key => $value) {
                    if (gettype($key) == "integer") {
                        $arr[$key]['username'] = isset($value['username']) ? $value['username'] : '';
                        $arr[$key]['name'] = isset($value['nickname']) ? $value['nickname'] : '';
                        $arr[$key]['email'] = isset($value['email']) ? $value['email'] : '';
                        if (!empty($value['properties']['property'])) {
                            foreach ($value['properties']['property'] as $property) {
                                $arr[$key][$property['name']] = $property['value'];
                            }
                        }
                    } else {
                        if ($key == "username") {
                            $arr[0]['username'] = isset($value) ? $value : '';
                        } else if ($key == "nickname") {
                            $arr[0]['name'] = isset($value) ? $value : '';
                        } else if ($key == "email") {
                            $arr[0]['email'] = isset($value) ? $value : '';
                        } else if (!empty($return['roomMembers']['roomMember']['properties']['property'])) {

                            foreach ($return['roomMembers']['roomMember']['properties']['property'] as $property) {
                                $arr[0][$property['name']] = isset($property['value']) ? $property['value'] : '';
                            }
                        }
                    }
                }
            }
            $roomproname = "";
            $roomproval = "";
            $oneproperty = false;
            if (!empty($return['properties']['property'])) {
                foreach ($return['properties']['property'] as $key => $value) {
                    if (gettype($key) == "integer") {
                        $return[$value['name']] = $value['value'];
                    } else {
                        if ($key == "name") {
                            $roomproname = $value;
                        }
                        if ($key == "value") {
                            $roomproval = $value;
                        }
                        $oneproperty = true;
                    }

                    //Set default
                    $return['total_score'] = 1;
                    $return['weekly_rank'] = 1;
                    $return['last_week_rank'] = 1;
                }
                if ($oneproperty == true) {
                    $return[$roomproname] = $roomproval;
                }
            }
            unset($return['roomMembers']);
            unset($return['properties']);
            $return['roomMembers'] = $arr;
            $this->log('RETURN API :' . json_encode($return), 'debug');
            if ($return != false) {
                $this->Response->responseApi(SUCCESS, null, $return);
            } else {
                $this->Response->responseApi(TEAM_NOT_EXIST);
            }
        }
    }

    /**
     * Module leave team
     * @return json
     */
    private function _leave_team() {
        
    }

    /**
     * Module update team
     * @return json
     */
    private function _update_team() {
        $room_id = isset($this->requests['room_id']) ? $this->requests['room_id'] : '';
        $teamname = isset($this->requests['teamName']) ? $this->requests['teamName'] : '';
        $team_level = isset($this->requests['team_level']) ? $this->requests['team_level'] : '';
        $team_goal = isset($this->requests['team_goal']) ? $this->requests['team_goal'] : '';
        $claps = isset($this->requests['claps']) ? $this->requests['claps'] : '';
        $description = isset($this->requests['description']) ? $this->requests['description'] : '';
        if ($room_id == '') {
            $this->Response->responseApi(TEAM_ID_EMPTY);
        } else {
            $param = array(
                'roomID' => $room_id,
                'teamName' => $teamname,
                'properties' => array(
                    'property' => [
                        [
                            "name" => "team_level",
                            "value" => $team_level
                        ],
                        [
                            "name" => "team_goal",
                            "value" => $team_goal
                        ],
                        [
                            "name" => "claps",
                            "value" => $claps
                        ],
                        [
                            "name" => "description",
                            "value" => $description
                        ]
                    ]
                )
            );
            $params = json_encode($param, true);
            $this->log('Param Update Team :' . json_encode($param), 'debug');
            $link = API_TEAM . '/' . $room_id;
            $return = $this->Common->putdata_openfire($link, $params);
            $this->log('Result API update team :' . json_encode($return), 'debug');
            if ($return == 200) {
                $data_result = array(
                    'roomID' => $room_id,
                    'teamName' => $teamname,
                    'team_level' => $team_level,
                    'team_goal' => $team_goal,
                    'claps' => $claps,
                    'description' => $description
                );
                $this->Response->responseApi(SUCCESS, null, $data_result);
            } else {
                $this->Response->responseApi(SAVE_DATA_FALSE, null, false);
            }
        }
    }

    /**
     * Module ranking
     * @return json
     */
    private function _ranking() {
        $data = $this->Ranking->get_ranking();
        $this->Response->responseApi(SUCCESS, null, $data);
    }

    /**
     * Module results of team
     * @return json
     */
    private function _get_results_of_team() {
        
    }

    /**
     * Check member exist team
     */
    public function _exist_team_with_member($member_id = null) {
        $teaminfo = $this->Team->find('first', array(
            'conditions' => array(
                'OR' => array('member1_id' => $member_id, 'member2_id' => $member_id, 'member3_id' => $member_id)
            )
        ));
        if (!empty($teaminfo)) {
            return $teaminfo['Team'];
        }
        return false;
    }

    public function _set_update_team($team_id, $update = []) {
        $this->Team->id = $team_id;
        if ($this->Team->save($update)) {
            return $this->_get_team_info($team_id);
        }
        return false;
    }

    private function _search_team() {
        $page = isset($this->requests ['page']) ? $this->requests ['page'] : '0';
        $condition = isset($this->requests ['condition']) ? $this->requests ['condition'] : '';
        $access_token = isset($this->requests ['_token']) ? $this->requests ['_token'] : '';
        $link = API_TEAM . '/searchteams/' . $page . '?token=' . $access_token;
        $con_entity = array(
            'condition' => $condition
        );
        $con_entity = json_encode($con_entity, true);
        $data = $this->Common->searchdata_openfire($link, $con_entity);
        $return = $this->Common->conver_xml($data);
        if (!empty($return)) {
            foreach ($return['chatRoom'] as $key => $value) {
                if (gettype($key) == 'integer') {
                    if (isset($value['roomMembers']['roomMember'])) {
                        $arr = array();
                        foreach ($value['roomMembers']['roomMember'] as $key1 => $value1) {
                            if (gettype($key1) == "integer") {
                                $arr[] = array(
                                    'username' => isset($value1['username']) ? $value1['username'] : '',
                                    'name' => isset($value1['nickname']) ? $value1['nickname'] : '',
                                    'nick_name' => isset($value1['nickname']) ? $value1['nickname'] : '',
                                    'email' => isset($value1['email']) ? $value1['email'] : ''
                                );
                            } else {
                                if ($key1 == "username") {
                                    $arr[0]['username'] = isset($value1) ? $value1 : '';
                                }
                                if ($key1 == "nickname") {
                                    $arr[0]['name'] = isset($value1) ? $value1 : '';
                                    $arr[0]['nick_name'] = isset($value1) ? $value1 : '';
                                }
                                if ($key1 == "email") {
                                    $arr[0]['email'] = isset($value1) ? $value1 : '';
                                }
                            }
                        }
                        unset($return['chatRoom'][$key]['roomMembers']);
                        $return['chatRoom'][$key]['roomMembers'] = $arr;
                    }
                } else {
                    if ($key == 'roomMembers') {
                        if (isset($value['roomMember'])) {
                            $arr = array();
                            foreach ($value['roomMember'] as $key1 => $value1) {
                                if (gettype($key1) == "integer") {
                                    $arr[] = array(
                                        'username' => isset($value1['username']) ? $value1['username'] : '',
                                        'name' => isset($value1['nickname']) ? $value1['nickname'] : '',
                                        'nick_name' => isset($value1['nickname']) ? $value1['nickname'] : '',
                                        'email' => isset($value1['email']) ? $value1['email'] : ''
                                    );
                                } else {
                                    if ($key1 == "username") {
                                        $arr[0]['username'] = isset($value1) ? $value1 : '';
                                    }
                                    if ($key1 == "nickname") {
                                        $arr[0]['name'] = isset($value1) ? $value1 : '';
                                        $arr[0]['nick_name'] = isset($value1) ? $value1 : '';
                                    }
                                    if ($key1 == "email") {
                                        $arr[0]['email'] = isset($value1) ? $value1 : '';
                                    }
                                }
                            }
                            unset($return['chatRoom'][$key]['roomMembers']);
                            $team['roomMembers'] = $arr;
                        }
                    } else {
                        $team[$key] = $value;
                        unset($return['chatRoom'][$key]);
                    }
                    $return['chatRoom'] = array($team);
                }
            }
            $this->log('RETURN API :' . json_encode($return), 'debug');
            $this->Response->responseApi(SUCCESS, null, $return);
        } else {
            $return = array();
            $this->Response->responseApi(SUCCESS, null, $return);
        }
    }

    private function _search_team_new() {
        $page = isset($this->requests ['page']) ? $this->requests ['page'] : '0';
        $condition = isset($this->requests ['condition']) ? $this->requests ['condition'] : '';
        $access_token = isset($this->requests ['_token']) ? $this->requests ['_token'] : '';

        //$link = API_TEAM.'?page='.$page.'&condition='.$condition;
        //$this->log ("Link search team: ". $link, 'debug' );
        //$data = $this->Common->getdata_openfire($link);

        $link = API_TEAM . '/searchteams/' . $page . '?token=' . $access_token;
        $con_entity = array(
            'condition' => $condition
        );
        $con_entity = json_encode($con_entity, true);
        $data = $this->Common->searchdata_openfire($link, $con_entity);

        $return = $this->Common->conver_xml($data);
        //     	pr($return); die();
        if (!empty($return)) {
            foreach ($return['chatRoom'] as $key => $value) {
                if (!empty($value['roomMembers'])) {
                    $arr = array();
                    foreach ($value['roomMembers']['roomMember'] as $key1 => $value1) {
                        if (gettype($key1) == "integer") {
                            $arr[] = array(
                                'username' => isset($value1['username']) ? $value1['username'] : '',
                                'name' => isset($value1['nickname']) ? $value1['nickname'] : '',
                                'nick_name' => isset($value1['nickname']) ? $value1['nickname'] : '',
                                'email' => isset($value1['email']) ? $value1['email'] : ''
                            );
                        } else {
                            //$arr[] = array(
                            //	'username' => isset($value['roomMembers']['roomMember']['username'])?$value['roomMembers']['roomMember']['username']:'',
                            //	'name' => isset($value['roomMembers']['roomMember']['nickname'])?$value['roomMembers']['roomMember']['nickname']:''
                            //);
                            if ($key1 == "username") {
                                $arr[0]['username'] = isset($value1) ? $value1 : '';
                            }
                            if ($key1 == "nickname") {
                                $arr[0]['name'] = isset($value1) ? $value1 : '';
                                $arr[0]['nick_name'] = isset($value1) ? $value1 : '';
                            }
                            if ($key1 == "email") {
                                $arr[0]['email'] = isset($value1) ? $value1 : '';
                            }
                        }
                    }
                    unset($return['chatRoom'][$key]['roomMembers']);
                    $return['chatRoom'][$key]['roomMembers'] = $arr;
                }
            }
            $this->log('RETURN API :' . json_encode($return), 'debug');
            $this->Response->responseApi(SUCCESS, null, $return);
        } else {
            $return = array();
            $this->Response->responseApi(SUCCESS, null, $return);
        }
    }

    public function _achivement() {
        $team_id = isset($this->requests ['team_id']) ? $this->requests ['team_id'] : 0;
        //total score 
        $total_score = $this->Ranking->find('all', array(
            'fields' => array("SUM(Ranking.score) AS total_point"),
            'conditions' => array(
                'Ranking.team_id' => $team_id
            )
        ));
        $total_point = !empty($total_score[0][0]['total_point']) ? $total_score[0][0]['total_point'] : 0;
        // week rank
        $year = date('Y');
//        $week = date('W');
        $month = date('m');
        $weekly_ranking = array();
        for ($i = 1; $i < 53; $i++) {
            $weekly_ranking[$i] = $this->Ranking->get_ranking_for_team($team_id, $i, $month, $year);
        }
        // lastweek rank
        $year = date('Y', strtotime("-1 week"));
        $week = date('W', strtotime("-1 week"));
        $month = date('m', strtotime("-1 week"));
        $last_week_ranking = $this->Ranking->get_ranking_for_team($team_id, $week, $month, $year);
        $rep_arr = array(
            'total_score' => $total_point,
            'weekly_ranking' => $weekly_ranking,
            'last_weekly_ranking' => $last_week_ranking,
        );
        $this->Response->responseApi(SUCCESS, null, $rep_arr);
    }

}
