<?php
App::uses('Component', 'Controller');
class PushNotificationsComponent extends Component {
    // the other component your component uses
    public $components = array('Response');
    protected $requests = [];

    // (Android)API access key from Google API's Console.
    private static $API_ACCESS_KEY = API_ACCESS_KEY;
    // (iOS) Private key's passphrase.
    private static $PASS_PHRASE = 'joashp';
//    private static $PEM_KEY = PEM_KEY;

    function initialize(Controller $controller) {
        $this->Controller = $controller;
    }

    public function connectModule($module_name=null) {
        $this->{'_'.$module_name}();
    }
    /**
     * Sends Push notification for Android users
     * parameter : $data , $registration_id
     **/
    public function _android() {
        $reg_id = $_POST['registration_id'];
        $data =  $_POST['data'];
        $url = 'https://fcm.googleapis.com/fcm/send';
        $message = array(
            'title' => $data['title'],
            'body' => $data['body'],
            'subtitle' => '',
            'icon' => 1,
            'sound' => 1
        );

        $headers = array(
            'Authorization: key=' .self::$API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $fields = array(
            'to' => $reg_id,
            'notification' => $message,
        );

        return $this->useCurl($url, $headers, json_encode($fields));
    }

    /**
     * Sends Push notification for iOS users
     * parameter : $data ,$deviceToken,$env : DEV_ENV_PUSH = 1 ,DIS_ENV_PUSH = 2 ;
     **/
    public function _iOS() {
        try {
            $deviceToken = $_POST['device_token'];
            $env = $_POST['env'];
            $title = $_POST['title'];
            $desc= $_POST['desc'];
            $badge= isset($_POST['badge'])?$_POST['badge']:0;
            $category = isset($_POST['category'])?$_POST['category']:'Notification';
            $body = isset($_POST['body'])?$_POST['body']:'body';
            
            
            $ctx = stream_context_create();
            // ck.pem is your certificate file
            //        stream_context_set_option($ctx, 'ssl', 'local_cert', self::$PEM_KEY);
            stream_context_set_option($ctx, 'ssl', 'local_cert', dirname(__FILE__) . '/' .'ios_push_certificate.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', self::$PASS_PHRASE);
            // Open a connection to the APNS server
            
            if($env == DEV_ENV_PUSH){
                $remote_socket  ='ssl://gateway.sandbox.push.apple.com:2195';
            }else{
                $remote_socket  ='ssl://gateway.push.apple.com:2195';
            }
            $fp = stream_socket_client(
                $remote_socket, $err,
                $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
            if (!$fp)
            {
                exit("Failed to connect: $err $errstr" . PHP_EOL);                
            }
                // Create the payload body
            $push['aps'] = array(
                'alert' => array(
                    'title' => $title,
                    'body' => $desc
                ),
                'sound' => 'default',
                'badge' => $badge,
                'category' => $category
            );
            $push['cus'] = array(
                'type' => $category,
                'body' => $body
            );
                // Encode the payload as JSON
                
            $payload = json_encode($push);
            $this->log("Push Notification: " . $payload, 'debug');
            //Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));
            // Close the connection to the server
            fclose($fp);
            if (!$result){
                //return 'Message not delivered' . PHP_EOL;
                $this->Response->responseApi(PUSH_MESSAGE_FAIL);
            }else{
                //return 'Message successfully delivered' . PHP_EOL;
                $this->Response->responseApi(SUCCESS);
            }
            
        } catch (Exception $ex)
        {
            $this->log('_iOS: '.$ex->getMessage(), 'debug');
            $this->Response->responseApi(PUSH_MESSAGE_FAIL);
        }
    }

    // Curl
    private function useCurl(
//        &$model,
        $url, $headers, $fields = null) {
        // Open connection
        $ch = curl_init();
        if ($url) {
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            if ($fields) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            }

            // Execute post
            $result = curl_exec($ch);
            if ($result === FALSE) {
                $this->Response->responseApi(PUSH_MESSAGE_FAIL);
                die('Curl failed: ' . curl_error($ch));
            }else{
                $this->Response->responseApi(SUCCESS);
            }
            // Close connection
            curl_close($ch);

            return $result;
        }
    }
}