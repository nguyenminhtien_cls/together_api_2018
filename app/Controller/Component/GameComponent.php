<?php
App::uses('Component', 'Controller');
class GameComponent extends Component {
    public function initialize(Controller $controller)
    {
    }

	public function responseApi($code = 0, $message = null, $data = null) {
		http_response_code(200);
		if (!$message) {
            $message = isset(Configure::read('api_error')[$code]) ? Configure::read('api_error')[$code] : 'unknown';
        }
		if ($code != 0) {
            http_response_code(400);
            $data = null;
        } else {
            $message = "";
        }

        exit(json_encode(array(
            'error_code' => $code,
            'error_msg' => $message,
            'data' => $data
            )
        ));
	}
}