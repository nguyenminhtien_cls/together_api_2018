<?php

App::uses('Component', 'Controller');

class DynamicModelComponent extends Component
{
	public function registerModel($model=null) {
		$model = ClassRegistry::init($model);
		return $model;
	}
}