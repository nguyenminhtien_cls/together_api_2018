<?php 
App::uses('Component', 'Controller');
class VerifyInputComponent extends Component {
	public $list_url_allow_token = array(
		DOMAIN.'/userapi/normal_login',
		DOMAIN.'/userapi/normal_register',
		DOMAIN.'/userapi/facebook_login',
		DOMAIN.'/userapi/google_login',
		DOMAIN.'/userapi/twitter_login', 
	);

	public $allow_method = array(
		'GET' => array(
		),
		'POST' => array(
			DOMAIN.'/userapi/normal_login',
			DOMAIN.'/userapi/normal_register',
			DOMAIN.'/userapi/facebook_login',
			DOMAIN.'/userapi/google_login',
			DOMAIN.'/userapi/twitter_login',
		),
		'DELETE' => array(
		)
	);
	function __construct($init_params, $inputs) {
 			       
    }

    public function checkAllowAToken() {
    	if (!in_array($this->allow_url, $this->list_url_allow_token)) {
    		$this->responseApi(100001);
    	}
    }


    // function responseApi($code = 0, $message = '', $data = array(), $header_type = 'json') {
    //     if ($header_type == 'json') {
    //         header('Content-Type: application/json');
    //     } else {
    //         header('Content-Type: text/html');
    //     }


    //     if ($code == 0) {
    //         $this->setHeader(200);
    //         exit(json_encode(array(
    //             'code' => $code,
    //             'message' => $message,
    //             'data' => $data
    //                 )
    //         ));
    //     } else {
    //         $this->setHeader(400);
    //         if (!$message) {
    //             $message = isset($this->api_error[$code]) ? $this->api_error[$code] : '';
    //         }
    //         exit(json_encode(array(
    //             'code' => $code,
    //             'message' => $message,
    //             'data' => $data
    //                 )
    //         ));
    //     }
    // }
}