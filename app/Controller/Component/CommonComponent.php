<?php
App::uses ( 'Component', 'Controller' );
App::uses ( 'CakeEmail', 'Network/Email' );
class CommonComponent extends Component {
	public function initialize(Controller $controller) {
	}
	// API Open fire
	function getdata_openfire($link) {
		$ch = curl_init ();
		curl_setopt_array ( $ch, [ 
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => OPENFIRE_HOST . '/' . $link,
				CURLOPT_HTTPHEADER => [ 
						'Content-Type:application/json',
						'Authorization:3QaqHOlLCF2tOG5D'
				] 
		] );
		$response = curl_exec ( $ch );
		curl_close ( $ch );
		$jsonDecoded = json_decode ( $response ); // Returns an array
		return $response;
	}
	function postdata_openfire($link, $data) {
		$ch = curl_init ();
		curl_setopt_array ( $ch, [ 
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_URL => OPENFIRE_HOST . '/' . $link,
				CURLOPT_HTTPHEADER => [ 
						'Content-Type:application/json',
						'Authorization:3QaqHOlLCF2tOG5D' 
				] 
		
		] );
		// set URL and other appropriate options
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
		curl_setopt ( $ch, CURLOPT_HEADER, true );
		curl_setopt ( $ch, CURLOPT_POST, true );
		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		$status = substr ( $result, 9, 3 );
		// close cURL resource, and free up system resources
		$data = json_decode ( $result, true );
		return $status;
	}
	function putdata_openfire($link, $data) {
		$ch = curl_init ();
		curl_setopt_array ( $ch, [ 
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_URL => OPENFIRE_HOST . '/' . $link,
				CURLOPT_HTTPHEADER => [ 
						'Content-Type:application/json',
						'Authorization:3QaqHOlLCF2tOG5D' 
				] 
		
		] );
		// set URL and other appropriate options
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
		curl_setopt ( $ch, CURLOPT_HEADER, true );
		curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "PUT" );
		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		$this->log($result, 'debug');
		$status = substr ( $result, 9, 3 );
		// close cURL resource, and free up system resources
		$data = json_decode ( $result, true );
		return $status;
	}
	
	function searchdata_openfire($link, $data) {
	    $ch = curl_init ();
	    curl_setopt_array ( $ch, [
	        CURLOPT_RETURNTRANSFER => true,
	        CURLOPT_URL => OPENFIRE_HOST . '/' . $link,
	        CURLOPT_HTTPHEADER => [
	            'Content-Type:application/json',
	            'Authorization:3QaqHOlLCF2tOG5D'
	        ]
	        
	    ] );
	    // set URL and other appropriate options
	    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
	    curl_setopt ( $ch, CURLOPT_HEADER, true );
	    curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "PUT" );
	    $result = curl_exec ( $ch );
	    
	    curl_close ( $ch );
	    $data = substr ( $result, stripos($result,'<?xml'), strlen($result) - stripos($result,'<?xml'));
	    // close cURL resource, and free up system resources
	    return $data;
	}
	
	function deletedata_openfire($link) {
		$link_delete = OPENFIRE_HOST . '/' . $link;
		// $this->log("link delete: ".$link_delete,'debug');
		$ch = curl_init ();
		curl_setopt_array ( $ch, [ 
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $link_delete,
				CURLOPT_HTTPHEADER => [ 
						'Content-Type:application/json',
						'Authorization:3QaqHOlLCF2tOG5D' 
				] 
		] );
		
		// curl_setopt($ch, CURLOPT_URL,$link_delete);
		curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "DELETE" );
		$result = curl_exec ( $ch );
		$httpCode = curl_getinfo ( $ch, CURLINFO_HTTP_CODE );
		curl_close ( $ch );
		$data = json_decode ( $result, true );
		// $this->log("data delete: ".$result.'data'.$data,'debug');
		
		return $data;
	}
	public function create_user($param) {
		$user = $this->postdata_openfire ( OPENFIRE_USER_API, $param );
		$this->log ( "RESULT :" . json_encode ( $user ), 'debug' );
		return $user;
	}
	public function get_user() {
		$user = $this->getdata_openfire ( OPENFIRE_USER_API );
		return $user;
	}
	public function get_user_info($username) {
		$link = OPENFIRE_USER_API.'/'.$username;
		$this->log('Link Get user info'.json_encode($link),'debug');
		$info = $this->getdata_openfire($link);
		$info = $this->conver_xml($info);
		$this->log('API RESLUT :'.json_encode($info),'debug');
		return $info;
	}
	public function update_user($username, $param) {
		$link = OPENFIRE_USER_API . '/' . $username;
		$this->log ( "Update User link:" . $link, 'debug' );
		$user = $this->putdata_openfire ( $link, $param );
		$this->log ( "Update User :" . ($user), 'debug' );
		return $user;
	}
	public function convert_phone_number($code, $phone) {
		if (strstr ( $phone, '+' )) {
			$phone = preg_replace ( '/[^0-9]/s', '', $phone );
			if ($phone > 0) {
				return $phone;
			}
			return false;
		} else if ($phone) {
			$phone = preg_replace ( '/[^0-9]/s', '', $phone );
			$phone = round ( $phone );
			if ($phone > 0) {
				return $code . $phone;
			}
			return false;
		} else {
			return false;
		}
	}
	public function get_alluser() {
		$data = $this->get_user ();
		$this->log ( 'USER :' . json_encode ( $data ), 'debug' );
		$xml = simplexml_load_string ( $data, "SimpleXMLElement", LIBXML_NOCDATA );
		$json = json_encode ( $xml );
		$array = json_decode ( $json, TRUE );
		return $array;
	}
	public function send_sms($phone, $sms) {
		try {
			require_once (ROOT . '/app/Lib/twilio-php/Services/Twilio.php'); // Loads the library
			                                                                 // Your Account Sid and Auth Token from twilio.com/user/account
			$sid = "ACdb1110c84737527c390b74ea2740395b";
			$token = "a7af1f7fef99baea088cfdbf9e899943";
			$client = new Services_Twilio ( $sid, $token );
			if ($client->account->messages->sendMessage ( "+19284517281", $phone, $sms ))
			{
				return true;
			}
			else
			{
				return false;
			}
		} catch ( Services_Twilio_RestException $e ) {
			return false;
		}
	}
	public function push($devtoken, $msg = '', $badge = null, $identifier = null, $source = null, $data = null) {
		if (empty ( $devtoken )) {
			return false;
		}
		require_once ROOT . '/app/Lib/ApnsPHP/Autoload.php';
		$push = new ApnsPHP_Push ( ApnsPHP_Abstract::ENVIRONMENT_SANDBOX, Configure::read ( 'apn.server_certificates_file' ) );
		// $push->setProviderCertificatePassphrase(Configure::read('apn.certificate_passphrase'));
		$push->connect ();
		$message = new ApnsPHP_Message ( $devtoken );
		$message->setText ( $msg );
		if (isset ( $badge )) {
			$message->setBadge ( ( int ) $badge );
		}
		if (isset ( $identifier )) {
			$message->setCustomIdentifier ( $identifier );
		}
		if (isset ( $source )) {
			$message->setSound ( $source );
		}
		if (isset ( $data )) {
			$message->setCustomProperty ( 'data', $data );
		}
		$push->add ( $message );
		$push->send ();
		$push->disconnect ();
		if ($push->getErrors () == '') {
			return true;
		}
		return false;
	}
	public function change_info($property) {
		if (! empty ( $property )) {
			foreach ( $property as $key => $value ) {
				if ($value ['@attributes'] ['key'] == 'device_type') {
					$device_type = $property [$key] ['@attributes'] ['value'];
					$user_info ['device_type'] = $device_type;
				}
				if ($value ['@attributes'] ['key'] == 'toeic_level_id') {
					$toeic_level_id = $property [$key] ['@attributes'] ['value'];
					$user_info ['toeic_level_id'] = $toeic_level_id;
				}
				if ($value ['@attributes'] ['key'] == 'word_category') {
					$word_category = $property [$key] ['@attributes'] ['value'];
					$user_info ['word_category'] = $word_category;
				}
				if ($value ['@attributes'] ['key'] == 'avatar_url') {
					$avatar_url = $property [$key] ['@attributes'] ['value'];
					$user_info ['avatar_url'] = $avatar_url;
				}
				if ($value ['@attributes'] ['key'] == 'pass_word') {
					$pass_word = $property [$key] ['@attributes'] ['value'];
					$user_info ['pass_word'] = $pass_word;
				}
				if ($value ['@attributes'] ['key'] == 'push_token') {
					$push_token = $property [$key] ['@attributes'] ['value'];
					$user_info ['push_token'] = $push_token;
				}
				if ($value ['@attributes'] ['key'] == 'nick_name') {
					$nick_name = $property [$key] ['@attributes'] ['value'];
					$user_info ['nick_name'] = $nick_name;
				}
				if ($value ['@attributes'] ['key'] == 'quote') {
					$quote = $property [$key] ['@attributes'] ['value'];
					$user_info ['quote'] = $quote;
				}
				if ($value ['@attributes'] ['key'] == 'goal') {
					$goal = $property [$key] ['@attributes'] ['value'];
					$user_info ['goal'] = $goal;
				}
				if ($value ['@attributes'] ['key'] == 'birthday') {
					$birthday = $property [$key] ['@attributes'] ['value'];
					$user_info ['birthday'] = $birthday;
				}
				if ($value ['@attributes'] ['key'] == 'gender') {
					$gender = $property [$key] ['@attributes'] ['value'];
					$user_info ['gender'] = $gender;
				}
				if ($value ['@attributes'] ['key'] == 'status') {
					$status = $property [$key] ['@attributes'] ['value'];
					$user_info ['status'] = $status;
				}
				if ($value ['@attributes'] ['key'] == 'team_id') {
					$team_id = $property [$key] ['@attributes'] ['value'];
					$user_info ['team_id'] = $team_id;
				}
			}
			return $user_info;
		} else {
			return null;
		}
	}
	public function get_param($return_user) {
		if (! empty ( $return_user )) {
			$data = array (
					'property' => [ 
							[
									"@key" => "in_room",
									"@value" => isset ( $return_user ['in_room'] ) ? $return_user ['in_room'] : false
							],
							[ 
									"@key" => "device_type",
									"@value" => isset ( $return_user ['device_type'] ) ? $return_user ['device_type'] : '' 
							],
							[ 
									"@key" => "toeic_level_id",
									"@value" => isset ( $return_user ['toeic_level_id'] ) ? $return_user ['toeic_level_id'] : '' 
							],
							[ 
									"@key" => "word_category",
									"@value" => isset ( $return_user ['word_category'] ) ? $return_user ['word_category'] : '' 
							],
							[ 
									"@key" => "avatar_url",
									"@value" => isset ( $return_user ['avatar_url'] ) ? $return_user ['avatar_url'] : '' 
							],
							[ 
									"@key" => "pass_word",
									"@value" => isset ( $return_user ['pass_word'] ) ? $return_user ['pass_word'] : '' 
							],
							[ 
									"@key" => "push_token",
									"@value" => isset ( $return_user ['push_token'] ) ? $return_user ['push_token'] : '' 
							],
							[ 
									"@key" => "nick_name",
									"@value" => isset ( $return_user ['nick_name'] ) ? $return_user ['nick_name'] : '' 
							],
							[ 
									"@key" => "team_id",
									"@value" => isset ( $return_user ['team_id'] ) ? $return_user ['team_id'] : '' 
							],
							[ 
									"@key" => "status",
									"@value" => isset ( $return_user ['status'] ) ? $return_user ['status'] : '' 
							],
							[ 
									"@key" => "gender",
									"@value" => isset ( $return_user ['gender'] ) ? $return_user ['gender'] : '' 
							],
							[ 
									"@key" => "birthday",
									"@value" => isset ( $return_user ['birthday'] ) ? $return_user ['birthday'] : '' 
							],
							[ 
									"@key" => "goal",
									"@value" => isset ( $return_user ['goal'] ) ? $return_user ['goal'] : '' 
							],
							[ 
									"@key" => "quote",
									"@value" => isset ( $return_user ['quote'] ) ? $return_user ['quote'] : '' 
							],
							[ 
									"@key" => "token",
									"@value" => isset ( $return_user ['token'] ) ? $return_user ['token'] : '' 
							],
							[ 
									"@key" => "token_reset",
									"@value" => isset ( $return_user ['token_reset'] ) ? $return_user ['token_reset'] : '' 
							],
							[
									"@key" => "claps",
									"@value" => isset ( $return_user ['claps'] ) ? $return_user ['claps'] : ''
							],
							[
									"@key" => "is_favorited",
									"@value" => isset ( $return_user ['is_favorited'] ) ? $return_user ['is_favorited'] : false
							],
							[
									"@key" => "in_room",
							         "@value" => isset ( $return_user ['in_room'] ) ? $return_user ['in_room'] : false
							]
					] 
			);
			
			return $data;
		} else {
			return null;
		}
	}
	public function conver_xml($data){
		$xml = simplexml_load_string ( $data, "SimpleXMLElement", LIBXML_NOCDATA );
		$json = json_encode ( $xml );
		$array = json_decode ( $json, TRUE );
		return $array;
	}
	public function format_email($email){
		if(!empty($email)){
			$username = str_replace("@","_",$email);
			return $username;
		}else {
			return null;
		}
	}
	
	public function validate_email($email){
	    $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
	    return preg_match($regex, $email);
	}
	/**
	 *
	 * @param array $errors        	
	 * @return string -----------------------------
	 *         Author : ndhung.dev@gmail.com
	 *         Date : 11/01/2017
	 */
	public function convertErrorsToString($errors = []) {
		if ($errors && ! empty ( $errors )) {
			$string = '';
			foreach ( $errors as $key => $un ) {
				foreach ( $un as $v ) {
					$string .= $v . "</br>";
				}
			}
			return $string;
		}
		return false;
	}
	public function test() {
		die ( 'xxxx' );
	}
	public function _generate_properties($newProperties, $oldProperties = null)
	{
	    if(!empty($newProperties))
	    {
	        $salt = Configure::read('Security.salt');
	        $email = $newProperties ['email'];
	        $username = $this->format_email( $email);
	        if(isset($username))
	        {
	            $name = explode("_",$username);
	        }
            $userProperties = array(
                'property' => [
                    [
	                   "name" => "is_online",
	                   "value" => isset($newProperties['is_online'])?$newProperties['is_online']: (isset($oldProperties)?$this->_get_value("is_online", $oldProperties):'')
                    ],
                    [
                        "name" => "device_type",
                        "value" => isset($newProperties['device_type'])?$newProperties['device_type']:(isset($oldProperties)?$this->_get_value("device_type", $oldProperties):'')
                    ],
                    [
                        "name" => "toeic_level_id",
                        "value" => isset($newProperties['toeic_level_id'])?$newProperties['toeic_level_id']:(isset($oldProperties)?$this->_get_value("toeic_level_id", $oldProperties):'')
                    ],
                    [
                        "name" => "word_category",
                        "value" => isset($newProperties['word_category'])?$newProperties['word_category']:(isset($oldProperties)?$this->_get_value("word_category", $oldProperties):'')
                    ],
                    [
                        "name" => "avatar_url",
                        "value" => isset($newProperties['avatar_url'])?$newProperties['avatar_url']:(isset($oldProperties)?$this->_get_value("avatar_url", $oldProperties):'')
                    ],
                    [
                        "name" => "pass_word",
                        "value" => isset($newProperties['password'])?md5($salt.$newProperties['password']):(isset($newProperties['pass_word'])?md5($salt.$newProperties['pass_word']):(isset($oldProperties)?$this->_get_value("pass_word", $oldProperties):''))
                    ],
                    [
                        "name" => "push_token",
                        "value" => isset($newProperties['push_token'])?$newProperties['push_token']:(isset($oldProperties)?$this->_get_value("push_token", $oldProperties):'')
                    ],
                    [
                        "name" => "nick_name",
                        "value" => isset($newProperties['nick_name'])?$newProperties['nick_name']:(isset($oldProperties)?$this->_get_value("nick_name", $oldProperties):$name[0])
                    ],
                    [
                        "name" => "team_id",
                        "value" => isset($newProperties['team_id'])?$newProperties['team_id']:(isset($oldProperties)?$this->_get_value("team_id", $oldProperties):'')
                    ],
                    [
                        "name" => "status",
                        "value" => isset($newProperties['status'])?$newProperties['status']:(isset($oldProperties)?$this->_get_value("status", $oldProperties):'')
                    ],
                    [
                        "name" => "gender",
                        "value" => isset($newProperties['gender'])?$newProperties['gender']:(isset($oldProperties)?$this->_get_value("gender", $oldProperties):'')
                    ],
                    [
                        "name" => "birthday",
                        "value" => isset($newProperties['birthday'])?$newProperties['birthday']:(isset($oldProperties)?$this->_get_value("birthday", $oldProperties):'')
                    ],
                    [
                        "name" => "goal",
                        "value" => isset($newProperties['goal'])?$newProperties['goal']:(isset($oldProperties)?$this->_get_value("goal", $oldProperties):'')
                    ],
                    [
                        "name" => "quote",
                        "value" => isset($newProperties['quote'])?$newProperties['quote']:(isset($oldProperties)?$this->_get_value("quote", $oldProperties):'')
                    ],
                    [
                        "name" => "token",
                        "value" => isset($newProperties['token'])?$newProperties['token']:(isset($oldProperties)?$this->_get_value("token", $oldProperties):$new_access_token = md5(uniqid().time()))
                    ],
                    [
                        "name" => "token_reset",
                        "value" => isset($newProperties['token_reset'])?$newProperties['token_reset']:(isset($oldProperties)?$this->_get_value("token_reset", $oldProperties):'')
                    ],
                    [
                        "name" => "claps",
                        "value" => isset($newProperties['claps'])?$newProperties['claps']:(isset($oldProperties)?$this->_get_value("claps", $oldProperties):'')
                    ],
                    [
                        "name" => "is_favorited",
                        "value" => isset($newProperties['is_favorited'])?$newProperties['is_favorited']:(isset($oldProperties)?$this->_get_value("is_favorited", $oldProperties):false)
                    ],
                    [
                        "name" => "in_room",
                        "value" => isset($newProperties['in_room'])?$newProperties['in_room']:(isset($oldProperties)?$this->_get_value("in_room", $oldProperties):false)
                    ],
                    [
                        "name" => "native",
                        "value" => isset($newProperties['native'])?$newProperties['native']:(isset($oldProperties)?$this->_get_value("native", $oldProperties):'')
                    ],
                    [
                        "name" => "description",
                        "value" => isset($newProperties['description'])?$newProperties['description']:(isset($oldProperties)?$this->_get_value("description", $oldProperties):'')
                    ],
                    [
                        "name" => "together_id",
                        "value" => isset($newProperties['together_id'])?$newProperties['together_id']:(isset($oldProperties)?$this->_get_value("together_id", $oldProperties):'')
                    ],
                    [
                        "name" => "full_name",
                        "value" => isset($newProperties['full_name'])?$newProperties['full_name']:(isset($oldProperties)?$this->_get_value("full_name", $oldProperties):'')
                    ],
                    [
                        "name" => "default_id",
                        "value" => isset($newProperties['default_id'])?$newProperties['default_id']:(isset($oldProperties)?$this->_get_value("default_id", $oldProperties):'')
                    ],
                    [
                        "name" => "facebook_id",
                        "value" => isset($newProperties['facebook_id'])?$newProperties['facebook_id']:(isset($oldProperties)?$this->_get_value("facebook_id", $oldProperties):'')
                    ],
                    [
                        "name" => "is_teacher",
                        "value" => isset($newProperties['is_teacher'])?$newProperties['is_teacher']:(isset($oldProperties)?$this->_get_value("is_teacher", $oldProperties):'')
                    ],
	                ]
	            );
	            return $userProperties;
	    }
	    else
	    {
	        return null;
	    }
	}
	public function _get_value($name, $properties)
	{
	    foreach ($properties as $value)
	    {
	        if($value['name'] == $name)
	        {
	            return ($value['value']!=null)?$value['value']:'';
	        }
	    }
	    return '';
	}
	
	// API Speech2Text 
	function getdata_speech2text($link) {
	    $ch = curl_init ();
	    curl_setopt_array ( $ch, [
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_URL => SPEECH2TEXT_HOST . '/' . $link,
	        CURLOPT_HTTPHEADER => [
	            'Content-Type:application/json'
	        ]
	    ] );
	    $response = curl_exec ( $ch );
	    curl_close ( $ch );
	    $jsonDecoded = json_decode ( $response ); // Returns an array
	    return $response;
	}
	
}

?>

