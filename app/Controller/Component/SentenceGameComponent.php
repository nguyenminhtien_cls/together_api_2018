<?php 
App::uses('Component', 'Controller');

class SentenceGameComponent extends Component {
    // the other component your component uses
    public $components = array('Response', 'Team');
    protected $requests = [];
    function initialize(Controller $controller) {
        $this->Controller = $controller;
        $this->Game = ClassRegistry::init('Game');
        $this->GameData = ClassRegistry::init('GameDataSentence');
	}

    public function connectModule($module_name=null) {
        $this->requests = $this->Controller->send_request;
        $this->userinfo = $this->Controller->userinfo;
        $this->time_today = $this->Controller->time_today;
        $this->{'_'.$module_name}();
    }
    /**
     * Module today game
     * @return json
     */
    private function _today_game() {
        $teaminfo = $this->Team->_exist_team_with_member($this->userinfo['id']);
        if (!$teaminfo || $teaminfo['id'] != $this->requests['team_id']) {
            $this->Response->responseApi(YOU_NOT_BELONG_TEAM);
        }
        $conditions = array(
            'team_id' => $this->requests['team_id'],
            'user_id' => $this->userinfo['id'],
            'week' => $this->time_today['week'],
            'month' => $this->time_today['month'],
            'day_in_week' => $this->time_today['day_in_week'],
            'year' => $this->time_today['year']
        ); 
        $item = array();
        $questions = $this->_get_game_data($conditions);
        if ($questions) {
            for ($i=1; $i<=5; $i++) {
                $item['word'.$i] = $questions['word'.$i];
                $item['word'.$i.'_answer_status'] = ($questions['word_answer'.$i] !=null)?true:false;
            }
        }
        $this->Response->responseApi(SUCCESS, null, $item);
    }

    /**
     * Module submit answer
     * @return json
     */
    private function _submit_answer() {
        $teaminfo = $this->Team->_exist_team_with_member($this->userinfo['id']);
        if (!$teaminfo || $teaminfo['id'] != $this->requests['team_id']) {
            $this->Response->responseApi(YOU_NOT_BELONG_TEAM);
        }

        $conditions = array(
            'team_id' => $this->requests['team_id'],
            'user_id' => $this->userinfo['id'],
            'week' => $this->time_today['week'],
            'month' => $this->time_today['month'],
            'day_in_week' => $this->time_today['day_in_week'],
            'year' => $this->time_today['year']
        ); 
        $questions = $this->_get_game_data($conditions);
        if ($questions) {
            $update = array('score' => $questions['score']);
            $num_of_word = str_word_count($this->requests['answer']);
            $name_txt = uniqid() . '_' . time() . '.txt';
            $name = "/opt/lang_service/source_txt/" . $name_txt;
            $handle = fopen($name, "w");
            fwrite($handle, $this->requests['answer']);
            fclose($handle);
            chmod($name, 0777);
            exec('java -jar /opt/lang_service/LanguageTool/languagetool-commandline.jar -l en-US --api ' . $name, $out, $status);
            if (0 === $status) {
                $xml = simplexml_load_string(implode('', $out));
                $json = json_encode($xml);
                $array = json_decode($json, TRUE);
                $score_data = $this->_calculate_scoring($array, $num_of_word);
                $update['score'] = $update['score'] + $score_data['score']; 
                $update['word_answer'.$this->requests['answer_num']] = trim($this->requests['answer']);
            } 
            if ($this->_update_game_data($questions['id'],$update)) {
                $this->Response->responseApi(SUCCESS, null, array('score' => $update['score'], 'answer' => (isset($true))?true:false));
            } 
            $this->Response->responseApi(SAVE_DATA_FALSE);
        }
        $this->Response->responseApi(USER_DATA_INVALID);
    }

    /**
     * Module add clap
     * @return json
     */
    private function _add_clap() {
        
    }

    /**
     * Module get word for game
     * @return json
     */
    private function _get_game_data($conditions=array()) {
        $data = $this->GameData->find('first', array(
            'conditions' => $conditions
        ));
        if (isset($data['GameDataSentence']) && !empty($data['GameDataSentence'])) {
            return $data['GameDataSentence'];
        }
        return false;
    }

    /**
     * Update Sentence Game
     * @return json
     */
    private function _update_game_data($game_data_id = null, $update=array()) {
        $this->GameData->id = $game_data_id;
        if ($this->GameData->save($update)) {
            return $this->_get_game_data(array('GameDataSentence.id' => $game_data_id));
        } 
        return false;
    }

    private function _calculate_scoring($input = array(), $num_of_word = 1) {
        $score = 10;
        $error_data = array();
        if (isset($input['error']) && !empty($input['error'])) {
            $num_error = count($input['error']);
            foreach ($input['error'] as $key => $value) {
                $i = 0;
                if ($num_error == 1) {
                    $category = $value['categoryid'];
                } else {
                    $category = $value['@attributes']['categoryid'];
                }
                $error_data[$i]['offset'] = isset($value['@attributes']['offset']) ? $value['@attributes']['offset'] : '';
                $error_data[$i]['errorlength'] = isset($value['@attributes']['errorlength']) ? $value['@attributes']['errorlength'] : '';
                $error_data[$i]['type'] = isset($value['@attributes']['categoryid']) ? $value['@attributes']['categoryid'] : 'Other';
                switch ($category) {
                    case 'TYPOS'://loi chinh ta
                        $score = $score - 2;
                        break;
                    case 'CASING'://sai viet hoa
                        $score = $score - 0.5;
                        break;
                    case 'CONFUSED_WORDS': //tu nham lan
                    case 'MISC'://Linh tinh cac tu, cac tu bi lan lon
                    case 'REDUNDANCY'://cum tu thua
                        $score = $score - 1;
                        break;
                    case 'GRAMMAR':
                        $score = $score - 3;
                        break;
                    default:
                        break;
                }
                $i = $i + 1;
            }
            $percent_error = ($num_error / $num_of_word) * 100;
            if ($percent_error < 30 && $num_of_word >= 5) {
                $score = $score * 1.1;
            } elseif ($percent_error >= 70) {
                $score = 0;
            }
            if ($score < 0) {
                $score = 0;
            }
            if ($score > 10) {
                $score = 10;
            }
            $score = round($score);
        }
        $score_data = array(
            'score' => $score,
            'error' => $error_data,
        );
        return $score_data;
    }
}
