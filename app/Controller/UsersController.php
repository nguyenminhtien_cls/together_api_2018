<?php

App::uses('AppController', 'Controller');


/**
 * Created by PhpStorm.
 * User: Phan Minh
 * Date: 7/20/2016
 * Time: 2:28 PM
 */
class UsersController extends AppController
{
    public $uses = array('OfUser', 'Team', 'Message');
    public $helpers = array('Paginator', 'Html', 'Form');
    public $components = array('Upload', 'Session', 'Paginator','Common');

    public function index()
    {
        $this->set('title_for_layout', 'List of users');
        $search = array();
        $conditions = [];
//         // set conditions
        if (isset($this->params->query['filters'])) { 
            $filters = $this->params->query['filters'];
            if (trim($filters['username']) != '') {
                $conditions[] = "OfUser.username LIKE '%".trim($filters['username'])."%'";
            }
            if ($filters['email'] != '') {
                $conditions[] = "OfUser.email LIKE '%".trim($filters['email'])."%'";
            }
            if (isset($filters['status']) && in_array($filters['status'], ['ACTIVATED','DEACTIVATED'])) {
                if ($filters['status'] == 'ACTIVATED') {
                    $status = 1;
                } else {
                    $status = 0;
                }
                $conditions[] = "User.account_status = '".$status."'";
            }
        }

        $this->paginate = array(
            'fields' => 'OfUser.*',
            'order' => array(
                'id' => 'DESC'
            ),
            'conditions' => $conditions,
            'limit' => 15,
            'page' => 1,
        );
        $users = $this->paginate('OfUser');
//         pr($users); die();
        $this->set('users', $users);
        if (isset($filters)) {
            $this->set('filters', $filters);
        }
        $this->set('page_header', 'Users list');
    }

    public function view($username = null)
    {
        $this->set('title_for_layout', 'User detail');
        if (!$username) {
            throw new NotFoundException(__('Invalid user'));
        }
        
        $data = $this->Common->get_user_info($username);
        if (!$data) {
            throw new NotFoundException(__('Invalid user'));
        }else {
        	$array = $data;
//         	pr($array); die();
        	$user_info['username'] =  $array['username'];
        	$user_info['email'] =  $array['email'];
        	$user_info['name'] =  isset($array['name'])?$array['name']:'';
        	// query database in Mysql
        	$conditions[] = "OfUser.username LIKE '%".trim($array['username'])."%'";
        	$this->paginate = array(
        			'fields' => 'OfUser.*',
        			'order' => array(
        					'id' => 'DESC'
        			),
        			'conditions' => $conditions,
        			'limit' => 15,
        			'page' => 1,
        	);
        	$users_base = $this->paginate('OfUser');
        	$user_info['created_datetime'] = $users_base['0']['OfUser']['creationDate'];
        	
        	if(!empty($array['properties']['property'])){
        		foreach ($array['properties']['property'] as $value){
        			if(isset($value['@attributes']['key']) == 'device_type'){
        				$user_info['device_type'] =  $value['@attributes']['value'];
        			}
        			if(isset($value['@attributes']['key']) == 'toeic_level_id'){
        				$user_info['toeic_level_id'] =  $value['@attributes']['value'];
        			}
        			if(isset($value['@attributes']['key']) == 'word_category'){
        				$user_info['word_category'] =  $value['@attributes']['value'];
        			}
        			if(isset($value['@attributes']['key']) == 'avatar_url'){
        				$user_info['avatar_url'] =  $value['@attributes']['value'];
        			}
        			if(isset($value['@attributes']['key']) == 'nick_name'){
        				$user_info['nick_name'] =  $value['@attributes']['value'];
        			}
        			if(isset($value['@attributes']['key']) == 'quote'){
        				$user_info['quote'] =  $value['@attributes']['value'];
        			}
        			if(isset($value['@attributes']['key']) == 'goal'){
        				$user_info['goal'] =  $value['@attributes']['value'];
        			}
        			if(isset($value['@attributes']['key']) == 'birthday'){
        				$user_info['birthday'] =  $value['@attributes']['value'];
        			}
        			if(isset($value['@attributes']['key'])== 'gender'){
        				$user_info['gender'] =  $value['@attributes']['value'];
        			}
        			if(isset($value['@attributes']['key'])== 'status'){
        				$user_info['status'] =  $value['@attributes']['value'];
        			}
        			if(isset($value['@attributes']['key']) == 'team_id'){
        				$user_info['team_id'] =  $value['@attributes']['value'];
        			}else {
        				$user_info['team_id'] =  0;
        			}
        		}
        	}
        	$this->set('user', $user_info);
        }
        $this->set('page_header', 'User detail');
    }

    public function edit()
    {
        $this->set('title_for_layout', 'Edit user');
        $url =  Router::url( $this->here, true );
        $url = (explode("/",$url));
        $username = $url['5'];
        $user_info = $this->Common->get_user_info($username);
        if(!empty($user_info['properties']['property'])){
            foreach ($user_info['properties']['property'] as $key => $value){
        		if($value['@attributes']['key'] == 'pass_word'){
        			$pass_word =  $value['@attributes']['value'];
        		}
        		if($value['@attributes']['key'] == 'avatar_url'){
        			$avatar_url =  $value['@attributes']['value'];
        		}
        		
        		if($value['@attributes']['key'] == 'birthday'){
        		    $user_info['birthday'] =  $value['@attributes']['value'];
        		}
        		
        		if($value['@attributes']['key'] == 'toeic_level_id'){
        		    $user_info['toeic_level_id'] =  $value['@attributes']['value'];
        		}
        		
        		if($value['@attributes']['key'] == 'nick_name'){
        		    $user_info['nick_name'] =  $value['@attributes']['value'];
        		}
        		
        		if($value['@attributes']['key'] == 'status'){
        		    $user_info['status'] =  $value['@attributes']['value'];
        		}
        		
        		if($value['@attributes']['key'] == 'gender'){
        		    $user_info['gender'] =  $value['@attributes']['value'];
        		}
        		
        		if($value['@attributes']['key'] == 'goal'){
        		    $user_info['goal'] =  $value['@attributes']['value'];
        		}
        		
        		if($value['@attributes']['key'] == 'quote'){
        		    $user_info['quote'] =  $value['@attributes']['value'];
        		}
        		
        		if($value['@attributes']['key'] == 'birthday'){
        		    $user_info['birthday'] =  $value['@attributes']['value'];
        		}
        	}
        	
        	unset($user_info['properties']);
        }
        
        $this->set('array',$user_info);
        if ($this->request->is('post') || $this->request->is('put')) {
        	$data = $this->request->data;
        	$birthday = $data['User']['birthday']['day'].'/'.$data['User']['birthday']['month'].'/'.$data['User']['birthday']['year'];
        	if(!empty($array)){
        	$param = array (
        			'email' => $array['email'],
        			'username' => $username,
        			'name' => $data['User']['name'],
        			'properties' => array (
        					'property' => [
        							[
        									"@key" => "device_type",
        									"@value" => isset ( $device_type ) ? $device_type : 'IOS'
        							],
        							[
        									"@key" => "toeic_level_id",
        									"@value" => isset ( $data['User']['toeic_level_id'] ) ? $data['User']['toeic_level_id']: ''
        							],
        							[
        									"@key" => "word_category",
        									"@value" => isset ( $word_category ) ? $word_category : '1'
        							],
        							[
        									"@key" => "avatar_url",
        									"@value" => isset ( $avatar_url ) ? $avatar_url : ''
        							],
        							[
        									"@key" => "pass_word",
        									"@value" => isset ( $pass_word ) ? $pass_word : ''
        							],
        							[
        									"@key" => "push_token",
        									"@value" => isset ( $push_token ) ? $push_token : ''
        							],
        							[
        									"@key" => "nick_name",
        									"@value" => isset ( $data['User']['nickname']) ? $data['User']['nickname']: ''
        							],
        							[
        									"@key" => "team_id",
        									"@value" => isset ( $team_id) ? $team_id: ''
        							],
        							[
        									"@key" => "status",
        									"@value" => isset ( $data['User']['account_status']) ? $data['User']['account_status']: ''
        							],
        							[
        									"@key" => "gender",
        									"@value" => isset ( $data['User']['gender']) ? $data['User']['gender']: ''
        							],
        							[
        									"@key" => "birthday",
        									"@value" => isset ( $birthday) ? $birthday: ''
        							],
        							[
        									"@key" => "goal",
        									"@value" => isset ( $data['User']['goal']) ? $data['User']['goal']: ''
        							],
        							[
        									"@key" => "quote",
        									"@value" => isset ( $data['User']['quote']) ? $data['User']['quote']: ''
        							],
        							[
        									"@key" => "token",
        									"@value" => isset($token)?$token:''
        							]
        					]
        			)
        	);
        	
        	$this->log ( "Param Update user:" . json_encode ( $param ), 'debug' );
        	$param = json_encode ( $param, true );
        	$return = $this->Common->update_user( $username, $param );
        	$this->log ( "Return API:" . json_encode ( $return), 'debug' );
        	return $this->redirect(array('action' => 'view/'.$username));
        	}else {
        		return $this->redirect(array('action' => 'index'));
        		$this->Flash->set('update fail');
        	}
        } else {
//             $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
        $this->set('page_header', 'Edit user');
    }

    public function changestatus($user_id=null, $status=null) {
        if($this->request->isAjax()) {
            if ($this->request->is('post')) {
                $this->User->id = $user_id;
                if ($this->User->exists()) {
                    $data_update = ['account_status' => $status];
                    if ($this->User->save($data_update)) {
                        exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                    }
                }
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not change status item',)));  
    }

    public function remove() 
    {
        $id = $this->request->data['id'];
        if (is_array($id) && !empty($id)) {
            foreach ($id as $item) {
                $this->User->delete($id);
            }
        } else {
            $this->User->delete($id);
        }
        exit(json_encode(array('status' => true, 'message' => 'Successfully',)));
    }
}

?>