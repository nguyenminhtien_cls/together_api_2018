<?php  
App::uses('AppController', 'Controller');
Class PrivacyController extends AppController{
	public $uses = array(
		'Privacy',
	);
	public $helpers = array (
		'Paginator',
		'Html',
		'Form',
		'Session' 
	);
	public $components = array (
		'Session' 
	);
	public function index(){
		$page_header = "Privacy";
		$data = $this->Privacy->find("all");
		$this->set('data', $data);
	}
	public function add(){
		if($this->request->is('post')){
			// pr($this->request->data);die;
			$this->Privacy->set($this->request->data);
			
			$this->Privacy->saveField('update_time', date("Y-m-d H:i:s"));		
			if($this->Privacy->save($this->request->data)){
				$this->Session->setFlash(__('Success'),'default',array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash("Fail");
				
			}
		
			$this->redirect(array('action'=>'index'));			
		}
	}
	public function edit($id = null){	
		if(empty($this->data)) {
    		$this->data = $this->Privacy->read(null, $id);//Lấy thông tin 
		}
		else{
			$this->Privacy->id = $id;
			$this->Privacy->set($this->data);
			$this->Privacy->saveField('update_time', date("Y-m-d H:i:s"));
			$this->Privacy->save($this->data);
			$this->Session->setFlash("Cập nhật thành công");
     		$this->redirect("index"); //chuyển đến index
 
		}		
	}

	public function delete($id = null){
		if (isset($id) && !empty($id)){
			$this->Privacy->delete($id);
		}
		$this->redirect(array('action'=>'index'));
	}
	}
?>