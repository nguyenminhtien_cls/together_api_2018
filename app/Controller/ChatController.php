<?php
App::uses('AppController','Controller' );
App::uses('Xmpp', 'Lib');
/**
 * Chat Controller
 */
class ChatController extends AppController {
	public $components = array (
			'Paginator' 
	);
	public $uses = array (
			'User',
			'Conversation',
			'Message' 
	);
	public function conversation() {
		$this->set ( 'title_for_layout', 'Conversation' );
	}
	public function index() {
		$this->set ( 'title_for_layout', 'Chat log' );
		$filter = $this->request->query ( 'filter' );
		$this->Message->virtualFields ['msg_num'] = 'COUNT(Message.id)';
		
		$this->Message->unbindModel ( array (
				'belongsTo' => array (
						'User',
						'Conversation' 
				) 
		) );
		
		$conversation_url = '';
		$conditions = array (
				'UserFrom.name IS NOT NULL',
				'UserTo.name IS NOT NULL',
				'Message.content IS NOT NULL' 
		);
		$conditions ['Message.type ='] = MESS_TYPE;
		if (isset ( $filter ['datetime'] )) {
			if (isset ( $filter ['datetime'] ['date_from'] ) && isset ( $filter ['datetime'] ['time_from'] ) && (! empty ( $filter ['datetime'] ['date_from'] ))) {
				$conditions ['Message.created_date >='] = date ( 'Y-m-d H:i:s', strtotime ( $filter ['datetime'] ['date_from'] . ' ' . $filter ['datetime'] ['time_from'] ) );
				$conversation_url .= '&filter[datetime][date_from]=' . $filter ['datetime'] ['date_from'] . '&filter[datetime][time_from]=' . $filter ['datetime'] ['time_from'];
			}
			if (isset ( $filter ['datetime'] ['date_to'] ) && isset ( $filter ['datetime'] ['time_to'] ) && (! empty ( $filter ['datetime'] ['date_to'] ))) {
				$conditions ['Message.created_date <='] = date ( 'Y-m-d H:i:s', strtotime ( $filter ['datetime'] ['date_to'] . ' ' . $filter ['datetime'] ['time_to'] ) );
				$conversation_url .= '&filter[datetime][date_to]=' . $filter ['datetime'] ['date_to'] . '&filter[datetime][time_to]=' . $filter ['datetime'] ['time_to'];
			}
		}
		
		if (isset ( $filter ['name'] ) && ! empty ( $filter ['name'] )) {
			$conditions ['OR'] = array (
					array (
							'UserFrom.name LIKE' => '%' . trim ( $filter ['name'] ) . '%' 
					),
					array (
							'UserTo.name LIKE' => '%' . trim ( $filter ['name'] ) . '%' 
					) 
			);
			$filter ['user_id'] = 0;
		}
		
		$having_conditions = '';
		if (isset ( $filter ['msg_num'] )) {
			if (isset ( $filter ['msg_num'] ['from'] ) && ($filter ['msg_num'] ['from'] > 0)) {
				$having_conditions .= ' AND message__msg_num >= ' . $filter ['msg_num'] ['from'];
			}
			if (isset ( $filter ['msg_num'] ['to'] ) && ($filter ['msg_num'] ['to'] > 0)) {
				$having_conditions .= ' AND message__msg_num <= ' . $filter ['msg_num'] ['to'];
			}
		}
		
		$joins = array (
				array (
						'table' => 'users',
						'alias' => 'UserFrom',
						'type' => 'LEFT',
						'conditions' => array (
								'Message.from_user = UserFrom.id' 
						) 
				),
				array (
						'table' => 'users',
						'alias' => 'UserTo',
						'type' => 'LEFT',
						'conditions' => array (
								'Message.to_user = UserTo.id' 
						) 
				) 
		);
		
		$this->paginate = array (
				'fields' => '*',
				'conditions' => $conditions,
				'joins' => $joins,
				'recursive' => 1,
				'limit' => 20,
				'group' => 'Message.conversation_id HAVING 1=1 ' . $having_conditions,
				'order' => 'Message.created_date DESC',
				'paramType' => 'querystring' 
		);
		
		$data = $this->Paginator->paginate ( 'Message' );
		
		try {
			$this->set ( 'data', $data );
			$this->set ( 'filter', $filter );
			$this->set ( 'conversation_url', $conversation_url );
		} catch ( NotFoundException $e ) {
			throw new NotFoundException ( 'Could not find that conversation !' );
		}
	}
	public function detail() {
		$trans = Configure::read ( 'setting.masterdata_words' );
		foreach ( $trans as $item ) {
			$list_trans [$item] = "***";
			$list_trans [strtoupper ( $item )] = "***";
		}
		$this->set ( 'trans', $list_trans );
		$id = ( int ) $this->request->query ( 'id' );
		
		$this->set ( 'title_for_layout', 'ç®¡ç�†ãƒ¦ãƒ¼ã‚¶ãƒ¼ä¸€è¦§' );
		$filter = $this->request->query ( 'filter' );
		
		// get conversation detail
		unset ( $this->Conversation->UserFrom->virtualFields ['age'] );
		unset ( $this->Conversation->UserTo->virtualFields ['age'] );
		$conversation = $this->Conversation->findById ( $id );
		$this->set ( 'conversation', $conversation );
		
		if (isset ( $filter ['datetime'] )) {
			if (isset ( $filter ['datetime'] ['date_from'] ) && isset ( $filter ['datetime'] ['time_from'] ) && (! empty ( $filter ['datetime'] ['date_from'] ))) {
				$conditions ['Message.created_datetime >='] = strtotime ( $filter ['datetime'] ['date_from'] . ' ' . $filter ['datetime'] ['time_from'] );
			}
			if (isset ( $filter ['datetime'] ['date_to'] ) && isset ( $filter ['datetime'] ['time_to'] ) && (! empty ( $filter ['datetime'] ['date_to'] ))) {
				$conditions ['Message.created_datetime <='] = strtotime ( $filter ['datetime'] ['date_to'] . ' ' . $filter ['datetime'] ['time_to'] );
			}
		}
		
		$conditions ['Message.conversation_id'] = $id;
		
		// get all messages
		$this->Message->unbindModel ( array (
				'belongsTo' => array (
						'Conversation',
						'User' 
				) 
		) );
		$this->paginate = array (
				'fields' => '*',
				'conditions' => $conditions,
				'recursive' => 1,
				'limit' => 20,
				'order' => 'Message.updated_date DESC',
				'paramType' => 'querystring' 
		);
		$messages = $this->Paginator->paginate ( 'Message' );
		
		// get summary
		$this->Message->virtualFields ['msg_num'] = 'COUNT(Message.id)';
		$this->Message->virtualFields ['image_num'] = 'SUM(if(Message.image_id IS NOT NULL, 1, 0))';
		$this->Message->virtualFields ['present_num'] = 'SUM(Message.present_point)';
		$this->Message->virtualFields ['restrict_word_num'] = 'SUM(Message.restrict_word_num)';
		
		$conversation_summary = $this->Message->find ( 'first', array (
				'conditions' => $conditions,
				'recursive' => - 1,
				'order' => 'Message.created_datetime DESC' 
		) );
		
		$this->set ( 'id', $id );
		$this->set ( 'filter', $filter );
		$this->set ( 'conversation', $conversation );
		$this->set ( 'conversation_summary', $conversation_summary );
		$this->set ( 'messages', $messages );
	}
	public function delete_message() {
		if ($this->request->allowMethod ( 'post' )) {
			$id = ( int ) $this->request->data ( 'id' );
			try {
				// find conversation contain this message
				$this->Message->unbindModel ( array (
						'belongsTo' => array (
								'User',
								'Image',
								'Stamp' 
						) 
				) );
				$mess = $this->Message->findById ( $id );
				if ($mess) {
					// delete mess in db
					$this->Message->delete ( $id );
					if ($mess ['Message'] ['image_id'] > 0) // delete image
{
						$this->Image->delete ( $mess ['Message'] ['image_id'] );
					}
					
					// send notification to user
					$admin_id = Configure::read ( 'admin_id' );
					$admin = $this->User->findById ( $admin_id );
					if ($admin) {
						$XMPP = new Xmpp ( $admin_id, $admin ['User'] ['plainPassword'] );
						$XMPP->connect ();
						$XMPP->send_message ( $mess ['Conversation'] ['user_id_1'], 'delete_message', '{"conversation_id":' . $mess ['Conversation'] ['id'] . ',"message_id":' . $id . '}' );
						$XMPP->send_message ( $mess ['Conversation'] ['user_id_2'], 'delete_message', '{"conversation_id":' . $mess ['Conversation'] ['id'] . ',"message_id":' . $id . '}' );
						$XMPP->disconnect ();
						
						$this->responseApi ( 'OK', 'Remove successful!' );
					} else {
						$this->responseApi ( 'ERROR', 'Error find admin user to send messages to user' );
					}
				} else {
					$this->responseApi ( 'ERROR', 'Not found that message' );
				}
			} catch ( Exception $e ) {
				$this->responseApi ( 'ERROR', $e->getMessage () );
			}
		}
	}
	public function delete_conversation() {
		if ($this->request->allowMethod ( 'post' )) {
			$ids = ( array ) $this->request->data ( 'id' );
			$this->Conversation->unbindModel ( array (
					'belongsTo' => array (
							'UserFrom',
							'UserTo' 
					) 
			) );
			$conversations = $this->Conversation->find ( 'all', array (
					'conditions' => array (
							'Conversation.id' => $ids 
					) 
			) );
			if ($conversations) {
				$this->Conversation->deleteAll ( array (
						'Conversation.id' => $ids 
				) ); // delete conversations
				$this->Message->deleteAll ( array (
						'Message.conversation_id' => $ids 
				) ); // delete all message
				                                                                     
				// send notification to user
				$admin_id = Configure::read ( 'admin_id' );
				$admin = $this->User->findById ( $admin_id );
				if ($admin) {
					$XMPP = new Xmpp ( $admin_id, $admin ['User'] ['plainPassword'] );
					$XMPP->connect ();
					foreach ( $conversations as $conversation ) {
						$XMPP->send_message ( $conversation ['Conversation'] ['user_id_1'], 'delete_conversation', '{"conversation_id":' . $conversation ['Conversation'] ['id'] . '}' );
						$XMPP->send_message ( $conversation ['Conversation'] ['user_id_2'], 'delete_conversation', '{"conversation_id":' . $conversation ['Conversation'] ['id'] . '}' );
					}
					$XMPP->disconnect ();
					
					$this->responseApi ( 'OK', 'Remove conversation successful!', array (
							'ids' => $ids 
					) );
				} else {
					$this->responseApi ( 'ERROR', 'Error find admin user to send messages to user' );
				}
			} else {
				$this->responseApi ( 'ERROR', 'Not found any conversation' );
			}
		}
	}
}