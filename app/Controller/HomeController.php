<?php
App::uses('AppController', 'Controller');

/**
 * Home Controller
 *
 */
class HomeController extends AppController {
	public $uses = array();

    public function index() {
    	$this->set('title_for_layout', 'welcome together admin');
    	$this->set('page_header', 'Dashboard');
	}

}
