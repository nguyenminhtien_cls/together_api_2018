<?php
App::uses('AppController', 'Controller');

class ForgotPasswordController extends AppController
{
	public $helpers = array('Html', 'Form', 'Paginator');
	public $components = array('Paginator','User','Common');
	public function beforeFilter(){
		$this->Auth->allow('index');
		$this->Auth->allow('changepass');
	}
	
	public function index()
	{
		$this->layout = false;
		$email = $this->request->query('email');
		$code = $this->request->query('code');
		$arr = array(
			'email' => $email,
			'code' => $code
		);
		$this->Session->write('Reset', $arr);
	}


	public function changepass(){
		$this->layout = false;
		$param = $this->Session->read('Reset');
		$data = $this->request->data;
		$new_pass = $data['newpass'];
		$this->log($new_pass, 'debug');
		$email = isset($param['email'])?$param['email']:'';
		$username = $this->Common->format_email($email);

		$link_check = OPENSMART_USER_API.'?page=0&username='.$username;
		$data_check = $this->Common->getdata_openfire($link_check);
		$data_check = $this->Common->conver_xml($data_check);
		
		if(empty($data_check['user']['properties']['property']))
		{
			pr("<div style=\"width:556px; margin:15em auto 0em;\"><img src=\"../../img/changepassfail.jpg\"></div>");
			die();
		}
		else
		{
			foreach($data_check['user']['properties']['property'] as $value)
			{
				if($value['name'] == 'token_reset')
					$token_reset = $value['value'];
			}
		}

		if(isset($token_reset)&&($param['code']==(isset($token_reset)?$token_reset:'')))
		{
			$param_openfire = array(
				'email' => $email,
				'username' => $username,
				'name' => isset($data_check['user']['name'])?$data_check['user']['name']:'',
				'password' => $new_pass
			);
			     $salt = Configure::read('Security.salt');
			    $param_openfire['properties']['property'] = array(
			        [
			            'name' => 'pass_word',
			            'value' => md5($salt.$new_pass)
			        ],
			        [
			            'name' => 'token_reset',
			            'value' => ''
			        ]
			    );
			    
			    $link_update = OPENSMART_USER_API.'/updateProfile/'.$username;
			    $userEntity = json_encode($param_openfire, true);
			    $ret = $this->Common->putdata_openfire($link_update, $userEntity);
			 
			if($ret == 200)
			{
			    pr("<div style=\"width:556px; margin:15em auto 0em;\"><img src=\"../../img/changepasssuccess.jpg\"></div>");
				die();
			}
			else
			{
			    pr("<div style=\"width:556px; margin:15em auto 0em;\"><img src=\"../../img/changepassfail.jpg\"></div>");
				die();
			}
		}
		else
		{
		    pr("<div style=\"width:556px; margin:15em auto 0em;\"><img src=\"../../img/changepassfail.jpg\"></div>");
			die();
		}
	}
}
