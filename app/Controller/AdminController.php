<?php
App::uses('AppController', 'Controller');
/**
 * Author: ndhung.dev@gmail.com
 * Date: 19/01/2017
 */
class AdminController extends AppController
{
	public $uses = array('Admin');
	public $helpers = array('Paginator', 'Html', 'Form');
	public $components = array('Upload', 'Session', 'Paginator');

	/**
	 * General setting
	 * @return [type] [description]
	 */
	public function index() {
        $this->paginate = array(
            'conditions' => ['type' => '0'],
            'order' => ['id' => 'desc'],
            'limit' => 15,
            'page' => 1,
        );
        $users = $this->paginate('User');
        $this->set('users', $users);
        $data = array(
            'page_header' => "Admins system",
        );
        $this->set($data);
    }

	/**
	 * Change password myaccount
	 * @return [type] [description]
	 */
	public function changepassword() {
        $data = array(
            'page_header' => "Changepassword myaccount",
        );
        $this->set($data);	
    }

	public function register($name = null, $password = null, $type=null)
	{
        $data_user = array(
            'password' => $password
        );
        if(!$type){
            $data_user['userlogin'] = $name;
        } else {
            $data_user['email'] = '';
        }
        if ($this->Admin->save($data_user)) {
            die('ok');
        }
        die('Invalid');
    }
	public function login() {
	}
}