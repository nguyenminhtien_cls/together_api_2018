<?php
/**
 * Author : ndhung.dev@gmail.com
 * Date : 12/02/2017
 */
App::uses('AppController', 'Controller');

class ToeicLevelController extends AppController
{
    public $uses = array('Word', 'WordToeicLevel', 'ToeicLevel');
    public $helpers = array('Html', 'Form', 'Paginator');
    public $components = array('Paginator');

    public function index()
    {
        $page_header = 'ToeicLevel';
        $title_for_layout = 'Toeic levels list';
        $filters = [];
        $conditions = [];
        if (isset($this->params->query['filters'])) {
            $filters = $this->params->query['filters'];
            if (trim($filters['keysearch']) != '') {
                $conditions[] = "ToeicLevel.title LIKE '%".trim($filters['keysearch'])."%'";
            }
        }
        $this->paginate = array(
            'fields' => '*',
            'order' => array(
                'level' => 'DESC'
            ),
            'conditions' => $conditions,
            'limit' => 30
        );
        $toeiclevels = $this->paginate('ToeicLevel');
        $data = compact('title_for_layout','page_header', 'filters', 'conditions', 'toeiclevels');
        $this->set($data);
        $this->render('/ToeicLevel/index');
    }

    public function words_in_toeiclevel ($toeiclevel_id = null) {
        $page_header = 'ToeicLevel';
        $title_for_layout = 'Words in toeic levels';
        $filters = [];
        $conditions['ToeicLevel.id'] = $toeiclevel_id;
        if (isset($this->params->query['filters'])) {
            $filters = $this->params->query['filters'];
            if (trim($filters['keysearch']) != '') {
                $conditions[] = "Word.word LIKE '%".trim($filters['keysearch'])."%'";
            }
        }
        $this->paginate = array(
            'fields' => 'WordToeicLevel.id, Word.*, ToeicLevel.title',
            'contain' => array(
                'Word',
                'ToeicLevel'
            ),
            'conditions' => $conditions,
            'order' => array('Word.id' => 'desc'),
            'limit' => 25
        );
        $words = $this->paginate('WordToeicLevel');
        $data = compact('title_for_layout','page_header','filters', 'conditions', 'filters', 'toeiclevel_id', 'words');
        $this->set($data);
        $this->render('/ToeicLevel/list_words');
    }

    /**
     * add new toeic levels
     */
    public function add()
    {
        $this->set('title_for_layout', 'Add level');
        $this->ToeicLevel->create();
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if ($this->ToeicLevel->save($data)) {
            }
            $this->Session->setFlash(__('Unable to add your toeic level.'));
        }
    }

    /**
     * update toeic levels use ajax
     */
    public function ajaxupdate($toeiclevel_id=null) {
        if ($this->request->isAjax()) {
            if ($this->request->is('post')) {
                $this->ToeicLevel->id = $toeiclevel_id;
                if ($this->ToeicLevel->exists()) {
                    $data_update = [
                        'level' => $this->request->data('dataPost')[0],
                        'title' => $this->request->data('dataPost')[1],
                        'point_from' => $this->request->data('dataPost')[2],
                        'point_to' => $this->request->data('dataPost')[3]
                    ]; 
                    try {
                        if ($this->ToeicLevel->save($data_update)) {
                            exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                        }
                    } catch (Exception $e) {
                        exit(json_encode(array('error' => 1, 'message' => 'Can not update item',)));  
                    }
                }
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not update item',)));  
    }

    /**
     *  Change status ToeicLevel 
     */
    public function changestatus($toeiclevel_id = null, $status = null) {
        if($this->request->isAjax()) {
            if ($this->request->is('post')) {
                $this->ToeicLevel->id = $toeiclevel_id;
                if ($this->ToeicLevel->exists()) {
                    $data_update = ['status' => $status];
                    if ($this->ToeicLevel->save($data_update)) {
                        exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                    }
                }
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not change status item',)));  
    }

    /**
     * Delete ToeicLevel
     */
    public function delete($toeiclevel_id = null)
    {
        if (!is_numeric($toeiclevel_id)) {
            die('Invalid');
        }
        if($this->request->isAjax()) {
            $this->ToeicLevel->id = $toeiclevel_id;
            if ($this->request->is('post')) {
                if ($this->ToeicLevel->exists()) {
                    if ($this->ToeicLevel->delete()) {
                        exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                    }
                }
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not delete item',))); 
    }

    /**
     * remove word out toeic levels
     */
    public function remove_word_out_toeiclevel($word_toeiclevel_id = null) {
        if (!is_numeric($word_toeiclevel_id)) {
            die('Invalid');
        }
        if($this->request->isAjax()) {
            if ($this->request->is('post')) {
                $this->WordToeicLevel->id = $word_toeiclevel_id;
                if ($this->WordToeicLevel->exists()) {
                    if ($this->WordToeicLevel->delete()) {
                        exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                    }
                }
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not delete item',)));
    }
}