<?php
/**
 * Created by PhpStorm.
 * User: PhanMinh
 * Date: 7/29/2016
 * Time: 11:43 AM
 */
App::uses('AppController', 'Controller');

class WordsController extends AppController
{
    public $uses = array('Word', 'WordCategory', 'WordCollection');
    public $helpers = array('Html', 'Form', 'Paginator');
    public $components = array('Paginator');

    public function index()
    {
        $page_header = 'Words';
        $title_for_layout = 'Words';
        $filters = [];
        $conditions = [];
        if (isset($this->params->query['filters'])) {
            $filters = $this->params->query['filters'];
            if (trim($filters['keysearch']) != '') {
                $conditions[] = "Word.word LIKE '%".trim($filters['keysearch'])."%'";
            }
        }
        $this->paginate = array(
            'fields' => '*',
            'conditions' => $conditions,
            'order' => array(
                'created_datetime' => 'DESC'
            ),
            'limit' => 15,
            'page' => 1,
        );
        $words = $this->paginate('Word');
        $data = compact('title_for_layout','page_header','filters', 'conditions', 'filters', 'words');
        $this->set($data);
    }

    public function view($id = null)
    {
        $this->set('title_for_layout', 'Word details');
        if (!$id) {
            throw new NotFoundException(__('Invalid word'));
        }

        $word = $this->Word->findById($id);
        if (!$word) {
            throw new NotFoundException(__('Invalid word'));
        }
        $this->set('word', $word);
    }

    public function add()
    {
        $this->set('title_for_layout', 'Add word');
        $this->Word->create();
        if ($this->request->is('post')) {
            if ($this->Word->save($this->request->data)) {
                $this->Session->setFlash(__('Your word has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Unable to add your word.'));
        }
    }

    public function edit($id = null)
    {
        $this->set('title_for_layout', 'Edit word');
        $word = $this->Word->findById($id);
        if (!$word) {
            throw new Exception("Error Processing Request", 1);
        }
        $this->set('w', $word['Word']['word']);
        $word = json_decode($word['Word']['meaning_en'], true);
        $this->set('wType', (isset($word['type']) ? $word['type'] : []));
        $this->Word->id = $id;
        if (!$this->Word->exists()) {
            throw new NotFoundException(__('Invalid word existed!'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Word->save($this->request->data)) { //the data will be executed in Model/Word/beforeSave()
                $this->Session->setFlash(__('Your word has been updated'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The word could not be saved. Please, try again.')
            );
        } else {
            $this->request->data = $this->Word->findById($id);
        }
    }

    public function edit_raw($id = null) {
        $this->set('title_for_layout', 'Edit word(raw)');
        $word = $this->Word->findById($id);
        $this->set('word', $word);
        $this->Word->id = $id;
        if (!$this->Word->exists()) {
            throw new NotFoundException(__('Invalid word dont exist!'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Word->save($this->request->data)) {
                $this->Session->setFlash(__('Your word has been updated'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The word could not be saved. Please, try again.')
            );
        }
    }

    /**
     * Delete word
     */
    public function delete($word_id = null)
    {
        if (!is_numeric($word_id)) {
            die('Invalid');
        }
        if($this->request->isAjax()) {
            $this->Word->id = $word_id;
            if ($this->request->is('post')) {
                if ($this->Word->exists()) {
                    if ($this->Word->delete()) {
                        exit(json_encode(array('error' => 0, 'message' => 'Successfully')));
                    }
                }
            }
        }
        exit(json_encode(array('error' => 1, 'message' => 'Can not delete item',))); 
    }
}