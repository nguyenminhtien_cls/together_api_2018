<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><span> Community</span></li>
      <li><a href="users/"> Members</a></li>
      <li><span style="font-style: italic; font-weight: bold"> User profile</span></li>
    </ul>
</div>
<div class="box">
    <div class="box-header" style="background:#ffffff">
        <a href="/users" class="pull-left btn btn-warning btn-sm" style="margin:10px;"> Back</a>
        <a href="/users/edit/<?=$user['username']?>" class="pull-left btn btn-success btn-sm" style="margin:10px;"> EDIT USER</a>
    </div>
    <?php 
        if ($flash = $this->Session->flash()): 
    ?>
    <div style="padding: 10px 10px; background: #699673; z-index: 999999; font-size: 16px; font-weight: 400; color:white;"><?=$flash;?></div>
    <?php 
        endif; 
    ?>
    <hr/>
    <div class="box-content">
        <form role="form" class="form-horizontal" method="POST">
            <div class="row" style="text-align: center">
                <div class="col-lg-2 col-md-4 col-sm-4 col-xs-4 ">
                    <img src="#" height="250"
                         width="250" class="user-profile-image-display"/>
                </div>
                <div class="col-lg-8 col-md-8  col-sm-8 col-xs-8" style="text-align: left">
                    <h1 class="blue bold"><?= !empty($user['name']) ? $user['name'] : 'untitle' ?></h1>
                    <h3 class="red"> <?= (isset($user['type']) ? ($user['type'] == 0 ? 'Administrator' : 'Unsorted User') : '') ?> </h3>
                </div>
            </div>
            <hr style="border: solid #000 1px">
            <hr style="border: solid #000 1px">

            <table class="table">
                <tr>
                    <td class="table_left">Username</td>
                    <td><?= !empty($user['username']) ? $user['username'] : 'Untitled' ?></td>
                    <td class="table_left">Email</td>
                    <td><?= !empty($user['email']) ? $user['email'] : '' ?></td>
                </tr>
                <tr>
                    <td class="table_left">Name</td>
                    <td><?= !empty($user['name']) ? $user['name'] : 'Untitled' ?></td>
                    <td class="table_left">Sex</td>
                    <td>
                        <?= (isset($user['gender']) ? ($user['gender'] != 0) ? 'Female' : 'Male' : 'Unknow') ?>
                    </td>
                </tr>
                <tr>
                    <td class="table_left">Team</td>
                    <td>
                        <?php echo isset($user['team_id'])?$user['team_id']:'0';?>
                    </td>
                    <td class="table_left">Device type:</td>
                    <td>
                        <?= (!empty($user['device_type']) ? $user['device_type'] : "Unknow") ?>
                    </td>
                </tr>
                <tr>
                    <td class="table_left">Nickname</td>
                    <td><?= isset($user['nick_name'])?$user['nick_name']:''?></td>
                    <td class="table_left">Birthday</td>
                    <td>
                        <?= isset($user['birthday'])?$user['birthday']:''?>
                    </td>
                </tr>
                <tr>
                    <td class="table_left">Quote</td>
                    <td><?= isset($user['quote'])?$user['quote']:''?></td>
                    <td class="table_left">Goal</td>
                    <td>
                        <?= isset($user['goal'])?$user['goal']:''?>
                    </td>
                </tr>
                <tr>
                    <td class="table_left">Toeic level(id)</td>
                    <td><?= isset($user['toeic_level_id'])?$user['toeic_level_id']:''?></td>
                    <td class="table_left">Claps</td>
                    <td>
                        <?= isset($user['claps'])?$user['claps']:''?>
                    </td>
                </tr>
                <tr>
                    <td class="table_left">Created</td>
                    <td><?= date("d-m-Y",$user['created_datetime']/1000) ?></td>
                    <td class="table_left">Updated</td>
                    <td>
                        <?= date("d-m-Y",$user['created_datetime']/1000) ?>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<style>
    table.table {
        border: 1px #999 solid;
        width: 100%;
        margin-bottom: 25px !important;
        padding: 7px;
    }

    table td, table th {
        border-top: 1px #999 solid !important;
        border-bottom: 1px #999 solid !important;
        width: 30%;
    }

    table .table_left {
        background: #eee;
        width: 20%;
    }

    .table_like .table_left {
        width: 7%;
    }

    .box-content {
        float: left;
        width: 100%;
    }
</style>