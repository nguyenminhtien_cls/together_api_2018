<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><a href="games">Games</a></li>
      <li><span style="font-style: italic; font-weight: bold"> Schedure</span></li>
    </ul>
</div>
<div class="box">
    <div class="box" style="margin-bottom:1px;">
    <div class="box-header" style="background:#ffffff">
        <a href="/users" class="pull-left btn btn-success btn-sm" style="margin:10px;"><i class="fa fa-list"></i> USERS LIST</a>
    </div>
    <?php 
        if ($flash = $this->Session->flash()): 
    ?>
    <div style="padding: 10px 10px; background: #f76767; z-index: 999999; font-size: 16px; font-weight: 400; color:white;"><?=$flash;?></div>
    <?php 
        endif; 
    ?>
    <hr/>
     <div class="box-content">
        <?php echo $this->Form->create('User', array(
            'class' => 'form-horizontal ',
            'enctype' => 'multipart/form-data',
            'inputDefaults' => array(
                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                'div' => array('class' => 'from-group'),
                'class' => array('form-control'),
                'label' => array('class' => 'control-label'),
                'between' => '<div class="controls">',
                'after' => '</div>',
                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
            ))); ?>
        <fieldset>
            <div class="col-sm-6">
                <?php
                    echo $this->Form->input('username', array('type' => 'text', "autocomplete"=>"off"));
                    echo $this->Form->input('password', array('type' => 'password'));
                    echo $this->Form->input('token', array('type' => 'text'));
                    echo $this->Form->input('name', array('type' => 'text'));
                    echo $this->Form->input('birthday', array('type' => 'date', 'class' => 'datepicker', 'label' => 'Date of Birth',
                        'empty' => TRUE, 'minYear' => '1900'));
                    echo '<div style="clear:both;"></div>';
                ?>
                <div class="form-group">
                    <div class="media-body">
                        <label for="image-avatar"> Avatar
                            <input type="file" accept="image/*" class="form-control" name="data[avatar]"
                                   id="image-avatar"/>
                        </label>
                        <span class="help-block">Accept file type GIF, JPG, PNG.</span>
                        <div class="text-danger error-image"></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <?php
                echo $this->Form->radio('gender', ['0' => 'Male', '1' => 'Female'], ['separator' => '&nbsp;&nbsp;&nbsp;&nbsp;', 'legend' => '<span style="font-size: 14px; padding-top: 7px; font-weight:bold;">Genre</span>', 'default' => 0]);
                echo $this->Form->input('quote', array('type' => 'text',));
                echo $this->Form->input('goal', array('type' => 'text'));
                echo $this->Form->input('toeic_level_id', array('type' => 'text'));
                echo $this->Form->input('team_id', array(
                    'empty' => true, 'options' => $teams
                ));
                echo $this->Form->radio('account_status', ['1' => 'Activated', '0' => 'Deactivated'], ['separator' => '&nbsp;&nbsp;&nbsp;&nbsp;', 'legend' => '<span style="font-size: 14px; padding-top: 7px; font-weight:bold;">Status</span>', 'default' => 1]);
                ?>
            </div>
            </br>
        </fieldset>
        <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
        <?= $this->Form->button('Created', array('class' => 'btn btn-primary')); ?>
        <?= $this->Form->end() ?>
    </div>
</div>

