<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><span> Community</span></li>
      <li><a href="users/"> Members</a></li>
      <li><span style="font-style: italic; font-weight: bold"> Edit User</span></li>
    </ul>
</div>
<div class="box">
    <div class="box" style="margin-bottom:1px;">
    <div class="box-header" style="background:#ffffff">
        <a href="users/" class="pull-left btn btn-back btn-sm" style="margin:10px;"></i> Back</a>
    </div>
    <hr/>

    <div class="box-content">
        <?php echo $this->Form->create('User', array(
        'class' => 'form-horizontal ',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'control-label'),
            'between' => '<div class="controls">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <div class="col-sm-6">
            <?php
                echo $this->Form->input('username', array(
                		'type' => 'text',
                		'default' => isset ( $array["username"] ) ? $array["username"]: '',
                		'disabled' => 'disabled',
                ));
                echo $this->Form->input('name', array('type' => 'text', 
                		'default' => isset ( $array["name"] ) ? $array["name"]: ''));
        
                echo $this->Form->input('birthday', array('type' => 'date', 'class' => 'datepicker', 'label' => 'Date of Birth',
                    'empty' => TRUE, 'minYear' => '1900'));
                echo '<div style="clear:both;"></div>';
            ?>
            <!-- </div> -->
        </div>
        <div class="col-sm-6">
            <?php
                echo $this->Form->input('email', array(
                		'type' => 'email',
                		'default' => isset ( $array["email"] ) ? $array["email"]: '',
                		'disabled' => 'disabled',
                ));
                echo $this->Form->input('nickname', array('type' => 'text',
                		'default' => isset ( $array["nick_name"] ) ? $array["nick_name"]: ''));
                echo $this->Form->input('quote', array('type' => 'text',
                		'default' => isset ( $array["quote"] ) ? $array["quote"]: ''));
                echo $this->Form->radio('gender', ['0' => 'Male', '1' => 'Female'], $attributes=['separator' => '&nbsp;&nbsp;&nbsp;&nbsp;', 'legend' => '<span style="font-size: 14px; padding-top: 7px; font-weight:bold;">Genre</span>']);
                echo $this->Form->input('goal', array('type' => 'text',
                		'default' => isset ( $array["goal"] ) ? $array["goal"]: ''));
                echo $this->Form->input('toeic_level_id', array('type' => 'text',
                		'default' => isset ( $array["toeic_level_id"] ) ? $array["toeic_level_id"]: ''));
//                 echo $this->Form->input('team_id', array(
//                     'empty' => true, 'options' => $teams
//                 ));
                echo $this->Form->radio('account_status', ['0' => 'Activated', '1' => 'Deactivated'], $attributes=['separator' => '&nbsp;&nbsp;&nbsp;&nbsp;', 'legend' => '<span style="font-size: 14px; padding-top: 7px; font-weight:bold;">Status</span>']);
            ?>
        </div>
        <br>
    </fieldset>
    <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Update', array('class' => 'btn btn-primary')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>
