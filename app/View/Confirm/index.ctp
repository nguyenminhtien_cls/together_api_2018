<link href="assets/css/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<?php if ($inactive) { ?>
    <div class="container">
        <h2 style="text-align: center">This link invalid or overtime to confirm</h2>
    </div>
<?php } else { ?>
    <div class="container">
        <h2 style="text-align: center">Reset password</h2>
        <p></p>
        <form method="POST" id="myForm">
            <input type="hidden" class="form-control" id="usr" name="email" value="<?php echo $email ?>">
            <div class="form-group">
                <label for="usr">Password:</label>
                <input type="text" class="form-control" id="usr" name="password">
            </div>
            <div class="form-group">
                <label for="pwd">Retype Password:</label>
                <input type="password" class="form-control" id="pwd" name="re_password">
            </div>
            <div class="form-group">
                <button class="btn btn-success">Submit</button>
            </div>
        </form>
    </div>
<?php }
?>

<script>
    $("#myForm").submit(function (e) {
        var url = "/confirm/index"; // the script where you handle the form input.
        $.ajax({
            type: "POST",
            url: url,
            data: $("#myForm").serialize(), // serializes the form's elements.
            success: function (data)
            {
                alert(data);
            }
        });
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>
