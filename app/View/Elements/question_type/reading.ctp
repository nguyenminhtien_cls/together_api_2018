<div>
	<div class="from-group">
		<label for="QuestionStatement">Document</label>
		<textarea name="data[Question][document]" class="form-control" id="document" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 100px;"></textarea>
	</div>
	<div class="from-group">
		<label for="QuestionStatement">Statement one</label><input
		name="data[Question][statementone]" class="form-control" maxlength="255" type="text" id="QuestionStatement1">
	</div>
	<table class="table table-striped table-bordered table-user-list">
		<thead>
			<tr>
				<th class="sorting">statement</th>
				<th class="sorting" style="width: 10%;">style</th>
				<th style="width: 2%"><center>isCorrect</center></th>
			</tr>
		</thead>
		<tbody>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_one][statement][1]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_one][listing_style][1]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td><center>
						<input type="checkbox" name="data[QuestionChoice_one][is_correct][1]">
					</center></td>
			</tr>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_one][statement][2]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_one][listing_style][2]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td><center>
						<input type="checkbox" name="data[QuestionChoice_one][is_correct][2]">
					</center></td>
			</tr>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_one][statement][3]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_one][listing_style][3]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td>
					<center>
						<input type="checkbox" name="data[QuestionChoice_one][is_correct][3]">
					</center>
				</td>
			</tr>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_one][statement][4]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_one][listing_style][4]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td><center>
						<input type="checkbox" name="data[QuestionChoice_one][is_correct][4]">
					</center></td>
			</tr>
		</tbody>
	</table>
	<!-- <button id="btn_add" class="btn btn-warning btn-sm add-row"
		type="button">add</button> -->
	<div style="color: red;" id="choice_"></div>
</div>
<div>
	<div class="from-group">
		<label for="QuestionStatement">Statement two</label><input
		name="data[Question][statementtwo]" class="form-control" maxlength="255" type="text" id="QuestionStatement2">
	</div>
	<table class="table table-striped table-bordered table-user-list">
		<thead>
			<tr>
				<th class="sorting">statement</th>
				<th class="sorting" style="width: 10%;">style</th>
				<th style="width: 2%"><center>isCorrect</center></th>
			</tr>
		</thead>
		<tbody>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_two][statement][1]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_two][listing_style][1]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td><center>
						<input type="checkbox" name="data[QuestionChoice_two][is_correct][1]">
					</center></td>
			</tr>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_two][statement][2]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_two][listing_style][2]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td><center>
						<input type="checkbox" name="data[QuestionChoice_two][is_correct][2]">
					</center></td>
			</tr>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_two][statement][3]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_two][listing_style][3]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td>
					<center>
						<input type="checkbox" name="data[QuestionChoice_two][is_correct][3]">
					</center>
				</td>
			</tr>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_two][statement][4]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_two][listing_style][4]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td><center>
						<input type="checkbox" name="data[QuestionChoice_two][is_correct][4]">
					</center></td>
			</tr>
		</tbody>
	</table>
	<!-- <button id="btn_add" class="btn btn-warning btn-sm add-row"
		type="button">add</button> -->
	<div style="color: red;" id="choice_"></div>
</div>
<div>
	<div class="from-group">
		<label for="QuestionStatement">Statement three</label><input
		name="data[Question][statementthree]" class="form-control" maxlength="255" type="text" id="QuestionStatement3">
	</div>
	<table class="table table-striped table-bordered table-user-list">
		<thead>
			<tr>
				<th class="sorting">statement</th>
				<th class="sorting" style="width: 10%;">style</th>
				<th style="width: 2%"><center>isCorrect</center></th>
			</tr>
		</thead>
		<tbody>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_three][statement][1]"class="form-control validate_" type="text" id="QuestionChoiceStatement1">
				</div></td>
				<td><div class="from-group"><input name="data[QuestionChoice_three][listing_style][1]"class="form-control" type="text" id="QuestionChoiceListingStyle1">
				</div></td>
				<td><center><input type="checkbox" name="data[QuestionChoice_three][is_correct][1]">
				</center></td>
			</tr>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_three][statement][2]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_three][listing_style][2]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td><center>
						<input type="checkbox" name="data[QuestionChoice_three][is_correct][2]">
					</center></td>
			</tr>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_three][statement][3]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_three][listing_style][3]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td>
					<center>
						<input type="checkbox" name="data[QuestionChoice_three][is_correct][3]">
					</center>
				</td>
			</tr>
			<tr class="question">
				<td><div class="from-group">
						<input name="data[QuestionChoice_three][statement][4]"
							class="form-control validate_" type="text"
							id="QuestionChoiceStatement1">
					</div></td>
				<td><div class="from-group">
						<input name="data[QuestionChoice_three][listing_style][4]"
							class="form-control" type="text" id="QuestionChoiceListingStyle1">
					</div></td>
				<td><center>
						<input type="checkbox" name="data[QuestionChoice_three][is_correct][4]">
					</center></td>
			</tr>
		</tbody>
	</table>
	<!-- <button id="btn_add" class="btn btn-warning btn-sm add-row"
		type="button">add</button> -->
	<div style="color: red;" id="choice_"></div>
</div>