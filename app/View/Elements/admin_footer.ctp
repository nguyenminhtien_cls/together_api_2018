<footer class="row">
    <div class="col-sm-5">
        &copy; 2016 <?php echo Configure::read('project_title'); ?> <?php echo Configure::read('version'); ?>
    </div><!--/.col-->

    <div class="col-sm-7 text-right">
        Powered by: <a href="#">CodeLovers Vietnam</a>. CakePHP <?php echo Configure::version(); ?>
    </div><!--/.col-->
</footer>