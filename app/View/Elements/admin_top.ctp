<!-- start: Header -->
<header class="navbar" style="border-radius: 0px;">
	<div class="container">
		<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".sidebar-nav.nav-collapse">
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		</button>
		<a id="main-menu-toggle" class="hidden-xs open"><i class="fa fa-bars"></i> <?=(isset($page_header))? $page_header:'';?></a>
			<a class="navbar-brand col-lg-2 col-sm-1 col-xs-12" href="home/"><span><?php echo Configure::read('project_title'); ?></span></a>
		<!-- start: Header Menu -->
		<div class="nav-no-collapse header-nav">
			<ul class="nav navbar-nav pull-right">
				<!-- start: User Dropdown -->
				<li class="dropdown">
					<a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
						<div class="user">
							<span class="name"><i class="fa fa-user"></i> <?php echo AuthComponent::user('name') ?></span>
						</div>
					</a>
					<ul class="dropdown-menu pull-right">
						<li><a href="auth/logout"><i class="fa  fa-power-off"></i> Logout</a></li>
					</ul>
				</li>
				<!-- end: User Dropdown -->
			</ul>
		</div>
		<!-- end: Header Menu -->
	</div>
</header>
<!-- end: Header -->