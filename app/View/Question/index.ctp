<?php 
// echo $this->Html->css(array(
//     'bootstrap',
//     'list',
// ));

// echo $this->Html->script(array(
//     'jquery-3.1.1.min',
//     'bootstrap.min',
//     'img-animation',
//     'file-animation',
// ));
?>
<style type="text/css">

/*body{
    background: #ddd;
}*/

/*.container{
    margin-top: 40px;
    margin-bottom: 40px;
    background: #fff;
}*/
/*.menu-right{
    background: #222;
    margin: 0;
    padding: 0;
}*/
.list-content{
    background: #fff;
    padding: 10px;
}

.tab-content{

}
.tab-content h4{
    margin-top: 10px;
    margin-bottom: 10px;
    font-weight: bold;
}
.tab-content h3{
    padding-top: 20px;
    padding-bottom: 10px;
    font-weight: bold;
}
.cor-ans{
    color: #fff;
}
.sbm{
    margin-top: 20px;
    margin-bottom: 20px;
}
.image-preview-input {
    position: relative;
    overflow: hidden;
    margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
.file {
    visibility: hidden;
    position: absolute;
}
/*.row-eq-height {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    flex-wrap: wrap;222
}*/
.img-audio{
    width: 8em;
    height: auto;
}
.b-play{
    font-size: 4em;
}
.input-group{
    margin-top: 15px;
    margin-bottom: 15px;
}
</style>

<div class="box">
    <div class="box-header" style="background:#ffffff">
    </div>
    <div class="list-content ">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#part1">P1:Picture Description</a></li>
            <li><a href="Question/part2">P2:Question Response</a></li>
            <li><a href="Question/part3">P3:Conversation</a></li>
            <li><a href="Question/part4">P4:Short Talk</a></li>
            <li><a href="Question/part5">P5:Incomplete Sentences</a></li>
            <li><a href="Question/part6">P6:Text Completion</a></li>
            <li><a href="Question/part7">P7:Reading Comprehension</a></li>
        </ul>
        <div class="tab-content">
            <!-- Part 1 -->
            <div id="part1" class="tab-pane fade in active">
                <div class="row">
                    <?php echo $this->Form->create("search"); ?>
                    <div class="col-md-3  col-sm-3 col-xs-12" style="margin-top: 13px;">
                        <!-- <div class="btn-group" style="margin-top: 15px;" >
                          <button type="button" class="btn btn-primary">Draft</button>
                          <button type="button" class="btn btn-primary">Active</button>
                          <button type="button" class="btn btn-primary">Inactive</button>
                        </div> -->
                        <?php  
                            $options = array(
                                'draft' => 'Draft', 
                                'public' => 'Public',
                                'out of date' => 'Out of date', 
                                 
                            );
                            $attributes = array('legend' => false, 'style' => 'margin:7px;');
                            echo $this->Form->radio("stt", $options, $attributes);
                            ?>
                        
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12" style="margin-top: 13px;">
                        <!-- <form method="post" class="input-group add-on">
                            <div class="col-md-10">
                                <input class="form-control" name="search_input" class="srch-term" id="input" type="text" placeholder="Search..." >
                            </div>
                            <div class="input-group-btn col-md-2" >
                                <button class="btn btn-primary" type="submit">Search</button>
                            </div>
                        </form> -->
                        
                        <div class="col-md-10">
                            <!-- <input class="form-control" name="search_input" class="srch-term" id="input" type="text" placeholder="Search..." > -->
                            
                            <?php echo $this->Form->input("txt_search", array(
                                'label' => false,
                                'class' => 'form-control srch-term',
                                'placeholder' => 'Search . . .'
                                ));
                            ?>
                            
                        </div>
                        <div class="input-group-btn col-md-2">
                            <button class="btn btn-primary" type="submit">Search</button>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </div>
                    <?php echo $this->Form->end() ?>
                    <div class="col-md-2" style="padding-top: 13px;">
                        <a href="Question/add_p1" style="float: right; margin-right: 1.3em;"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                    </div>
                </div>
 
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center" width="5%">ID</th>
                            <th class="text-center" width="5%">AUDIO</th>
                            <th class="text-center" width="20%">IMAGE</th>
                            <th class="text-center" width="20%">CORRECT ANSWER</th>
                            <th class="text-center" width="40%">TAPE SCRIPT</th>
                            <th class="text-center" width="5%">STATUS</th>
                            <th class="text-center" width="5%">OPTION</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data as $item){ ?>
                        <tr>
                         <?php if($item['tg_question']['id_part']==1){ ?>
                            <?php if($item['tg_answer']['iscorrect']==1){ ?>
                            <!-- id -->
                            <td class="text-center"><?php echo $item['tg_question']['id_ques'] ?></td>
                            <!-- Audio -->
                            <td class="text-center" >
                                <audio src="<?php echo Configure::read('upload_question_bank_path'); ?>Audio/<?php echo $item['tg_audio']['audio'] ?>" id="audio"></audio>
                                <span class="glyphicon glyphicon-play-circle b-play btn-player"></span>
                            </td>
                            <!-- Image -->
                            <td class="text-center">
                                <a href="<?php echo Configure::read('upload_question_bank_path'); ?>img/<?php echo $item['tg_question']['images'] ?>" target="new">
                                    <img src="<?php echo Configure::read('upload_question_bank_path'); ?>img/<?php echo $item['tg_question']['images'] ?>" class="img-audio img-responsive" attributes="#" style= "margin: 0 auto">
                                </a>                               
                            </td>
                            <!-- Correct Answer -->
                            <td><?php echo $item['tg_answer']['answer'] ?></td>

                            <!-- Tape Script -->
                            <td><?php echo $item['tg_audio']['tape_script'] ?></td>
                            <!-- Status -->
                            <td class="text-center"><?php echo $item['tg_audio']['status'] ?></td>
                            <!--View Edit delete -->
                            <td class="text-center">
                                <!-- View -->
                                <a href="Question/view_p1/<?php echo $item['tg_question']['id_ques'] ?>"><span class="btn btn-success glyphicon glyphicon-eye-open"></span></a><br>
                                <!-- Edit -->
                                <a href="Question/edit_p1/<?php echo $item['tg_question']['id_ques'] ?>"><span class="btn btn-info glyphicon glyphicon-edit"></span></a><br>
                                <!-- Delete -->
                                <a onclick="return confirm('Bạn chắc chắn xóa?')" href="Question/delete/<?php echo $item['tg_question']['id_ques'] ?>"><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
                            </td>
                        </tr>
                        <?php }}} ?>
                        
                    </tbody>
                </table>
                <!-- <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#"><<</a></li>
                        <li><a href="#"><</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">></a></li>
                        <li><a href="#">>></a></li>
                    </ul> 
                </div> -->
                <div style="text-align: center">
                    <?php echo $this->element('pagination'); ?>
                </div>
            </div>

            <!-- Part 2  -->
            <div id="part2" class="tab-pane fade">
                <div class="row">                    
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <form method="post" class="input-group add-on">
                            <input class="form-control" name="search_input" class="srch-term" id="input" type="text" placeholder="Search...">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3  col-sm-3 col-xs-12">
                        <div class="btn-group" style="margin-top: 15px;" >
                          <button type="button" class="btn btn-primary">Draft</button>
                          <button type="button" class="btn btn-primary">Active</button>
                          <button type="button" class="btn btn-primary">Inactive</button>
                        </div> 
                    </div>
                    <div class="col-md-2 col-md-offset-2" style="padding-top: 13px;">
                        <a href="Question/add_p2"><button type="button" class="btn btn-primary">Add</button></a>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">AUDIO</th>
                            <th class="text-center">CORRECT ANSWER</th>
                            <!-- <th class="text-center">CORRECT ANSWER</th> -->
                            <th class="text-center" width="30%">TAPE SCRIPT</th>
                            <th class="text-center">OPTION</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data as $item){ ?>
                        <tr>
                        <?php if($item['tg_question']['id_part']==2){ ?>
                            <?php if($item['tg_answer']['iscorrect']==1){ ?>
                            <!-- id -->
                            <td class="text-center"><?php echo $item['tg_question']['id_ques'] ?></td>
                            <!-- Image -->
                           
                            <!-- Audio -->
                            <td class="text-center" >
                                <audio src="<?php echo $this->webroot; ?>Audio/<?php echo $item['tg_audio']['audio'] ?>" id="audio"></audio>
                                <span class="glyphicon glyphicon-play-circle b-play btn-player"></span>
                            </td>
                            <!-- Correct Answer -->
                            <td class="text-center"><?php echo $item['tg_answer']['answer'] ?></td>

                            <!-- Tape Script -->
                            <td class="text-center" ><?php echo $item['tg_audio']['tape_script'] ?></td>
                            <!-- View Edit delete -->
                            <td class="text-center" >
                                <!-- View -->
                                <a href="Question/view_p2/<?php echo $item['tg_question']['id_ques'] ?>"><span class="btn btn-success glyphicon glyphicon-eye-open"></span></a><br>
                                <!-- Edit -->
                                <a href="Question/edit_p2/<?php echo $item['tg_question']['id_ques'] ?>"><span class="btn btn-info glyphicon glyphicon-edit"></span></a><br>
                                <!-- Delete -->
                                <a onclick="return confirm('Bạn chắc chắn xóa?')" href="Question/delete/<?php echo $item['tg_question']['id_ques'] ?>"><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
                            </td>
                           
                        </tr>
                        <?php }}} ?>
                        
                    </tbody>
                </table>
                <!-- <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#"><<</a></li>
                        <li><a href="#"><</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">></a></li>
                        <li><a href="#">>></a></li>
                    </ul> 
                </div> -->
                <div style="text-align: center">
                    <?php echo $this->element('pagination'); ?>
                </div>
            </div>

            <!-- Part3 -->
            <div id="part3" class="tab-pane fade">
                <div class="row">                    
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <form method="post" class="input-group add-on">
                            <input class="form-control" name="search_input" class="srch-term" id="input" type="text" placeholder="Search...">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3  col-sm-3 col-xs-12">
                        <div class="btn-group" style="margin-top: 15px;" >
                          <button type="button" class="btn btn-primary">Draft</button>
                          <button type="button" class="btn btn-primary">Active</button>
                          <button type="button" class="btn btn-primary">Inactive</button>
                        </div> 
                    </div>
                    <div class="col-md-2 col-md-offset-2" style="padding-top: 13px;">
                        <a href="Question/add_p3"><button type="button" class="btn btn-primary">Add</button></a>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">AUDIO</th>
                            <th class="text-center">QUESTION & CORRECT ANSWER</th>
                            <!-- <th class="text-center">CORRECT ANSWER</th> -->
                            
                            <th class="text-center"  width="30%">TAPE SCRIPT</th>
                            <th class="text-center">OPTION</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data1 as $item){ 
                        if($item['tg_question']['id_part'] == 3){
                    ?>
                    
                        <tr>
                         
                            
                            <!-- id -->
                            <td class="text-center"><?php echo $item['tg_audio']['id_audio'] ?></td>
                            <!-- Image -->
                            
                            <!-- Audio -->
                            <td class="text-center" >
                                <audio src="<?php echo $this->webroot; ?>Audio/<?php echo $item['tg_audio']['audio'] ?>" id="audio"></audio>
                                <span class="glyphicon glyphicon-play-circle b-play btn-player"></span>
                            </td>

                            <td class="text-center" >
                                <table align="center" >
                                    <?php foreach($data as $item2){ ?>
                                    <?php if($item2['tg_answer']['iscorrect']==1){ ?>
                                    <?php if($item2['tg_question']['id_audio'] == $item['tg_audio']['id_audio']){?>
                                    <tr>
                                        <td style="padding-right: 2em;"><?php echo $item2['tg_question']['question']; ?></td>
                                        <td style="padding-left: 2em;"><?php echo $item2['tg_answer']['answer'] ?></td>
                                    </tr>
                                    <?php }}}?>
                                </table>
                            </td>
                            <!-- Correct Answer -->
                            

                            <!-- Tape Script -->
                            <td class="text-center" ><?php echo $item['tg_audio']['tape_script'] ?></td>
                            <!--View Edit delete -->
                            <td class="text-center" >
                                <!-- View -->
                                <a href="Question/view_p3/<?php echo $item['tg_audio']['id_audio'] ?>"><span class="btn btn-success glyphicon glyphicon-eye-open"></span></a><br>
                                <!-- Edit -->
                                <a href="Question/edit_p3/<?php echo $item['tg_audio']['id_audio'] ?>"><span class="btn btn-info glyphicon glyphicon-edit"></span></a><br>
                                <!-- Delete -->
                                <a onclick="return confirm('Bạn chắc chắn xóa?')" href="Question/delete_p/<?php echo $item['tg_audio']['id_audio'] ?>"><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
                            </td>
                            
                        </tr>
                    <?php }} ?>
                        
                    </tbody>
                </table>
                <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#"><<</a></li>
                        <li><a href="#"><</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">></a></li>
                        <li><a href="#">>></a></li>
                    </ul> 
                </div>
            </div>
            <!-- Part 4 -->
            <div id="part4" class="tab-pane fade">
                <div class="row">                    
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <form method="post" class="input-group add-on">
                            <input class="form-control" name="search_input" class="srch-term" id="input" type="text" placeholder="Search...">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3  col-sm-3 col-xs-12">
                        <div class="btn-group" style="margin-top: 15px;" >
                          <button type="button" class="btn btn-primary">Draft</button>
                          <button type="button" class="btn btn-primary">Active</button>
                          <button type="button" class="btn btn-primary">Inactive</button>
                        </div> 
                    </div>
                    <div class="col-md-2 col-md-offset-2" style="padding-top: 13px;">
                        <a href="Question/add_p4"><button type="button" class="btn btn-primary">Add</button></a>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">AUDIO</th>
                            <th class="text-center">QUESTION & CORRECT ANSWER</th>
                            <!-- <th class="text-center">CORRECT ANSWER</th> -->
                            
                            <th class="text-center"  width="30%">TAPE SCRIPT</th>
                            <th class="text-center">OPTION</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data1 as $item){ 
                        if($item['tg_question']['id_part'] == 4){
                    ?>
                    
                        <tr>
                         
                            
                            <!-- id -->
                            <td class="text-center"><?php echo $item['tg_audio']['id_audio'] ?></td>
                            <!-- Image -->
                            
                            <!-- Audio -->
                            <td class="text-center" >
                                <audio src="<?php echo $this->webroot; ?>Audio/<?php echo $item['tg_audio']['audio'] ?>" id="audio"></audio>
                                <span class="glyphicon glyphicon-play-circle b-play btn-player"></span>
                            </td>

                            <td class="text-center" >
                                <table align="center" >
                                    <?php foreach($data as $item2){ ?>
                                    <?php if($item2['tg_answer']['iscorrect']==1){ ?>
                                    <?php if($item2['tg_question']['id_audio'] == $item['tg_audio']['id_audio']){?>
                                    <tr>
                                        <td style="padding-right: 2em;"><?php echo $item2['tg_question']['question']; ?></td>
                                        <td style="padding-left: 2em;"><?php echo $item2['tg_answer']['answer'] ?></td>
                                    </tr>
                                    <?php }}}?>
                                </table>
                            </td>
                            <!-- Correct Answer -->
                            

                            <!-- Tape Script -->
                            <td class="text-center" ><?php echo $item['tg_audio']['tape_script'] ?></td>
                            <!--View Edit delete -->
                            <td class="text-center" >
                                <!-- View -->
                                <a href="Question/view_p4/<?php echo $item['tg_audio']['id_audio'] ?>"><span class="btn btn-success glyphicon glyphicon-eye-open"></span></a><br>
                                <!-- Edit -->
                                <a href="Question/edit_p4/<?php echo $item['tg_audio']['id_audio'] ?>"><span class="btn btn-info glyphicon glyphicon-edit"></span></a><br>
                                <!-- Delete -->
                                <a onclick="return confirm('Bạn chắc chắn xóa?')" href="Question/delete_p/<?php echo $item['tg_audio']['id_audio'] ?>"><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
                            </td>
                            
                        </tr>
                    <?php }} ?>
                        
                    </tbody>
                </table>
                <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#"><<</a></li>
                        <li><a href="#"><</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">></a></li>
                        <li><a href="#">>></a></li>
                    </ul> 
                </div>
            </div>
            <!-- Part 5  -->
            <div id="part5" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <form method="post" class="input-group add-on">
                            <input class="form-control" name="search_input" class="srch-term" id="input" type="text" placeholder="Search...">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3  col-sm-3 col-xs-12">
                        <div class="btn-group" style="margin-top: 15px;" >
                          <button type="button" class="btn btn-primary">Draft</button>
                          <button type="button" class="btn btn-primary">Active</button>
                          <button type="button" class="btn btn-primary">Inactive</button>
                        </div> 
                    </div>
                    <div class="col-md-2 col-md-offset-2" style="padding-top: 13px;">
                        <a href="Question/add_p5"><button type="button" class="btn btn-primary">Add</button></a>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">QUESTION</th>
                            <th class="text-center">CORRECT ANSWER</th>
                            <!-- <th class="text-center">CORRECT ANSWER</th> -->
                            <th class="text-center">OPTION</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data as $item){ ?>
                        <tr>
                            <?php if($item['tg_question']['id_part']==5){ ?>
                            <?php if($item['tg_answer']['iscorrect']==1){ ?>
                            <!-- id -->
                            <td class="text-center"><?php echo $item['tg_question']['id_ques'] ?></td>
                            <!-- Question -->
                            <td class="text-center">
                                <?php echo $item['tg_sentence']['sentence'] ?>
                            </td>
                            <!-- Correct Answer -->
                            <td class="text-center"><?php echo $item['tg_answer']['answer'] ?></td>
                            <!-- <td class="text-center" rowspan="4">A</td> -->
                            <!-- View Edit Delete -->
                            <td class="text-center" >
                                <!-- View -->
                                <a href="Question/view_p5/<?php echo $item['tg_question']['id_ques'] ?>"><span class="btn btn-success glyphicon glyphicon-eye-open"></span></a><br>
                                <!-- Edit -->
                                <a href="Question/edit_p5/<?php echo $item['tg_question']['id_ques'] ?>"><span class="btn btn-info glyphicon glyphicon-edit"></span></a><br>
                                <!-- Delete -->
                                <a onclick="return confirm('Bạn chắc chắn xóa?')" href="Question/delete/<?php echo $item['tg_question']['id_ques'] ?>"><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
                            </td>
                            
                            <?php }}} ?>
                        </tr>

                    </tbody>
                </table>
                <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#"><<</a></li>
                        <li><a href="#"><</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">></a></li>
                        <li><a href="#">>></a></li>
                    </ul> 
                </div>
            </div>
            <!-- Part 6  -->
            <div id="part6" class="tab-pane fade">
                <div class="row">                    
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <form method="post" class="input-group add-on">
                            <input class="form-control" name="search_input" class="srch-term" id="input" type="text" placeholder="Search...">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3  col-sm-3 col-xs-12">
                        <div class="btn-group" style="margin-top: 15px;" >
                          <button type="button" class="btn btn-primary">Draft</button>
                          <button type="button" class="btn btn-primary">Active</button>
                          <button type="button" class="btn btn-primary">Inactive</button>
                        </div> 
                    </div>
                    <div class="col-md-2 col-md-offset-2" style="padding-top: 13px;">
                        <a href="Question/add_p6"><button type="button" class="btn btn-primary">Add</button></a>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">SENTENCE</th>
                            <th class="text-center">CORRECT ANSWER</th>
                            <!-- <th class="text-center">CORRECT ANSWER</th> -->
                            
                            
                            <th class="text-center">OPTION</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data6 as $item){ 
                        if($item['tg_question']['id_part'] == 6){
                    ?>
                    
                        <tr>
                         
                            
                            <!-- id -->
                            <td class="text-center"><?php echo $item['tg_sentence']['id_sen'] ?></td>
                            <!-- Image -->
                            
                            <!-- Audio -->
                            <td class="text-center" >
                                <?php echo $item['tg_sentence']['sentence'] ?>
                            </td>

                            <td class="text-center" >
                                <table align="center" >
                                    <?php foreach($data as $item2){ ?>
                                    <?php if($item2['tg_answer']['iscorrect']==1){ ?>
                                    <?php if($item2['tg_question']['id_sen'] == $item['tg_sentence']['id_sen']){?>
                                    <tr>
                                        
                                        <td style="padding-left: 2em;"><?php echo $item2['tg_answer']['answer'] ?></td>
                                    </tr>
                                    <?php }}}?>
                                </table>
                            </td>
                            <!-- Correct Answer -->
                            

                            <!-- Tape Script -->
                            
                            <!--View Edit delete -->
                            <td class="text-center" >
                                <!-- View -->
                                <a href="Question/view_p6/<?php echo $item['tg_sentence']['id_sen'] ?>"><span class="btn btn-success glyphicon glyphicon-eye-open"></span></a><br>
                                <!-- Edit -->
                                <a href="Question/edit_p6/<?php echo $item['tg_sentence']['id_sen'] ?>"><span class="btn btn-info glyphicon glyphicon-edit"></span></a><br>
                                <!-- Delete -->
                                <a onclick="return confirm('Bạn chắc chắn xóa?')" href="Question/delete_p6/<?php echo $item['tg_sentence']['id_sen'] ?>"><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
                            </td>
                            
                        </tr>
                    <?php }} ?>
                        
                    </tbody>
                </table>
                <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#"><<</a></li>
                        <li><a href="#"><</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">></a></li>
                        <li><a href="#">>></a></li>
                    </ul> 
                </div>
            </div>
            <!-- Part 7 -->
            <div id="part7" class="tab-pane fade">
                <div class="row">                    
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <form method="post" class="input-group add-on">
                            <input class="form-control" name="search_input" class="srch-term" id="input" type="text" placeholder="Search...">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3  col-sm-3 col-xs-12">
                        <div class="btn-group" style="margin-top: 15px;" >
                          <button type="button" class="btn btn-primary">Draft</button>
                          <button type="button" class="btn btn-primary">Active</button>
                          <button type="button" class="btn btn-primary">Inactive</button>
                        </div> 
                    </div>
                    <div class="col-md-2 col-md-offset-2" style="padding-top: 13px;">
                        <a href="Question/add_p7"><button type="button" class="btn btn-primary">Add</button></a>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center">ID</th>
                            <th class="text-center">SENTENCE</th>
                            <th class="text-center">QUESTION & CORRECT ANSWER</th>
                            <!-- <th class="text-center">CORRECT ANSWER</th> -->
                            
                            
                            <th class="text-center">OPTION</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data6 as $item){ 
                        if($item['tg_question']['id_part'] == 7){
                    ?>
                    
                        <tr>
                         
                            
                            <!-- id -->
                            <td class="text-center"><?php echo $item['tg_sentence']['id_sen'] ?></td>
                            <!-- Image -->
                            
                            <!-- Audio -->
                            <td class="text-center" >
                                <?php echo $item['tg_sentence']['sentence'] ?>
                            </td>

                            <td class="text-center" >
                                <table align="center" >
                                    <?php foreach($data as $item2){ ?>
                                    <?php if($item2['tg_answer']['iscorrect']==1){ ?>
                                    <?php if($item2['tg_question']['id_sen'] == $item['tg_sentence']['id_sen']){?>
                                    <tr>
                                        <td style="padding-right: 2em;"><?php echo $item2['tg_question']['question'] ?></td>
                                        <td style="padding-left: 2em;"><?php echo $item2['tg_answer']['answer'] ?></td>
                                    </tr>
                                    <?php }}}?>
                                </table>
                            </td>
                            <!-- Correct Answer -->
                            

                            <!-- Tape Script -->
                            
                            <!--View Edit delete -->
                            <td class="text-center" >
                                <!-- View -->
                                <a href="Question/view_p7/<?php echo $item['tg_sentence']['id_sen'] ?>"><span class="btn btn-success glyphicon glyphicon-eye-open"></span></a><br>
                                <!-- Edit -->
                                <a href="Question/edit_p7/<?php echo $item['tg_sentence']['id_sen'] ?>"><span class="btn btn-info glyphicon glyphicon-edit"></span></a><br>
                                <!-- Delete -->
                                <a onclick="return confirm('Bạn chắc chắn xóa?')" href="Question/delete_p6/<?php echo $item['tg_sentence']['id_sen'] ?>"><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
                            </td>
                            
                        </tr>
                    <?php }} ?>
                        
                    </tbody>
                </table>
                <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#"><<</a></li>
                        <li><a href="#"><</a></li>
                        <li><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">></a></li>
                        <li><a href="#">>></a></li>
                    </ul> 
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.btn-player', function() {
        var audio = $(this).parent().find('audio')[0];
        console.log(audio);
        if (audio.paused) {
            audio.play();
            $(this).removeClass('glyphicon-play-circle')
            $(this).addClass('glyphicon-pause')
        }else{
            audio.pause();
            // audio.currentTime = 0
            $(this).addClass('glyphicon-play-circle')
            $(this).removeClass('glyphicon-pause')
        }
    });
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>