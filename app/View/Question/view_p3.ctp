<style type="text/css">
/*    *{
margin: 0;
padding: 0;
}
h1,h2,h3,h4,h5,h6,ul,li,p{
margin: 0;
padding: 0;
}
a{
text-decoration: none;
}
body{
background: #ddd;
}

.container{
margin-top: 40px;
margin-bottom: 40px;
background: #fff;
}*/
/*.menu-right{
background: #202227;
margin: 0;
padding: 0;
}*/
/*.content{

}
.logo{
background: #202227;

}
.logo h3{
color: #fff;
padding-top: 10px;
padding-bottom: 10px;
}*/
.content{
	background: #fff;
	padding: 10px;
}
.tab-content{
	margin: 10px;
}
.tab-content h4{
	margin-top: 10px;
	margin-bottom: 10px;
	font-weight: bold;
}
.tab-content h3{
	padding-top: 20px;
	padding-bottom: 10px;
	font-weight: bold;
}
.sbm{
	margin-top: 20px;
	margin-bottom: 20px;
}
.image-preview-input {
	position: relative;
	overflow: hidden;
	margin: 0px;    
	color: #333;
	background-color: #fff;
	border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
	margin-left:2px;
}
/*.file {
    visibility: hidden;
    position: absolute;
    }*/
    .row-eq-height {
    	display: -webkit-box;
    	display: -webkit-flex;
    	display: -ms-flexbox;
    	display: flex;
    	flex-wrap: wrap;
    }
    #avatar{
    	cursor: pointer;
    }
    .input-field{
    	margin-top: 30px;
    }
    .input-control{
    	width: 500px;
    	margin-top: 40px;
    }
    .q-p6{
    	margin-top: 40px;
    }
</style>
<script src="<?php echo $this->webroot ?>js/ckeditor/ckeditor.js"></script>
<div id="part3" class="tab-pane fade in active">
	<!-- <h3>Part 5: Incomplete Sentences</h3> -->
	<?=$this->Form->create('Question', array(
		'enctype'=>'multipart/form-data',
		'type' => 'file',
		)); ?>

	<div class="row input-field">
		<div class="col-md-2 col-xs-12">
			<h4><label>Audio</label></h4>
		</div>
		<div class="col-md-10 col-xs-12">
			<!-- <input type="file" id="input"/> -->
			<?=$this->Form->input('Audio.audio', array(
				'type' => 'file',
				'label'=>false,
				'id' => 'input',

				)) ;?>
			<audio id="sound" controls src="<?php echo $this->webroot; ?>Audio/<?php echo $data[0]['Audio']['audio'] ?>"></audio>
		</div>
	</div>
	<?php
	foreach($data as $key => $item){
	?>
	
	<div class="form-group">
		
		<div class="row input-field">
			<div class="col-md-2 col-xs-12">
				<h4><label for="que">Question</label></h4>
			</div>
			<div class="col-md-10 col-xs-12 input-control">
				<?=$this->Form->input($key.'][question]', array(
					'type'=>'text',
					'class'=>'form-control', 
					'id' => 'que',
					'label' => false,
					'value' => $item['Question']['question']
				))?>
			</div>
		</div>
	</div>
	<div class="row input-field">
		<div class="col-md-2 col-xs-12">
			<h4><label>Answers</label></h4>
		</div>
		<div class="col-md-10 col-xs-12 input-control">
			
			<?php
			$i=0;$iscorrect=0;
			 foreach ($item['Answer'] as $a){ 
			 	// pr($a);die;
				if($a['iscorrect']==1){$iscorrect=$i;}
				// echo $iscorrect.' ';
			?>
			
			<div class="form-group row">
				
					<label for="focusedInput" class="col-md-1"></label>

				<!-- <input class="form-control col-md-11" id="focusedInput" type="text"> -->
				<?=$this->Form->input($key.'][Answer]['.$i.'][answer]', array(
					'type' => 'text',
					'label' => false,
					'class' => 'form-control col-md-10',
					'id' => 'focusedInput',
					'value' => $a['answer']
				)) ?>
				
			</div>
			<?php

				$i++;
			 } ?>
			
		</div>
	</div>
	<div class="row input-field">
		<div class="col-md-2 col-xs-12">
			<h4><label>Correct Answer</label></h4>
		</div>
		<div class="col-md-10 col-xs-12">
                <!-- <label class="radio-inline"><input type="radio" name="optradio">A</label>
                <label class="radio-inline"><input type="radio" name="optradio">B</label>
                <label class="radio-inline"><input type="radio" name="optradio">C</label>
                <label class="radio-inline"><input type="radio" name="optradio">D</label>
                <label class="radio-inline"><input type="radio" name="optradio">E</label> -->
                <?php 
                $options = array(
                	0 => 'A', 
                	1 => 'B',
                	2 => 'C', 
                	3 => 'D', 

                	);
                $attributes = array('legend' => false, 'style' => 'margin:7px;', 'default' => $iscorrect);
                echo $this->Form->radio($key.'][abc]', $options, $attributes);
                ?>
        </div>
    </div>
	<?php 
	} ?>
    
	<br>
	
	<div class="row input-field">
		<div class="form-group">
			<div class="col-md-2 col-xs-12">
				<h4><label>Tape Script</label></h4>
			</div>
			<div class="col-md-10 col-xs-12 input-control">
				<?=$this->Form->input('Audio.tape_script', array('type'=>'textarea','class'=>'ckeditor', 'label' => false, 'value' =>  $data[0]['Audio']['tape_script'] ))  ?>
			</div>
		</div>
	</div>
	<div class="form-group row input-field">
		<div class="col-md-2 col-xs-12">
			<label for="sel1">Status:</label>
		</div>
		<div class="col-md-10 col-xs-12" style="width: 200px;">
			<?=$this->Form->input('Audio.status',  array(
				'options'=>array('draft'=>'Draft','public'=> 'Public', 'out of date'=>'Out of date'), 
				'label' => false,
				'class' => 'form-control',
			))?>
		</div>
	</div>
	<!-- <div class="text-center">
		<button type="submit" class="btn btn-primary sbm">Submit</button>
		<a href="#" class="btn btn-danger">Cancel</a>
	</div> -->
	<?=$this->Form->end();?>
</div>
<script type="text/javascript">
    $(document).on('click', '.browse', function(){
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });
    $(document).on('change', '.file', function(){
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
</script>

<script type="text/javascript">
    function changeImg(input){
    //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
    if(input.files && input.files[0]){
        var reader = new FileReader();
        //Sự kiện file đã được load vào website
        reader.onload = function(e){
            //Thay đổi đường dẫn ảnh
            $('#avatar').attr('src',e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function() {
    $('#avatar').click(function(){
        $('#img').click();
    });
});
</script>
<script type="text/javascript">
    input.onchange = function(e){
      var sound = document.getElementById('sound');
      sound.src = URL.createObjectURL(this.files[0]);
  // not really needed in this exact case, but since it is really important in other cases,
  // don't forget to revoke the blobURI when you don't need it
  sound.onend = function(e) {
    URL.revokeObjectURL(this.src);
}
}
</script>