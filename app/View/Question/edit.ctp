<div class="box" style="margin: 0px;">
	<ul class="breadcrumb">
		<li><a href="/question">List question</a></li>
		<li><span style="font-style: italic; font-weight: bold"> Edit question</span></li>
	</ul>
</div>
<?php echo $this->Session->flash('error'); ?>
<div class="box">
	<div class="box-content">
    <?php
				
echo $this->Form->create ( 'Question', array (
						'class' => 'form-horizontal form-data',
						'enctype' => 'multipart/form-data',
						'inputDefaults' => array (
								'format' => array (
										'before',
										'label',
										'between',
										'input',
										'error',
										'after' 
								),
								'div' => array (
										'class' => 'from-group' 
								),
								'class' => array (
										'form-control' 
								),
								'error' => array (
										'attributes' => array (
												'wrap' => 'span',
												'class' => 'help-inline' 
										) 
								) 
						) 
				) );
				?>
    <fieldset>
        <?php
        echo $this->Form->input ( 'id',[
        		'type' =>'hidden'
        ] );
        echo $this->Form->input ( 'statement' );
								
								?>
        <div class="from-group ">
				<label for="QuestionStatement">Belong to</label> <br> 
				<?php foreach ($banks as $key => $value):?>
				<label class="checkbox-inline bank_check"> <input type="checkbox" value="<?php echo $key ?>"><?php echo 'bank'.$key?>
				</label> 
				<?php endforeach;?>
			</div>
        <?php
								echo $this->Form->input ( 'type', array (
										'options' => $types,
										'label' => 'Question type',
										'empty' => 'Choose one ...' 
								) );
								echo '<label for="content">Supplement content</label>';
								echo $this->Form->textarea ( 'supplement_content', array (
										'class' => 'form-control' 
								) );
								?>
        <br />
			<div>
				<label for="content">Question Choice</label>
				<table class="table table-striped table-bordered table-user-list">
					<thead>
						<tr>
							<th class="sorting">statement</th>
							<th class="sorting" style="width: 10%;">style</th>
							<th style="width: 2%"><center>isCorrect</center></th>
							<th style="width: 10%"><center>Options</center></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($question_choi as $key => $value):?>
						<tr class = ".question">
							<td><div class="from-group">
									<input value = "<?php echo $value['QuestionChoice']['statement']?>" name="data[QuestionChoice][statement][<?php echo $key?>]" class="form-control validate_" type="text" id="<?php echo 'QuestionChoiceStatement'.$key?>">
								</div></td>
							<td><div class="from-group">
									<input value = "<?php echo $value['QuestionChoice']['listing_style']?>" name="data[QuestionChoice][listing_style][<?php echo $key?>]" class="form-control" type="text" id="<?php echo 'QuestionChoiceListingStyle'.$key?>">
								</div></td>
							<td><center>
									<input class = "check_box" data_check = "<?php echo $value['QuestionChoice']['is_correct']?>" value = "" type="checkbox" name="data[QuestionChoice][is_correct][<?php echo $key?>]">
								</center></td>
							<td class="action-data"><center>
									<a class="btn btn-danger btn-sm remove-row" href="#"><i
										class="fa fa-trash-o "></i></a>
								</center></td>
						</tr>
						<?php endforeach;?>
						<!-- <tr>
                    <td>1</td>
                    <td></td>
                    <td><center><input type="checkbox" name="" /></center></td>
                    <td class="action-data">
                        <center>
                            <a class="btn btn-warning btn-sm edit-row" href="#">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a class="btn btn-danger btn-sm remove-row" href="#">
                                <i class="fa fa-trash-o "></i>
                            </a>
                        </center>
                    </td>
                </tr> -->
					</tbody>
				</table>
				<button class="btn btn-warning btn-sm add-row" type="button">add</button>
			</div>
		</fieldset>
		<br>
    <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Update', array('class' => 'btn btn-primary submit-team', 'type' => 'submit','id'=>'btn_update')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>
<?php echo $this->Html->script('edit_question'); ?>
<script>
   $('button.add-row').click(function () {
       var row = $('table tbody tr').length +1 ;
       $('table tbody').append('<tr class = "question"><td><div class="from-group"><input name="data[QuestionChoice][statement]['+row+']" class="form-control validate_" type="text" id="QuestionChoiceStatement1"></div></td>'
           +'<td><div class="from-group"><input name="data[QuestionChoice][listing_style]['+row+']" class="form-control" type="text" id="QuestionChoiceListingStyle1"></div></td></td>'
           +'<td><center><input type="checkbox" name="data[QuestionChoice][is_correct]['+row+']" /></center></td>'
           +'<td class="action-data">'
           +'<center>'
           +'<a class="btn btn-danger btn-sm remove-row" href="#">'
           +'<i class="fa fa-trash-o "></i>'
           +'</a>'
           +' </center>'
           +'</td></tr>');
   });
</script>