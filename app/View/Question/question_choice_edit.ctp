<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><a href="/question">List question choice</a></li>
      <li><span style="font-style: italic; font-weight: bold"> Edit question choice</span></li>
    </ul>
</div>
<?php echo $this->Session->flash('error'); ?>
<div class="box">
    <div class="box-content">
    <?php echo $this->Form->create('QuestionChoice', array(
        'class' => 'form-horizontal form-data',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <?php
        echo $this->Form->input('id', array('type' => 'hidden'));
        echo $this->Form->input('question_id', array('type'=> 'text'));
        echo $this->Form->input('statement');
        echo $this->Form->input('listing_style');
        echo $this->Form->input('is_correct', array('label' => 'Is correct &nbsp; &nbsp;','type' => 'checkbox', 'class' => false));
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Created', array('class' => 'btn btn-primary submit-team', 'type' => 'submit')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>