<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><span style="font-style: italic; font-weight: bold"> List question bank meta</span></li>
    </ul>
</div>
<?php echo $this->Session->flash(); ?>
<div class="box" style="margin-bottom:1px;">
    <hr>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Enter statement or content" name="filters[title]" value="<?= (isset($filters['title']))?$filters['title'] : ''?>">
                    </div>
                </div>
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                      <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                      <a href="/Questions" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
                <div class="col-md-1 pull-right">
                    <div class="form-group">
                      <a href="/question/question_bank_meta_add" class="btn btn-success form-control">ADD</a>
                    </div>
                </div>
            </div>
        </form>
        <table class="table table-striped table-bordered table-user-list">
            <thead>
            <tr>
                <th class="sorting" ><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                <th class="sorting"><?php echo $this->Paginator->sort('field_name', 'Bank name'); ?></th>
                <th class="sorting"  style="width: 70%" ><?php echo $this->Paginator->sort('description'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('data_type'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('created_datetime'); ?></th>
                <th style="width:8%"><center>Options</center></th>
            </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
            <?php if (isset($records)): ?>
                <?php foreach ($records as $item): ?>
                    <tr>
                        <td class="id-preview"><?php echo $item['QuestionBankMeta']['id']; ?></td>
                        <td><?php echo $item['QuestionBankMeta']['field_name']; ?></td>
                        <td><?php echo $item['QuestionBankMeta']['description']; ?></td>
                        <td><?php echo $item['QuestionBankMeta']['data_type']; ?></td>
                        <td><?= ($item['QuestionBankMeta']['created_datetime'])?$item['QuestionBankMeta']['created_datetime']:''?></td>
                        <td class="action-data">
                            <center>
                            <a class="btn btn-warning btn-sm" href="question/question_bank_meta_edit/<?php echo $item['QuestionBankMeta']['id']; ?>">
                                <i class="fa fa-edit "></i>
                            </a>
                            <a class="btn btn-danger btn-sm remove-row" data-url-remove="<?= 'question/question_bank_meta_remove/'.$item['QuestionBankMeta']['id']?>"
                               href="javascript:;">
                                <i class="fa fa-trash-o "></i>
                            </a>
                            </center>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
        <div style="text-align: center">
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
