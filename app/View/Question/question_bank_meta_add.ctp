<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><a href="/question_bank_meta_index">List question bank</a></li>
      <li><span style="font-style: italic; font-weight: bold"> Add question bank </span></li>
    </ul>
</div>
<?php echo $this->Session->flash('error'); ?>
<div class="box">
    <div class="box-content">
    <?php echo $this->Form->create('QuestionBankMeta', array(
        'class' => 'form-horizontal form-data',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <?php
        echo $this->Form->input('field_name', array('label' => 'Bank name','type' => 'text'));
        echo $this->Form->input('data_type', array(
            'options' => array(),
            'label' => 'Question type',
            'empty' => 'Choose one ...'
        ));
        echo '<label for="content">Description</label>';
        echo $this->Form->textarea('description', array('class' => 'form-control'));
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Created', array('class' => 'btn btn-primary submit-team', 'type' => 'submit','id'=>'btn_bank')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>
<script>
	$(document).ready(function(){
		$('#btn_bank').click(function(){
			check = 0;
			var value = $('#QuestionBankMetaFieldName').val();
			if(value == ''){
				check = 1;
				$('#QuestionBankMetaFieldName').addClass('error_')
			}
			if(check ==1){
				return false;
			}
		});
		$('#QuestionBankMetaFieldName').click(function(){
			$(this).removeClass('error_')
		});
	});
</script>