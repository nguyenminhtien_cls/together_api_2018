<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><a href="/question">List question</a></li>
      <li><span style="font-style: italic; font-weight: bold"> Add question</span></li>
    </ul>
</div>
<?php echo $this->Session->flash('error'); ?>
<div class="box">
    <div class="box-content">
    <?php echo $this->Form->create('QuestionType', array(
        'class' => 'form-horizontal form-data',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <?php
        echo $this->Form->input('id', array('type' => 'hidden'));
        echo $this->Form->input('title');
        echo '<label for="content">Description</label>';
        echo $this->Form->textarea('description', array('class' => 'form-control'));
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Update', array('class' => 'btn btn-primary submit-team', 'type' => 'submit','id'=>'btn_update')); ?>
    </div>
</div>
<script>
	$(document).ready(function(){
		$('#btn_update').click(function(){
			check = 0;
			var value = $('#QuestionTypeTitle').val();
			if(value == ''){
				check = 1;
				$('#QuestionTypeTitle').addClass('error_')
			}
			if(check ==1){
				return false;
			}
		});
		$('#QuestionTypeTitle').click(function(){
			$(this).removeClass('error_')
		});
	});
</script>