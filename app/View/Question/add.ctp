<?php 
// echo $this->Html->css(array(
//     'bootstrap',
//     'add',
// ));

// echo $this->Html->script(array(
//     'jquery-3.1.1.min',
//     'bootstrap.min',
//     'img-animation',
//     'file-animation'
// ));
?>
<style type="text/css">
/*    *{
    margin: 0;
    padding: 0;
}
h1,h2,h3,h4,h5,h6,ul,li,p{
    margin: 0;
    padding: 0;
}
a{
    text-decoration: none;
}
body{
    background: #ddd;
}

.container{
    margin-top: 40px;
    margin-bottom: 40px;
    background: #fff;
    }*/
/*.menu-right{
    background: #202227;
    margin: 0;
    padding: 0;
    }*/
/*.content{

}
.logo{
    background: #202227;
    
}
.logo h3{
    color: #fff;
    padding-top: 10px;
    padding-bottom: 10px;
    }*/
    .content{
        background: #fff;
        padding: 10px;
    }
    .tab-content{
        margin: 10px;
    }
    .tab-content h4{
        margin-top: 10px;
        margin-bottom: 10px;
        font-weight: bold;
    }
    .tab-content h3{
        padding-top: 20px;
        padding-bottom: 10px;
        font-weight: bold;
    }
    .sbm{
        margin-top: 20px;
        margin-bottom: 20px;
    }
    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;    
        color: #333;
        background-color: #fff;
        border-color: #ccc;    
    }
    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    .image-preview-input-title {
        margin-left:2px;
    }
    /*.file {
        visibility: hidden;
        position: absolute;
    }*/
    .row-eq-height {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        flex-wrap: wrap;
    }
    #avatar{
        cursor: pointer;
    }
    .input-field{
        margin-top: 30px;
    }
    .input-control{
        width: 500px;
        margin-top: 40px;
    }
    .q-p6{
        margin-top: 40px;
    }
    .text-check{
        margin-right: 7px;
        margin-top: 15px;
        font-weight: bold;
    }
</style>
<script src="<?php echo $this->webroot ?>js/ckeditor/ckeditor.js"></script>
<div class="box">
    <div class="box-header" style="background:#ffffff">
    </div>
    <div class="content ">
        <ul class="nav nav-tabs">
            
            <li class="active"><a data-toggle="tab" href="#part1">P1:Picture Description</a></li>
            <li><a data-toggle="tab" href="#part2">P2:Question Response</a></li>
            <li><a data-toggle="tab" href="#part3">P3:Conversation</a></li>
            <li><a data-toggle="tab" href="#part4">P4:Short Talk</a></li>
            <li><a data-toggle="tab" href="#part5">P5:Incomplete Sentences</a></li>
            <li><a data-toggle="tab" href="#part6">P6:Text Completion</a></li>
            <li><a data-toggle="tab" href="#part7">P7:Reading Comprehension</a></li>
        </ul>
        
        <div class="tab-content">
            
            <!-- Part 1 -->
            
            <div id="part1" class="tab-pane fade in active">
                <?=$this->Form->create('Question', array(
                    'enctype'=>'multipart/form-data',
                    'type' => 'file',
                )); ?>
                <?=$this->Form->input('id_part', array(
                    'type' => 'text',
                    'value' => '1',
                    'label' => false,
                    'class' => 'hidden',
                    'readonly', 
                )) ?>
                    <!-- Input Image -->
                    <!-- <h3>Part 1: Picture Description</h3> -->
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Image</h4>
                        </div>
                        <div class="form-group col-md-10 col-xs-12" >
                            
                            <?=$this->Form->input(
                                'Question.images',array(
                                    'label'=>false,
                                    'type'=>'file',
                                    'id'=>'img',
                                    // 'name' => 'anh',
                                     'class'=>'hidden',
                                    'onChange'=>'javascript:changeImg(this);'
                                )
                            ) ?>
                            <?=$this->Html->image(
                                'upload.jpg', array(
                                    'id'=>'avatar',
                                    'class'=>'thumbnail',
                                    'width'=>'200px',
                                )
                            ) ?>
                        </div>
                    </div>
                    
                   <!-- Input Audio -->
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Audio</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                           <!-- <input type="file" id="input"/> -->
                            <?=$this->Form->input('Audio.audio', array(
                                'type' => 'file',
                                'label'=>false,
                                'id' => 'input',
                                
                            )) ;?>
                            <audio id="sound" controls></audio>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                           <!-- <label class="checkbox-inline"><input type="checkbox" value="">A</label>
                           <label class="checkbox-inline"><input type="checkbox" value="">B</label>
                           <label class="checkbox-inline"><input type="checkbox" value="">C</label>
                           <label class="checkbox-inline"><input type="checkbox" value="">D</label>
                           <label class="checkbox-inline"><input type="checkbox" value="">E</label> --> 
                           <?php
                           echo $this->form->checkbox('Answer.0.answer', array(
                            // 'type' => 'select',
                            // 'multiple' => 'checkbox',
                            'label' =>false,
                            'class' => 'checkbox-inline',
                            'style' => 'margin-right:7px;',
                            'value' => 'A'
                            )) ?><span class="text-check">A</span>
                            <?php
                           echo $this->form->checkbox('Answer.1.answer', array(
                            // 'type' => 'select',
                            // 'multiple' => 'checkbox',
                            'label' =>false,
                            'class' => 'checkbox-inline',
                            'style' => 'margin-right:7px;',
                            'value' => 'B'
                            )) ?><span class="text-check">B</span>
                            <?php
                           echo $this->form->checkbox('Answer.2.answer', array(
                            // 'type' => 'select',
                            // 'multiple' => 'checkbox',
                            'label' =>false,
                            'class' => 'checkbox-inline',
                            'style' => 'margin-right:7px;',
                            'value' => 'C'
                            )) ?><span class="text-check">C</span>
                            <?php
                           echo $this->form->checkbox('Answer.3.answer', array(
                            // 'type' => 'select',
                            // 'multiple' => 'checkbox',
                            'label' =>false,
                            'class' => 'checkbox-inline',
                            'style' => 'margin-right:7px;',
                            'value' => 'D'
                            )) ?><span class="text-check">D</span>
                            <?php
                           echo $this->form->checkbox('Answer.4.answer', array(
                            // 'type' => 'select',
                            // 'multiple' => 'checkbox',
                            'label' =>false,
                            'class' => 'checkbox-inline',
                            'style' => 'margin-right:7px;',
                            'value' => 'E'
                            )) ?><span class="text-check">E</span>

                            
                       </div>

                    </div>
                    <!-- Input Answer -->
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <!-- <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                            <label class="radio-inline"><input type="radio" name="optradio">E</label> -->
                            <?php 
                            $options = array(
                                'A' => 'A', 
                                'B' => 'B',
                                'C' => 'C', 
                                'D' => 'D', 
                                'E' => 'E');
                            $attributes = array('legend' => false, 'style' => 'margin:7px;');
                            echo $this->Form->radio('abc', $options, $attributes);
                            ?>
                        </div>
                    </div>

                    
                            <!-- Input Tape Script -->
                    <div class="row input-field">
                        <div class="form-group">
                            <div class="col-md-2 col-xs-12">
                                <h4>Tape Script</h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <?=$this->Form->input('Audio.tape_script', array('type'=>'textarea','class'=>'ckeditor', 'label' => false))  ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <label for="sel1">Status:</label>
                        </div>
                        <div class="col-md-10 col-xs-12" style="width: 200px;">
                            <?=$this->Form->input('Question.status',  array('options'=>array('draft'=>'Draft','public'=> 'Public', 'out of date'=>'Out of date'), 'label' => false))?>
                        </div>
                    </div>
                     
                    <!-- Submit -->
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary sbm">Submit</button>
                        <a href="#" class="btn btn-danger">Cancel</a>
                    </div>
                    <?=$this->Form->end();?>
            </div>
            
            <!-- Part 2  -->
            <div id="part2" class="tab-pane fade ">
                <?=$this->Form->create('Question', array(
                    'enctype'=>'multipart/form-data',
                    'type' => 'file',
                )); ?>
                <?=$this->Form->input('id_part', array(
                    'type' => 'text',
                    'value' => '2',
                    'label' => false,
                    'class' => 'hidden',
                    'readonly', 
                )) ?>
                    <!-- Input Image -->
                    
                      
                   <!-- Input Audio -->
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Audio</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                           <!-- <input type="file" id="input"/> -->
                            <?=$this->Form->input('Audio.audio', array(
                                'type' => 'file',
                                'label'=>false,
                                'id' => 'input',
                                
                            )) ;?>
                            <audio id="sound" controls></audio>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                           <!-- <label class="checkbox-inline"><input type="checkbox" value="">A</label>
                           <label class="checkbox-inline"><input type="checkbox" value="">B</label>
                           <label class="checkbox-inline"><input type="checkbox" value="">C</label>
                           <label class="checkbox-inline"><input type="checkbox" value="">D</label>
                           <label class="checkbox-inline"><input type="checkbox" value="">E</label> --> 
                           <?=$this->Form->input('Answer.0.answer', array(
                                'type' => 'text',
                                'value' => 'A',
                                'label' => false,
                                'readonly', 
                            )) ?>
                            <?=$this->Form->input('Answer.1.answer', array(
                                'type' => 'text',
                                'value' => 'B',
                                'label' => false,
                                'readonly', 
                            )) ?>
                            <?=$this->Form->input('Answer.2.answer', array(
                                'type' => 'text',
                                'value' => 'C',
                                'label' => false,
                                'readonly', 
                            )) ?>
                            <?=$this->Form->input('Answer.3.answer', array(
                                'type' => 'text',
                                'value' => 'D',
                                'label' => false,
                                'readonly', 
                            )) ?>
                       </div>

                    </div>
                    <!-- Input Answer -->
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <!-- <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                            <label class="radio-inline"><input type="radio" name="optradio">E</label> -->
                            <?php 
                            $options = array(
                                'A' => 'A', 
                                'B' => 'B',
                                'C' => 'C', 
                                'D' => 'D', 
                                'E' => 'E');
                            $attributes = array('legend' => false, 'style' => 'margin:7px;');
                            echo $this->Form->radio('abc', $options, $attributes);
                            ?>
                        </div>
                    </div>

                    
                            <!-- Input Tape Script -->
                    <div class="row input-field">
                        <div class="form-group">
                            <div class="col-md-2 col-xs-12">
                                <h4>Tape Script</h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <?=$this->Form->input('Audio.tape_script', array('type'=>'textarea','class'=>'ckeditor', 'label' => false))  ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <label for="sel1">Status:</label>
                        </div>
                        <div class="col-md-10 col-xs-12" style="width: 200px;">
                            <?=$this->Form->input('Question.status',  array('options'=>array('draft'=>'Draft','public'=> 'Public', 'out of date'=>'Out of date'), 'label' => false))?>
                        </div>
                    </div>
                     
                    <!-- Submit -->
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary sbm">Submit</button>
                        <a href="#" class="btn btn-danger">Cancel</a>
                    </div>
                    <?=$this->Form->end();?>
            </div>

            <!-- Part3 -->
            <div id="part3" class="tab-pane fade">
                <!-- <h3>Part 3: Conversation</h3> -->
                    <!-- Input Audio -->
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Audio</h4>
                        </div>
                    
                        <div class="col-md-10 col-xs-12">
                            <input type="file" id="input"/>
                            <audio id="sound" controls></audio>
                        </div>
                    </div>
                    <!-- Question Answer -->
                    
                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question 1</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question 1</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question 1</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>

                    <!-- Tape Script -->
                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Tape Script</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <textarea class="form-control" rows="5" id="comment"></textarea>
                        </div>
                    </div> 

                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <label for="sel1">Status:</label>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <select class="form-control" id="sel1" style="width: 200px;">
                                <option>Draft</option>
                                <option>Active</option>
                                <option>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <!-- Submit -->
            </div>
            <!-- Part 4 -->
            <div id="part4" class="tab-pane fade">
                <!-- <h3>Part 3: Conversation</h3> -->
                    <!-- Input Audio -->
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Audio</h4>
                        </div>
                    
                        <div class="col-md-10 col-xs-12">
                            <input type="file" id="input"/>
                            <audio id="sound" controls></audio>
                        </div>
                    </div>
                    <!-- Question Answer -->
                    
                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question 1</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question 2</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question 3</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>

                    <!-- Tape Script -->
                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Tape Script</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <textarea class="form-control" rows="5" id="comment"></textarea>
                        </div>
                    </div> 

                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <label for="sel1">Status:</label>
                        </div>
                        <div class="col-md-10 col-xs-12" style="width: 200px;">
                            <select class="form-control" id="sel1">
                                <option>Draft</option>
                                <option>Active</option>
                                <option>Inactive</option>
                            </select>
                        </div>
                    </div>
                    <!-- Submit -->
            </div>
            <!-- Part 5  -->
            <div id="part5" class="tab-pane fade">
                    <!-- <h3>Part 5: Incomplete Sentences</h3> -->
                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>
                    <br>

                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <label for="sel1">Status:</label>
                        </div>
                        <div class="col-md-10 col-xs-12" style="width: 200px;">
                            <select class="form-control" id="sel1">
                                <option>Draft</option>
                                <option>Active</option>
                                <option>Inactive</option>
                            </select>
                        </div>
                    </div>
            </div>
            <!-- Part 6  -->
            <div id="part6" class="tab-pane fade">

                <!-- <h3>Part 6: Text Completion</h3> -->
                    <!-- Sentence -->
                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Sentence</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <textarea class="form-control" rows="5" id="comment"></textarea>
                        </div>
                    </div>
                    <div class="q-p6">                       
                        <h4>Question 1</h4>                       
                    </div>
                    <!-- Q&A -->
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>
                    
                    <div class="q-p6">                       
                        <h4>Question 2</h4>                       
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>
                        
                    <div class="q-p6">                       
                        <h4>Question 3</h4>                       
                    </div>

                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>

                    <div class="q-p6">                       
                        <h4>Question 4</h4>                       
                    </div>

                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>
                    <br>

                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <label for="sel1">Status:</label>
                        </div>
                        <div class="col-md-10 col-xs-12" style="width: 200px;">
                            <select class="form-control" id="sel1">
                                <option>Draft</option>
                                <option>Active</option>
                                <option>Inactive</option>
                            </select>
                        </div>
                    </div>
            </div>
            <!-- Part 7 -->
            <div id="part7" class="tab-pane fade">

                <!-- <h3>Part 7: Reading Comprehension</h3> -->
                    <!-- Sentence -->
                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Sentence</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <textarea class="form-control" rows="5" id="comment"></textarea>
                        </div>
                    </div>
                    <!-- Q&A -->
                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row input-field">
                            <div class="col-md-2 col-xs-12">
                                <h4><label for="que">Question</label></h4>
                            </div>
                            <div class="col-md-10 col-xs-12 input-control">
                                <input type="text" class="form-control" id="que">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Answers</h4>
                        </div>
                        <div class="col-md-10 col-xs-12 input-control">
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">1</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">2</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">3</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                            <div class="form-group row">
                                <label for="focusedInput" class="col-md-1">4</label>
                                <input class="form-control col-md-11" id="focusedInput" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row input-field">
                        <div class="col-md-2 col-xs-12">
                            <h4>Correct Answer</h4>
                        </div>
                        <div class="col-md-10 col-xs-12">
                            <label class="radio-inline"><input type="radio" name="optradio">A</label>
                            <label class="radio-inline"><input type="radio" name="optradio">B</label>
                            <label class="radio-inline"><input type="radio" name="optradio">C</label>
                            <label class="radio-inline"><input type="radio" name="optradio">D</label>
                        </div>
                    </div>
                    <br>

                    <div class="form-group row input-field">
                        <div class="col-md-2 col-xs-12">
                            <label for="sel1">Status:</label>
                        </div>
                        <div class="col-md-10 col-xs-12" style="width: 200px;">
                            <select class="form-control" id="sel1">
                                <option>Draft</option>
                                <option>Active</option>
                                <option>Inactive</option>
                            </select>
                        </div>
                    </div>
            </div> 
        </div>
        
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.browse', function(){
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });
    $(document).on('change', '.file', function(){
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });
</script>

<script type="text/javascript">
    function changeImg(input){
    //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
    if(input.files && input.files[0]){
        var reader = new FileReader();
        //Sự kiện file đã được load vào website
        reader.onload = function(e){
            //Thay đổi đường dẫn ảnh
            $('#avatar').attr('src',e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function() {
    $('#avatar').click(function(){
        $('#img').click();
    });
});
</script>
<script type="text/javascript">
    input.onchange = function(e){
      var sound = document.getElementById('sound');
      sound.src = URL.createObjectURL(this.files[0]);
  // not really needed in this exact case, but since it is really important in other cases,
  // don't forget to revoke the blobURI when you don't need it
  sound.onend = function(e) {
    URL.revokeObjectURL(this.src);
}
}
</script>