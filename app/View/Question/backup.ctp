public function edit_p4($id = null){
		$page_header = "Edit part 4";
		$page_header = "Edit";
		$filename = null;
		$filename2= null;
		$tmp_name = null;
    		// $data = $this->Audio->find('first',array(
    		// 	'conditions'=> array('Audio.id_audio' => $id),
    		// 	'contain' => array(
    		// 		'Question','Answer')
    		// 	));//Lấy thông tin 
		$data = $this->Question->find('all',array(
    			'conditions'=> array('Question.id_audio' => $id),
    			// 'contain' => array(
    			// 	'Question','Answer')
    			));

		$this->set('data', $data);
		
		if($this->request->is('post')){
			// pr($this->request->data); die;
			//Upload Audio
			if(!empty($this->request->data['Audio']['audio']['name'])){
				$Audio=$this->request->data['Audio']['audio']['name'];
				$Audio=sha1($this->request->data['Audio']['audio']['name'].rand(0,100)).'-'.$Audio;
				$filename2 = WWW_ROOT. 'Audio'.DS.$Audio; 
				$tmp_name=$this->request->data['Audio']['audio']['tmp_name'];
				$this->request->data['Audio']['audio']=$Audio;
			}
			else{
				unset($this->request->data['Audio']['audio']);
			}
			move_uploaded_file($tmp_name,$filename2);
			/*$i=0;
			foreach ($this->request->data['Answer'] as $item) {
				if($i == $this->request->data['Audio']['abc']){
					$this->request->data['Answer'][$i]['iscorrect'] = 1;
				}
				// pr($item);
				$i++; 
			}*/
			// pr($this->request->data); die; 

			$data_audi['audio']=$Audio;	
			// pr($data_audi['audio']); die;
			$this->Audio->save($data_audi);	
			// pr($this->Audio->id);
			$id_audio = $id;
			$tape = $this->request->data['Audio']['tape_script'];
			$status = $this->request->data['Audio']['status'];
			$this->Audio->saveField('tape_script', $tape);
			$this->Audio->saveField('status', $status);
			$data=$this->request->data;
			// $dataT = $this->request->data['Audio']['tape_script'];
			// $this->Audio->save($dataT);
			// pr($dataT);
			foreach ($data['Question'] as $item) {
				$item['id_part']=4;
				$item['id_audio']=$id_audio;

				// $i=0;
				// foreach ($item['Answer'] as $value) {
				// 	if($i == $item['abc']){
				// 		$item['Answer'][$i]['iscorrect'] = 1;
				// 	}
				// 	$i++;
				// }die;
				foreach ($item['Answer'] as $key => $value) {
					if($key == $item['abc']){
						$item['Answer'][$key]['iscorrect'] = 1;
					}
				}
				// pr($item);die;
				$this->Question->saveAssociated($item);

			}
			
			$this->redirect(array('action'=>'index'));
		}
	}

	<div class="row input-field">
			<div class="col-md-2 col-xs-12">
				<h4><label for="que">Sentence</label></h4>
			</div>
			<div class="col-md-10 col-xs-12 input-control">
				<?=$this->Form->input('Sentence.sentence', array(
					'type'=>'textarea',
					'class'=>'form-control ckeditor', 
					'id' => 'que',
					'label' => false
				))?>
			</div>
		</div>