<?php 
// echo $this->Html->css(array(
//     'bootstrap',
//     'list',
// ));

// echo $this->Html->script(array(
//     'jquery-3.1.1.min',
//     'bootstrap.min',
//     'img-animation',
//     'file-animation',
// ));
?>
<style type="text/css">

/*body{
    background: #ddd;
}*/

/*.container{
    margin-top: 40px;
    margin-bottom: 40px;
    background: #fff;
}*/
/*.menu-right{
    background: #222;
    margin: 0;
    padding: 0;
}*/
.list-content{
    background: #fff;
    padding: 10px;
}

.tab-content{

}
.tab-content h4{
    margin-top: 10px;
    margin-bottom: 10px;
    font-weight: bold;
}
.tab-content h3{
    padding-top: 20px;
    padding-bottom: 10px;
    font-weight: bold;
}
.cor-ans{
    color: #fff;
}
.sbm{
    margin-top: 20px;
    margin-bottom: 20px;
}
.image-preview-input {
    position: relative;
    overflow: hidden;
    margin: 0px;    
    color: #333;
    background-color: #fff;
    border-color: #ccc;    
}
.image-preview-input input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
.image-preview-input-title {
    margin-left:2px;
}
.file {
    visibility: hidden;
    position: absolute;
}
/*.row-eq-height {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    flex-wrap: wrap;222
}*/
.img-audio{
    width: 8em;
    height: auto;
}
.b-play{
    font-size: 4em;
}
.input-group{
    margin-top: 15px;
    margin-bottom: 15px;
}
</style>
<div class="box">
    <div class="box-header" style="background:#ffffff">
    </div>
    <div class="list-content ">
        <ul class="nav nav-tabs">
            <li><a href="Question/index">P1:Picture Description</a></li>
            <li><a href="Question/part2">P2:Question Response</a></li>
            <li><a href="Question/part3">P3:Conversation</a></li>
            <li><a href="Question/part4">P4:Short Talk</a></li>
            <li><a href="Question/part5">P5:Incomplete Sentences</a></li>
            <li><a href="Question/part6">P6:Text Completion</a></li>
            <li class="active"><a href="Question/part7">P7:Reading Comprehension</a></li>
        </ul>
        <div class="tab-content">
            <!-- Part 1 -->
            

            <!-- Part 2  -->
            

            <!-- Part3 -->
            
            <!-- Part 4 -->
            
            <!-- Part 5  -->
            
            <!-- Part 6  -->
            
            <!-- Part 7 -->
            <div id="part7" class="tab-pane fade in active">
                <div class="row">
                    <?php echo $this->Form->create("search"); ?>
                    <div class="col-md-3  col-sm-3 col-xs-12" style="margin-top: 13px;">
                        <!-- <div class="btn-group" style="margin-top: 15px;" >
                          <button type="button" class="btn btn-primary">Draft</button>
                          <button type="button" class="btn btn-primary">Active</button>
                          <button type="button" class="btn btn-primary">Inactive</button>
                        </div> -->
                        <?php  
                            $options = array(
                                'draft' => 'Draft', 
                                'public' => 'Public',
                                'out of date' => 'Out of date', 
                                 
                            );
                            $attributes = array('legend' => false, 'style' => 'margin:7px;');
                            echo $this->Form->radio("stt", $options, $attributes);
                            ?>
                        
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12" style="margin-top: 13px;">
                        <!-- <form method="post" class="input-group add-on">
                            <div class="col-md-10">
                                <input class="form-control" name="search_input" class="srch-term" id="input" type="text" placeholder="Search..." >
                            </div>
                            <div class="input-group-btn col-md-2" >
                                <button class="btn btn-primary" type="submit">Search</button>
                            </div>
                        </form> -->
                        
                        <div class="col-md-10">
                            <!-- <input class="form-control" name="search_input" class="srch-term" id="input" type="text" placeholder="Search..." > -->
                            
                            <?php echo $this->Form->input("txt_search", array(
                                'label' => false,
                                'class' => 'form-control srch-term',
                                'placeholder' => 'Search . . .'
                                ));
                            ?>
                            
                        </div>
                        <div class="input-group-btn col-md-2">
                            <button class="btn btn-primary" type="submit">Search</button>
                            <input class="btn btn-warning" type="reset" value="Reset">
                        </div>
                    </div>
                    <?php echo $this->Form->end() ?>
                    <div class="col-md-2" style="padding-top: 13px;">
                        <a href="Question/add_p7" style="float: right; margin-right: 1.3em;"><button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i></button></a>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="text-center" width="2%">ID</th>
                            <th class="text-center" width="30%">SENTENCE</th>
                            <th class="text-center" width="20%">QUESTION</th>
                            <th class="text-center" width="20%">CORRECT ANSWER</th>
                            <th class="text-center" width="10%">STATUS</th>
                            <th class="text-center" width="5%">OPTION</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($datas as $item){ 
                    	foreach($item['Question'] as $ks => $vs){
                    		// pr($vs['id_part']);
                    	}
                        if($vs['id_part'] == 7){
                    ?>
                    
                        <tr>
                         
                            
                            <!-- id -->
                            <td class="text-center"><?php echo $item['Sentence']['id_sen'] ?></td>
                            <!-- Image -->
                            
                            <!-- Audio -->
                            <td>
                                <?php echo $item['Sentence']['sentence'] ?>
                            </td>

                            <td class="text-center" >
                                <table align="center">
                                    <?php foreach($data as $item2){ ?>
                                    <?php if($item2['tg_answer']['iscorrect']==1){ ?>
                                    <?php if($item2['tg_question']['id_sen'] == $item['Sentence']['id_sen']){?>
                                    <tr>
                                        <td  class="text-left"><?php echo $item2['tg_question']['question'] ?></td>
                                        
                                    </tr>
                                    <?php }}}?>
                                </table>
                            </td>
                            <td >
                                <table align="center">
                                    <?php foreach($data as $item2){ ?>
                                    <?php if($item2['tg_answer']['iscorrect']==1){ ?>
                                    <?php if($item2['tg_question']['id_sen'] == $item['Sentence']['id_sen']){?>
                                    <tr>
                                        
                                        <td  class="text-left"><?php echo $item2['tg_answer']['answer'] ?></td>
                                    </tr>
                                    <?php }}}?>
                                </table>
                            </td>
                            <!-- Correct Answer -->
                            <!-- Status -->
                            <td class="text-center"><?php echo $item['Sentence']['status'] ?></td>
                            <!-- Tape Script -->
                            
                            <!--View Edit delete -->
                            <td class="text-center" >
                                <!-- View -->
                                <a href="Question/view_p7/<?php echo $item['Sentence']['id_sen'] ?>"><span class="btn btn-success glyphicon glyphicon-eye-open"></span></a><br>
                                <!-- Edit -->
                                <a href="Question/edit_p7/<?php echo $item['Sentence']['id_sen'] ?>"><span class="btn btn-info glyphicon glyphicon-edit"></span></a><br>
                                <!-- Delete -->
                                <a onclick="return confirm('Bạn chắc chắn xóa?')" href="Question/delete_p6/<?php echo $item['Sentence']['id_sen'] ?>"><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
                            </td>
                            
                        </tr>
                    <?php }} ?>
                        
                    </tbody>
                </table>
                <div style="text-align: center">
                    <?php echo $this->element('pagination'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.btn-player', function() {
        var audio = $(this).parent().find('audio')[0];
        console.log(audio);
        if (audio.paused) {
            audio.play();
            $(this).removeClass('glyphicon-play-circle')
            $(this).addClass('glyphicon-pause')
        }else{
            audio.pause();
            // audio.currentTime = 0
            $(this).addClass('glyphicon-play-circle')
            $(this).removeClass('glyphicon-pause')
        }
    });
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>