<style type="text/css">
/*    *{
margin: 0;
padding: 0;
}
h1,h2,h3,h4,h5,h6,ul,li,p{
margin: 0;
padding: 0;
}
a{
text-decoration: none;
}
body{
background: #ddd;
}

.container{
margin-top: 40px;
margin-bottom: 40px;
background: #fff;
}*/
/*.menu-right{
background: #202227;
margin: 0;
padding: 0;
}*/
/*.content{

}
.logo{
background: #202227;

}
.logo h3{
color: #fff;
padding-top: 10px;
padding-bottom: 10px;
}*/
.content{
	background: #fff;
	padding: 10px;
}
.tab-content{
	margin: 10px;
}
.tab-content h4{
	margin-top: 10px;
	margin-bottom: 10px;
	font-weight: bold;
}
.tab-content h3{
	padding-top: 20px;
	padding-bottom: 10px;
	font-weight: bold;
}
.sbm{
	margin-top: 20px;
	margin-bottom: 20px;
}
.image-preview-input {
	position: relative;
	overflow: hidden;
	margin: 0px;    
	color: #333;
	background-color: #fff;
	border-color: #ccc;    
}
.image-preview-input input[type=file] {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	font-size: 20px;
	cursor: pointer;
	opacity: 0;
	filter: alpha(opacity=0);
}
.image-preview-input-title {
	margin-left:2px;
}
/*.file {
    visibility: hidden;
    position: absolute;
    }*/
    .row-eq-height {
    	display: -webkit-box;
    	display: -webkit-flex;
    	display: -ms-flexbox;
    	display: flex;
    	flex-wrap: wrap;
    }
    #avatar{
    	cursor: pointer;
    }
    .input-field{
    	margin-top: 30px;
    }
    .input-control{
    	width: 500px;
    	margin-top: 40px;
    }
    .q-p6{
    	margin-top: 40px;
    }
</style>
<script src="<?php echo $this->webroot ?>js/ckeditor/ckeditor.js"></script>
<div id="part5" class="tab-pane fade in active">
	<!-- <h3>Part 5: Incomplete Sentences</h3> -->
	<?=$this->Form->create('Question', array(
		'enctype'=>'multipart/form-data',
		'type' => 'file',
		)); ?>
	<?=$this->Form->input('id_part', array(
		'type' => 'text',
		'value' => '5',
		'label' => false,
		'class' => 'hidden',
		'readonly', 
	)) ?>
	<div class="form-group">
		<div class="row input-field">
			<div class="col-md-2 col-xs-12">
				<h4><label for="que">Question</label></h4>
			</div>
			<div class="col-md-10 col-xs-12 input-control">
				<?=$this->Form->input('Sentence.sentence', array(
					'type'=>'text',
					'class'=>'form-control', 
					'id' => 'que',
					'label' => false
				))?>
			</div>
		</div>
	</div>
	<div class="row input-field">
		<div class="col-md-2 col-xs-12">
			<h4><label>Answers</label></h4>
		</div>
		<div class="col-md-10 col-xs-12 input-control">
			<div class="form-group row">
				<label for="focusedInput" class="col-md-1">1</label>
				<!-- <input class="form-control col-md-11" id="focusedInput" type="text"> -->
				<?=$this->Form->input('Answer.0.answer', array(
					'type' => 'text',
					'label' => false,
					'class' => 'form-control col-md-10',
					'id' => 'focusedInput',
				)) ?>
			</div>

			<div class="form-group row">
				<label for="focusedInput" class="col-md-1">2</label>
				<!-- <input class="form-control col-md-11" id="focusedInput" type="text"> -->
				<?=$this->Form->input('Answer.1.answer', array(
					'type' => 'text',
					'label' => false,
					'class' => 'form-control col-md-10',
					'id' => 'focusedInput',
				)) ?>
			</div>
			<div class="form-group row">
				<label for="focusedInput" class="col-md-1">3</label>
				<!-- <input class="form-control col-md-11" id="focusedInput" type="text"> -->
				<?=$this->Form->input('Answer.2.answer', array(
					'type' => 'text',
					'label' => false,
					'class' => 'form-control col-md-10',
					'id' => 'focusedInput',
				)) ?>
			</div>
			<div class="form-group row">
				<label for="focusedInput" class="col-md-1">4</label>
				<!-- <input class="form-control col-md-11" id="focusedInput" type="text"> -->
				<?=$this->Form->input('Answer.3.answer', array(
					'type' => 'text',
					'label' => false,
					'class' => 'form-control col-md-10',
					'id' => 'focusedInput',
				)) ?>
			</div>
		</div>
	</div>
	<div class="row input-field">
		<div class="col-md-2 col-xs-12">
			<h4><label>Correct Answer</label></h4>
		</div>
		<div class="col-md-10 col-xs-12">
                <!-- <label class="radio-inline"><input type="radio" name="optradio">A</label>
                <label class="radio-inline"><input type="radio" name="optradio">B</label>
                <label class="radio-inline"><input type="radio" name="optradio">C</label>
                <label class="radio-inline"><input type="radio" name="optradio">D</label>
                <label class="radio-inline"><input type="radio" name="optradio">E</label> -->
                <?php 
                $options = array(
                	'0' => 'A', 
                	'1' => 'B',
                	'2' => 'C', 
                	'3' => 'D', 
                	'4' => 'E');
                $attributes = array('legend' => false, 'style' => 'margin:7px;');
                echo $this->Form->radio('abc', $options, $attributes);
                ?>
        </div>	
    </div>
	<br>

	<div class="form-group row input-field">
		<div class="col-md-2 col-xs-12">
			<label for="sel1">Status:</label>
		</div>
		<div class="col-md-10 col-xs-12" style="width: 200px;">
			<?=$this->Form->input('Sentence.status',  array(
				'options'=>array('draft'=>'Draft','public'=> 'Public', 'out of date'=>'Out of date'), 
				'label' => false,
				'class' => 'form-control',
			))?>
		</div>
	</div>
	<div class="text-center">
		<button type="submit" class="btn btn-primary sbm">Submit</button>
		<a href="#" class="btn btn-danger">Cancel</a>
	</div>
	<?=$this->Form->end();?>
</div>