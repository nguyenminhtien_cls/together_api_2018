<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><span> Settings</span></li>
      <li><span style="font-style: italic; font-weight: bold">General</span></li>
    </ul>
</div>
<div class="box" style="margin-bottom:1px;">
    <!--Begin table-->
    <div class="box-header" style="background:#ffffff">
        <a href="javascript:window.history.back();" class="pull-left btn btn-warning btn-sm" style="margin:10px;">BACK</a>
    </div>
    <hr>
    <div class="box-content">
    <?php echo $this->Form->create('GameList', array(
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
    <?php 
         echo $this->Form->input('name',array('label' => 'Set quanlity teams on top', 'value' => 10)); 
     ?> 
    </fieldset>

    </div>
    <div class="box-content">
    <?php echo $this->Form->create('GameList', array(
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
    <?php 
         echo $this->Form->input('name',array('label' => 'Set members in a team', 'value' => 10)); 
     ?> 
    </fieldset>
    </div>
    <div class="box-content">
    <?php echo $this->Form->create('GameList', array(
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
    <?php 
         echo $this->Form->input('name',array('label' => 'Set Prohibit worlds', 'value' => 'vai, ngu')); 
     ?> 
    </fieldset>
    </div>
    <div class="box-content">
    <?php echo $this->Form->create('GameList', array(
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Apply', array('class' => 'btn btn-success')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>


