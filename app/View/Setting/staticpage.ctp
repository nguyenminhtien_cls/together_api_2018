<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><span> Settings</span></li>
      <li><span style="font-style: italic; font-weight: bold">  Static pages</span></li>
    </ul>
</div>

<div class="box">
    <div class="box-header" style="background:#fff; padding:7px;">
        <span style="color: #443e3e; font-weight: bold">  Welcome Screen</span>
    </div>
    <hr>
    <div class="box-content">
        <input type="file" name="">
        <table class="table table-striped table-bordered table-user-list">
            <thead>
                <tr>
                    <th>Image</th>
                    <th style="width:10%"><center>Status</center></th>
                    <th style="width:10%"><center>Order</center></th>
                    <th style="width:10%"><center>Options</center></th>
                </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
                <?php 
                    for($i=1; $i<=4; $i++): 
                ?>
                <tr>
                    <td><img src="img/photo.jpg" style="width: 80px; height: 80px;"></td>
                    <td>
                        <center>
                            <label class="switch switch-success">
                              <input type="checkbox" class="switch-input" checked="checked">
                              <span class="switch-label" data-on="On" data-off="Off"></span>
                              <span class="switch-handle"></span>
                            </label>
                        </center>
                    </td>
                    <td>
                        <center>
                        <span class="btn btn-sm btn-success"><i class="fa fa-arrow-up"></i></span>
                        <span class="btn btn-sm btn-danger"><i class="fa fa-arrow-down"></i></span>
                        </center>
                    </td>
                    <td>
                        <center>
                        <a class="btn btn-danger btn-sm" href="javascript:;">
                            <i class="fa fa-recycle"></i>
                        </a>
                        </center>
                    </td>
                </tr>
                <?php 
                    endfor; 
                ?>
            </tbody>
        </table>
    </div>
</div>
<div class="box">
    <div class="box-header" style="background:#fff; padding:7px;">
        <span style="color: #443e3e; font-weight: bold"> QA Screen</span>
    </div>
    <hr>
    <div class="box-content">
    <?php echo $this->Form->create('GameList', array(
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
    <?php 
       for ($i=1; $i<=2; $i++) :
       echo $this->Form->input('name',array('label' => 'Question '.$i. '?')); 
       echo $this->Form->input('name',array('label' => 'Answer '.$i)); 
        endfor;
     ?> 
    </fieldset>
    <?= $this->Form->button('Cancel', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Update', array('class' => 'btn btn-primary', 'type' => 'button', 'onclick' => 'confirm_update()')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>

<div class="box">
    <div class="box-header" style="background:#fff; padding:7px;">
        <span style="color: #443e3e; font-weight: bold"> Tutorial Screen</span>
    </div>
    <hr>
    <div class="box-content">
    <?php echo $this->Form->create('GameList', array(
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
    <?php 
         echo $this->Form->input('file',array('label' => 'Upload new video', 'value' => 10, 'type' => 'file')); 
         echo $this->Form->input('url video',array('label' => 'Url', 'value' => 10, 'type' => 'text', 'value' => 'video/tutorial.mp4')); 
     ?> 
    </fieldset>
    <?= $this->Form->button('Cancel', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Update', array('class' => 'btn btn-primary','type' => 'button', 'onclick' => 'confirm_update()')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>

<script type="text/javascript">
    function confirm_update() {
      swal({
          title: "",
          text: "Do you want change it?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
        function(){
          swal("Updated!", "Info has been update successlly.", "success");
        });  
    }
</script>


