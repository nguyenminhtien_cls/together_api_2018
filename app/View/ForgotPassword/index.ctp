<div class="elelment">
<div class="element-logo">
<img src="../../img/logo.jpg" align="top">
</div>
<?= $this->Form->create ( 'form_pass', array (
									'id' => 'login',
									'class' => 'form',
                        			'url' => '/ForgotPassword/changepass'
							) );
							?>
	<div class="element-main">
		<h1>Reset Password</h1>
		
			<input id = 'password' type="password" placeholder = "New Password" title="Password must contain at least 8 characters, including UPPER/lowercase and numbers." type="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" name = 'newpass'>
			<input id = 'repassword' type="password" placeholder = "Re-enter New Password" title="Please enter the same Password as above." type="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" name = 'renewpass'>
	</div>
	<div class="element-submit">
		<input type="submit" value="Reset your password">
	</div>
<?php 
	echo $this->Form->end ();
    ?>
</div>

<script type="text/javascript">

  document.addEventListener("DOMContentLoaded", function() {

    // JavaScript form validation

    var checkPassword = function(str)
    {
      var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
      return re.test(str);
    };

    var checkForm = function(e)
    {
      if(this.newpass.value != "" && this.newpass.value == this.renewpass.value) {
        if(!checkPassword(this.newpass.value)) {
          alert("The password you have entered is not valid!");
          this.newpass.focus();
          e.preventDefault();
          return;
        }
      } else {
        alert("Error: Please check that you've entered and confirmed your password!");
        this.newpass.focus();
        e.preventDefault();
        return;
      }
    };

    var myForm = document.getElementById("login");
    myForm.addEventListener("submit", checkForm, true);

    // HTML5 form validation

    var supports_input_validity = function()
    {
      var i = document.createElement("input");
      return "setCustomValidity" in i;
    }

    if(supports_input_validity()) {
      var pwd1Input = document.getElementById("password");
      pwd1Input.setCustomValidity(pwd1Input.title);

      var pwd2Input = document.getElementById("repassword");

      // input key handlers

      pwd1Input.addEventListener("keyup", function() {
        this.setCustomValidity(this.validity.patternMismatch ? pwd1Input.title : "");
        if(this.checkValidity()) {
          pwd2Input.pattern = this.value;
          pwd2Input.setCustomValidity(pwd2Input.title);
        } else {
          pwd2Input.pattern = this.pattern;
          pwd2Input.setCustomValidity("");
        }
      }, false);

      pwd2Input.addEventListener("keyup", function() {
        this.setCustomValidity(this.validity.patternMismatch ? pwd2Input.title : "");
      }, false);

    }

  }, false);

</script>
<style>
html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,dl,dt,dd,ol,nav ul,nav li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline;}
article, aside, details, figcaption, figure,footer, header, hgroup, menu, nav, section {display: block;}
ol,ul{list-style:none;margin:0;padding:0;}
blockquote,q{quotes:none;}
blockquote:before,blockquote:after,q:before,q:after{content:'';content:none;}
table{border-collapse:collapse;border-spacing:0;}
/* start editing from here */
a{text-decoration:none;}
.txt-rt{text-align:right;}/* text align right */
.txt-lt{text-align:left;}/* text align left */
.txt-center{text-align:center;}/* text align center */
.float-rt{float:right;}/* float right */
.float-lt{float:left;}/* float left */
.clear{clear:both;}/* clear float */
.pos-relative{position:relative;}/* Position Relative */
.pos-absolute{position:absolute;}/* Position Absolute */
.vertical-base{	vertical-align:baseline;}/* vertical align baseline */
.vertical-top{	vertical-align:top;}/* vertical align top */
.underline{	padding-bottom:5px;	border-bottom: 1px solid #eee; margin:0 0 20px 0;}/* Add 5px bottom padding and a underline */
nav.vertical ul li{	display:block;}/* vertical menu */
nav.horizontal ul li{	display: inline-block;}/* horizontal menu */
img{max-width:100%;}
 body{
   font-size: 100%;
   /*background:#0086E5;*/
 	background:#ffffff; 
   font-family: 'Roboto', sans-serif;
}
a {
  text-decoration: none;
}
a:hover {
  transition: 0.5s all;
  -webkit-transition: 0.5s all;
  -moz-transition: 0.5s all;
  -o-transition: 0.5s all;
}
/*--elemt style strat here--*/
.elelment h2 {
    font-size: 2.5em;
    color: #fff;
    text-align: center;
    margin-top:2em;
    font-weight: 700;
}

.element-logo {
    width:480px;
    background: #ffffff;
    margin:4em auto 0em;
	padding-left: 8em;
	padding-right: 1.7em;
}

.element-main {
    width:480px;
    background: #2ec4b6;
    margin:4em auto 0em;
    border-radius: 17px;
    padding-bottom: 1.3em;
	padding-left: 1.7em;
	padding-right: 1.7em;
	padding-top: 2.7em;
    margin-top: 20px
}

.element-submit {
    width:480px;
    background: #ffffff;
    margin:4em auto 0em;
    border-radius: 17px;
    margin-top: 8px
}

.element-submit input[type="submit"] {
    font-size: 1.6em;
	font-family: Segoe UI;
    color: #fff;
    background:#2ec4b6;
    width: 336px;
    padding: 0.4em 0em;
    outline: none;
    border: none;
    border-radius: 10px;
    cursor: pointer;
    display: block;
    margin: 0em auto 0em;
	margin-top: 1.5em;
}


.element-main h1 {
    text-align: center;
	font-family: Segoe UI;
    font-size: 2.3em;
    color:#ffffff;
    font-weight: 700;
    margin-bottom: 50px;
}
.element-main p {
    font-size: 1em;
    color: #696969;
    line-height: 1.5em;
    margin: 1.5em 0em;
    text-align:center;
}
.element-main input[type="text"] {
    font-size: 1.4em;
	font-style: italic;
	font-family: Segoe UI;
    color: #A29E9E;
    padding: 0.8em 0.8em;
    display: block; 
    width: 432px;
	border-radius: 10px;
    outline: none;
	margin-left: 1em;
	margin-right: 1.5em;
	margin-top: 1.3em;
	margin-bottom: 1em;
    text-align:left;
    border: 1px solid #ffffff;
}

.element-main input[type="password"] {
    font-size: 1.4em;
	font-style: italic;
	font-family: Segoe UI;
    color: #A29E9E;
    padding: 0.8em 0.8em;
    display: block; 
    width: 432px;
	border-radius: 10px;
    outline: none;
	margin-left: 1em;
	margin-right: 1.5em;
	margin-top: 1.3em;
	margin-bottom: 1em;
    text-align:left;
    border: 1px solid #ffffff;
}

.element-main input[type="submit"] {
    font-size: 1em;
    color: #fff;
    background:#0086E5;
    width: 50%;
    padding: 0.8em 0em;
    outline: none;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    border-bottom: 3px solid #045B99;
    display: block;
    margin: 1.5em auto 0;
}

.element-main input[type="submit"]:hover{
    background:#1D1C1C;
    border-bottom: 3px solid #2F2F2F;  
    transition: 0.5s all;
  -webkit-transition: 0.5s all;
  -moz-transition: 0.5s all;
  -o-transition: 0.5s all;
}


/*---copyrights--*/
.copy-right {
    margin: 9em 0em 2em 0em;
}
.copy-right p {
    text-align: center;
    font-size:1em;
    color:#fff;
    line-height: 1.5em;
}
.copy-right p a{
  color:#fff;
}
.copy-right p a:hover{
	 color:#000;
	 transition: 0.5s all;
  -webkit-transition: 0.5s all;
  -moz-transition: 0.5s all;
  -o-transition: 0.5s all;
}
/*--element end here--*/
/*--media quiries start here--*/
@media(max-width:1440px){
	
}
@media(max-width:1366px){
	
}
@media(max-width:1280px){
.elelment h2 {
    margin-top: 1em;	
}
.copy-right {
    margin: 6em 0em 2em 0em;
}
.element-main {
    width: 480px;
}
}
@media(max-width:1024px){
.element-main {
    width: 480px;	
}
}
@media(max-width:768px){
.element-main {
    width: 480px;
}	
.elelment h2 {
    font-size: 2em;
}
.element-main {
    width: 480px;
}
.element-main h1 {
    font-size: 2em;
}
}
@media(max-width:640px){
	
}
@media(max-width:480px){
.element-main {
    width: 480px;
    padding: 3em 1.5em;
}	
.copy-right {
    margin: 5em 0em 2em 0em;
}
.copy-right p {
    font-size: 0.9em;
}
}
@media(max-width:320px){
.elelment h2 {
    font-size: 1.5em;
}
.element-main h1 {
    font-size: 1.5em;
}
.element-main {
    width: 80%;
    margin: 2em auto 0em;
    padding: 1.5em 1.5em;
}
.element-main p {
    font-size: 0.9em;	
}
.element-main input[type="submit"] {
    font-size:0.9em;
    width: 75%;
}
.element-main input[type="text"] {
    font-size: 1em;
    padding: 1em 1em;
}
.copy-right {
    margin: 3em 0em 2em 0em;
}
.copy-right p {
    font-size: 0.85em;
	padding:0 4px;
}
}
/*--media quiries end here--*/
</style>