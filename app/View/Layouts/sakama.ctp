<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo $this->fetch('title'); ?></title>
    <!-- end: Meta -->
    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- end: Mobile Specific -->
    <base href="<?php echo BASE_URL ?>"/>
    <!-- start: CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/retina.min.css" rel="stylesheet"/>
    <link href="assets/css/jquery-ui.min.css" rel="stylesheet"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bower_components/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bower_components/select2/dist/css/select2.min.css">
    <link href="assets/css/halflings.css" rel="stylesheet"/>
    <link href="assets/css/social.css" rel="stylesheet"/>
    <link href="http://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet"/>
    <link href="http://fonts.googleapis.com/css?family=Lato:400" rel="stylesheet"/>
    <link href="http://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet"/>
    <link href="assets/css/style.min.css" rel="stylesheet"/>
    <link href="https://genesisui.com/demo/simpliq/bootstrap3/assets/css/chosen.css" rel="stylesheet"/>
    <link href="assets/css/customs.css" rel="stylesheet"/>
    <link href="css/style-custom.css" rel="stylesheet"/>
    <link href="css/breadcrumb-handle.css" rel="stylesheet"/>
    <link href="css/custom.css" rel="stylesheet"/>
    <link rel="stylesheet" href="assets/plugins/jquery-toggles-master/css/toggles.css">
    <link rel="stylesheet" href="assets/plugins/jquery-toggles-master/css/themes/mystyle.css">
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <!--    <link href="assets/css/datepicker.css" rel="stylesheet"/>-->
    <!-- end: CSS -->

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <script src="assets/css/ie6-8.css"></script>
    <![endif]-->
    <!-- start: Favicon and Touch Icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="assets/ico/apple-touch-icon-57-precomposed.png"/>
    <link rel="shortcut icon" href="assets/img/favicon.png"/>
    <!-- end: Favicon and Touch Icons -->

    <?php
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>
</head>

<body>
<?php echo $this->element("admin_top"); ?>

<div class="container">
    <div class="row">
        <!-- start: Main Menu -->
        <div id="sidebar-left" class="col-lg-2 col-sm-1">
            <?php echo $this->element("admin_left_menu"); ?>
        </div>
        <!-- end: Main Menu -->
        <!-- start: Content -->
        <div id="content" class="col-lg-10 col-sm-11" style="padding-top: 20px;">
           <!--  <div class="row" id="cake-flash-message">
                <div class="alert alert-info">
                    <?= $this->Session->flash();
                    //= (count($this->Session->flash()) > 1) ? '<div class=" alert alert-info">' . $this->Session->flash() . '</div>' : ''
                    ?>
                </div>
            </div> -->
            <div class="row">
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
    </div><!--/row-->
</div><!--/container-->


<div class="clearfix"></div>

<?php echo $this->element("admin_footer"); ?>

<div class="page-loading">
    <img src="assets/img/loading.gif" alt="Loading..."/><br/>Loading...
</div>
<script src="assets/js/jquery.confirm.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/respond.min.js"></script>
<script src="assets/js/underscore.js"></script>
<script src="assets/js/ejs.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/bootstrap-timepicker.min.js"></script>
<script src="assets/js/moment.min.js"></script><!-- lib for daterangepicker-->
<script src="assets/js/daterangepicker.min.js"></script><!-- daterangepicker-->
<script src="assets/js/jquery.autosize.min.js"></script>
<script src="assets/js/jquery.gritter.min.js"></script>
<script src="assets/js/ajaxfileupload.js"></script><!-- ajax upload file-->
<script src="assets/js/jquery-impromptu.js"></script><!-- popup-->
<script src="assets/js/jquery.flot.min.js"></script><!-- chart-->
<script src="assets/js/jquery.flot.time.js"></script><!-- chart-->
<script src="assets/js/jquery.sparkline.min.js"></script><!-- chart-->
<script src="assets/js/jquery.knob.modified.min.js"></script><!-- chart-->
<script src="assets/plugins/bower_components/sweetalert/dist/sweetalert.min.js"></script><!-- chart-->
<script src="assets/plugins/bower_components/select2/dist/js/select2.min.js"></script><!-- chart-->
<!-- theme scripts -->
<script src="assets/js/core.min.js"></script>
<script src="https://genesisui.com/demo/simpliq/bootstrap3/assets/js/jquery.chosen.min.js"></script>
<script src="assets/plugins/jquery-toggles-master/toggles.js" type="text/javascript"></script>
<script src="assets/js/customs.js"></script>

<script src="js/common.js"></script>
<script src="assets/myjs/mygeneral.js"></script>
<!-- end: JavaScript-->
</body>
</html>
