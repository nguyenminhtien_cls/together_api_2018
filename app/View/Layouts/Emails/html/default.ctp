<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="ja">
    <head>
        <meta http-equiv="Content-Language" content="ja">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>SUGUTOMO</title>
        <meta http-equiv="Content-Style-Type" content="text/css">
        <style type="text/css">
            body{margin:0;padding:0;font-size:14px;}
        </style>
    </head>
    <body>
        <center>
            <table border="0" cellspacing="0" cellpadding="0" style="background:#e8e8e8;">
                <tr>
                    <td colspan="3"><img src="http://sugutomo.codelovers.vn/img/email/mail_top.png" width="480" height="15" /></td>
                </tr>

                <tr>
                    <td width="35"></td>
                    <td>
                        <center><img src="http://sugutomo.codelovers.vn/img/email/logo_mail.png" width="141" height="88" /></center>
                    </td>
                    <td width="35"></td>
                </tr>

                <?php echo $this->fetch('content'); ?>

                <tr>
                    <td width="35"></td>
                    <td width="410" style="text-align:center;">
                        <br /><br /><br />
                        <p style="text-align:left;color:#666;font-size:12px;margin:0px 0 20px 0;">
                            このメッセージは aaaaaa@gmail.com に送信されました。<br />
                            これらのメールを すぐとも から今後受け取らない場合は、メールの配信を停止してください。
                        </p>
                        <p style="text-align:center;color:#ff3300;font-size:14px;margin:10px 0 10px 0;">プライバシーポリシー｜ＦＡＱ</p>
                        <p style="text-align:center;color:#666;font-size:12px;margin:10px 0 0px 0;">(c)すぐとも</p>
                    </td>
                    <td width="35"></td>
                </tr>

                <tr>
                    <td colspan="3"><img src="http://sugutomo.codelovers.vn/img/email/mail_bottom.png" width="480" height="15" /></td>
                </tr>
            </table>
        </center>
    </body>
</html>