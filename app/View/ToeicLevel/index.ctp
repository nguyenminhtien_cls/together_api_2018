
<ul class="breadcrumb">
  <li>Dictionary</li>
  <li><span style="font-style: italic; font-weight: bold"> Toeic levels List</span></li>
</ul>
<div class="box" style="margin-bottom:1px;">
    <hr>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="word" placeholder="Enter toeic level name ..." name="filters[keysearch]" value="<?= (isset($filters['keysearch']))?$filters['keysearch'] : ''?>">
                    </div>
                </div>
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                      <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                      <a href="/ToeicLevel" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
            </div>
        </form>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th style="width: 30px;"><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                <th style="width: 5%;"><?php echo $this->Paginator->sort('level'); ?></th>
                <th style="width: 20%;"><?php echo $this->Paginator->sort('title', 'Level title'); ?></th>
                <th style="width: 20%;"><?php echo $this->Paginator->sort('point_from'); ?></th>
                <th style="width: 20%;"><?php echo $this->Paginator->sort('point_to'); ?></th>
                <th style="width: 10%;"><?php echo $this->Paginator->sort('created_datetime', 'Created'); ?></th>
                <th class="sorting" style="width:10%"><center><?php echo $this->Paginator->sort('status', 'Status'); ?></center>
                <th style="width: 10%">Options</th>
            </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
            <?php foreach ($toeiclevels as $r): ?>
                <tr>
                    <td class="id-preview"><?php echo $r['ToeicLevel']['id']; ?></td>
                    <td class="editableColumns"><?php echo $r['ToeicLevel']['level']; ?></td>
                    <td class="editableColumns">
                        <?php echo $this->Html->link($r['ToeicLevel']['title'],
                            array('controller' => 'ToeicLevel', 'action' => 'words_in_toeiclevel', $r['ToeicLevel']['id'])); ?>
                    </td>
                    <td class="editableColumns"><?php echo $r['ToeicLevel']['point_from']; ?></td>
                    <td class="editableColumns"><?php echo $r['ToeicLevel']['point_to']; ?></td>
                    <td><?php echo $r['ToeicLevel']['created_datetime']; ?></td>
                    <td>
                        <?php 
                            $status = "false";
                            if ($r['ToeicLevel']['status'] == 1) {
                                $status = "true";
                            }
                        ?>
                        <center>
                            <div class="toggle toggle-modern" style="width: 80px" data-toggle-on="<?=$status?>" data-value="<?=$r['ToeicLevel']['id'];?>" data-url-changestatus="<?= 'ToeicLevel/changestatus/'.$r['ToeicLevel']['id']?>">
                        </center>
                    </td>
                    <td class="action-data">
                        <a class="btn btn-info btn-sm"
                           href="ToeicLevel/words_in_toeiclevel/<?php echo $r['ToeicLevel']['id']; ?>">
                            <i class="fa fa-eye "></i>
                        </a>
                        <a class="btn btn-warning btn-sm edit-row"
                           href="javascript:;">
                            <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-primary btn-sm update-row" style="display: none;" 
                           href="javascript:;" data-url-update="<?= 'ToeicLevel/ajaxupdate/'.$r['ToeicLevel']['id']?>">
                            <i class="fa fa-save "></i>
                        </a>
                        <a class="btn btn-danger btn-sm remove-row" data-url-remove="<?= 'ToeicLevel/delete/'.$r['ToeicLevel']['id']?>"
                           href="javascript:;">
                            <i class="fa fa-trash-o "></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php unset($toeiclevels); ?>
            </tbody>
        </table>
        <?php echo $this->element('pagination'); ?>
    </div>
</div>
<!--End table-->
