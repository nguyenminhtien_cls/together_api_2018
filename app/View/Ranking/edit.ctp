<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><span>Community</span></li>
      <li><a href="users/">Ranking</a></li>
      <li><span style="font-style: italic; font-weight: bold"> Edit ranking</span></li>
    </ul>
</div>
<div class="box">
    <div class="box" style="margin-bottom:1px;">
    <div class="box-header" style="background:#ffffff">
        <a href="/ranking" class="pull-left btn btn-warning btn-sm" style="margin:10px;"><i class="fa fa-list"></i>Back</a>
    </div>
    <hr/>
    <?= $this->Session->flash('success');?> 
    <?= $this->Session->flash('error');?> 
    <hr/>
    <div class="box-content">
    <?php echo $this->Form->create('Ranking', array(
        'class' => 'form-horizontal form-data',
        'enctype' => 'multipart/form-data',
        'method' => 'post',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <?php
            echo $this->Form->input('id',array('value' => $ranking['Ranking']['id'], 'type' => 'hidden'));
            echo $this->Form->input('team_id',array('label' => 'Team name', 'type' => 'text', 'value' => $ranking['Team']['title'], 'disabled'));
            echo $this->Form->input('menber_score_1',array('value' => $ranking['Ranking']['menber_score_1']));
            echo $this->Form->input('menber_score_2',array('value' => $ranking['Ranking']['menber_score_2']));
            echo $this->Form->input('menber_score_3',array('value' => $ranking['Ranking']['menber_score_3']));
            echo $this->Form->input('level_bonus',array('label' => 'Set level bonus', 'value' => $ranking['Ranking']['level_bonus']>0?$ranking['Ranking']['level_bonus']:'NULL'));
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Updated', array('class' => 'btn btn-primary')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>

