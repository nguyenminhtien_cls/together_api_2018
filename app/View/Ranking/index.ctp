<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><span class="fa fa-cogs"> Community</span></li>
      <li><span class="fa fa-submenu" style="font-style: italic; font-weight: bold"> Ranking</span></li>
    </ul>
</div>
<div class="box" style="margin-bottom:1px;">
    <hr>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-1" style="margin-left: 15px; margin-right: 0px; padding: 0px; width: 8%;">
                    <div class="form-group" style="padding:0px;">
                      <input type="submit" class="form-control btn btn-<?php echo (!isset($filters['type_top']) || $filters['type_top']=='ALL')?'success':'default';?>" style="padding:0px; border: 0;box-shadow:none;border-radius: 0px;" name="filters[type_top]" value="ALL">
                    </div>
                </div>
                <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding: 0px; width: 10%;">
                    <div class="form-group" style="padding:0px;">
                      <input type="submit" class="form-control btn btn-<?php echo (isset($filters['type_top']) && $filters['type_top']=='ON WEEKEND')?'success':'default';?>" name="filters[type_top]" style="border: 0;box-shadow:none;border-radius: 0px;" value="ON WEEKEND">
                    </div>
                </div>
                <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding: 0px; width: 10%;">
                    <div class="form-group" style="padding:0px;">
                      <input type="submit" class="form-control btn btn-<?php echo (isset($filters['type_top']) && $filters['type_top']=='LAST WEEKEND')?'success':'default';?>" name="filters[type_top]" style="border: 0;box-shadow:none;border-radius: 0px;" value="LAST WEEKEND">
                    </div>
                </div>
                <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding: 0px; width: 10%;">
                    <div class="form-group" style="padding:0px;">
                      <input type="submit" class="form-control btn btn-<?php echo (isset($filters['type_top']) && $filters['type_top']=='TWO WEEKEN AGO')?'success':'default';?>" name="filters[type_top]" style="border: 0;box-shadow:none;border-radius: 0px;" value="TWO WEEKEN AGO">
                    </div>
                </div>
                <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding: 0px; width: 10%;">
                    <div class="form-group" style="padding:0px;">
                      <input type="submit" class="form-control btn btn-<?php echo (isset($filters['level_bonus']) && $filters['level_bonus']=='RANKING BY ADMIN')?'danger':'default';?>" name="filters[level_bonus]" style="border: 0;box-shadow:none;border-radius: 0px;" value="RANKING BY ADMIN">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="username" placeholder="group name" name="filters[group_name]" value="<?= (isset($filters['group_name']))?$filters['group_name'] : ''?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="week" placeholder="week" name="filters[week]" value="<?= (isset($filters['week']))?$filters['week'] : ''?>">
                    </div>
                </div>
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                      <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                      <a href="/ranking" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
            </div>
        </form>
        <table class="table table-striped table-bordered table-user-list">
            <thead>
            <tr>
                <th style="width: 1%"><input type="checkbox" class="check-all"></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('week', 'Week | ').$this->Paginator->sort('month', 'month | ').$this->Paginator->sort('year', 'year '); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('team_id', 'Name group'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('member_score_1', 'member score 1'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('member_score_2', 'menber score 2'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('member_score_3', 'menber score 3'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('score', 'Score'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('level_bonus', 'Level bonus'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('clap','Clap'); ?></th>
                <th style="width:8%"><center>Options</center></th>
            </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
            <?php if (isset($ranking)): ?>
                <?php foreach ($ranking as $item): ?>
                    <tr>
                        <td><input type="checkbox" name="checkbox_id" value="<?= $item['Ranking']['id']?>" id="select-<?= $item['Ranking']['id']?>"></td>
                        <td><?php echo $item['Ranking']['id']; ?></td>
                        <td><?php echo $item['Ranking']['week'].' | '. $item['Ranking']['month']. ' | '.$item['Ranking']['year']; ?></td>
                        <td><?php echo (!empty($item['Team']))?$item['Team']['title']:''; ?></td>
                        <td><?php echo $item['Ranking']['menber_score_1']; ?></td>
                        <td><?php echo $item['Ranking']['menber_score_2']; ?></td>
                        <td><?php echo $item['Ranking']['menber_score_3']; ?></td>
                        <td><?php echo $item['Ranking']['score']; ?></td>
                        <td><?php echo ($item['Ranking']['level_bonus'])?$item['Ranking']['level_bonus']:'No bonus'; ?></td>
                        <td><?php echo $item['Ranking']['clap']; ?></td>
                        <td>
                            <center>
                            <a class="btn btn-success btn-sm" href="ranking/edit/<?php echo $item['Ranking']['id']; ?>">
                                <i class="fa fa-edit"></i>
                            </a>
                            </center>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php unset($ranking); ?>
            <?php endif; ?>
            </tbody>
        </table>
        <div style="text-align: center">
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<!--End table-->




