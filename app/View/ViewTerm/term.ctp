<title>TERM OF SERVICE</title>
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style type="text/css">
*{
	font-family: Arial;
}
.content{
	margin-top: 50px;
	margin-right: 20px;
	margin-left: 20px;
}
.title{
	font-weight: bold;
}

</style>

<div class="row content">
	<div>
		<h4 class="text-center title">TERM OF SERVICE</h4>
	</div>	
	<div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
		<?php
		foreach($data as $item){
			if($item['ViewTerm']['status'] == 'public'){
				echo $item['ViewTerm']['term'];
			}
		}
		?>
	</div>
</div>


