<style type="text/css">
	.body-add{
		background: #fff;
	}
</style>
<script src="<?php echo $this->webroot ?>js/ckeditor/ckeditor.js"></script>
<div class="box">
	<div class="row body-add">
		<div class="col-xs-12 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2" style="margin-top: 50px;">
			<?=$this->Form->create();?>
				<!-- <label>Term</label> -->
				<?=$this->Form->input('term', array('type'=>'textarea','class'=>'ckeditor'))  ?>
				<!-- <label>Privacy</label> -->
				<?=$this->Form->input('policy', array('type'=>'textarea','class'=>'ckeditor'))  ?>
				<?=$this->Form->input('status', array('options'=>array('draft'=>'Draft','public'=> 'Public', 'out of date'=>'Out of date')))?>
				<div class=" " style="margin-top: 20px;">
					<button class="btn btn-primary" type="submit" >Submit</button>
					<input type="reset" class="btn btn-default" value="Reset">
				</div>				
			<?=$this->Form->end();?>
		</div>
	</div>
</div>