<style type="text/css">
.tos{
background: #fff;
}
.add{
    margin-right: 20px;
}
.tb-tos{
    margin: 20px;
}
</style>
<div class="box">
    <div class="row tos">       
        <table class="table tb-tos">
            <thead>
                <tr>
                    <td>
                        <a href="Privacy/add"><button type="button" class="btn btn-primary">Add</button></a>
                    </td>
                </tr>
                <tr>
                    <th class="text-center" width="5%">ID</th>
                    <th class="text-center" width="35%">TERM</th>
                    <th class="text-center" width="35%">PRIVACY OF POLICY</th>
                    <th class="text-center">STATUS</th>
                    <th class="text-center">EDIT</th>
                    <th class="text-center">DELETE</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data as $item){ ?>
                <tr>
                    <!-- id -->
                    
                    <td class="text-center"><?php echo $item['Privacy']['id'] ?></td>
                    <td class="justify"><?php echo $item['Privacy']['term'] ?></td>
                    <td class="justify"><?php echo $item['Privacy']['policy'] ?></td>
                    <!-- Answer --> 
                    <td class="text-center"><?php echo $item['Privacy']['status'] ?></td>
                    <td class="text-center">
                        <a href="Privacy/edit/<?php echo $item['Privacy']['id'] ?>"><span class="btn btn-info glyphicon glyphicon-edit"></span></a>
                    </td>
                    <td class="text-center" >
                        <a onclick="return confirm('Bạn chắc chắn xóa?')" href="Privacy/delete/<?php echo $item['Privacy']['id'] ?>"><span class="btn btn-danger glyphicon glyphicon-trash"></span></a>
                    </td>
                    
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>