<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li>My account</li>
      <li><span style="font-style: italic; font-weight: bold">Change Password</span></li>
    </ul>
</div>
<div class="box" style="margin-bottom:1px;">
    <!--Begin table-->
    <div class="box-header" style="background:#ffffff">
        <a href="javascript:window.history.back();" class="pull-left btn btn-warning btn-sm" style="margin:10px;">BACK</a>
    </div>
    <hr>
    <div class="box-content">
    <?php echo $this->Form->create('Admin', array(
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <?php
        echo $this->Form->input('title',array('label' => 'Group Name', 'value' => AuthComponent::user('account'), 'disabled'));
        echo $this->Form->input('current_password',array());
        echo $this->Form->input('new_password',array());
        echo $this->Form->input('repeat_new_password', array());
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button('Cancel', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Update', array('class' => 'btn btn-primary', 'type' => 'button', 'onclick' => 'confirm_update()')); ?>
    <?= $this->Form->end() ?> 
    </div>
</div>
<script type="text/javascript">
    function confirm_update() {
      swal({
          title: "",
          text: "do you want change password of this account?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
        function(){
          swal("Updated!", "this account has been updated.", "success");
        });  
    }
</script>
</div>
</div>

