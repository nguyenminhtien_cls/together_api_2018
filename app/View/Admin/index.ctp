<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li>Settings</li>
      <li><span style="font-style: italic; font-weight: bold">Admin Users</span></li>
    </ul>
</div>
<div class="box" style="margin-bottom:1px;">
    <!--Begin table-->
    <div class="box-header" style="background:#ffffff">
        <a href="javascript:window.history.back();" class="pull-left btn btn-warning btn-sm" style="margin:10px;">BACK</a>
    </div>
    <hr>
<div class="box-content">
    <table class="table table-striped table-bordered table-user-list">
        <thead>
        <tr>
            <th class="sorting" ><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
            <th class="sorting" ><?php echo $this->Paginator->sort('username', 'Account Login'); ?></th>
            <th class="sorting" >Name</th>
            <th class="sorting" style="width:30%">Password</th>
            <th class="sorting" style="width:2%"><?php echo $this->Paginator->sort('status', 'Status'); ?></th>
            <th style="width:20%"><center>Options</center></th>
        </tr>
        </thead>
        <!-- Here is where we loop through our $users array, printing out post info -->
        <tbody>
        <?php if (isset($users)): ?>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td id="id-preview"><?php echo $user['User']['id']; ?></td>
                    <td>
                        <?php echo $this->Html->link($user['User']['username'],
                            array('controller' => 'users', 'action' => 'view', $user['User']['id'])); ?>
                    </td>
                    <td class="editableColumns"><input type="text" name="name" class="form-control"></td>
                    <td class="editableColumns"><input type="password" name="password" class="form-control"></td>
                    <td>
                        <?php 
                            $status = "false";
                            if ($user['User']['status'] == 1) {
                                $status = "true";
                            }
                        ?>
                        <center>
                            <div class="toggle toggle-modern" style="width: 80px" data-toggle-on="<?=$status?>" data-value="<?=$user['User']['id'];?>" data-url-changestatus="<?= 'admin/changestatus/'.$user['User']['id']?>">
                        </center>
                    </td>
                    <td>
                        <center>
                          <a class="btn btn-warning btn-sm edit-row"
                             href="javascript:;">
                              <i class="fa fa-edit "></i>
                          </a>
                          <a class="btn btn-primary btn-sm update-row" style="display: none;" 
                             href="javascript:;" data-url-update="<?= 'admin/ajaxupdate/'.$user['User']['id']?>">
                              <i class="fa fa-save "></i>
                          </a>
                          <a class="btn btn-danger btn-sm remove-row" data-url-remove="<?= 'admin/delete/'.$user['User']['id']?>"
                             href="javascript:;">
                              <i class="fa fa-trash-o "></i>
                          </a>
                        </center>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php unset($user); ?>
        <?php endif; ?>
        </tbody>
    </table>
    <div style="text-align: center">
        <?php echo $this->element('pagination'); ?>
    </div>
</div>
<script>
    function confirm_update() {
      swal({
          title: "",
          text: "do you want change info of this account?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
        function(){
          swal("Updated!", "this account has been updated.", "success");
        });  
    }

    function confirm_delete() {
      swal({
          title: "",
          text: "do you want remove this acount out of system?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
        function(){
          swal("Deleted!", "account had removed out system", "success");
        });  
    }
</script>
</div>


