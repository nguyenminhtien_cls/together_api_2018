<ul class="breadcrumb">
  <li><span> Dictionary</span></li>
  <li><a href="Words"> Words</a></li>
  <li><span style="font-style: italic; font-weight: bold"> Edit Word</span></li>
</ul>
<div class="box">
<div class="box-content">
    <?php echo $this->Form->create('Word', array(
        'class' => 'form-horizontal ',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'control-label'),
            'between' => '<div class="controls">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <?= $this->Form->input('word', array('type' => 'hidden', )); ?>
        <?= $this->Form->input('pronounce', array('type' => 'text')); ?>
        <br/>
        <h2><b>Meaning:</b></h2>
        <?php
        foreach ($wType as $k => $v) { ?>
            <h3><b><?= $k ?></b></h3>
            <?php foreach ($v as $item => $value) { ?>
                <h4><?= $item ?></h4>
                <div class="from-group">
                    <label class="control-label">Mean:</label>
                    <div class="controls">
                        <input name="data[Word][meaning][type][<?= $k ?>][<?= $item ?>][mean]"
                               class="form-control data-input-mean" data-type="<?= $k ?>"
                               type="text"
                               value="<?= $value['mean'] ?>"/>
                    </div>
                </div>
                <div class="from-group">
                    <label class="control-label">Example:</label>
                    <div class="controls">
                        <input name="data[Word][meaning][type][<?= $k ?>][<?= $item ?>][ex]"
                               class="form-control data-input-ex"
                               type="text"
                               value="<?= $value['ex'] ?>"/>
                    </div>
                </div>
                <?php
            }
        } ?>
        <div class="new-meaning">
            <h3>New meaning:</h3>
            <div class="form-group">
                <label class="control-label">Select word type:</label>
                <div class="controls">
                    <select name="n-type" class="form-control">
                        <option value=""></option>
                        <option value="n.">n.</option>
                        <option value="v.">v.</option>
                        <option value="v.tr.">v.tr.</option>
                        <option value="v.intr.">v.intr.</option>
                        <option value="adj.">adj.</option>
                        <option value="adv.">adv.</option>
                        <option value="interj.">interj.</option>
                        <option value="v.i.">v.i.</option>
                        <option value="tr.v.">tr.v.</option>
                        <option value="adv.|Informal">adv.|Informal</option>
                    </select>
                </div>
            </div>
            <div id="new-meaning-input">
                <div class="from-group">
                    <label class="control-label">Mean:</label>
                    <div class="controls">
                        <input name="n-mean" class="form-control new-mean"
                               type="text"
                               value=""/>
                    </div>
                </div>
                <div class="from-group">
                    <label class="control-label">Example:</label>
                    <div class="controls">
                        <input name="n-ex" class="form-control new-word-ex"
                               type="text"
                               value=""/>
                    </div>
                </div>
            </div>
        </div>
        <br/>
            <button title="Add new meaning" type="button" onclick="showAddMean()" class="btn btn-success">Add new meaning</i></button>&nbsp;
            <a class="btn btn-warning" href="words/edit_raw/<?=$this->request->data['Word']['id']?>">Edit Raw word</a> &nbsp;&nbsp;&nbsp;
            <button type="submit" class="btn btn-primary">Apply</button>&nbsp;
    </fieldset>
    <?= $this->Form->end() ?>
</div>
</div>

