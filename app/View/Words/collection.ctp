<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><span class="fa fa-book"> Dictionary</span></li>
      <li><span class="fa fa-compress" style="font-style: italic; font-weight: bold">  Collections</span></li>
    </ul>
</div>
<div class="box" style="margin-bottom:1px;">
    <hr>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="username" placeholder="Enter keyword" name="filters[group_name]" value="<?= (isset($filters['group_name']))?$filters['group_name'] : ''?>">
                    </div>
                </div>
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                      <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                      <a href="/ranking" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Word</th>
                <th>Category</th>
                <th>Option</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($collections as $collection): ?>
                <tr>
                    <td><?php echo $collection['id']; ?></td>
                    <td>
                        <?php echo $this->Html->link($collection['word'],
                            array('controller' => 'words', 'action' => 'view', $collection['id'])); ?>
                    </td>
                    <td><?php echo $this->Html->link($collection['category'],
                            array('controller' => 'words', 'action' => 'view_cat', $collection['id'])); ?></td>

                    <td>
                        <?php
                        echo $this->Html->link(
                            'Edit',
                            array('action' => 'edit_col', $collection['id'])
                        );
                        ?>
                        <?php
                        echo $this->Form->postLink(
                            'Delete',
                            array('action' => 'delete_col', $collection['id']),
                            array('confirm' => 'Are you sure?')
                        );
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php unset($collection); ?>
            </tbody>
        </table>
        <div style="text-align: center">
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<!--End table-->