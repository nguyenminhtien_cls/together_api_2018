<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-xs-12">

    <?php echo $this->Form->create('WordCategory', array(
        'class' => 'form-horizontal ',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'control-label'),
            'between' => '<div class="controls">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>

        <legend class="title"><?= __('Add Category') ?></legend>
        <?php

        echo $this->Form->input('category_title', array('type' => 'text'));
        echo $this->Form->input('category_description', array('type' => 'textarea'));
        ?>
    </fieldset>
    <?= $this->Form->button('Submit') ?>
    <?= $this->Form->end() ?>
</div>

