<ul class="breadcrumb">
  <li><span> Dictionary</span></li>
  <li><a href="Words"> Words</a></li>
  <li><span style="font-style: italic; font-weight: bold"> Edit</span></li>
</ul>
<div class="box">
    <div class="box-content">
        <table class="table">
            <tr>
                <td class="table_left">Word</td>
                <td><?= !empty($word['Word']['word']) ? $word['Word']['word'] : 'Untitled' ?></td>
            </tr>
            <tr>
                <td class="table_left">Pronounce</td>
                <td><?= !empty($word['Word']['pronounce']) ? $word['Word']['pronounce'] : 'undefined' ?></td>
            </tr>
            <tr>
                <td class="table_left">Meaning</td>
                <td><?php
                    $w = json_decode($word['Word']['meaning_en'])
                    ?>
                    <?php if (!empty($w->type)) {
                        foreach ($w->type as $k => $v) { ?>
                            <ul>
                                <li><?= $k ?> <br> <?php foreach ($v as $k2 => $v2) {
                                        echo $k2 . '. ' . $v2->mean . ' Example: ' . $v2->ex . '<br/>';
                                    } ?></li>
                            </ul>
                        <?php }
                    } ?></td>
            </tr>
        </table>
    <button class="btn btn-info" type="reset">Edit</button>
    </div>
    <br/>
    <br/>
</div>

<style>
    table .table_left {
        background: #eee;
        width: 20%;
    }
</style>
