<div class="box">
    <div class="box-content" style="overflow: hidden">
        <form class="form-horizontal" action="" method="post">
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label for="word" class=" control-label">Word:</label>
                <input type="text" class="form-control" value="<?= $word['Word']['word'] ?>" id="word"
                       name="data[Word][word]">
            </div>
            <div class="form-group">
                <label for="pronounce" class=" control-label">Pronounce:</label>
                <input type="text" class="form-control" value="<?= $word['Word']['pronounce'] ?>"
                       id="pronounce" name="data[Word][pronounce]">
            </div>
            <div class="form-group">
                <label for="meaning_en" class=" control-label">Pronounce:</label>
                <textarea class="form-control" rows="10"
                          id="meaning_en" name="data[Word][meaning_en]"> <?= $word['Word']['meaning_en'] ?> </textarea>
            </div>
            <div class="form-group">
            </div>
            <input type="reset" name="reset" value="Cancel" class="btn btn-default">
            <input type="submit" name="submit" value="Submit" class="btn btn-primary">
        </form>
    </div>
</div>