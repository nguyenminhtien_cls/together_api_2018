<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-xs-12">

    <?php echo $this->Form->create('WordCollection', array(
        'class' => 'form-horizontal ',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'label' => array('class' => 'control-label'),
            'between' => '<div class="controls">',
            'after' => '</div>',
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>

        <legend class="title"><?= __('Add Category') ?></legend>
        <?php

        echo $this->Form->input('word_id', array(
            'options' => $words,
            'label' => 'Select Word: '
        ));
        echo $this->Form->input('category_id', array(
            'options' => $categories,
            'label' => 'Select Category: '
        ));
        ?>
        <multiple-autocomplete ng-model="selectedList"
                               object-property="name"
                               suggestions-arr="optionsList">
        </multiple-autocomplete>

        <script type="text/javascript">

        </script>


    </fieldset>
    <?= $this->Form->button('Submit') ?>
    <?= $this->Form->end() ?>
</div>

