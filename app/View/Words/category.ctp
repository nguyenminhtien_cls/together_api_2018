<ul class="breadcrumb">
  <li>Dictionary</li>
  <li><span style="font-style: italic; font-weight: bold"> Collections</span></li>
</ul>
<div class="box" style="margin-bottom:1px;">
    <hr>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="username" placeholder="Enter keyword" name="filters[group_name]" value="<?= (isset($filters['group_name']))?$filters['group_name'] : ''?>">
                    </div>
                </div>
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                      <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                      <a href="/ranking" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
            </div>
        </form>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th style="width: 30px;"><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                <th style="width: 20%;"><?php echo $this->Paginator->sort('category_title', 'Collection'); ?></th>
                <th >Description</th>
                <th style="width: 10%;"><?php echo $this->Paginator->sort('created_datetime', 'Created'); ?></th>
                <th style="width: 10%">Options</th>
            </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
            <?php foreach ($wordsCategory as $category): ?>
                <tr>
                    <td><?php echo $category['WordCategory']['id']; ?></td>
                    <td>
                        <?php echo $this->Html->link($category['WordCategory']['category_title'],
                            array('controller' => 'words', 'action' => 'view_cat', $category['WordCategory']['id'])); ?>
                    </td>
                    <td><?php echo $category['WordCategory']['category_description']; ?></td>
                    <td><?php echo $category['WordCategory']['created_datetime']; ?></td>

                    <td>
                        <a class="btn btn-info btn-sm"
                           href="words/view_cat/<?php echo $category['WordCategory']['id']; ?>">
                            <i class="fa fa-eye "></i>
                        </a>
                        <a class="btn btn-warning btn-sm"
                           href="words/edit_cat/<?php echo $category['WordCategory']['id']; ?>">
                            <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger btn-sm btn-delete-user confirmDelete"
                           href="javascript:afterConfirm(<?= $category['WordCategory']['id'] ?>);">
                            <i class="fa fa-trash-o "></i>
                        </a>
                        <form action="words/delete_cat/<?= $category['WordCategory']['id'] ?>" method="post"
                              data-id="<?= $category['WordCategory']['id'] ?>">
                            <input type="hidden" name="_method" value="POST">
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php unset($category); ?>
            </tbody>
        </table>
        <?php echo $this->element('pagination'); ?>
    </div>
</div>
<!--End table-->