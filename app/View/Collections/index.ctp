<ul class="breadcrumb">
  <li>Dictionary</li>
  <li><span style="font-style: italic; font-weight: bold"> Collections List</span></li>
</ul>
<div class="box" style="margin-bottom:1px;">
    <hr>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="word" placeholder="Enter collection name ..." name="filters[keysearch]" value="<?= (isset($filters['keysearch']))?$filters['keysearch'] : ''?>">
                    </div>
                </div>
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                      <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                      <a href="/collection" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
            </div>
        </form>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th style="width: 30px;"><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                <th style="width: 20%;"><?php echo $this->Paginator->sort('collection_title', 'Collection name'); ?></th>
                <th >Description</th>
                <th style="width: 10%;"><?php echo $this->Paginator->sort('created_datetime', 'Created'); ?></th>
                <th class="sorting" style="width:10%"><center><?php echo $this->Paginator->sort('status', 'Status'); ?></center>
                <th style="width: 10%">Options</th>
            </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
            <?php foreach ($collections as $r): ?>
                <tr>
                    <td class="id-preview"><?php echo $r['Collection']['id']; ?></td>
                    <td class="editableColumns">
                        <?php echo $this->Html->link($r['Collection']['collection_title'],
                            array('controller' => 'collection', 'action' => 'words_in_collection', $r['Collection']['id'])); ?>
                    </td>
                    <td class="editableColumns"><?php echo $r['Collection']['collection_description']; ?></td>
                    <td><?php echo $r['Collection']['created_datetime']; ?></td>
                    <td>
                        <?php 
                            $status = "false";
                            if ($r['Collection']['status'] == 1) {
                                $status = "true";
                            }
                        ?>
                        <center>
                            <div class="toggle toggle-modern" style="width: 80px" data-toggle-on="<?=$status?>" data-value="<?=$r['Collection']['id'];?>" data-url-changestatus="<?= 'collection/changestatus/'.$r['Collection']['id']?>">
                        </center>
                    </td>
                    <td class="action-data">
                        <a class="btn btn-info btn-sm"
                           href="collection/words_in_collection/<?php echo $r['Collection']['id']; ?>">
                            <i class="fa fa-eye "></i>
                        </a>
                        <a class="btn btn-warning btn-sm edit-row"
                           href="javascript:;">
                            <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-primary btn-sm update-row" style="display: none;" 
                           href="javascript:;" data-url-update="<?= 'collection/ajaxupdate/'.$r['Collection']['id']?>">
                            <i class="fa fa-save "></i>
                        </a>
                        <a class="btn btn-danger btn-sm remove-row" data-url-remove="<?= 'collection/delete/'.$r['Collection']['id']?>"
                           href="javascript:;">
                            <i class="fa fa-trash-o "></i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php unset($collections); ?>
            </tbody>
        </table>
        <?php echo $this->element('pagination'); ?>
    </div>
</div>
<!--End table-->
