<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li>Dictionary</li>
      <li><a href="collection">Collections</a></li>
      <li><span style="font-style: italic; font-weight: bold">Words in collections</span></li>
    </ul>
</div>
<div class="box" style="margin-bottom:1px;">
    <hr>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="word" placeholder="Enter word ..." name="filters[keysearch]" value="<?= (isset($filters['keysearch']))?$filters['keysearch'] : ''?>">
                    </div>
                </div>
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                      <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                      <a href="/collection/words_in_collection/<?= $collection_id?>" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
            </div>
        </form>
        <span class="collection_id" style="display: none;"><?= $collection_id;?></span>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th style="width: 10%;"><?php echo $this->Paginator->sort('word', 'Word'); ?></th>
                <th style="width: 10%"><?php echo $this->Paginator->sort('pronounce', 'Pronounce'); ?></th>
                <th><center>Meaning</center></th>
                <th style="width:12%"><center>Options</center></th>
            </tr>
            </thead>

            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
            <?php foreach ($words as $word): $w = json_decode($word['Word']['meaning_en']) ?>
                <tr>
                    <td class="id-preview" style="display: none;"><?= $word['Word']['id'];?></td>
                    <td>
                        <?php echo $this->Html->link($word['Word']['word'],
                            ['controller' => 'words', 'action' => 'view', $word['Word']['id']]); ?>
                    </td>
                    <td><?php echo $word['Word']['pronounce']; ?></td>
                    <td><?php if (!empty($w->type)) {
                            foreach ($w->type as $k => $v) { ?>
                                <ul>
                                    <li><?= $k ?> <br> <?php foreach ($v as $k2 => $v2) {
                                            echo $k2 . '. ' . $v2->mean . ' Example: ' . $v2->ex . '<br/>';
                                        } ?></li>
                                </ul>
                            <?php }
                        } ?>
                    </td>
                    <td class="action-data">
                        <center>
                        <a class="btn btn-info btn-sm" href="words/view/<?php echo $word['Word']['id']; ?>">
                            <i class="fa fa-eye "></i>
                        </a>
                        <a class="btn btn-warning btn-sm" href="words/edit/<?php echo $word['Word']['id']; ?>">
                            <i class="fa fa-edit "></i>
                        </a>

                        <a class="btn btn-danger btn-sm remove-row" data-url-remove="<?= 'collection/remove_word_out_collection/'.$word['WordCollection']['id'];?>"
                           href="javascript:;">
                            <i class="fa fa-trash-o "></i>
                        </a>
                        </center>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php unset($word); ?>
            </tbody>
        </table>
        <div style="text-align: center">
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<!--End table-->