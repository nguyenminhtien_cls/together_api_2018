<!--begin Search-->
<div class="box">
    <div class="box-header">
        <h2><i class="fa fa-search"></i> Advanced Search </h2>
        <div class="box-icon"><a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a></div>
    </div>
    <div class="box-content">
        <form class="form-inline">
            <div class="form-group">
                <label for="exampleInputName2">Status</label>
                <input style="padding-left: 5px;" type="text" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
            </div>
            <div class="form-group" style="padding-left: 10px;">
                <label for="exampleInputEmail2">Email</label>
                <input style="padding-left: 5px;" type="email" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com">
            </div>
            <div class="form-group" style="padding-left: 10px;">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>

        </form>

    </div>
</div>
<!--end search-->
<!--Begin table-->
<div class="box">
    <div class="box-header">
        <h2><i class="fa fa-search"></i>Users</h2>
        <div class="box-icon">
            <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
        </div>
    </div>
    <div class="box-content">
        <table class="table table-striped table-bordered table-user-list">
            <thead>
                <tr>
                    <th class="sorting" ><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                    <th class="sorting" ><?php echo $this->Paginator->sort('username', 'Account'); ?></th>
                    <th class="sorting" ><?php echo $this->Paginator->sort('name', 'Team'); ?></th>
                    <th class="sorting" style="width:10%"><?php echo $this->Paginator->sort('type', 'Type'); ?></th>
                    <th class="sorting" style="width:10%"><?php echo $this->Paginator->sort('created_datetime', 'Created'); ?></th>
                    <th class="sorting" style="width:10%"><?php echo $this->Paginator->sort('updated_datetime', 'Updated'); ?></th>
                    <th style="width:8%">Option</th>
                </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
                <?php if (isset($users)): ?>
                    <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?php echo $user['User']['id']; ?></td>
                            <td>
                                <?php echo $this->Html->link($user['User']['username'], array('controller' => 'users', 'action' => 'view', $user['User']['id']));
                                ?>
                            </td>
                            <td><?php echo $user['Team']['title']; ?></td>
                            <td><?php echo $user['User']['type']; ?></td>
                            <td><?= date('Y-m-d H:i:s', $user['User']['created_datetime']) ?></td>
                            <td><?= date('Y-m-d H:i:s', $user['User']['updated_datetime']) ?></td>
                            <td>
                                <a class="btn btn-info btn-sm" href="users/view/<?php echo $user['User']['id']; ?>">
                                    <i class="fa fa-search-plus "></i>
                                </a>
                                <!--                            <a class="btn btn-warning btn-sm" href="users/edit/--><?php //echo $user['User']['id'];     ?><!--">-->
                                <!--                                <i class="fa fa-edit "></i>-->
                                <!--                            </a>-->
                                <a class="btn btn-danger btn-sm btn-delete-user confirmDelete"
                                   href="javascript:afterConfirm(<?= $user['User']['id'] ?>);">
                                    <i class="fa fa-trash-o "></i>
                                </a>
                                <form action="users/delete/<?= $user['User']['id'] ?>" method="post"
                                      data-id="<?= $user['User']['id'] ?>">
                                    <input type="hidden" name="_method" value="POST">
                                </form>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php unset($user); ?>
                <?php endif; ?>
            </tbody>
        </table>
        <div style="text-align: center">
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<!--End table-->


