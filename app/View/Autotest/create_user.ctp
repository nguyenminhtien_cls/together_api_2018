<div class="box">
    <div class="box-header">
        <h2><i class="fa fa-search"></i> Auto create user  </h2>
        <div class="box-icon"><a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a></div>
    </div>
    <div class="box-content">
        <form class="form-horizontal validation-required" method="POST">
            <div class="form-group">
                <label class="col-lg-2 control-label">Number of user</label>
                <div class="col-lg-5">
                    <div class="form-row-input">
                        <input type="number" class="form-control" name="number_user"  value=""/>
                    </div>
                </div>
            </div>  
            <div class="form-group">
                <label class="col-lg-2 control-label">Username root</label>
                <div class="col-lg-5">
                    <div class="form-row-input">
                        <input type="text" class="form-control" name="username_root" placeholder="" value=""/>
                    </div>
                </div>
            </div>  
            <div class="form-group">
                <label class="col-lg-2 control-label">Password for user</label>
                <div class="col-lg-5">
                    <div class="form-row-input">
                        <input type="text" class="form-control" name="password"  value=""/>
                    </div>
                </div>
            </div>  
            <div class="form-group">
                <label class="col-lg-2 control-label"></label>
                <div class="col-lg-5">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>  

        </form>
    </div>
</div>


