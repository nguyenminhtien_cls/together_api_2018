<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><a href="games">Games</a></li>
      <li><span style="font-style: italic; font-weight: bold"> Game settings</span></li>
    </ul>
</div>
<div class="box" style="margin-bottom:1px;">
    <!--Begin table-->
    <div class="box-header" style="background:#ffffff">
        <a href="/games" class="pull-left btn btn-warning btn-sm" style="margin:10px;"> BACK</a>
    </div>
    <hr>
    <div class="box-content">
        <div class="row">
            <div id="wrapper">
            <?php echo $this->Session->flash(); ?>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th class="sorting" >Day in week</th>
                        <th class="sorting" >Games</th>
                        <th class="sorting" style="width: 1%"><center>Actions</center></th>
                    </tr>
                    </thead>
                    <!-- Here is where we loop through our $items array, printing out post info -->
                    <tbody>
                        <?php 
                            $days = ['monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday', 'sunday'];
                            if (!empty($days)) :
                                foreach ($days as $k=> $r) :
                        ?>
                            <tr class="<?=$r;?>">
                                <td style="width: 1%"><?= ucfirst($r); ?></td>
                                <td>
                                <?php 
                                    if (!empty($list_apps)): 
                                ?>
                                    <table class="table table-bordered table-<?=$k?>">
                                        <form class="table-form" id="form-<?=$r?>">
                                            <tr>
                                                <th style="width: 20%"><input type="text" name="name" placeholder="Search app on <?= ucfirst($r);?> ..." class="form-control skills" data="<?=$k?>" style="border-color: #4eb5e5; border-style: solid;"></th>
                                                <th>Set time push notifications</th>
                                                <th style="width: 10%">Status</th>
                                            </tr>
                                            <?php 
                                                foreach ($list_apps as $l => $v): 
                                            ?>
                                            <tr>
                                                <td>
                                                    <?= $v['game_name'];?>
                                                    <input type="hidden" name="data[<?=$l?>][game_id]" value="<?= $v['game_id']?>">
                                                    <input type="hidden" name="data[<?=$l?>][day]" value="<?= $r?>">
                                                    <input type="hidden" name="data[<?=$l?>][type_scheduler]" value="<?= $type_scheduler?>">
                                                </td>
                                                <td>
                                                    <span class="btn btn-primary btn-sm show-scheduler" data-id="<?=$l;?>">View</span>
                                                    <table class="table table-bordered table-scheduler" id="table-scheduler-<?=$l;?>" style="display: none;">
                                                    <?php 
                                                        for($i=0; $i<12; $i++) : 
                                                            $hour = ($i<10)?'0'.$i:$i;
                                                            echo "<tr>";
                                                            echo "<td><center>".$i."h</center></td>";
                                                            for($k=0; $k<=50; $k=$k+10) :
                                                                $minuter = ($k<10)?'0'.$k:$k;
                                                                $checked = "";
                                                                if (isset($v['scheduler'][$r][$type_scheduler]) && in_array($hour.':'.$minuter,$v['scheduler'][$r][$type_scheduler])) {
                                                                    $checked = "checked";
                                                                    $value_checked = $hour.':'.$minuter;
                                                                }
                                                                echo "<td><center><input type='radio' name='data[{$l}][{$type_scheduler}][]' value='$hour:$minuter' {$checked}> ".$hour.':'.$minuter."</center></td>";
                                                            endfor;
                                                            echo "<tr>";
                                                        endfor; 
                                                    ?>
                                                    </table>
                                                    <?php 
                                                        echo (isset($value_checked))?'<span style="color:green;">Setup at '.$value_checked.'</span>':'<span style="color:red">not setup</span>';
                                                        unset($value_checked);
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        if (isset($v['scheduler'][$r])) :
                                                            $status = "false";
                                                            if ($v['scheduler'][$r]['status_'.$type_scheduler] == 1) {
                                                                $status = "true";
                                                            }
                                                    ?> 
                                                        <center>
                                                            <div class="toggle toggle-modern" style="width: 80px" data-toggle-on="<?=$status?>" data-value="<?=$v['scheduler'][$r]['id'];?>" data-url-changestatus="<?= 'games/changestatus_scheduler/'.$v['scheduler'][$r]['id'].'/'.$type_scheduler;?>">
                                                        </center>
                                                    <?php
                                                        else :
                                                            echo 'not setup';
                                                        endif;
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php 
                                            endforeach; 
                                        ?>
                                         </form>
                                    </table>
                                <?php 
                                    endif; 
                                ?>
                                </td>
                                <td>
                                   <?php 
                                       if (!empty($list_apps)): 
                                   ?>
                                   <center><input type="button" class="btn btn-sm btn-success save-element" value="save" data="<?= $r;?>"></center>
                                   <?php 
                                       endif; 
                                   ?>
                                </td>
                            </tr>

                            <?php 
                                    endforeach;
                                endif;
                            ?>

                    </tbody>
                </table>
            </div>
        </div>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( ".skills" ).autocomplete({
                  source: 'games/searchapp',
                  select: function( event, ui ) {
                    console.log(ui);
                    var id = $(this).attr('data');
                    var 
                        $table = $(this).closest('.table-'+id), 
                        $tr = $(this).closest('tr').siblings(':last-child'), 
                        $newRow = $tr.clone();
                        $table.append($('<tbody>').append($newRow));
                  }
                });
            });

            $('.show-scheduler').click(function(){
                $('.table-scheduler').hide();
                if ($(this).text()=='View') {
                    var table_scheduler_id = $(this).attr('data-id');
                    $(this).text('Hide');
                    $(this).parents('td').each(function(index, el) {
                        $(this).find('#table-scheduler-'+table_scheduler_id).show();
                    });
                } else {
                    $(this).text('View');
                }
            });

            // Update scheduler push notifications
            $('.save-element').click(function(event) {
                var id = $(this).attr('data');
                var dataform = $("#form-"+id);
                swal({
                  title: "Are you sure update?",
                  text: "",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes",
                  closeOnConfirm: false
                },
                function(){
                  $.ajax({
                        url: 'games/save_setting/',
                        type: 'POST',
                        dataType: 'json',
                        data: dataform.serialize(),
                    })
                    .done(function(res) {
                        swal((res.error!=0)?"Error":"Done",res.message, (res.error!=0)?"error":"success");
                    })
                    .fail(function() {
                        swal("Error","have a problem when update data, please check again", "error");
                    })
                    .complete(function(){
                        id = 'undefined';
                    })
                });
            });
        </script>
    </div>
</div>

