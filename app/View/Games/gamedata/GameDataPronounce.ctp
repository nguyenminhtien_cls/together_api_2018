<style type="text/css">
    .styletab {
        width: 100px !important; margin-left: 0px; margin-right: 0px; padding: 0px; width: 8%;
    }
    .styleinput {
        padding:0px; border: 0;box-shadow:none;border-radius: 0px;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background-color:#78cd51;
    }
</style>
<ul class="breadcrumb">
  <li><span> Game Manager</span></li>
  <li><a href="games"> Games</a></li>
  <li><span style="font-style: italic; font-weight: bold"> Edit Game</span></li>
</ul>
<div class="box">
    <div class="box-header" style="background:#ffffff">
        <ul class="nav nav-tabs">
          <li><a href="games/edit/<?= $detailGame['Game']['id']?>">General</a></li>
          <li class="active"><a href="javascript:;">Game Data</a></li>
          <li><a href="games/gamecalendar/<?= $detailGame['Game']['id']?>">Game Calendar</a></li>
        </ul>
    </div>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-1" style="margin-left: 15px; margin-right: 0px; padding: 0px; width: 8%;">
                    <div class="form-group">
                        <div class="controls">
                          <select name="filters[week]" id="week" class="form-control">
                            <?php 
                                for ($i=1; $i<=52; $i++) : 
                            ?>
                            <option value="<?= $i;?>" <?= (isset($filters['week']) && $filters['week']==$i)?'selected':'';?>><?= 'WEEK '.$i;?></option>
                            <?php 
                                endfor; 
                            ?>
                          </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-1" style="margin-left: 15px; margin-right: 0px; padding: 0px; width: 8%;">
                    <div class="form-group">
                        <div class="controls">
                          <select name="filters[year]" id="year" class="form-control">
                            <?php 
                                for ($i=2016; $i<=2020; $i++) : 
                            ?>
                            <option value="<?= $i;?>" <?= (isset($filters['year']) && $filters['year']==$i)?'selected':'';?>><?= 'YEAR '.$i;?></option>
                            <?php 
                                endfor; 
                            ?>
                          </select>
                        </div>
                    </div>
                </div>
                <?php
                    $days = ['monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday', 'sunday']; 
                    $days = ['monday', 'tuesday', 'wednesday','thursday', 'friday']; 
                    foreach ($days as $t => $v) :
                ?>
                    <div class="col-md-1 styletab" style="<?php if($t==0) echo 'margin-left: 15px;'?>">
                        <div class="form-group">
                          <input type="submit" class="form-control styleinput btn btn-<?php echo (isset($filters['day_in_week']) && $filters['day_in_week']==$v)?'success':'default';?>" name="filters[day_in_week]" value="<?= strtoupper($v);?>">
                        </div>
                    </div>
                <?php 
                    endforeach; 
                ?>
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="team_or_member" placeholder="Enter team or member ..." name="filters[team_or_member]" value="<?= (isset($filters['team_or_member']))?$filters['team_or_member'] : ''?>">
                    </div>
                </div>
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                      <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                      <a href="/games/gamedata/<?= $shortcode?>" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
            </div>
        </form>
        <?php 
            $shortcode = ucfirst($shortcode);
        ?>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th style="width: 30px;"><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                <th style="width: 15%;">Team</th>
                <th style="width: 15%;"><?php echo $this->Paginator->sort('word1'); ?></th>
                <th style="width: 15%;"><?php echo $this->Paginator->sort('word2'); ?></th>
                <th style="width: 15%;"><?php echo $this->Paginator->sort('word3'); ?></th>
                <th style="width: 15%;"><?php echo $this->Paginator->sort('word4'); ?></th>
                <th style="width: 15%;"><?php echo $this->Paginator->sort('word5'); ?></th>
                <th style="width: 10%"><center>Options</center></th>
            </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
            <?php foreach ($words as $r): ?>
                <tr>
                    <td class="id-preview"><?php echo $r['GameData'.$shortcode]['id']; ?></td>
                    <td class="editableColumns"><?php echo $r['Team']['title']; ?></td>
                    <td class="editableColumns"><?php echo $r['GameData'.$shortcode]['word1']; ?></td>
                    <td class="editableColumns"><?php echo $r['GameData'.$shortcode]['word2']; ?></td>
                    <td class="editableColumns"><?php echo $r['GameData'.$shortcode]['word3']; ?></td>
                    <td class="editableColumns"><?php echo $r['GameData'.$shortcode]['word4']; ?></td>
                    <td class="editableColumns"><?php echo $r['GameData'.$shortcode]['word5']; ?></td>
                    <td class="action-data">
                        <center>
                            <a class="btn btn-warning btn-sm edit-row"
                               href="javascript:;">
                                <i class="fa fa-edit "></i>
                            </a>
                            <a class="btn btn-primary btn-sm update-row" style="display: none;" 
                               href="javascript:;" data-url-update="<?= 'games/update_word_in_gamedata/'.$r['GameData'.$shortcode]['id'].'/'.$shortcode?>">
                                <i class="fa fa-save "></i>
                            </a>
                            <a class="btn btn-danger btn-sm remove-row" data-url-remove="<?= 'games/update_word_in_gamedata/'.$r['GameData'.$shortcode]['id'].'/'.$shortcode?>"
                               href="javascript:;">
                                <i class="fa fa-trash-o "></i>
                            </a>
                        </center>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php unset($words); ?>
            </tbody>
        </table>
        <div style="text-align: center">
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>