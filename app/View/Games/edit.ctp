<ul class="breadcrumb">
  <li><span> Game Manager</span></li>
  <li><a href="games"> Games</a></li>
  <li><span style="font-style: italic; font-weight: bold"> Edit Game</span></li>
</ul>
<div class="box">
    <div class="box-header" style="background:#ffffff">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#general">General</a></li>
          <li><a href="games/gamedata/<?= $Game['Game']['short_code'];?>">Game Data</a></li>
          <li><a href="games/gamecalendar/<?= $Game['Game']['id']?>">Game Calendar</a></li>
        </ul>
    </div>
    <?= $this->Session->flash('Game'); ?>
    <div class="box-content">
    <?php echo $this->Form->create('Game', array(
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <div class="tab-content">
            <div id="general" class="tab-pane fade in active">
            <?php
            echo $this->Form->input('id',array('label' => 'id', 'value' => $Game['Game']['id'], 'type' => 'hidden'));
            echo $this->Form->input('name',array('label' => 'Game name', 'value' => $Game['Game']['name']));
            echo $this->Form->input('short_code',array('value' => $Game['Game']['short_code']));
            echo $this->Form->input('description',array('value' => $Game['Game']['description']));
            ?>
            <?php 
            if (trim($Game['Game']['avatar'] != '')) :
                $image = '/uploads/files/'.$Game['Game']['avatar'];
            ?>
            <img src="<?= $image; ?>" style="width: 120px; height: 120px;">
            <?php 
                else:
                echo '<img src="http://placehold.it/230x230" alt="" style="width: 230px; height: 230px;">';
                endif; 
            ?>
            <?php
            echo $this->Form->input('avatar', array('type' => 'file')); 
            echo $this->Form->radio('account_status', ['1' => 'Activated', '0' => 'Deactivated'], ['value' => $Game['Game']['status'],'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;', 'legend' => '<span style="font-size: 14px; padding-top: 7px; font-weight:bold;">Status</span>'])."<br>";
            ?>
            </div>
            <!-- <div id="game-data" class="tab-pane fade">
            </div> -->
            <div id="game-calendar" class="tab-pane fade">
                <h2 style="font-weight:bold; color: #333; margin-left: 0px;"> Schedules push notification</h2>
                <!--Begin table-->
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="sorting" >Day in week</th>
                            <th class="sorting" >Time push notification</th>
                        </tr>
                        </thead>
                        <!-- Here is where we loop through our $items array, printing out post info -->
                        <tbody>
                            <?php 
                                $days = ['monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday', 'sunday'];
                                if (!empty($days)) :
                                    foreach ($days as $r) :
                            ?>
                                <tr>
                                    <td><?= ucfirst($r); ?></td>
                                    <td>
                                        <div class="form-group">
                                          <div class="row">
                                              <div class="col-md-12">
                                                  <div class="form-group">
                                                      <div class="controls">
                                                        <div class="input-group bootstrap-timepicker">
                                                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                            <input type="text" class="form-control time-picker time" id="" value="" disabled="disabled">
                                                        </div>  
                                                      </div>
                                                    </div>
                                              </div>
                                          </div>
                                        </div>
                                    </td>
                                </tr>
                                <?php 
                                        endforeach;
                                    endif;
                                ?>
                        </tbody>
                    </table>
                </div>

        </fieldset>
        <?= $this->Form->button('Cancel', array('class' => 'btn btn-default fire', 'type' => 'reset')); ?>
        <?= $this->Form->button('Update', array('class' => 'btn btn-primary fire')); ?>
        <?= $this->Form->end() ?>
        </div>
</div>
<script src="js/teams.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.time').timepicker({
        });
    });

    $('.tab-list').click(function(event) {
        $('.tab-list').removeClass('btn-success');
        $(this).addClass('btn-success');
        $('.game').hide();
        $('.'+$(this).attr('data')).show();
        $('.fire').show();
        if ($(this).attr('data') == 'game-calendar') {
            $('.fire').hide();
        }
    });
</script>
<style type="text/css">
   .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background-color:#78cd51;
   }
</style>


