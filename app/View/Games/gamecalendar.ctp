<ul class="breadcrumb">
  <li><span> Game Manager</span></li>
  <li><a href="games"> Games</a></li>
  <li><span style="font-style: italic; font-weight: bold"> Edit Game</span></li>
</ul>
<div class="box">
    <div class="box-header" style="background:#ffffff">
        <ul class="nav nav-tabs">
          <li><li><a href="games/edit/<?= $Game['Game']['id']?>">General</a></li></li>
          <li><a href="games/gamedata/<?= $Game['Game']['short_code'];?>">Game Data</a></li>
          <li class="active"><a data-toggle="tab" href="#gamecalendar">Game Calendar</a></li>
        </ul>
    </div>
    <div class="box-content">
        <h2 style="font-weight:bold; color: #333; margin-left: 0px;"> Schedules push notification</h2>
        <!--Begin table-->
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th class="sorting" >Day in week</th>
                    <th class="sorting" >Time push notification</th>
                </tr>
                </thead>
                <!-- Here is where we loop through our $items array, printing out post info -->
                <tbody>
                    <?php 
                        $days = ['monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday', 'sunday'];
                        if (!empty($days)) :
                            foreach ($days as $r) :
                    ?>
                        <tr>
                            <td><?= ucfirst($r); ?></td>
                            <td>
                                <div class="form-group">
                                  <div class="row">
                                      <div class="col-md-12">
                                          <div class="form-group">
                                              <div class="controls">
                                                <div class="input-group bootstrap-timepicker">
                                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                    <input type="text" class="form-control time-picker time" id="" value="" disabled="disabled">
                                                </div>  
                                              </div>
                                            </div>
                                      </div>
                                  </div>
                                </div>
                            </td>
                        </tr>
                        <?php 
                                endforeach;
                            endif;
                        ?>
                </tbody>
            </table>
    </div>
</div>
<style type="text/css">
   .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        background-color:#78cd51;
   }
</style>
