<ul class="breadcrumb">
  <li>Game manager</li>
  <li><span style="font-style: italic; font-weight: bold"> Games</span></li>
</ul>
<div class="box">
	<div class="box-content">
		<div id="wrapper" style="padding: 0 20px;">
			<?= $this->Session->flash('Game'); ?>
			<?php echo $this->Form->create('games')?>
			<div class="row">
		                <a href="/games/setting" class="btn btn-primary pull-right">
					Games
				</a>
				<a href="/games/cronsetting" class="btn btn-primary pull-right">
					Schedule
				</a>
		            </div>
			<div class="row">
			<?php 
				if (!empty($games)) :
					foreach ($games as $g) :
						$image = $g['Game']['avatar'];
						if (!$image || trim($image)=='') {
							$image = 'http://placehold.it/230x230';
						} else {
							$image = '/uploads/files/'.$image;
						}
						$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
    					$color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
    					$color = '#c1c7cc';
			?>
			  <div class="col-md-2">
			    <div class="thumbnail" href="/games/edit/<?= $g['Game']['id'];?>" style="background: <?= $color;?>">
			      <img src="<?= $image; ?>" alt="" style="width: 230px; height: 230px;">
			      <div class="caption">
			        <h3><?= $g['Game']['name'];?></h3>
			        <center><h3>
			        	<a href="/games/edit/<?= $g['Game']['id']; ?>" class="btn btn-info"> Setting</a>
			        </h3>
			        </center>
			      </div>
			    </div>
			  </div>
			<?php 
				endforeach;
				else : 
					echo "<center style='font-weight:bold; color:red;'>No application</center>";
				endif;
			?>
			</div>
		</div>
	</div>
</div>
