<div class="box">
    <div class="box" style="margin-bottom:1px;">
    <div class="box-header" style="background:#ffffff">
        <a href="/games" class="pull-left btn btn-success btn-sm" style="margin:10px;"><i class="fa fa-list"></i> GAMES LIST</a>
    </div>
    <hr/>
    <?= $this->Session->flash('gamelist'); ?>
    <div class="box-content">
    <?php echo $this->Form->create('GameList', array(
        'class' => 'form-horizontal',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <?php
        echo $this->Form->input('name',array('label' => 'Game name'));
        echo $this->Form->input('description',array());
        echo $this->Form->input('avatar', array('type' => 'file')); 
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Create', array('class' => 'btn btn-primary')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>
<script src="js/teams.js"></script>

