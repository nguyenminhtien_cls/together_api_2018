<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li>Community</li>
      <li><span style="font-style: italic; font-weight: bold"> Detail</span></li>
    </ul>
</div>
<div class="col-lg-12">
    <div class="box">
        <div class="box-header">
            <h2><i class="fa fa-edit"></i>Form Elements</h2>
            <div class="box-icon">
                <a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                <a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                <a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="control-label" for="selectError2">Group Select</label>
                    <div class="controls">
                        <select class="form-control" data-placeholder="Your Favorite Football Team" id="selectError2" data-rel="chosen">
                            <option value=""></option>
                            <optgroup label="NFC EAST">
                                <option>Dallas Cowboys</option>
                                <option>New York Giants</option>
                                <option>Philadelphia Eagles</option>
                                <option>Washington Redskins</option>
                            </optgroup>
                            <optgroup label="NFC NORTH">
                                <option>Chicago Bears</option>
                                <option>Detroit Lions</option>
                                <option>Green Bay Packers</option>
                                <option>Minnesota Vikings</option>
                            </optgroup>
                            <optgroup label="NFC SOUTH">
                                <option>Atlanta Falcons</option>
                                <option>Carolina Panthers</option>
                                <option>New Orleans Saints</option>
                                <option>Tampa Bay Buccaneers</option>
                            </optgroup>
                            <optgroup label="NFC WEST">
                                <option>Arizona Cardinals</option>
                                <option>St. Louis Rams</option>
                                <option>San Francisco 49ers</option>
                                <option>Seattle Seahawks</option>
                            </optgroup>
                            <optgroup label="AFC EAST">
                                <option>Buffalo Bills</option>
                                <option>Miami Dolphins</option>
                                <option>New England Patriots</option>
                                <option>New York Jets</option>
                            </optgroup>
                            <optgroup label="AFC NORTH">
                                <option>Baltimore Ravens</option>
                                <option>Cincinnati Bengals</option>
                                <option>Cleveland Browns</option>
                                <option>Pittsburgh Steelers</option>
                            </optgroup>
                            <optgroup label="AFC SOUTH">
                                <option>Houston Texans</option>
                                <option>Indianapolis Colts</option>
                                <option>Jacksonville Jaguars</option>
                                <option>Tennessee Titans</option>
                            </optgroup>
                            <optgroup label="AFC WEST">
                                <option>Denver Broncos</option>
                                <option>Kansas City Chiefs</option>
                                <option>Oakland Raiders</option>
                                <option>San Diego Chargers</option>
                            </optgroup>
                        </select>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <div class="box chat chat-full noOverflow">

        <div class="box-header">
            <h2><i class="fa fa-list"></i>Chat Full width</h2>
            <div class="box-icon">
                <a href="widgets.html#" class="btn-close"><i class="fa fa-times"></i></a>
            </div>
        </div>

        <div class="contacts">
            <ul class="list">
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="status online"></span>
                    <span class="important">4</span>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar2.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="status offline"></span>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar3.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="status busy"></span>
                </li>
            </ul>
        </div>
        <div class="conversation">
            <div class="actions">
                <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar3.jpg" alt="avatar">
                <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar4.jpg" alt="avatar">
            </div>
            <ul class="talk">
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
            </ul>
            <div class="form">
                <input type="text" class="write-message" placeholder="Write Message">
            </div>
        </div>

    </div>

</div>