<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li>Community</li>
      <li><span style="font-style: italic; font-weight: bold"> Conversation</span></li>
    </ul>
</div>
<div class="col-xs-12">
    <div class="box">
        <div class="box-content">
            <div class="row">
                <form class="form-horizontal">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label class="control-label" for="selectError2">Text</label>
                        <input type="text" class="form-control" id="username" placeholder="input text" name="filters[title]" value="">
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <label class="control-label" for="selectError2">Team Select</label>
                        <div class="controls">
                            <select class="form-control" data-placeholder="Team Name" id="selectError" data-rel="chosen">
                                <option value=""></option>
                                <option>196 Jefe Shark Hats</option>
                                <option>2 Legit 2 Quit</option>
                                <option>4 Angry Monkeys</option>
                                <option>4 Fast 4 Furious</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <label class="control-label" for="selectError2">From</label>
                        <div class="controls">
                            <select class="form-control" data-placeholder="Member Name" id="selectError2" data-rel="chosen">
                                <option value=""></option>
                                <option>Bee Gees</option>
                                <option>Biggest Fake Liar</option>
                                <option>Blackberry Pie</option>
                                <option>Black Jelly Beanz</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6">
                        <label class="control-label" for="selectError2">To</label>
                        <div class="controls">
                            <select class="form-control" data-placeholder="Member Name" id="selectError3" data-rel="chosen">
                                <option value=""></option>
                                <option>Bee Gees</option>
                                <option>Biggest Fake Liar</option>
                                <option>Blackberry Pie</option>
                                <option>Black Jelly Beanz</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-xs-12">
                        <button class="btn btn-primary" style="margin-right: 10px">Search</button>
                        <button class="btn btn-default">Reset</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div class="clearfix"></div>
    <div class="box chat chat-full noOverflow">
        <div class="contacts">
            <div class="conversation custom">
                <ul class="talk list" style="overflow-y: scroll;list-style: none;margin: 0;padding: 0;">
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom active">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>
                    <li class="custom">
                        <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                        <span class="name">Łukasz Holeczek</span>
                        <span class="time">1:32PM</span>
                        <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                    </li>

                </ul>

            </div>
            <div class="clearfix"></div>
            <nav aria-label="...">
                <ul class="pager custom">
                    <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
                    <li class="next disabled"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
                </ul>
            </nav>
        </div>

        <div class="conversation">
            <div class="actions">
                <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar3.jpg" alt="avatar">
                <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar4.jpg" alt="avatar">
            </div>
            <ul class="talk">
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
                <li>
                    <img class="avatar" src="https://genesisui.com/demo/simpliq/bootstrap3/assets/img/avatar.jpg" alt="avatar">
                    <span class="name">Łukasz Holeczek</span>
                    <span class="time">1:32PM</span>
                    <div class="message">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</div>
                </li>
            </ul>
        </div>
    </div>
</div>