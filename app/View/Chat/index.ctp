<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li>Community</li>
      <li><span style="font-style: italic; font-weight: bold"> Chats</span></li>
    </ul>
</div>
<div class="box">
    <div class="box-header">
        <h2><i class="fa fa-search"></i>Advanced search</h2>
        <div class="box-icon">
            <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
        </div>
    </div>
    <div class="box-content">
        <form id="frm-conversation-search" class="form-horizontal">
            <div class="form-group">
                <label class="col-md-2 control-label">Period</label>
                <div class="col-md-10">
                    <div class="input-group date" style="float:left; width: 200px;">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" class="form-control date-picker" data-date-format="yyyy/mm/dd"
                               name="filter[datetime][date_from]"
                               value="<?php if (isset($filter['datetime']['date_from'])) {
                                   echo $filter['datetime']['date_from'];
                               } ?>"/>
                    </div>
                    <div class="input-group bootstrap-timepicker" style="float:left; width: 150px; margin-left: 5px;">
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        <input type="text" class="form-control timepicker" name="filter[datetime][time_from]"
                               value="<?php if (isset($filter['datetime']['time_from'])) {
                                   echo $filter['datetime']['time_from'];
                               } ?>"/>
                    </div>

                    <div style="float:left; margin: 5px 20px;">
                        <center>~</center>
                    </div>

                    <div class="input-group date" style="float:left; width: 200px;">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" class="form-control date-picker" data-date-format="yyyy/mm/dd"
                               name="filter[datetime][date_to]"
                               value="<?php if (isset($filter['datetime']['date_to'])) {
                                   echo $filter['datetime']['date_to'];
                               } ?>"/>
                    </div>
                    <div class="input-group bootstrap-timepicker" style="float:left; width: 150px; margin-left: 5px;">
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        <input type="text" class="form-control timepicker" name="filter[datetime][time_to]"
                               value="<?php if (isset($filter['datetime']['time_to'])) {
                                   echo $filter['datetime']['time_to'];
                               } ?>"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Name</label>
                <div class="col-md-10">
                    <div style="float:left; width: 200px; margin-right: 10px;">
                        <input class="form-control" type="text" name="filter[name]"
                               value="<?php if (isset($filter['name'])) {
                                   echo $filter['name'];
                               } ?>" style="width: 248px;"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 control-label">Number of message</label>
                <div class="col-md-10">
                    <div class="input-group" style="float:left; width: 100px;">
                        <input type="number" min="0" class="form-control" name="filter[msg_num][from]"
                               value="<?php if (isset($filter['msg_num']['from'])) {
                                   echo $filter['msg_num']['from'];
                               } ?>"/>
                    </div>

                    <div style="float:left; margin: 5px 20px;">
                        <center>~</center>
                    </div>

                    <div class="input-group date" style="float:left; width: 100px;">
                        <input type="number" min="0" class="form-control" name="filter[msg_num][to]"
                               value="<?php if (isset($filter['msg_num']['to'])) {
                                   echo $filter['msg_num']['to'];
                               } ?>"/>
                    </div>
                </div>
            </div>


            <div class="form-group" style="margin-top: 15px;">
                <div class="col-md-10 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>

    </div>
</div>
<div class="box">
    <div class="box-header">
        <h2><i class="fa fa-comments-o"></i><?= $title_for_layout ?></h2>
        <div class="box-icon">
            <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
        </div>
    </div>
    <div class="box-content">
        <form id="frm-conversation-list" onsubmit="return Chat.delete_conversation()">
            <table class="table table-striped table-bordered table-conversation-list">
                <thead>
                <tr>
                    <th style="width: 40px;"><input type="checkbox" class="conversation-list-checkall"/></th>
                    <th style="text-align: center"><?php echo $this->Paginator->sort('Message.created_date', 'Date', array('url' => array('page' => 1))); ?></th>
                    <th colspan="2" style="text-align: center">Name</th>
                    <th style="text-align: center"><?php echo $this->Paginator->sort('Message.msg_num', 'Message count', array('url' => array('page' => 1))); ?></th>

                    <th style="width: 80px; text-align: center"></th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($data as $item) { ?>

                    <tr id="conversation-<?= $item['Message']['id']; ?>">
                        <td>
                            <input type="checkbox" name="id[]" value="<?= $item['Message']['id']; ?>"/>
                        </td>
                        <td><?= $item['Message']['created_date'] ?></td>
                        <td style="width: 200px;"><?= $item['UserFrom']['name'] ?></td>
                        <td style="width: 200px;"><?= $item['UserTo']['name'] ?></td>
                        <td style="text-align: center"><?= $item['Message']['msg_num'] ?></td>
                        <td style="padding: 4px; text-align: center;">
                            <a class="btn btn-primary btn-sm"
                               href="chat/detail?id=<?= $item['Message']['id'] . $conversation_url; ?>">Detail</i></a>
                        </td>
                    </tr>

                <?php } ?>
                </tbody>
            </table>
            <br/>
            <div class="form-group">
                <b> Delete selected items</b>
                <button type="submit" class="btn btn-danger" style="margin-left: 20px;width: 100px"><b>Delete?</b>
                </button>
            </div>
            <div style="text-align: center">
                <?php echo $this->element('pagination'); ?>
            </div>
        </form>
    </div>
</div>