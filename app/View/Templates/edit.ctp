<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-xs-12">
<!-- <script type=”text/javascript”>
CKEDITOR.replace( '_txt_sendmail_content', {
language: 'vi',
});
</script> -->
<?php echo $this->Html->script('ckeditor/ckeditor');?>

<!-- Include JS file. -->
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/js/froala_editor.min.js'></script>
    <?php echo $this->Form->create('Template', array(
        'class' => 'form-horizontal ',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset id="wrapper">
         <div class="legend">
         <h1 class="title pull-left"><?= __('Edit Template') ?></h1>
         <?php
            echo $this->Html->link(
                 __('Back to List Template'),
                array('controller' => 'Templates','action' => 'index'),
                array('escape' => FALSE,'class' => 'btn btn-info pull-right')
            ); 
        ?>
         </div>
         <div class="clearfix"></div>
         <?php
            echo $this->Session->flash();
           echo $this->Form->input('title', array('type' => 'text','default' => $template['Template']['title']));
           echo $this->Form->input('code', array('type' => 'text','default' => $template['Template']['code'])); 
         ?>
         <label>Content</label>
        <?php 
           echo $this->Form->textarea('content',array('class'=>'ckeditor','value'=> $template['Template']['content']));
         ?>
         <div class="form-actions text-center">
         <?php 
            echo $this->Form->button(
                'Save changes', 
                array('class' => 'btn btn-primary','type' => 'submit')
            ); 
            echo $this->Form->button(
                'Cancel', 
                array('class' => 'btn btn-danger','type' => 'reset')
            );
        ?>
        </div>
    </fieldset>
    <?= $this->Form->end() ?>
</div>

