<!--Begin table-->
<div class="box">
    <div class="box-content" id = "wrapper">
    <div class="row">
    <?php
        echo $this->Form->create('Template');
        $action = array('delete' => 'Delete');
        echo $this->Form->input(
            'action',
            array('options' => $action,'class' => 'form-control','empty' => 'Action','div' => array('class' => 'col-md-2 col-xs-4'
                ),'label' => false,'multiple' => false,)
        );
        echo $this->Form->button('Apply', array('type' => 'button','class' => 'btn btn-info','onclick' => 'check()',));
        echo $this->Html->link(
             __('Add New'),
            array('controller' => 'Templates','action' => 'add'),
            array('escape' => FALSE,'class' => 'btn btn-warning')
        );

    ?>
    </div>
    <?php echo $this->Session->flash(); ?>
        <table class="table table-striped table-bordered table-user-list">
            <thead>
            <tr>
                <th style="width: 1%"><input type="checkbox" class="check-all"></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('title', 'title'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('code', 'code'); ?></th>
                <th style="width:8%">Option</th>
            </tr>
            </thead>
            <!-- Here is where we loop through our $items array, printing out post info -->
            <tbody>
            <?php if (isset($template_all)): ?>
                <?php foreach ($template_all as $item): ?>
                    <tr>
                        <td><input type="checkbox" name="checkbox_id" value="<?= $item['Template']['id']?>" class="checkbox"></td>
                        <td><?php echo $item['Template']['id']; ?></td>
                        <td><?php echo $item['Template']['title'] ?></td>
                        <td><?php echo $item['Template']['code'] ?></td>
                        <td class='text-center'>
                        <?php
                            echo $this->Html->link(
                                 $this->Html->tag('span', '', array('class' => 'fa fa-edit')),
                                array('controller' => 'Templates','action' => 'edit',$item['Template']['id']),
                                array('escape' => FALSE,'class' => 'btn btn-success btn-sm','style' => 'min-width:auto')
                            );

                        ?>
                         <!-- <a class="btn btn-success btn-sm" href="users/edit/<?php ?>">
                                <i class="fa fa-edit"></i>
                            </a> -->
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php unset($item); ?>
            <?php endif; ?>
            </tbody>
        </table>
        <div style="text-align: center">
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<!--End table-->
<script>
function check(){
    var action = $('#TemplateAction').val();
    if(action == ''){
        alert('<?php echo __("Please choose an action") ?>');
        return false;
    } else{
        if ($('.checkbox:checked').size() == 0) {
            alert('<?php echo __('Please choose Item')?>');
            return false;
        }
    }
    if(!confirm('<?php echo __("Are you sure") ?>')){
        return false;   
    } else {
        $("#TemplateIndexForm").submit();
    }
}
</script>

