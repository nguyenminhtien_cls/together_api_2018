<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li>Community</li>
      <li><a href="teams/">Teams</a></li>
      <li><span style="font-style: italic; font-weight: bold">Detail team</span></li>
    </ul>
</div>
<div class="box">
    <div class="box-header" style="background:#ffffff">
        <a href="/teams" class="pull-left btn btn-warning btn-sm" style="margin:10px;">Back</a>
    </div>
    <hr/>
    <div class="box-content">
    <?php echo $this->Form->create('Team', array(
        'class' => 'form-horizontal form-data',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <?php
        $member = '';
        if (!empty($list_member)) {
            foreach ($list_member as $key => $value) {
                $member .= array_values($value)[0].', ';
            }
        }
        echo $this->Form->input('id',array('label' => 'Id', 'id' => 'team_id', 'type' => 'hidden', 'value' => $group_info['id']));
        echo $this->Form->input('title',array('label' => 'Group Name', 'value' => $group_info['title']));
        echo $this->Form->input('list members',array('value' => trim($member, ', '), 'disabled'));
        ?>
        <div style="position:relative;">
        <a class='btn btn-default' href='javascript:;'>
            Choose File...
            <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="data[Team][avatar]"" id ="TeamAvatar" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
        </a>
        &nbsp;
        <span class='label label-info' id="upload-file-info"><?php echo $group_info['avatar']?></span>
        </d}v>
        <?php 
            if ($group_info['avatar']): 
        ?>
            <img src="<?php echo $this->webroot.'uploads/files/'.$group_info['avatar']; ?>" widht="80px" height="80px">
        <?php
            endif; 
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Update', array('class' => 'btn btn-primary ', 'type' => 'button')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>
<script src="js/teams.js"></script>
<?php //echo $this->Form->button('Updated', array('class' => 'btn btn-primary submit-team', 'type' => 'button')); ?>

