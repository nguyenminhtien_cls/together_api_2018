<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li>Community</li>
      <li><span style="font-style: italic; font-weight: bold"> Teams</span></li>
    </ul>
</div>
<div class="box" style="margin-bottom:1px;">
    <hr>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-1" style="margin-left: 15px; margin-right: 0px; padding: 0px; width: 8%;">
                    <div class="form-group" style="padding:0px;">
                      <input type="submit" class="form-control btn btn-<?php echo (!isset($filters['status']) || $filters['status']=='ALL')?'success':'default';?>" style="padding:0px; border: 0;box-shadow:none;border-radius: 0px;" name="filters[status]" value="ALL">
                    </div>
                </div>
                <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding: 0px; width: 10%;">
                    <div class="form-group" style="padding:0px;">
                      <input type="submit" class="form-control btn btn-<?php echo (isset($filters['status']) && $filters['status']=='ACTIVATED')?'success':'default';?>" name="filters[status]" style="border: 0;box-shadow:none;border-radius: 0px;" value="ACTIVATED">
                    </div>
                </div>
                <div class="col-md-1" style="margin-left: 0px; margin-right: 0px; padding: 0px; width: 10%;">
                    <div class="form-group" style="padding:0px;">
                      <input type="submit" class="form-control btn btn-<?php echo (isset($filters['status']) && $filters['status']=='DEACTIVATED')?'success':'default';?>" name="filters[status]" style="border: 0;box-shadow:none;border-radius: 0px;" value="DEACTIVATED">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="username" placeholder="Enter group name or member" name="filters[title]" value="<?= (isset($filters['title']))?$filters['title'] : ''?>">
                    </div>
                </div>
                <!-- <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="member" placeholder="member" name="filters[member]" value="<?= (isset($filters['member']))?$filters['member'] : ''?>">
                    </div>
                </div> -->
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                      <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                      <a href="/teams" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
            </div>
        </form>
        <table class="table table-striped table-bordered table-user-list">
            <thead>
            <tr>
                <!-- <th style="width: 1%"><input type="checkbox" class="check-all"></th> -->
                <th class="sorting" ><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('title', 'Team Name'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('member1_id', 'Member 1'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('member2_id', 'Member 2'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('member3_id', 'Member 3'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('created_datetime','Created'); ?></th>
                <th class="sorting" style="width:10%"><center><?php echo $this->Paginator->sort('status', 'Status'); ?></center></th>
                <th style="width:8%"><center>Options</center></th>
            </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
            <?php if (isset($team_all)): ?>
                <?php foreach ($team_all as $item): ?>
                    <tr>
                        <!-- <td><input type="checkbox" name="checkbox_id" value="<?= $item['Team']['id']?>" id="select-<?= $item['Team']['id']?>"></td> -->
                        <td><?php echo $item['Team']['id']; ?></td>
                        <td><?php echo $item['Team']['title']; ?></td>
                        <?php
                            $td= "<td style='color:#ff3f3f'></td>";
                            for ($i=1; $i<=3 ; $i++) {
                                if (!empty($item['User'])) {
                                    foreach ($item['User'] as $k => $u) {
                                        if ($u['id'] == $item['Team']["member{$i}_id"]) {
                                            $td = "<td><a style='color: #2966b5;' href='/users/view/".$u['id']."'><i class='fa fa-angle-right'></i> ".$u['username']."</a></td>";
                                        } else {
                                            $td = "<td style='color:#ff3f3f'></td>";
                                        }
                                    }
                                } 
                                echo $td;
                            }
                        ?>
                        <td><?= date('Y-m-d', $item['Team']['created_datetime']) ?></td>
                        <td>
                            <?php 
                                $status = "false";
                                if ($item['Team']['is_published'] == 1) {
                                    $status = "true";
                                }
                            ?>
                            <center>
                                <div class="toggle toggle-modern" style="width: 80px" data-toggle-on="<?=$status?>" data-value="<?=$item['Team']['id'];?>" data-url-changestatus="<?= 'teams/changestatus/'.$item['Team']['id']?>">
                            </center>
                        </td>
                        <td>
                            <center>
                            <a class="btn btn-success btn-sm" href="teams/edit/<?php echo $item['Team']['id']; ?>">
                                <i class="fa fa-edit"></i>
                            </a>
                            </center>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php unset($team_all); ?>
            <?php endif; ?>
            </tbody>
        </table>
        <div style="text-align: center">
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<!--End table-->




