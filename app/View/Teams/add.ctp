<div class="box">
    <div class="box" style="margin-bottom:1px;">
    <div class="box-header" style="background:#ffffff">
        <a href="/users" class="pull-left btn btn-success btn-sm" style="margin:10px;"><i class="fa fa-list"></i> TEAMS LIST</a>
    </div>
    <hr/>
    <div class="box-content">
    <?php echo $this->Form->create('Team', array(
        'class' => 'form-horizontal form-data',
        'enctype' => 'multipart/form-data',
        'inputDefaults' => array(
            'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
            'div' => array('class' => 'from-group'),
            'class' => array('form-control'),
            'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        ))); ?>
    <fieldset>
        <?php
        echo $this->Form->input('title',array('label' => 'Group Name'
            ));
        echo $this->Form->input('list_member',array(
            'options' => [], 'class' => 'members-of-team', 'multiple'=>"multiple", 'placeholder'=>'Please search data', "style"=>"width: 100%"
            ));
        echo $this->Form->input('avatar', array('type' => 'file')); 
        ?>
    </fieldset>
    <br>
    <?= $this->Form->button('Reset', array('class' => 'btn btn-default', 'type' => 'reset')); ?>
    <?= $this->Form->button('Created', array('class' => 'btn btn-primary submit-team', 'type' => 'button')); ?>
    <?= $this->Form->end() ?>
    </div>
</div>
<script src="js/teams.js"></script>

