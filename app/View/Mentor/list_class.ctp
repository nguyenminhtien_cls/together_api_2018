<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
        <li><span> Mentor</span></li>
        <li><span style="font-style: italic; font-weight: bold"> List class</span></li>
    </ul>
</div>
<div class="box" style="margin-bottom:1px;">
    <hr>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" placeholder="class ID" name="filters[class_id]" value="<?= (isset($filters['class_id'])) ? $filters['class_id'] : '' ?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <input type="text" class="form-control" id="email" placeholder="Class name" name="filters[class_name]" value="<?= (isset($filters['class_name'])) ? $filters['class_name'] : '' ?>">
                    </div>
                </div>
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                        <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <a href="/mentor/list_class" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
            </div>
        </form>
        <table class="table table-striped table-bordered table-user-list">
            <thead>
                <tr>
                    <th class="sorting" ><?php echo $this->Paginator->sort('roomID', 'Class ID'); ?></th>
                    <th class="sorting" ><?php echo $this->Paginator->sort('naturalName', 'Class name'); ?></th>
                    <th class="sorting" ><?php echo $this->Paginator->sort('description', 'Description'); ?></th>
                    <th class="sorting" ><?php echo $this->Paginator->sort('maxUsers', 'Max Users'); ?></th>
                    <th class="sorting" style="width:10%"><?php echo $this->Paginator->sort('creationDate', 'Created time'); ?></th>
                    <th class="sorting" >Option</th>
                </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
                <?php if (isset($class_data)): ?>
                    <?php foreach ($class_data as $key => $class): ?>
                        <tr>
                            <td><?php echo $class['OfMucRoom']['roomID']; ?></td>
                            <td><?php echo $class['OfMucRoom']['naturalName']; ?></td>
                            <td><?php echo $class['OfMucRoom']['description']; ?></td>
                            <td><?php echo $class['OfMucRoom']['maxUsers']; ?></td>
                            <td><?php echo date('m-d-Y', floor($class['OfMucRoom']['creationDate'] / 1000)); ?></td>
                            <td>
                    <center>
                        <a class="btn btn-info btn-sm" href="mentor/list_member_of_class?class_code=<?php echo $class['OfMucRoom']['roomID']; ?>">
                            <i class="fa fa-eye"></i>
                        </a>
                    </center>
                    </form>
                    </td>
                    </tr>
                <?php endforeach; ?>
                <?php unset($user); ?>
            <?php endif; ?>
            </tbody>
        </table>
        <div style="text-align: center">
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<!--End table-->