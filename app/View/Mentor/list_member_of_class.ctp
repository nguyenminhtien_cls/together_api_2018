<div class="box" style="margin: 0px;">
    <ul class="breadcrumb">
      <li><span> Mentor</span></li>
      <li><span style="font-style: italic; font-weight: bold"> List member</span></li>
    </ul>
</div>
<div class="box" style="margin-bottom:1px;">
    <hr>
    <div class="box-content">
        <form data="search">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="username" placeholder="username" name="filters[username]" value="<?= (isset($filters['username']))?$filters['username'] : ''?>">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                      <input type="text" class="form-control" id="email" placeholder="email" name="filters[email]" value="<?= (isset($filters['email']))?$filters['email'] : ''?>">
                    </div>
                </div>
                <div class="col-md-1" style="padding: 0px;">
                    <div class="form-group">
                      <input type="submit" class="form-control btn btn-success" name="search" value="SEARCH">
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                      <a href="/users" class="btn btn-warning form-control">RESET</a>
                    </div>
                </div>
            </div>
        </form>
        <table class="table table-striped table-bordered table-user-list">
            <thead>
            <tr>
                <th style="width: 1%"><input type="checkbox" class="check-all"></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('username', 'Username'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('email', 'Email'); ?></th>
                <th class="sorting" ><?php echo $this->Paginator->sort('name', 'Team'); ?></th>
                <th class="sorting" style="width:10%"><?php echo $this->Paginator->sort('created_datetime', 'Created time'); ?></th>
                <th class="sorting" style="width:10%"><center><?php echo $this->Paginator->sort('status', 'Status'); ?></center></th>
            </tr>
            </thead>
            <!-- Here is where we loop through our $users array, printing out post info -->
            <tbody>
            <?php if (isset($users)): ?>
                <?php foreach ($users as $key => $user): ?>
                    <tr>
                        <td><input type="checkbox" name="checkbox_id" value="<?= $key?>" id="select-<?= $key?>"></td>
                        <td><?php echo $key; ?></td>
                        <td>
                            <?php echo $this->Html->link($user['OfUser']['username'],
                            		array('controller' => 'users', 'action' => 'view', $user['OfUser']['username'])); ?>
                        </td>
                        <td><?php echo $user['OfUser']['email']; ?></td>
                        <td>Team</td>
                        <td><?= date('Y-m-d', $user['OfUser']['creationDate']/1000) ?></td>
                        <td>
                            <center>
                            <a class="btn btn-info btn-sm" href="users/view/<?php echo $user['OfUser']['username']; ?>">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a class="btn btn-success btn-sm" href="users/edit/<?php echo $user['OfUser']['username'];?>">
                                <i class="fa fa-edit"></i>
                            </a>
                            </center>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php unset($user); ?>
            <?php endif; ?>
            </tbody>
        </table>
        <div style="text-align: center">
            <?php echo $this->element('pagination'); ?>
        </div>
    </div>
</div>
<!--End table-->