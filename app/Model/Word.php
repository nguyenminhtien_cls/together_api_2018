<?php
/**
 * Created by PhpStorm.
 * User: HM
 * Date: 7/29/2016
 * Time: 11:47 AM
 */

App::uses('AppModel', 'Model');

class Word extends AppModel
{
    public $actsAs = array('Containable');
    public $useTable = 'words';
    public $primaryKey = 'id';

    // association models together
    public $hasMany = array(
        'WordCollection' => array(
            'className' => 'WordCollection',
            'foreignKey' => 'word_id'
        ),
        'WordToeicLevel' => array(
            'className' => 'WordToeicLevel',
            'foreignKey' => 'word_id'
        )
    );
    public $hasAndBelongsToMany = array(
        'Collection' =>
            array(
                'className' => 'Collection',
                'joinTable' => 'word_collection',
                'foreignKey' => 'id',
                'associationForeignKey' => 'collection_id'
        ),
        'ToeicLevel' => 
            array(
                'className' => 'ToeicLevel',
                'joinTable' => 'word_toeic_level',
                'foreignKey' => 'id',
                'associationForeignKey' => 'level_id'
            )
    );

    public $validate = array();

    public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        if ((!$this->id) && (empty($this->data[$this->alias][$this->primaryKey]))) {
            $this->data[$this->alias]['created_datetime'] = date("Y-m-d H:i:s");
            $this->data[$this->alias]['updated_datetime'] = date("Y-m-d H:i:s");
        } else {
            $this->data[$this->alias]['updated_datetime'] = date("Y-m-d H:i:s");
            if (isset($this->data['Word']['meaning']) && !empty($this->data['Word']['meaning'])) {
                $mean = array_merge(['word' => $this->data['Word']['word']], ['pronounce' => $this->data['Word']['pronounce']], $this->data['Word']['meaning']);
                if (!empty($_POST['n-type']) && !empty($_POST['n-mean'])) {
                    if (array_key_exists($_POST['n-type'], $mean['type'])) {
                        $mean['type'][$_POST['n-type']][] = ['mean' => $_POST['n-mean'], 'ex' => $_POST['n-ex']];
                    } else {
                        $mean['type'][$_POST['n-type']][1] = ['mean' => $_POST['n-mean'], 'ex' => $_POST['n-ex']];
                    }
                }
                $this->data[$this->alias]['meaning_en'] = json_encode($mean);
            }
        }
        return true;
    }
    
    public function getRandanWord($username)
    {
        $sql = "SELECT words.id FROM words where";
        $sql .= " words.id not in (select game_words.word_id from game_words inner join game_stubs on game_words.stub_id =  game_stubs.id and game_stubs.username = '" . $username . "')";
        $wordCollection = $this->query($sql);
        //$word = $wordCollection[0]["A"]['id'];
        return $wordCollection;
    }

}

?>