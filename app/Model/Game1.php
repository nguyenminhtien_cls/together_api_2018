<?php

/**
 * Created by PhpStorm.
 * User: SonLT
 * Date: 8/2/2016
 * Time: 9:17 AM
 */
class Game1 extends AppModel
{
    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'words_collection';
    // public $wordCollection = null;
    public $tableName = null;

    /*
     * GetRamdom5Words
     * return array word 5 element
     */
    public function  GetRamdom5Words()
    {
        $arrWord = array();

        $arrWord["words1"] = $this->wordCollection[array_rand($this->wordCollection)]["A"]["word"];
        $arrWord["words2"] = $this->wordCollection[array_rand($this->wordCollection)]["A"]["word"];
        $arrWord["words3"] = $this->wordCollection[array_rand($this->wordCollection)]["A"]["word"];
        $arrWord["words4"] = $this->wordCollection[array_rand($this->wordCollection)]["A"]["word"];
        $arrWord["words5"] = $this->wordCollection[array_rand($this->wordCollection)]["A"]["word"];

        return $arrWord;
    }


    /*
     * Get All Words Collection
     *
     */
    public function  GetAllWordsCollection($word_category_id)
    {
        $sql = 'select * from ';
        $sql .= '(SELECT word FROM words_collection';
        $sql .= ' inner join words';
        $sql .= ' where words_collection.word_id = words.id';
        $sql .= ' and category_id=' . $word_category_id . ') AS A';
        $sql .= ' ORDER BY RAND( )';

        $this->wordCollection = $this->query($sql);
    }


    /*
     * Insert Data To Table Game
     */
    public function InsertDataToTableGame($info)
    {
        $this->tableName = 'Game1_Data';

        $sql = 'INSERT INTO '.$this->tableName.' (team_id, words_category, day, month, year, day1, day2, day3, day4, day5) ';
        $sql .= 'VALUES ';
        $sql .= "('".$info['team_id']."', '".$info['words_category']."', '".$info['day']."', '".$info['month']."', '".$info['year']."', '".json_encode($info['day1'])."', '".json_encode($info['day2'])."', '".json_encode($info['day3'])."', '".json_encode($info['day4'])."', '".json_encode($info['day5'])."'); ";

        $this->query($sql);
    }

    /*
     * BackupGameData
     */
    public function BackupGameData()
    {
        $tableName = 'Game1_Data_Bak';

        $sql = 'INSERT INTO '.$tableName.' (team_id, words_category, day, month, year, day1, day2, day3, day4, day5) ';
        $sql .= " SELECT team_id, words_category, day, month, year, day1, day2, day3, day4, day5 FROM  {$this->tableName} WHERE month = " . ( date('m')-2);


        $this->query($sql);
    }

    /*
     * DeleteOldGameData
     */
    public function DeleteOldGameData()
    {
        $tableName = 'Game1_Data';

        $sql = 'DELETE FROM '.$tableName." WHERE month = " . (date('m')-2);

        $this->query($sql);
    }


    /*
     * Create Table Game By Month
     */
    public function CreateTableGameByMonth()
    {
        $tableName = 'Game1_Data_Bak';

        //$sql ='DROP TABLE IF EXISTS '.$this->tableName.';';
        $sql = 'CREATE TABLE  '.$tableName.' (';
        $sql .= ' `team_id` BIGINT( 11 ) NOT NULL ,';
        $sql .= ' `words_category` BIGINT( 11 ) NOT NULL ,';
        $sql .= ' `day` BIGINT( 11 ) NOT NULL ,';
        $sql .= ' `month` BIGINT( 11 ) NOT NULL ,';
        $sql .= ' `year` BIGINT( 11 ) NOT NULL ,';
        $sql .= ' `day1` TEXT NOT NULL ,';
        $sql .= ' `day2` TEXT NOT NULL ,';
        $sql .= ' `day3` TEXT NOT NULL ,';
        $sql .= ' `day4` TEXT NOT NULL ,';
        $sql .= ' `day5` TEXT NOT NULL';
        $sql .= ') ENGINE = INNODB DEFAULT CHARSET = utf8';

        $this->query($sql);
    }

    public function week_number( $date = 'today' )
    {
        $arrDate = array();
        $arrDate['month'] = date("J", time());
        $arrDate['numberOfAWeekInAMonth'] = ceil( date( 'j', strtotime( $date ) ) / 7 );
        $arrDate['year'] = date("Y", time());
        return $arrDate;

    }
}