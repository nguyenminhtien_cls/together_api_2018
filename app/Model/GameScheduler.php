<?php
class GameScheduler extends AppModel
{
    public $useTable = 'game_scheduler'; 
    public $primaryKey = 'id'; 
    public $hasOne = array();
    public $hasMany = array();
    public $belongsTo = array(
    	'Game' => array(
    		'className' => 'Game',
    		'foreginKey' => 'game_id'
		)
	);
    public $hasAndBelongsToMany = array();
    public $validate = array();
}