<?php
class GameResultSentence extends AppModel
{
	public $actsAs = array('Containable');
    public $useTable = 'game_result_sentence'; 
    public $primaryKey = 'id'; 
    public $hasOne = array();
    public $hasMany = array();
    public $belongsTo = array();
    public $hasAndBelongsToMany = array();
    public $validate = array();
}