<?php
App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property Image $Image
 */
class Message extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'messages';
    public $primaryKey = 'id';
        /**
 * Validation rules
 *
 * @var array
 */
    // We just get message that belong to user
    public function getMessagesByConversationID($conversationID, $user_id, $page, $limit){
        
        $messages = array();
        $query_array = array(
            'conditions' => array('conversation_id' => $conversationID,
                'OR' => array(
                    array( 'from_user' => $user_id), 
                    array( 'FIND_IN_SET('.$user_id.', to_user)')
                ),
            ),
            'fields' => array(),
            'order' => array('id' => 'desc'),
            'limit' => $limit,
        );

        if($page != 0){
            $query_array['offset'] = $page * $limit;
        }

        $messages = $this->find('all', $query_array);
        
        return $messages;
    }
}
