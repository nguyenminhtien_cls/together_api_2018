<?php
/**
 * Created by PhpStorm.
 * User: Phan Minh
 * Date: 8/1/2016
 * Time: 10:28 AM
 */

App::uses('AppModel', 'Model');

class WordCategory extends AppModel
{
    public $useTable = 'words_category';
    public $primaryKey = 'id';

    public $validate = array();


    public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        if ((!$this->id) && (empty($this->data[$this->alias][$this->primaryKey]))) {
            $this->data[$this->alias]['created_datetime'] = date("Y-m-d H:i:s");
            $this->data[$this->alias]['updated_datetime'] = date("Y-m-d H:i:s");
        } else {
            $this->data[$this->alias]['updated_datetime'] = date("Y-m-d H:i:s");
        }
        return true;
    }

}

?>