<?php

App::uses('AppModel', 'Model');

class GameData extends AppModel {

    public $useTable = 'game_data';
    public $primaryKey = 'id';
}
