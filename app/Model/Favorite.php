<?php
App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property Image $Image
 */
class Favorite extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'favorites';
    public $primaryKey = 'id';
        /**
 * Validation rules
 *
 * @var array
 */
    // Get favoritee by user id
     //   $status = 0 , 1 , 2 => get all
    public function getFavoriteByUserID($user_id, $me, $status=1){

        $query_array = array(
            'conditions' => array('user_id_2' => $user_id, 'user_id_1' => $me),
            'fields' => array(),
        );

        if ($status != 2){
             $query_array['conditions']['status'] = $status;
        }

        $favorite = $this->find('first', $query_array);
        if(isset($favorite['Favorite']) ){
            return $favorite['Favorite'];
        }
        else{
            return;
        }
    }

    public function getFavoritesByUserID($me){

        $query_array = array(
            'conditions' => array('user_id_1' => $me, 'status' => 1),
            'fields' => array(),
        );

        $favorite = $this->find('all', $query_array);

        return $favorite;
    }
}
