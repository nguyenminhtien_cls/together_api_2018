<?php
/**
 *
 * @author Admin
 *        
 */
class GameDates extends AppModel
{
    // TODO - Insert your code here
    public $useTable = 'game_dates';
    public $primaryKey = 'id';
    public $hasOne = array();
    public $hasMany = array(
        'GameStubs' => array(
            'className' => 'GameStubs',
            'foreginKey' => 'game_date_id'
        )
    );
    public $belongsTo = array();
    public $hasAndBelongsToMany = array();
    public $validate = array();
}

