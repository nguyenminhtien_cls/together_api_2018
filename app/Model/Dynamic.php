<?php
class Dynamic extends AppModel
{
    public $actsAs = array('Containable');
    public $primaryKey = 'id';
    public $hasMany = array();
    public $hasAndBelongsToMany = array();
    public $validate = array();
}