<?php
class GameDataPronounce extends AppModel
{
    public $useTable = 'game_data_pronounce'; 
    public $primaryKey = 'id'; 
    public $hasOne = array();
    public $hasMany = array();
    public $belongsTo = array(
    	'Team' => array(
    		'className' => 'Team',
    		'foreignKey' => 'team_id'
		)
	);
    public $hasAndBelongsToMany = array();
    public $validate = array();
}