<?php
App::uses('AppModel', 'Model');
class Sentence extends AppModel {
    public $useTable = 'tg_sentence';
   	public $primaryKey = 'id_sen'; 
    public $hasOne = array();
    public $hasMany =array(

    	'Question' => array(
    		'className' => 'Question',
    		'foreignKey' => 'id_sen',
            // 'conditions' => array('Question.id_part' => 7)

    	'QuestionSentence' => array(
    		'className' => 'Question',
    		'foreignKey' => 'id_sen',

    	),
    ));
    public $hasAndBelongsToMany = array();
    public $validate = array();
}