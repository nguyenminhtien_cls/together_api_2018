<?php
App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property Image $Image
 */
class Template extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'templates';
    public $primaryKey = 'id';
        /**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'title' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Title is not empty',
            ),
        'code' => array(
            'rule' => 'notEmpty',
            'required' => true,
            'message' => 'Code is not empty',
            ),
        );
}
