<?php
App::uses('AppModel', 'Model');
class Answer extends AppModel {
    public $useTable = 'tg_answer';
   	public $primaryKey = 'id_ans'; 
    public $hasOne = array();
    public $hasMany = array();
    public $belongsTo = array(

    	'Question' => array(
    		'className' => 'Question',
    		'foreignKey' => 'id_ques',

    	'QuestionAnswer' => array(
    		'className' => 'Question',
    		'foreignKey' => 'id_ans',

    	),
        // 'Audio' => array(
        //     'className' => 'Audio',
        //     'foreignKey' => 'id_audio',
        // ),
    ));
    public $hasAndBelongsToMany = array();
    public $validate = array();
}