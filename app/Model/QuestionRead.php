<?php
App::uses('AppModel', 'Model');
class QuestionRead extends AppModel {
	public $useTable = 'question';
	public $primaryKey = 'id';
	public $hasOne = array(
	);
	public $hasMany = array();
	public $hasAndBelongsToMany = array();
	public $validate = array();
}
