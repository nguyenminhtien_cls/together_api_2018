<?php
App::uses('AppModel', 'Model');
class QuestionBankMeta extends AppModel {

    public $useTable = 'question_bank_meta';
   	public $primaryKey = 'id'; 
    public $hasOne = array();
    public $hasMany = array();
    public $belongsTo = array();
    public $hasAndBelongsToMany = array();
    public $validate = array();
}
