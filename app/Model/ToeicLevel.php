<?php
App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property Image $Image
 */
class ToeicLevel extends AppModel
{

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'toeic_levels';
    public $primaryKey = 'id';

    // association models together
    public $hasMany = array(
        'WordToeicLevel' => array(
            'className' => 'WordToeicLevel',
            'foreignKey' => 'level_id'
        )
    );
    public $hasAndBelongsToMany = array(
        'Word' =>
            array(
                'className' => 'Word',
                'joinTable' => 'word_toeic_level',
                'foreignKey' => 'id',
                'associationForeignKey' => 'word_id'
        )
    );

    public function getToeicLevel()
    {
        $toeic_levels = $this->find('all');
        return $toeic_levels;
    }
}