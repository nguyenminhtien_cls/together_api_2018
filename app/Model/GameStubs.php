<?php
/**
 *
 * @author Admin
 *        
 */
class GameStubs extends AppModel
{
    // TODO - Insert your code here
    public $useTable = 'game_stubs';
    public $primaryKey = 'id';
    public $hasOne = array();
    public $hasMany = array(
    );
    public $belongsTo = array(
        'GameTimes' => array(
            'className' => 'GameTimes',
            'foreignKey' => 'game_time_id'
        )
    );
    public $hasAndBelongsToMany = array();
    public $validate = array();
}

