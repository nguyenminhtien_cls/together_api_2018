<?php
App::uses('AppModel', 'Model', 'Team');

/**
 * User Model
 *
 * @property Image $Image
 */
class UploadAttachment extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'uploadAttachment';
    public $primaryKey = 'id';
        /**
 * Validation rules
 *
 * @var array
 */
    // We just get message that belong to user
    public function getFileByFileID($fileID){
        
        $file = array();
        $query_array = array(
            'conditions' => array('id' => $fileID),
            'fields' => array(),
        );

        $file = $this->find('first', $query_array);
        
        return $file;
    }

}
