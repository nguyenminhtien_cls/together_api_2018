<?php
App::uses('AppModel', 'Model');
class Question extends AppModel {
    public $useTable = 'tg_question';
    public $primaryKey = 'id_ques'; 
    public $hasOne = array();
    public $hasMany = array(
        'Answer' => array(
            'className' => 'Answer',
            'foreignKey' => 'id_ques',
        ),
    );
    public $belongsTo = array(
        'Sentence' => array(
            'className' => 'Sentence',
            'foreignKey' => 'id_sen',
            
        ),
        'Audio' => array(
            'className' => 'Audio',
            'foreignKey' => 'id_audio',
        ),
    );
    public $hasAndBelongsToMany = array();
    public $validate = array();
    
    public function paginate($conditions, $fields, $order, $limit, $page = 1, $recursive = null, $extra = array()) {
        $query = $conditions['query'];
        $query= $query . ' LIMIT ' . (($page-1) * $limit) . ', ' . $limit;
        $results = $this->query($query);
        return $results;
    }
    
    public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
        $query = $conditions['query'];
        $sql = 'SELECT COUNT(*) as count FROM (' . $query. ') A';
        $this->recursive = -1;
        $results = $this->query($sql);
        return $results[0][0]['count'];
    }
}