<?php
App::uses('AppModel', 'Model');

class WordCollection extends AppModel {
    public $actsAs = array('Containable');
    public $useTable = 'word_collection';
    public $primaryKey = 'id';

    public $belongsTo = array(
        'Word' => array(
            'className' => 'Word',
            'foreignKey' => 'word_id',
        ),
        'Collection' => array(
            'className' => 'Collection',
            'foreignKey' => 'collection_id',
        )
    );


    public $validate = array(

    );

    public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        if ((!$this->id) && (empty($this->data[$this->alias][$this->primaryKey]))) {
            $this->data[$this->alias]['created_datetime'] = date("Y-m-d H:i:s");
            $this->data[$this->alias]['updated_datetime'] = date("Y-m-d H:i:s");
        } else {
            $this->data[$this->alias]['updated_datetime'] = date("Y-m-d H:i:s");
        }
        return true;
    }

}
?>