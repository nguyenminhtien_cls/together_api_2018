<?php
class Collection extends AppModel
{
	public $actsAs = array('Containable');
    public $useTable = 'collections';
    public $primaryKey = 'id';

    // association models together
    public $hasMany = array(
        'WordCollection' => array(
            'className' => 'WordCollection',
            'foreignKey' => 'collection_id'
        )
    );
    public $hasAndBelongsToMany = array(
        'Word' =>
            array(
                'className' => 'Word',
                'joinTable' => 'word_collection',
                'foreignKey' => 'id',
                'associationForeignKey' => 'word_id'
        )
    );
    public $validate = array();
    
}