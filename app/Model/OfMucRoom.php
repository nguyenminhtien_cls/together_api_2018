<?php
App::uses('AppModel', 'Model', 'OfUser');

class OfMucRoom extends AppModel
{
    public $useTable = 'ofMucRoom';
    public $primaryKey = 'roomID';
    
//    public $hasMany = array(
//        'OfUser' => array(
//            'className' => 'OfMucMember',
//            'foreignKey' => 'roomID',
//        ),
//    );
    
    public function getAllTeamsInfo() {
        $query = "select ofMucRoom.roomID, ofMucMember.nickname from ofMucRoom inner join ofMucRoomProp on ofMucRoom.roomID = ofMucRoomProp.roomID and ofMucRoomProp.name = 'is_new' and ofMucRoomProp.propValue='false' ";
        $query = $query . " inner join ofMucMember on ofMucRoom.roomID = ofMucMember.roomID";
        
        $users = $this->query($query);
        
        return $users;
    }
}

