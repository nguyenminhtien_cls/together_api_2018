<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * User Model
 *
 * @property Image $Image
 */
class User extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'users';
    public $primaryKey = 'id';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'username' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Plesae enter the account',
                'required' => 'create'
            ),
//            'email' => array(
//                'rule' => 'email',
//                'message' => 'Please enter a valid email'
//            ),
            'uniqueAccount' => array(
                'rule' => 'isUnique',
                'message' => 'This username has already been taken.',
                'required' => 'create'
            )
        ),
//        'name' => array(
//            'notEmpty' => array(
//                'rule' => 'notEmpty',
//                'message' => 'Plesae enter the name',
//                'required' => 'create'
//            )
//        ),
//        'birthday' => array(
//            'birthday' => array(
//                'rule' => 'validBirthday',
//                'message' => 'Please enter a valid date and time.'
//            )
//        ),
        'password' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter the password',
                'required' => 'create'
            )
        )
    );

    public function getUserInfo($user_id, $user_fields, $toeic_fields) {
        $user_info = $this->find('first', array(
            'conditions' => array('User.id' => $user_id),
            'fields' => $user_fields
        ));
        $obj = ClassRegistry::init('ToeicLevel');
        $toeic_info = $obj->find('first', array(
            'conditions' => array('id' => $user_info['User']['toeic_level_id']),
            'fields' => $toeic_fields
        ));
        $obj = ClassRegistry::init('OfUser');
        $openfire_info = $obj->find('first', array(
            'conditions' => array('username' => $user_info['User']['id']),
            'fields' => array('username', 'plainPassword')
        ));
        $user['profile'] = $user_info['User'];
        $user['toeic'] = $toeic_info['ToeicLevel'];
        $user['openfire'] = $openfire_info['OfUser'];
        $user['openfire']['server'] = Configure::read('openfire.server');
        return $user;
    }

    public function getUsers($me, $last_user_id, $limit, $view_type) {
        $users = array();
        if ($last_user_id == 0) {
            $users = $this->find('all', array(
                'conditions' => array(/* 'id <>' => $me, */
                    'type <>' => 0),
                'fields' => array(/* 'id','account','name','gender','avatar', 'team_id', 'toeic_level_id' */),
                'order' => array('id' => 'desc'),
                'limit' => $limit
            ));
        } else {
            $users = $this->find('all', array(
                'conditions' => array('id < ' => $last_user_id, /* 'id <>' => $me, */
                    'type <>' => 0),
                'fields' => array('id', 'account', 'name', 'gender', 'avatar'),
                'order' => array('id' => 'desc'),
                'limit' => $limit
            ));
        }
        return $users;
    }

    public function getUserByUserID($user_id) {
        $users = array();
        $user = $this->find('first', array(
            'conditions' => array('id' => $user_id),
            'fields' => array('id', 'username', 'name', 'gender', 'avatar',
                'quote', 'goal', 'toeic_level_id', 'nickname', 'email', 'display', 'team_id', 'team_state'),
        ));
        return $user;
    }

    public function getUsersByPage($me, $page, $limit, $view_type, $name) {

        $users = array();

        $query_array = array(
            'conditions' => array('type <>' => 0/* 'id <>' => $me */, array("(username like '%{$name}%' AND display='username') OR (nickname like '%{$name}%' AND display='nickname') ")),
            'fields' => array(/* 'id','account','name','gender','avatar' */),
            'order' => array('id' => 'desc'),
            'limit' => $limit
        );

        if ($page != 0) {
            $query_array['offset'] = $page * $limit;
        }

        $users = $this->find('all', $query_array);

        return $users;
    }

    public function getRandomFreeUsersByToeicLevel($me, $toeic_level_id) {

        $query_array = array(
            'conditions' => array('toeic_level_id' => $toeic_level_id, 'id <>' => $me, 'team_id' => "0"),
            'fields' => array("COUNT(id) as user_numbers"),
        );

        $user_numbers = $this->find('first', $query_array);

        $random_user = rand(0, $user_numbers[0]['user_numbers'] - 1);
        // Get User
        $query_array = array(
            'conditions' => array('toeic_level_id' => $toeic_level_id, 'id <>' => $me, 'team_id' => "0"),
            'fields' => array(),
            'offset' => $random_user
        );
        $user = $this->find('first', $query_array);
        return $user;
    }

    // auto encrypt password before save
    // auto add value plainPassword, creationDate, modificationDate // for openfire
    public function beforeSave($options = array()) {
        parent::beforeSave($options);

        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'md5'));
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }
        return true;
    }

    public function afterSave($created, $options = array()) {
        
    }

    public function afterDelete() {
        
    }

    // create encrypt password from plain password
    public function encryptPassword($password) {
        $passwordHasher = new SimplePasswordHasher(array('hashType' => 'md5'));
        return $passwordHasher->hash($password);
    }

    public function validBirthday($check) {
        list($yyyy, $mm, $dd) = explode('-', $check['birthday']);
        if (!checkdate($mm, $dd, $yyyy)) {
            return FALSE;
        }
        return TRUE;
    }

    public function checkLoginAccount($account, $password) {
        $account = $this->find('first', array(
            'conditions' => array(
                'account' => $account,
                'password' => $password,
            ),
        ));
        if (!empty($account)) {
            return 0;
        } else {
            return $account['User']['id'];
        }
    }

    public function check_exists_user($username) {
        $data = $this->find('all', array(
            'conditions' => array(
                'username' => $username
            )
        ));
        return $data;
    }

    public function check_email($email, $user_id) {
        $data = $this->find('all', array(
            'conditions' => array(
                'email' => $email,
                "User.id != $user_id"
            )
        ));
        return $data;
    }

    public function get_user_by_email($email) {
        $data = $this->find('first', array(
            'conditions' => array(
                'email' => $email,
            )
        ));
        return $data;
    }

}
