<?php
App::uses('AppModel', 'Model');
class QuestionType extends AppModel {

    public $useTable = 'question_type';
   	public $primaryKey = 'id'; 
    public $hasOne = array(
	);
    // public $hasMany = array(
    //     'Question' => array(
    //         'class' => 'Question',
    //         'foreginKey' => 'type'
    //     )
    // );
    public $hasAndBelongsToMany = array();
    public $validate = array();
}
