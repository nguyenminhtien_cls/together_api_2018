<?php

App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

/**
 * Admin Model
 *
 * @property Image $Image
 */
class Admin extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'admin';
    public $primaryKey = 'id';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'userlogin' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Plesae enter the account',
                'required' => 'create'
            ),
            'uniqueAccount' => array(
                'rule' => 'isUnique',
                'message' => 'This username has already been taken.',
                'required' => 'create'
            )
        ),
        'password' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter the password',
                'required' => 'create'
            )
        )
    );
    // auto encrypt password before save
    // auto add value plainPassword, creationDate, modificationDate // for openfire
    public function beforeSave($options = array()) {
        parent::beforeSave($options);

        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'md5'));
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }
        return true;
    }

    public function afterSave($created, $options = array()) {
        
    }

    public function afterDelete() {
        
    }

    // create encrypt password from plain password
    public function encryptPassword($password) {
        $passwordHasher = new SimplePasswordHasher(array('hashType' => 'md5'));
        return $passwordHasher->hash($password);
    }
}
