<?php
App::uses('AppModel', 'Model');
class QuestionChoice extends AppModel {

    public $useTable = 'question_choice';
   	public $primaryKey = 'id'; 
    public $hasOne = array();
    public $hasMany = array();
    public $belongsTo = array();
    public $hasAndBelongsToMany = array();
    public $validate = array();
}