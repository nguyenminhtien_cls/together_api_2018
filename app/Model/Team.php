<?php

App::uses('AppModel', 'Model', 'User');

/**
 * User Model
 *
 * @property Image $Image
 */
class Team extends AppModel {

    /**
     * Use table
     *
     * @var mixed False or table name
     */
    public $useTable = 'teams';
    public $primaryKey = 'id';
    // public $actsAs = array('Containable');

    /**
     * Validation rules
     *
     * @var array
     */
    // We just get message that belong to user

    public $hasMany = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'team_id',
        ),
    );
    public $validate = array(
        // 'member1_id' => array(
        //     'rule' => 'notEmpty',
        //     'required' => true,
        //     'message' => 'Member 1 is not empty'
        // ),
    );

    public function getTeamByUserID($user_id) {

        $team = array();

        $query_array = array('member1_id' => $user_id,
            'OR' => array('member2_id' => $user_id),
            'OR' => array('member3_id' => $user_id),
        );

        $team = $this->find('first', $query_array);

        return $team;
    }

    /*
     * getAllTeamsInfo()
     * return array
     * fields : 'id', 'category_id','member1_id', 'member2_id', 'member3_id'
     */

    public function getAllTeamsInfo() {
        $team = array();

        $query_array = array(
            'conditions' => array(
                'status' => 3
            ),
            'fields' => array('id', 'category_id', 'member1_id', 'member2_id', 'member3_id'),
            'order' => array('created_datetime' => 'desc'),
        );

        $team = $this->find('all', $query_array);

        return $team;
    }

    public function getTeamByID($team_id) {
        $team = array();

        $team = $this->find('first', array(
            'conditions' => array('id' => $team_id),
            'fields' => array(),
        ));
        if (isset($team["Team"])) {
            return $team["Team"];
        } else {
            return false;
        }
    }

    public function getActiveTeams() {

        $team = array();

        $query_array = array('id' => $user_id,
        );

        $team = $this->find('first', $query_array);

        return $team;
    }

    public function getAllTeams() {

        $team = array();

        $query_array = array(
            'conditions' => array(),
            'fields' => array(),
            'order' => array('created_datetime' => 'desc'),
            'limit' => $limit,
        );

        $team = $this->find('first', $query_array);

        return $team;
    }

    public function joinTeam($team_id, $user_id) {

        $joinInfo = $this->joinTeamBySlot($team_id, $user_id, 2);
        if (isset($joinInfo['team_info'])) {
            // Do nothing
        } else {
            $joinInfo = $this->joinTeamBySlot($team_id, $user_id, 3);
        }
        return $joinInfo;
    }

    /*
     * checkUserExistInTeam
     *
     */

    public function checkUserExistInTeam($team_id, $user_id) {
        $user_ids = $this->find('all', array(
            'conditions' => array('id' => $team_id),
            'fields' => array('member1_id', 'member2_id', 'member3_id'),
        ));

        if (in_array($user_id, $user_ids))
            return true;

        return false;
    }

    public function joinTeamBySlot($team_id, $user_id, $slot) {
        $team = $this->getTeamByID($team_id);

        if (!$team)
            return array('message' => 'The team doesn\'n exist.');

        if ($team["member1_id"] == $user_id || $team["member2_id"] == $user_id || $team["member2_id"] == $user_id) {
            return array('message' => 'You are already in this team.');
        }
        $user_model = new User();
        $user_info = $user_model->getUserByUserID($user_id);
        $user_info = $user_info['User'];
        if ($team) {
            switch ($slot) {
                case 1:
                    // Check slot 1 is availabel
                    if ($team["member1_id"]) {
                        return array('message' => 'This position is occupied, please refresh this list.');
                    } else {
                        $team['member1_id'] = $user_id;
                        $team['member1_join_date'] = time();
                        $team['title'] .= (', ' . $user_info['name']);
                        $team['status'] = $team['status'] + 1;
                        $user_info['team_id'] = $$team_id;
                        $user_model->save($user_info);

                        $owner_info = $user_model->getUserByUserID($team["member1_id"]);
                        $owner_info = $owner_info["User"];
                        $owner_info["team_id"] = $team_id;
                        $user_model->save($owner_info);
                    }

                    $this->save($team);

                    return array('message' => 'Congratulation! You have joined this team.', 'team_info' => $team);
                    // Insert to 1
                    break;

                case 2:
                    // Check slot 1 is availabel
                    // Insert to 1
                    if ($team["member2_id"]) {
                        return array('message' => 'This position is occupied, please refresh this list.');
                    } else {
                        $team['member2_id'] = $user_id;
                        $team['member2_join_date'] = time();
                        $team['title'] .= (', ' . $user_info['name']);
                        $team['status'] = $team['status'] + 1;
                        $user_info['team_id'] = $team_id;
                        $user_model->save($user_info);
                        //
                        $owner_info = $user_model->getUserByUserID($team["member1_id"]);
                        $owner_info = $owner_info["User"];
                        $owner_info["team_id"] = $team_id;
                        $user_model->save($owner_info);
                    }
                    $this->save($team);

                    return array('message' => 'Congratulation! You have joined this team.', 'team_info' => $team);

                    break;

                case 3:
                    // Check slot 1 is availabel
                    // Insert to 1
                    if ($team["member3_id"]) {
                        return array('message' => 'This position is occupied, please refresh this list.');
                    } else {
                        $team['member3_id'] = $user_id;
                        $team['member3_join_date'] = time();
                        $team['title'] .= (', ' . $user_info['name']);
                        $team['status'] = $team['status'] + 1;
                        $user_info['team_id'] = $team_id;
                        $user_model->save($user_info);
                        //
                        $owner_info = $user_model->getUserByUserID($team["member1_id"]);
                        $owner_info = $owner_info["User"];
                        $owner_info["team_id"] = $team_id;
                        $user_model->save($owner_info);
                    }

                    $this->save($team);
                    return array('message' => 'Congratulation! You have joined this team.', 'team_info' => $team);

                    break;

                default:
                    return array('message' => 'Fail to join this team, maybe this team is full filled!');
                    //  Join to any pos free
                    break;
            }
        }
    }

    public function leaveTeam($team_id, $user_id) {
        $team = $this->getTeamByID($team_id);
        $leave_success = array("code" => 0, "message" => "Leave successfull");

        if ($team["member1_id"] == $user_id) {
            $team["member1_id"] = NULL;
            $team["member1_leave_date"] = time();
            $team['status'] = $team['status'] - 1;
            $this->save($team);
            return $leave_success;
        }
        if ($team["member2_id"] == $user_id) {
            $team["member2_id"] = NULL;
            $team["member2_leave_date"] = time();
            $team['status'] = $team['status'] - 1;
            $this->save($team);
            return $leave_success;
        }
        if ($team["member3_id"] == $user_id) {
            $team["member3_id"] = NULL;
            $team["member3_leave_date"] = time();
            $team['status'] = $team['status'] - 1;
            $this->save($team);
            return $leave_success;
        }

        return array("code" => 140000, "message" => "Leave unsuccessfull");
    }

    public function getTeamsByPage($page, $limit, $name) {

        $team = array();

        $query_array = array(
            'conditions' => array(
                array("title like '%{$name}%'"),
                array("(member1_id IS NOT NULL AND member2_id IS NOT NULL) OR (member1_id IS NOT NULL AND member3_id IS NOT NULL) OR (member2_id IS NOT NULL AND member3_id IS NOT NULL)")
            ),
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'UserFirstJoin',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'UserFirstJoin.id = Team.member1_id'
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'UserSecondJoin',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'UserSecondJoin.id = Team.member2_id'
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'UserThirdJoin',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'UserThirdJoin.id = Team.member3_id'
                    )
                )
            ),
            'fields' => array('Team.*',
                'UserFirstJoin.name AS member1_name',
                'UserFirstJoin.created_datetime AS member1_created_date',
                'UserFirstJoin.avatar AS member1_avatar',
                'UserSecondJoin.name AS member2_name',
                'UserSecondJoin.created_datetime AS member2_created_date',
                'UserSecondJoin.avatar AS member2_avatar',
                'UserThirdJoin.name AS member3_name',
                'UserThirdJoin.created_datetime AS member3_created_date',
                'UserThirdJoin.avatar AS member3_avatar'
            ),
            'order' => array('created_datetime' => 'desc'),
            'limit' => $limit,
        );
        if ($page != 0) {
            $query_array['offset'] = $page * $limit;
        }
        $team = $this->find('all', $query_array);

        return $team;
    }

    public function getTeamsByTeamID($team_id) {
        $date = new DateTime(date('Y-m-d'));
        $week = $date->format("W");
        $team = array();

        $query_array = array(
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'UserFirstJoin',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'UserFirstJoin.id = Team.member1_id'
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'UserSecondJoin',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'UserSecondJoin.id = Team.member2_id'
                    )
                ),
                array(
                    'table' => 'users',
                    'alias' => 'UserThirdJoin',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'UserThirdJoin.id = Team.member3_id'
                    )
                ),
                array(
                    'table' => 'ranking',
                    'alias' => 'Ranking',
                    'type' => 'LEFT',
                    'conditions' => array(
                        "Ranking.team_id = {$team_id} AND Ranking.week={$week}"
                    )
                )
            ),
            'conditions' => array(array('Team.id' => $team_id)),
            'fields' => array('Team.*', 'Ranking.*',
                'UserFirstJoin.name AS member1_name',
                'UserFirstJoin.created_datetime AS member1_created_date',
                'UserFirstJoin.avatar AS member1_avatar',
                'UserSecondJoin.name AS member2_name',
                'UserSecondJoin.created_datetime AS member2_created_date',
                'UserSecondJoin.avatar AS member2_avatar',
                'UserThirdJoin.name AS member3_name',
                'UserThirdJoin.created_datetime AS member3_created_date',
                'UserThirdJoin.avatar AS member3_avatar'
            ),
            'order' => array('created_datetime' => 'desc'),
        );

        $team = $this->find('first', $query_array);

        return $team;
    }

    public function saveTeamInfo($teamInfo) {
        return $this->save($teamInfo);
    }

}
