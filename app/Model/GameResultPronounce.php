<?php
class GameResultPronounce extends AppModel
{
	public $actsAs = array('Containable');
    public $useTable = 'game_result_pronounce'; 
    public $primaryKey = 'id'; 
    public $hasOne = array();
    public $hasMany = array();
    public $belongsTo = array();
    public $hasAndBelongsToMany = array();
    public $validate = array();
}