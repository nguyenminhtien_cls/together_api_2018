<?php
App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property Image $Image
 */
class Conversation extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'conversations';
    public $primaryKey = 'id';
        /**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(

	);

    public function getConversationInfo($convesation_id, $convestaion_fields){
        $conversationInfo = $this->find('first',array(
            'conditions' => array('Conversation.id' => $convesation_id),
            'fields' => $convestaion_fields
        ));
        
        return $conversationInfo;
    }

    public function getConversationInfoByFriendID($owner_id, $friend_id, $convestaion_fields = NULL){

        $conversationInfo = $this->find('first',array(
            'conditions' => array(               
                'OR' => array(
                    array('owner_id' => $owner_id, " FIND_IN_SET('{$friend_id}', user_list) ",),
                    array('owner_id' => $friend_id," FIND_IN_SET('{$owner_id}', user_list) "),
                    ),
                ),
            'fields' => $convestaion_fields ? $convestaion_fields : array(),
        ));

        return $conversationInfo;
    }

    public function getMyConversationInfos($owner_id, $page, $limit, $title){
        
        $conversations = array();

        $query_array = array(
            'conditions' => array('owner_id' => $owner_id, " title like '%{$name}%' "),
            'fields' => array(),//array('id','owner_id','user_list','title','unread', "last_sender", "last_mess", "updated_datetime", "created_datetime"),
            'order' => array('id' => 'desc'),
            'limit' => $limit
        );

        if($page != 0){
            $query_array['offset'] = $page*$limit;
        }

        $conversations = $this->find('all', $query_array);
    
        return $conversations;
    }
}
