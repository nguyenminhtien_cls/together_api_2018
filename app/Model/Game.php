<?php

App::uses('AppModel', 'Model');

class Game extends AppModel {

    public $useTable = 'games';
   	public $primaryKey = 'id'; 
    public $hasOne = array();
    public $hasMany = array(
    	'GameScheduler' => array(
    		'className' => 'GameScheduler',
    		'foreginKey' => 'game_id'
		)
	);
    public $belongsTo = array();
    public $hasAndBelongsToMany = array();
    public $validate = array();
}
