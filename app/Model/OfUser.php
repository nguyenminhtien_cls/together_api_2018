<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 * @property Image $Image
 */
class OfUser extends AppModel {

    public $useTable = 'ofUser';
    public $primaryKey = 'username';

    public function beforeSave($options = array()) {
        parent::beforeSave($options);

        if (!$this->username) { // insert
            $this->data[$this->alias]['plainPassword'] = uniqid();
            $this->data[$this->alias]['creationDate'] = '00' . time() . '000';
            $this->data[$this->alias]['modificationDate'] = '00' . time() . '000';
        } else {
            $this->data[$this->alias]['modificationDate'] = '00' . time() . '000';
        }

        return true;
    }
    public function get_user_by_email($email) {
    	$data = $this->find('first', array(
    			'conditions' => array(
    					'email' => $email,
    			)
    	));
    	return $data;
    }
    public function check_email($email, $user_id) {
    	$data = $this->find('all', array(
    			'conditions' => array(
    					'email' => $email,
    					"User.id != $user_id"
    			)
    	));
    	return $data;
    }
    public function check_exists_user($username) {
    	$data = $this->find('all', array(
    			'conditions' => array(
    					'username' => $username
    			)
    	));
    	return $data;
    }
    public function getUserByUserID($user_id) {
    	$users = array();
    	$user = $this->find('first', array(
    			'conditions' => array('id' => $user_id),
    			'fields' => array('id', 'username', 'name', 'gender', 'avatar',
    					'quote', 'goal', 'toeic_level_id', 'nickname', 'email', 'display', 'team_id', 'team_state'),
    	));
    	return $user;
    }
    public function getUserInfo($user_id, $user_fields, $toeic_fields) {
    	$user_info = $this->find('first', array(
    			'conditions' => array('User.id' => $user_id),
    			'fields' => $user_fields
    	));
    	$obj = ClassRegistry::init('ToeicLevel');
    	$toeic_info = $obj->find('first', array(
    			'conditions' => array('id' => $user_info['User']['toeic_level_id']),
    			'fields' => $toeic_fields
    	));
    	$obj = ClassRegistry::init('ofUser');
    	$openfire_info = $obj->find('first', array(
    			'conditions' => array('username' => $user_info['User']['id']),
    			'fields' => array('username', 'plainPassword')
    	));
    	$user['profile'] = $user_info['User'];
    	$user['toeic'] = $toeic_info['ToeicLevel'];
    	$user['openfire'] = $openfire_info['OfUser'];
    	$user['openfire']['server'] = Configure::read('openfire.server');
    	return $user;
    }

}

?>
    