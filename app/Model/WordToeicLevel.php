<?php
App::uses('AppModel', 'Model');

class WordToeicLevel extends AppModel {
    public $actsAs = array('Containable');
    public $useTable = 'word_toeic_level';
    public $primaryKey = 'id';

    public $belongsTo = array(
        'Word' => array(
            'className' => 'Word',
            'foreignKey' => 'word_id',
        ),
        'ToeicLevel' => array(
            'className' => 'ToeicLevel',
            'foreignKey' => 'level_id',
        )
    );

    public $validate = array(

    );

    public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        if ((!$this->id) && (empty($this->data[$this->alias][$this->primaryKey]))) {
            $this->data[$this->alias]['created_datetime'] = date("Y-m-d H:i:s");
            $this->data[$this->alias]['updated_datetime'] = date("Y-m-d H:i:s");
        } else {
            $this->data[$this->alias]['updated_datetime'] = date("Y-m-d H:i:s");
        }
        return true;
    }

}
?>