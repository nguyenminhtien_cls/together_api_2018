<?php

App::uses('AppModel', 'Model');

class GameList extends AppModel {

    public $useTable = 'game_list';
   	public $primaryKey = 'id'; 
    public $hasOne = array();
    public $hasMany = array();
    public $belongsTo = array();
    public $hasAndBelongsToMany = array();
    public $validate = array();
}
