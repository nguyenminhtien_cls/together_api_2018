<?php
App::uses('AppModel', 'Model');
class Audio extends AppModel {
    public $useTable = 'tg_audio';
   	public $primaryKey = 'id_audio'; 
    public $hasOne = array();
    public $hasMany = array(
    	'Question' => array(
    		'className' => 'Question',
    		'foreignKey' => 'id_audio'
    	)
    );
    public $hasAndBelongsToMany = array();
    public $validate = array();
}