<?php
/**
 *
 * @author Admin
 *        
 */
class GameWords extends AppModel
{
    // TODO - Insert your code here
    public $useTable = 'game_words';
    public $primaryKey = 'id';
    public $hasOne = array();
    public $hasMany = array();
    public $belongsTo = array(
        'Meaning' => array(
            'className' => 'Word',
            'foreignKey' => 'word_id'
        )
    );
    public $hasAndBelongsToMany = array();
    public $validate = array();
}

