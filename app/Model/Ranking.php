<?php

App::uses('AppModel', 'Model');

class Ranking extends AppModel {

    public $useTable = 'ranking';
    public $belongsTo = array(
        'Team' => array(
            'className' => 'Team',
            'foreignKey' => 'team_id',
        )
    );

    public function get_ranking() {
        $rank_data = $this->find('all', array(
            'fields' => array('Ranking.team_id', 'Ranking.score', 'Ranking.clap', 'Team.title', 'Team.avatar', 'Team.title', 'Team.member1_id', 'Team.member2_id', 'Team.member3_id',),
            'conditions' => array(
                'Ranking.week' => date('W'),
                'Ranking.month' => date('m'),
                'Ranking.year' => date('Y'),
//                'Ranking.words_category' => $condition['words_category'],
            ),
            'order' => array('Ranking.score DESC'),
            'joins' => array(
                array(
                    'table' => 'teams',
                    'alias' => 'Team',
                    'type' => 'inner',
                    'conditions' => array(
                        "Ranking.team_id = Team.id"
                    )
                )
            ),
            'limit' => 40,
        ));
        return $rank_data;
    }

    public function get_ranking_for_team($team_id = 0, $week, $month, $year) {
        $week = !empty($week) ? $week : date('W');
        $month = !empty($month) ? $month : date('m');
        $year = !empty($year) ? $year : date('Y');
        try {
            $rank_data = $this->find('all', array(
                'fields' => array('Ranking.team_id'),
                'conditions' => array(
                    'Ranking.week' => $week,
//                    'Ranking.month' => $month,
                    'Ranking.year' => $year,
//                'Ranking.words_category' => $condition['words_category'],
                ),
                'order' => array('Ranking.score DESC'),
            ));
            $rank_data = Set::extract('/Ranking/.', $rank_data);
            $key = array_search($team_id, array_column($rank_data, 'team_id'));
            if (false !== $key) {
                $ranking = $key + 1;
            } else {
                 $ranking = 0;
            }
            return $ranking;
        } catch (Exception $exc) {
            echo $exc->getMessage();
            die;
        }
    }

}
