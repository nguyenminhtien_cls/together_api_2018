/**
 * teams.js
 * Author : hungsama@gmail.com
 * Created : 12/01/2017
 */
$(document).ready(function() {
	var processDataForm = $('.submit-team'); 
	// action create or edit a team
	processDataForm.click(function(event) {
		checkExistTeamUser();
	});

	var checkExistTeamUser = function () {
		$.ajax({
			url: '/teams/checkExistTeamUser',
			type: 'POST',
			data: $('.form-data').serialize(),
			success : function(res) {
				console.log(res);
				if (res) {
					swal({
					  title: "",
					  text: res,
					  type: "warning",
					  showCancelButton: true,
					  confirmButtonColor: "#DD6B55",
					  confirmButtonText: "Agree",
					  closeOnConfirm: false
					},
					function(){
						processFormTeam();
					});
				} else {
					processFormTeam();
				}
			}
		});
	}

	var processFormTeam = function () {
		var dataPost = new FormData($(".form-data")[0]);
		var url = '/teams/add/';
		if ($('#team_id').val() != undefined) {
			url = '/teams/edit/'+$('#team_id').val();
		}
		$.ajax({
			url: url,
			type: 'post',
			data: dataPost,
		    processData: false,
		    contentType: false,
			success : function (response) {
				var response = $.parseJSON(response);
				if (!response.error) {
					window.location.href = 'teams/edit/'+response.data.id;
				} else {
					sweetAlert("", response.msg, "error");
				}
			}
		});
	}

	$(".members-of-team").select2({
        maximumSelectionLength: 3,
            ajax: {
            url: "teams/searchmembers",
            dataType: 'json',
            delay: 250,
        data: function (params) {
            return {
                q: params.term, // search term
                page: params.page
            };
        },
        processResults: function (data, params) {
          params.page = params.page || 1;
          return {
            results: data.items,
            pagination: {
              more: (params.page * 30) < data.total_count
            }
          };
        },
        cache: false
      },
      escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
          minimumInputLength: 1,
          templateResult: formatRepo, // omitted for brevity, see the source of this page
          templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
});

function formatRepo (repo) {
    var markup = "<img src='"+repo.avatar+"' width='40' height='40'> " + repo.username + "</div>";
    return markup;
  }

function formatRepoSelection (repo) {
    return repo.username || repo.nickname;
}