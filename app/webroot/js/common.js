$('.check-all').click(function() {
    var checkboxes = $(this).closest('table').find(':checkbox');
    if($(this).is(':checked')) {
        checkboxes.prop('checked', true);
    } else {
        checkboxes.prop('checked', false);
    }
});

function action(value, url) {
    var checked = new Array();
    $('.table-user-list input:checkbox[name=checkbox_id]:checked').each(function() {
        if(this.checked) {
            checked.push($(this).val());
        }
    });
    if(checked.length == 0) {
        swal("",'Please select items list','warning');
        $('#change_data').val('');
        return;
    }
    if (["activated", "deactivated"].indexOf(value) != -1) {
    	var url = url+'/changestatus/'+value;
    } else if (["removed"].indexOf(value) != -1) {
    	var url = url+'/remove';
    } else {
    	swal("",'Invalid data, please review again.','error');
    	$('#change_data').val('');
        return;
    } 
   	swal({
        title: "",
        text: 'Are you sure '+value+'?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Agree",
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    },
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: 'post',
                url: url,
                data: {id: checked},
                success: function(response) {
                    var res = $.parseJSON(response);
                    if (res.status == true) {
                    	swal({
						  title: "",
						  text: "Successful! Popup will close in 0.5 seconds.",
						  type: "success",
						  timer: 1000,
						  showConfirmButton: true
						}, function(isConfirm){
							location.reload();
						});
                    } else {
                    	swal("",res.message, "error");
                    }
                    $('#change_data').val('');
                }
            });
        } else {
        	$('#change_data').val('');
        }
    });
}
function validate(id){
	var value = $('#'+id).val();
	if(value == ''){
		return false;
	}else {
		return true;
	}
}
