var SUCCESS = '<i class="glyphicon glyphicon-ok" style="color: #36a9e1;"></i>&nbsp;&nbsp;Success';
var ERROR = '<i class="fa fa-exclamation-triangle" style="color: red;"></i>&nbsp;&nbsp;Error';
$(document).ready(function () {
    $('#unblock').on('click', function () {
        var id = $('#id_user').val();
        var group_id = $("#group_id option:selected").val();
        $.ajax({
            url: 'user/unblock',
            type: 'POST',
            dataType: 'json',
            data: {
                id: id,
                group_id: group_id,
            },
            success: function () {
                location.reload();
            }
        });
    });

    $('.checked_gallery').click(function () {
        var value = $(this).val();
        if ($(this).is(':checked')) {
            $('#gallery_num' + value).css('background', '#36a9e1');
            $('#gallery_num' + value).css('color', '#fff');
        }
        else {
            $('#gallery_num' + value).css('background', '#fff');
            $('#gallery_num' + value).css('color', '#383e4b');
        }

    });
    $('.checkbox_user').click(function () {
        var i = 0;
        $('[name="user_checked[]"]').each(function () {
            if ($(this).is(':checked')) {
                i++;
            }
        });
        $("#check").html(i);
    });
    $('input[type=checkbox][name=checkall]').click(function () {
        if ($('input[type=checkbox][name=checkall]').is(':checked')) {
            checkboxes = $('.checkbox_user');
            $('#check').html(checkboxes.length);
        }
        else {
            $('#check').html('0');
        }
    });
    $(".btnPushNotification").on('click', function () {
        var users = "";
        $('[name="user_checked[]"]:checked').each(function () {
            users += $(this).attr('value') + ',';
        });
        window.location = "/notification/create?users=" + users;
        return false;
    })
    if (window.location.pathname.indexOf("support/detail") >= 0) {
        setInterval(Support.reload, 5000);
    }

    $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
        if (jqxhr.status == 403) {
            window.location = 'auth/login';
        } else {
            Common.notification(ERROR, 'Error while process data !!!');
        }
    });
    $(".timeline") && $(".timeslot").each(function () {
        var e = $(this).find(".task").outerHeight();
        $(this).css("height", e)
    });
    $('.date-picker').datepicker();
    $('.timepicker').timepicker({
        showSeconds: false,
        minuteStep: 15,
        showMeridian: false,
        showInputs: false,
        defaultTime: false
    });

    $('textarea').autosize();
    // setting
    $("#frm-setting").on('change', "input[name='setting[vip_active]']", function () {
        $("input[name^='setting[vip]']", "#frm-setting").prop("checked", $(this).is(':checked'));
    });
    $("#frm-setting").on('change', "input[name='setting[plan_active]']", function () {
        $("input[name^='setting[plan]']", "#frm-setting").prop("checked", $(this).is(':checked'));
    });
    $("#frm-setting").on('click', ".btn-add-vip-plan", function () {
        var setting_default = {
            vip_active: $("input[name='setting[vip_active]']", "#frm-setting").val(),
            index: $(".vip", ".vips-list").length,
            id: 0,
            plan_name: '',
            active: 0,
            plan_description: '',
            using_days: 0,
            fee: 0
        };
        var html = new EJS({url: 'assets/template/vip.ejs'}).render(setting_default);
        $("td:first", ".vips-list").attr("rowspan", $("tr.vip", ".vips-list").length + 1);
        $(".vips-list").append(html);
    });

    // ###############################
    // ############ PAGES ############
    // ###############################
    // remove page item
    $("#frm-pages-list").on('click', ".page-item-btn-remove", function () {
        var $item = $(this).parent().parent();	// tr.page-item
        var $item_page_id = $item.find('input.page-item-id').val();
        if ($item_page_id > 0) {
            if (confirm("Are you want remove this item?") == true) {
                $item.fadeOut(300, function () {
                    $(this).find("input.page-item-remove").val('1');
                    Pages.refresh();
                });
            }
        } else {
            $item.fadeOut(300, function () {
                $(this).find("input.page-item-remove").val('1');
                Pages.refresh();
            });
        }
    });

    // moveup page item
    $("#frm-pages-list").on('click', ".page-item-btn-up", function () {
        var $item = $(this).parent().parent();	// tr.page-item
        $item.after($item.prevAll('.page-item:visible').eq(0));
        Pages.refresh();
    });

    // movedown page item
    $("#frm-pages-list").on('click', ".page-item-btn-down", function () {
        var $item = $(this).parent().parent();	// tr.page-item
        $item.before($item.nextAll('.page-item:visible').eq(0));
        Pages.refresh();
    });

    // new page item
    $("#frm-pages").on('click', ".add-new-page-item", function () {
        var item_default = {
            index: $(".page-item", "#frm-pages-list").length,
            id: 0,
            title: '',
            content: '',
            order: $(".page-item", "#frm-pages-list").length
        };
        var html = new EJS({url: 'assets/template/page-item.ejs'}).render(item_default);
        $("#frm-pages-list").append(html);
        Pages.refresh();
        $(".page-item-title-edit:last", "#frm-pages-list").focus();
    });

    // ###############################
    // ####### PROFILE MASTER ########
    // ###############################
    // remove profile
    $("#frm-profiles-list").on('click', ".profile-btn-remove", function () {
        var $item = $(this).parent().parent();	// tr.profile
        var $item_profile_id = $item.find('input.profile-id').val();
        if ($item_profile_id > 0) {
            if (confirm("Remove this profile can affected to database system!") == true) {
                $item.fadeOut(300, function () {
                    $(this).find("input.profile-remove").val('1');
                    Profiles.refresh();
                });
            }
        } else {
            $item.fadeOut(300, function () {
                $(this).find("input.profile-remove").val('1');
                Profiles.refresh();
            });
        }
    });

    // moveup page item
    $("#frm-profiles-list").on('click', ".profile-btn-up", function () {
        var $item = $(this).parent().parent();	// tr.profile
        $item.after($item.prevAll('.profile:visible').eq(0));
        Profiles.refresh();
    });

    // movedown page item
    $("#frm-profiles-list").on('click', ".profile-btn-down", function () {
        var $item = $(this).parent().parent();	// tr.profile
        $item.before($item.nextAll('.profile:visible').eq(0));
        Profiles.refresh();
    });

    // new profile
    $("#frm-profiles").on('click', ".add-new-profile", function () {
        var profile_index = $(".profile", "#frm-profiles-list").length;
        var profile_default = {
            index: profile_index,
            profile_id: 0,
            profile_name: '',
            is_input: 1,
            profile_display_order: profile_index,
            ProfileItem: []
        };
        var html = new EJS({url: 'assets/template/profile.ejs'}).render(profile_default);
        $("#frm-profiles-list").append(html);
        Profiles.refresh();
        $(".profile_name", "#frm-profiles").focus();
    });

    // new profile-item
    $("#frm-profiles-list").on('click', ".profile-item-add", function () {
        $profile_item = $(this).parent().parent();
        $list_item = $profile_item.parent();
        // convert button add -> remove
        $profile_item.find(".profile-item-add").removeClass('profile-item-add').removeClass('plus').addClass('profile-item-remove').addClass('remove');

        // append new line to list
        var profile_item_default = {
            index: $list_item.attr("data-profile-index"),
            i: $list_item.find(".profile-item").length
        };
        var html = new EJS({url: 'assets/template/profile-item.ejs'}).render(profile_item_default);
        $list_item.append(html);
        Profiles.refresh();
    });

    // remove profile-item
    $("#frm-profiles-list").on('click', ".profile-item-remove", function () {
        $profile_item = $(this).parent().parent();
        $profile_item_id = $profile_item.find('input.profile-item-id').val();
        if ($profile_item_id > 0) {	// need confirm remove profile item exist in database
            if (confirm("Remove this profile item can affected to database system!") == true) {
                $profile_item.fadeOut(300, function () {
                    $(this).find("input.profile-item-remove").val('1');
                    Profiles.refresh();
                });
            }
        } else {
            $profile_item.fadeOut(300, function () {
                $(this).find("input.profile-item-remove").val('1');
                Profiles.refresh();
            });
        }
    });

    // ###############################
    // ############ STAMP ############
    // ###############################
    // remove stamp item
    $("#frm-stamps-list").on('click', ".stamp-btn-remove", function () {
        var $item = $(this).parent().parent();	// tr.stamp
        var $stamp_id = $item.find('input.stamp-id').val();
        if ($stamp_id > 0) {
            if (confirm("Are you want remove this stamp?") == true) {
                $item.fadeOut(300, function () {
                    $(this).find("input.stamp-remove").val('1');
                    Stamp.refresh();
                });
            }
        } else {
            $item.fadeOut(300, function () {
                $(this).find("input.stamp-remove").val('1');
                Stamp.refresh();
            });
        }
    });

    // moveup stamp item
    $("#frm-stamps-list").on('click', ".stamp-btn-up", function () {
        var $item = $(this).parent().parent();	// tr.page-item
        $item.after($item.prevAll('.stamp:visible').eq(0));
        Stamp.refresh();
    });

    // movedown page item
    $("#frm-stamps-list").on('click', ".stamp-btn-down", function () {
        var $item = $(this).parent().parent();	// tr.page-item
        $item.before($item.nextAll('.stamp:visible').eq(0));
        Stamp.refresh();
    });

    // upload image

    $("#frm-stamps-list").on('change', "input.stamp-input-file", function () {
        Stamp.upload_image($(this).parent().parent().parent().parent());
    });
    // new page item
    $("#frm-stamps").on('click', ".add-new-stamp", function () {
        var item_default = {
            index: $(".stamp", "#frm-stamps-list").length,
            stamp_id: 0,
            image: '',
            image_url: ''
        };
        var html = new EJS({url: 'assets/template/stamp.ejs'}).render(item_default);
        $("#frm-stamps-list").append(html);
        Stamp.refresh();
    });

    // ###############################
    // ########## SUPPORT ############
    // ###############################
    // upload new avatar
    $("#frm-support-profile").on('change', "input.support-profile-image-file", function () {
        Support.upload_profile_image();
        Support.upload_avt();
    });
    $("#frm-support-profile").on('change', "input.support-profile-image-file_fe", function () {
        //	Support.upload_profile_image();
        Support.upload_avt_fe();
    });


    // change order list support detail
    $("#supportItemOrder").on('change', function () {
        $list = $("#SupportDetailList");
        $list.children().each(function (i, li) {
            $list.prepend(li)
        });
    });

    // load template
    $("#frm-support").on('change', 'select#templateChoose', function () {
        $template_id = $(this).val();
        if ($template_id > 0) {
            $("#frm-support").find('textarea').attr('disabled', 'disabled');
            $.ajax({
                url: 'support_template/detail',
                type: 'POST',
                dataType: 'json',
                data: 'id=' + $template_id,
                success: function (data) {
                    $("#frm-support").find('textarea').removeAttr('disabled');
                    if (data.code == 'OK') {
                        $("#frm-support").find('textarea').val(data.data.content);
                        $('textarea').autosize();
                    }
                    else {
                        Common.notification(ERROR, data.message);
                    }
                }
            });
        } else {
            $("#frm-support").find('textarea').val('');
        }
    });

    // ###############################
    // ######## NOTIFICATION #########
    // ###############################
    // count characters
    var mess;
    $("#frmNotification").on("keyup", "textarea[name='notification[message]']", function () {
        mess = $("textarea[name='notification[message]']", "#frmNotification").val();
        if (mess.length > 0) {
            $('button[type=submit]', '#frmNotification').removeAttr('disabled');
        } else {
            $('button[type=submit]', '#frmNotification').attr('disabled', 'disabled');
        }
        if (mess.length <= 50) {
            $("#character-count").text(50 - mess.length);
        }
    });

    // ###############################
    // ########    MESSAGE   #########
    // ###############################
    $("#ConversationDetailList").on("click", ".btn-delete-message", function () {
        $message_id = $(this).attr("data-message-id");
        if (confirm("Are you want delete this message ?") == true) {
            $.ajax({
                url: 'chat/delete_message',
                type: 'POST',
                dataType: 'json',
                data: 'id=' + $message_id,
                success: function (data) {
                    if (data.code == 'OK') {
                        $("#message-id-" + $message_id).fadeOut();
                    }
                    else {
                        Common.notification(ERROR, data.message);
                    }
                }
            });
        }
    });

    $("#frm-conversation-list").on("click", ".conversation-list-checkall", function () {
        if ($(this).prop("checked")) {
            $('input[name="id[]"]', '.table-conversation-list').each(function (index) {
                $(this).prop("checked", true);
            });
        } else {
            $('input[name="id[]"]', '.table-conversation-list').each(function (index) {
                $(this).prop("checked", false);
            });
        }
    });


    // ###############################
    // ########## DASHBOARD ##########
    // ###############################
    $(".btn-show-chart").click(function (t) {
        var chartDetail = $(this).parent().parent().next(".box-content").find('.chart-detail');
        if (!chartDetail.is(":visible")) {
            chartDetail.show("slow", function () {
                loadChart($(this));
            });
        } else {
            chartDetail.hide("slow");
        }
    });
    $(".chart-detail").find('.daterange').val(moment().subtract('30', 'days').format('YYYY/MM/DD') + ' - ' + moment().format('YYYY/MM/DD'));
    $(".chart-detail").find("input[name='date-from']").val(moment().subtract('30', 'days').format('YYYY/MM/DD'));
    $(".chart-detail").find("input[name='date-to']").val(moment().format('YYYY/MM/DD'));
    $('.daterange').daterangepicker({
        format: 'YYYY/MM/DD',
        showDropdowns: true,
        opens: 'left'
    });
    $('.daterange').on('apply.daterangepicker', function (ev, picker) {
        $chartDetail = $(this).parent().parent();
        $chartDetail.find("input[name='date-from']").val(picker.startDate.format('YYYY/MM/DD'));
        $chartDetail.find("input[name='date-to']").val(picker.endDate.format('YYYY/MM/DD'));
        loadChart($chartDetail);
    });

    $('.btn-dashboard-groupby').on('click', 'button', function () {
        $(this).parent().find('.btn-primary').removeClass('btn-primary');
        $(this).addClass('btn-primary');
        loadChart($(this).parent().parent().parent());
    });

    /* circleChart */
    /* ---------- Init jQuery Knob - disbaled in IE8, IE7, IE6 ---------- */
    if (jQuery.browser.version.substring(0, 2) == "8.") {

    } else {
        $('.circleChart').each(function () {
            var circleColor = $(this).parent().css('color');
            $(this).knob({
                'min': 0,
                'max': 100,
                'readOnly': true,
                'width': 120,
                'height': 120,
                'fgColor': circleColor,
                'dynamicDraw': true,
                'thickness': 0.2,
                'tickColorizeValues': true,
                'skin': 'tron'
            });
        });
        $('.circleStatsItem .plus').css('display', 'block');
        $('.circleStatsItem .percent').css('display', 'block');
        $('.circleStatsItem .text').css('display', 'block');
    }


    //Word - advanced search
    $('#search-result-head, .new-meaning').hide()
    showAddMean = function () {
        $('.new-meaning').fadeToggle(500)
        $('#new-meaning-input').hide()
    }
    $('select[name=n-type]').on('change', function (e) {
        console.log(this.value)
        $('#new-meaning-input').show()
    })
    // console.log($('.new-mean').attr('name', 'asdasd'))
    var wmean = function (wtype) {
        r = '';
        $.each(wtype, function (i, e) {
            $.each(e, function (k, v) {
                r += v.mean + '<br>'
            })
        })
        return r
    }
    $('#search-product').keyup(function () {
        var keyword = $(this).val()
        if (keyword.length > 1) {
            $.ajax({
                type: "GET",
                url: "api/word/search_word?keyword=" + keyword,
                success: function (data) {
                    $('#search-result-body').empty()
                    $('#search-result-head').show()
                    $.each(data.data, function (i, e) {
                        var word = JSON.parse(e.Word.meaning_en)
                        $('#search-result-body').append('<tr><td><a class="btn btn-info btn-sm" href="words/view/' + e.Word.id + '"> <i class="fa fa-search-plus "></i> </a> </td> <td><a href="words/view/' + e.Word.id + '">' + word.word + '</a></td><td>' + word.pronounce + '</td><td>' + wmean(word.type) + '</td> </tr>')
                    })
                }
            })
        }
    })
});

var Common = new function () {
    this.notification = function (title, text, image) {
        if (text == '') {
            text = ' ';
        }
        Setting.unique_id_mess = $.gritter.add({
            title: title,
            text: text,
            image: image,
            sticky: false
        });
    };
    this.showLoading = function () {
        $('.page-loading').css('display', 'block');
    };

    this.closeLoading = function () {
        $('.page-loading').fadeOut('slow');
    };
    this.showPopup = function (data) {
        $.prompt(data, {
            show: "slideDown",
            top: "15%",
            buttons: {},
            opacity: 0.15,
            persistent: false,
            zIndex: 999
        });
        return false;
    };

    this.closePopup = function () {
        $('.jqibox').remove();
    };

    this.showValidation = function (formElement, validationErrors, prefix) {
        $('.error', formElement).hide().text('');
        $('.has-error', formElement).removeClass('has-error');
        for (var fieldName in validationErrors) {
            if (prefix) {
                $input = $("[name='" + prefix + "[" + fieldName + "]' ]", formElement);
            } else {
                $input = $("[name='" + fieldName + "']", formElement);
            }
            $input.parent().find('.error').text(validationErrors[fieldName]).show();
            $input.parent().addClass('has-error');
        }
        ;
    };
};

var Storage = new function () {
    this.set = function (key, value) {
        if (typeof(Storage) !== 'undefined') {
            localStorage.setItem(key, value);
        }
    };
    this.get = function (key) {
        if (typeof(Storage) !== 'undefined') {
            return localStorage.getItem(key);
        }
    };
};

var Setting = new function () {
    this.save = function () {
        Common.showLoading();
        $('button[type=submit]', '#frm-setting').attr('disabled', 'disabled');
        $.ajax({
            url: 'setting/save',
            type: 'POST',
            dataType: 'json',
            data: $('#frm-setting').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frm-setting').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    $('body,html').animate({scrollTop: 0}, 500);

                    // fill vipmaster to form
                    if (data.data["vips"].length > 0) {
                        $vip_active = $("input[name='setting[vip_active]']", "#frm-setting").is(':checked');
                        $(".vips-list").html('');
                        for (i = 0; i < data.data["vips"].length; i++) {
                            data.data["vips"][i]['VipMaster']['vip_active'] = $vip_active,
                                data.data["vips"][i]['VipMaster']['index'] = i;
                            var html = new EJS({url: 'assets/template/vip.ejs'}).render(data.data["vips"][i]['VipMaster']);
                            $(".vips-list").append(html);
                        }
                        $("td:first", ".vips-list").attr("rowspan", $("tr.vip", ".vips-list").length + 1);
                    }
                }
                else {
                    Common.notification(ERROR, data.message);
                }
            }
        });

        return false;
    };
};

var Pages = new function () {
    this.save = function () {
        this.refresh();
        Common.showLoading();
        $('button[type=submit]', '#frm-pages').attr('disabled', 'disabled');
        $.ajax({
            url: 'pages/save',
            type: 'POST',
            dataType: 'json',
            data: $('#frm-pages').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frm-pages').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    $('body,html').animate({scrollTop: 0}, 500);
                    Pages.loadPageFromData(data.data);
                }
                else {
                    Common.notification(ERROR, data.message);
                }
            }
        });

        return false;
    };

    this.refresh = function () {
        $(".page-item:visible", "#frm-pages-list").each(function (index) {
            $(this).find(".page-item-index").text((index + 1));
            $(this).find("input.page-item-title").val($(this).find(".page-item-title-edit").html());
            $(this).find("input.page-item-content").val($(this).find(".page-item-content-edit").html());
            $(this).find("input.page-item-order").val(index);
        });
        $(".page-item-btn-up", "#frm-pages-list").attr('disabled', false);
        $(".page-item-btn-up:visible:first", "#frm-pages-list").attr('disabled', true);
        $(".page-item-btn-down", "#frm-pages-list").attr('disabled', false);
        $(".page-item-btn-down:visible:last", "#frm-pages-list").attr('disabled', true);
    };

    this.loadPageFromData = function (data) {
        $("#frm-pages-list").html('');
        for (i = 0; i < data.length; i++) {
            data[i]['Page']['index'] = i;
            var html = new EJS({url: 'assets/template/page-item.ejs'}).render(data[i]['Page']);
            $("#frm-pages-list").append(html);
        }
        this.refresh();
    };
};

var Profiles = new function () {
    this.save = function () {
        if (confirm("Are you want save this setting ?") == true) {
            this.refresh();
            Common.showLoading();
            $('button[type=submit]', '#frm-profiles').attr('disabled', 'disabled');
            $.ajax({
                url: 'masterdata/profile',
                type: 'POST',
                dataType: 'json',
                data: $('#frm-profiles').serialize(),
                success: function (data) {
                    $('button[type=submit]', '#frm-profiles').removeAttr('disabled');
                    Common.closeLoading();
                    if (data.code == 'OK') {
                        Common.notification(SUCCESS, data.message);
                        $('body,html').animate({scrollTop: 0}, 500);
                        Profiles.loadProfileFromData(data.data);
                    }
                    else {
                        Common.notification(ERROR, data.message);
                    }
                }
            });
        }
        return false;
    };

    this.refresh = function () {
        $(".profile:visible", "#frm-profiles-list").each(function (index) {
            $(this).find("input.profile-display-order").val(index);
            $(".profile-item:visible", $(this).find(".profile-item-list")).each(function (i) {
                $(this).find("input.profile-item-display-order").val(i);
            });
        });
        $(".profile-btn-up", "#frm-profiles-list").attr('disabled', false);
        $(".profile-btn-up:visible:first", "#frm-profiles-list").attr('disabled', true);
        $(".profile-btn-down", "#frm-profiles-list").attr('disabled', false);
        $(".profile-btn-down:visible:last", "#frm-profiles-list").attr('disabled', true);
    };

    this.loadProfileFromData = function (data) {
        $("#frm-profiles-list").html('');
        for (i = 0; i < data.length; i++) {
            data[i]['Profile']['index'] = i;
            data[i]['Profile']['ProfileItem'] = data[i]['ProfileItem'];
            var html = new EJS({url: 'assets/template/profile.ejs'}).render(data[i]['Profile']);
            $("#frm-profiles-list").append(html);
        }
        this.refresh();
    };
};

var Masterdata = new function () {
    this.saveBroadcast = function () {
        Common.showLoading();
        $('button[type=submit]', '#frm-masterdata-broadcast').attr('disabled', 'disabled');
        $.ajax({
            url: 'masterdata/broadcast',
            type: 'POST',
            dataType: 'json',
            data: $('#frm-masterdata-broadcast').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frm-masterdata-broadcast').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    $('body,html').animate({scrollTop: 0}, 500);
                }
                else {
                    Common.notification(ERROR, data.message);
                }
            }
        });
        return false;
    };

    this.savePurchase = function () {
        Common.showLoading();
        $('button[type=submit]', '#frm-masterdata-purchase').attr('disabled', 'disabled');
        $.ajax({
            url: 'masterdata/purchase',
            type: 'POST',
            dataType: 'json',
            data: $('#frm-masterdata-purchase').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frm-masterdata-purchase').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    $('body,html').animate({scrollTop: 0}, 500);
                }
                else {
                    Common.notification(ERROR, data.message);
                }
            }
        });
        return false;
    };

    this.saveWords = function () {
        Common.showLoading();
        $('button[type=submit]', '#frm-masterdata-words').attr('disabled', 'disabled');
        $.ajax({
            url: 'masterdata/words',
            type: 'POST',
            dataType: 'json',
            data: $('#frm-masterdata-words').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frm-masterdata-words').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    $('body,html').animate({scrollTop: 0}, 500);
                }
                else {
                    Common.notification(ERROR, data.message);
                }
            }
        });
        return false;
    };
};

var Stamp = new function () {
    this.save = function () {
        Common.showLoading();
        $('button[type=submit]', '#frm-stamps').attr('disabled', 'disabled');
        $.ajax({
            url: 'masterdata/stamp',
            type: 'POST',
            dataType: 'json',
            data: $('#frm-stamps').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frm-stamps').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    $('body,html').animate({scrollTop: 0}, 500);
                    Stamp.loadStampFromData(data.data);
                }
                else {
                    Common.notification(ERROR, data.message);
                }
            }
        });
        return false;
    };

    this.upload_image = function ($stamp) {
        $stamp.find('.error-image').text('');
        $.ajaxFileUpload({
            url: 'masterdata/stamp_upload',
            secureuri: false,
            fileElementId: $stamp.find('input.stamp-input-file').attr('id'),
            dataType: "json",
            success: function (data, status) {
                if (status == 'success') {
                    if (data.code == 'OK') {	// upload success
                        Common.notification(SUCCESS, data.message);
                        $stamp.find('img.stamp-image-display').attr('src', data.data.full_path);
                        $stamp.find('input.stamp-image').val(data.data.file_name);
                    } else {
                        $stamp.find('.error-image').text(data.message);
                    }
                }
                else {
                    $stamp.find('.error-image').text('Upload image has error !');
                }
            }
        });
    };

    this.refresh = function () {
        $(".stamp:visible", "#frm-stamps-list").each(function (index) {
            $(this).find("input.stamp-order").val(index);
        });
        $(".stamp-btn-up", "#frm-stamps-list").attr('disabled', false);
        $(".stamp-btn-up:visible:first", "#frm-stamps-list").attr('disabled', true);
        $(".stamp-btn-down", "#frm-stamps-list").attr('disabled', false);
        $(".stamp-btn-down:visible:last", "#frm-stamps-list").attr('disabled', true);
    };

    this.loadStampFromData = function (data) {
        $("#frm-stamps-list").html('');
        for (i = 0; i < data.length; i++) {
            data[i]['Stamp']['index'] = i;
            var html = new EJS({url: 'assets/template/stamp.ejs'}).render(data[i]['Stamp']);
            $("#frm-stamps-list").append(html);
        }
        this.refresh();
    };
};

var SupportTemplate = new function () {
    this.remove = function (id) {
        if (confirm('Are you want remove this template?')) {
            Common.showLoading();
            $.ajax({
                url: 'support_template/remove',
                type: 'POST',
                dataType: 'json',
                data: 'id=' + id,
                success: function (data) {
                    Common.closeLoading();
                    if (data.code == 'OK') {
                        Common.notification(SUCCESS, data.message);
                        $("tr.support-template-" + id).remove();
                    }
                    else {
                        Common.notification(ERROR, data.message);
                    }
                }
            });
        }
        return false;
    };

    this.add = function () {
        var data = {
            id: 0,
            subject: '',
            content: ''
        };
        var html = new EJS({url: 'assets/template/form-support-template.ejs'}).render(data);
        Common.showPopup(html);
        $('textarea').autosize();
    };

    this.edit = function (id) {
        Common.showLoading();
        $.ajax({
            url: 'support_template/detail',
            type: 'POST',
            dataType: 'json',
            data: 'id=' + id,
            success: function (data) {
                Common.closeLoading();
                if (data.code == 'OK') {
                    var html = new EJS({url: 'assets/template/form-support-template.ejs'}).render(data.data);
                    Common.showPopup(html);
                    $('textarea').autosize();
                }
                else {
                    Common.notification(ERROR, data.message);
                }
            }
        });

    };

    this.save = function () {
        Common.showLoading();
        Common.showValidation('#frm-support-template', null, null);
        $('button[type=submit]', '#frm-support-template').attr('disabled', 'disabled');
        $.ajax({
            url: 'support_template/save',
            type: 'POST',
            dataType: 'json',
            data: $('#frm-support-template').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frm-support-template').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    Common.closePopup();
                    SupportTemplate.reloadList();
                } else if (data.code == 'ERROR_VALIDATION') {
                    Common.showValidation('#frm-support-template', data.data, 'data[SupportTemplate]');
                } else {
                    Common.notification(ERROR, data.message);
                }
            }
        });
        return false;
    };

    this.reloadList = function () {
        $.ajax({
            url: 'support_template/getall',
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if (data.code == 'OK') {
                    $("#support-template-list").html('');
                    for (i = 0; i < data.data.length; i++) {
                        var html = new EJS({url: 'assets/template/support-template-item.ejs'}).render(data.data[i]['SupportTemplate']);
                        $("#support-template-list").append(html);
                    }
                }
            }
        });
    };
};

var Support = new function () {
    this.isReload = false;
    this.save = function () {
        Common.showLoading();
        Common.showValidation('#frm-support', null, null);
        $('button[type=submit]', '#frm-support').attr('disabled', 'disabled');
        $.ajax({
            url: 'support/save',
            type: 'POST',
            dataType: 'json',
            data: $('#frm-support').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frm-support').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    $('textarea', '#frm-support').val('');
                    Support.reload();
                } else {
                    Common.notification(ERROR, 'Error send message to user');
                }
            }
        });
        return false;
    };

    this.reload = function () {
        var conversation_id = $("input[name='support[conversation_id]']").val();
        var last_message_id = $("#ConversationDetailList .timeslot").first().attr('data-message-id');
        if (!Support.isReload) {
            Support.isReload = $.getJSON("support/load_message?id=" + conversation_id + "&last_message_id=" + last_message_id, function (data) {
                var html = new EJS({url: 'assets/template/support-message-list.ejs'}).render(data);
                $("#ConversationDetailList").prepend(html);

                // auto resize all item
                $(".timeline") && $(".timeslot").each(function () {
                    var e = $(this).find(".task").outerHeight();
                    $(this).css("height", e)
                });
                Support.isReload = false;
            });
        }
    };

    this.upload_profile_image = function () {
        $frm = $("#frm-support-profile");
        $frm.find('.error-image').text('');
        $.ajaxFileUpload({
            url: 'support/avatar_upload',
            secureuri: false,
            fileElementId: 'userImageUrl',
            dataType: "json",
            data: $frm.serializeObject(),
            success: function (data, status) {
                if (status == 'success') {
                    if (data.code == 'OK') {	// upload success
                        Common.notification(SUCCESS, data.message);
                        $frm.find('img.support-profile-image-display').attr('src', data.data.image_full_url);
                        $frm.find('input.support-profile-image-id').val(data.data.image_id);
                    } else {
                        $frm.find('.error-image').text(data.message);
                    }
                }
                else {
                    $frm.find('.error-image').text('Upload image has error !');
                }
            }
        });
    };
    this.upload_avt = function () {
        $frm = $("#frm-support-profile");
        $frm.find('.error-image').text('');
        $.ajaxFileUpload({
            url: 'support/avatar_default',
            secureuri: false,
            fileElementId: 'userAvtUrl',
            dataType: "json",
            data: $frm.serializeObject(),
            success: function (data, status) {
                if (status == 'success') {
                    if (data.code == 'OK') {	// upload success
                        Common.notification(SUCCESS, data.message);
                        location.reload();
                    }
                }
            }
        });
    };
    this.upload_avt_fe = function () {
        $frm = $("#frm-support-profile");
        $frm.find('.error-image').text('');
        $.ajaxFileUpload({
            url: 'support/avatar_default',
            secureuri: false,
            fileElementId: 'userAvtUrl_fe',
            dataType: "json",
            data: $frm.serializeObject(),
            success: function (data, status) {
                if (status == 'success') {
                    if (data.code == 'OK') {	// upload success
                        Common.notification(SUCCESS, data.message);
                        location.reload();
                    }
                }
            }
        });
    };


    this.saveProfile = function () {
        Common.showLoading();
        Common.showValidation('#frm-support-profile', null, null);
        $('button[type=submit]', '#frm-support-profile').attr('disabled', 'disabled');
        $.ajax({
            url: 'support/profile',
            type: 'POST',
            dataType: 'json',
            data: $('#frm-support-profile').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frm-support-profile').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    SupportTemplate.reloadList();
                } else if (data.code == 'ERROR_VALIDATION') {
                    Common.showValidation('#frm-support-profile', data.data, 'data[User]');
                } else {
                    Common.notification(ERROR, data.message);
                }
            }
        });
        return false;
    };

};

var Admin = new function () {
    this.save = function () {
        Common.showValidation('#frm-admin-save', null, null);
        $("input[name='data[User][password_confirm]']", '#frm-admin-save').parent().find('.error').text('').hide();

        // check password
        var password = $("input[name='data[User][password]']", '#frm-admin-save').val();
        var password_confirm = $("input[name='data[User][password_confirm]']", '#frm-admin-save').val();
        if (password != password_confirm) {
            $("input[name='data[User][password_confirm]']", '#frm-admin-save').parent().find('.error').text('Password confirm not match').show();
        }
        else {
            Common.showLoading();
            $('button[type=submit]', '#frm-admin-save').attr('disabled', 'disabled');
            $.ajax({
                url: 'admin/save',
                type: 'POST',
                dataType: 'json',
                data: $('#frm-admin-save').serialize(),
                success: function (data) {
                    $('button[type=submit]', '#frm-admin-save').removeAttr('disabled');
                    Common.closeLoading();
                    if (data.code == 'OK') {
                        window.location.href = 'admin?sort=User.id&direction=desc';
                    } else if (data.code == 'ERROR_VALIDATION') {
                        Common.showValidation('#frm-admin-save', data.data, 'data[User]');
                    } else {
                        Common.notification(ERROR, data.message);
                    }
                }
            });
        }
        return false;
    };
};

var Notification = new function () {
    this.save = function () {
        Common.showValidation('#frmNotification', null, null);
        Common.showLoading();
        $('button[type=submit]', '#frmNotification').attr('disabled', 'disabled');
        $.ajax({
            url: 'notification/save',
            type: 'POST',
            dataType: 'json',
            data: $('#frmNotification').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frmNotification').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    if ($('input[name="notification[id]"]', '#frmNotification').val() == "0") {
                        $('#frmNotification')[0].reset();
                        $('button[type=submit]', '#frmNotification').attr('disabled', 'disabled');
                    }
                    $("#character-count").text(50);
                } else if (data.code == 'ERROR_VALIDATION') {
                    Common.showValidation('#frmNotification', data.data, 'notification');
                } else {
                    Common.notification(ERROR, data.message);
                }
            }
        });
        return false;
    }
}

var Chat = new function () {
    this.delete_conversation = function () {
        Common.showLoading();
        $('button[type=submit]', '#frm-conversation-list').attr('disabled', 'disabled');
        $.ajax({
            url: 'chat/delete_conversation',
            type: 'POST',
            dataType: 'json',
            data: $('#frm-conversation-list').serialize(),
            success: function (data) {
                $('button[type=submit]', '#frm-conversation-list').removeAttr('disabled');
                Common.closeLoading();
                if (data.code == 'OK') {
                    Common.notification(SUCCESS, data.message);
                    for (i = 0; i < data.data.ids.length; i++) {
                        $("#conversation-" + data.data.ids[i]).hide();
                    }
                } else {
                    Common.notification(ERROR, data.message);
                }
            }
        });
        return false;
    }
}


function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x - 125,
        border: '1px solid rgb(193, 193, 193)',
        padding: '5px',
        background: '#FFF',
        opacity: 0.85,
        'border-radius': '3px'
    }).appendTo("body").fadeIn(200);
}

var previousPoint = null;
$('.chart-image').bind("plothover", function (event, pos, item) {
    $("#x").text(pos.x.toFixed(2));
    $("#y").text(pos.y.toFixed(2));

    if (item) {
        if (previousPoint != item.dataIndex) {
            previousPoint = item.dataIndex;
            $("#tooltip").remove();
            if (dashboard == 'hour') {
                var x = new Date(item.datapoint[0]).toDateString() + " " + new Date(item.datapoint[0]).getHours() + ":00";
            }
            else {
                var x = new Date(item.datapoint[0]).toDateString();
            }

            y = item.datapoint[1];
            showTooltip(item.pageX, item.pageY, "" + x + "<br/> Total: <b>" + y + "</b>");
        }
    }
    else {
        $("#tooltip").remove();
        previousPoint = null;
    }
});
dashboard = '';
function loadChart(elm) {
    var url = 'dashboard/' + elm.attr('data-url');
    var date_from = elm.find("input[name='date-from']").val();
    var date_to = elm.find("input[name='date-to']").val();
    var group_by = elm.find("button.btn-primary").attr('rel');
    dashboard = group_by

    // console.log('date_from:' + date_from + ' ,date_to:' + date_to + ' ,group_by:' + group_by + ' ,url:' + url);
    $chartImage = elm.find('.chart-image');
    if ($chartImage.length) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {
                'date_from': date_from,
                'date_to': date_to,
                'group_by': group_by
            },
            success: function (data) {
                var plot = $.plot(
                    $chartImage,
                    [{data: data.data}],
                    {
                        xaxis: {
                            mode: "time",
                            // timeformat: "%I:%M %p",
                            minTickSize: [1, "hour"],
                            twelveHourClock: true,
                            ticks: 6,
                            tickDecimals: 0
                        },
                        yaxis: {ticks: 3, tickDecimals: 0},
                        series: {
                            lines: {
                                show: true,
                                lineWidth: 1,
                                fill: true, fillColor: {colors: [{opacity: 0.5}, {opacity: 0.2}]}
                            },
                            points: {
                                show: true,
                                lineWidth: 1
                            },
                            shadowSize: 0
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#f9f9f9",
                            borderWidth: 0
                        },
                        colors: ["#1BB2E9"]
                    }
                );
            }
        });
    }
}


// preview image
function readURL(input) {

    if (input.files && input.files[0]) {
        if (input.files[0].size < 2000000) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image-preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        } else {
            alert('The file size is too large !');
        }
    }
}

//For delete confirm
// Before click delete, confirm
$(".confirmDelete").confirm({
    title: "Delete confirmation",
    text: "This is very dangerous, you shouldn't do it! Are you really really sure?",
    confirmButton: "Yes I am",
    cancelButton: "No"
});
afterConfirm = function (id) {
    var $form = $('form[data-id="' + id + '"]');
    $form.submit();
};

//flash message timeout
setTimeout(function () {
    $("#cake-flash-message").remove();
}, 5000);

//reset user search form
function resetForm($form) {
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox')
        .removeAttr('checked').removeAttr('selected');
    $form.find('.number').val('');
}
$("#reset-filter-search").click(function () {
    resetForm($('.frm-user-search'));
});


// CUSTOM COMMON FORM
$(document).ready(function(){



    /* ---------- Choosen ---------- */
    $('[data-rel="chosen"],[rel="chosen"]').chosen();


});



