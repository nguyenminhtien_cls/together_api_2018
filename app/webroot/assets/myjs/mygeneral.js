/*
    author : ndhung.dev@gmail.com
    created : 12/02/2017
 */
$(document).ready(function() {
    var objectswal = {};
    var dataPost = {};
    var dataCallback = {};
    var url = null;

    // edit inline or remove row
    $('.action-data a').click(function() {
        if ($(this).hasClass('edit-row')) {
            $(this).parents('tr').find('td.editableColumns').each(function(index) {
                var html = $(this).text().trim();
                var input = $('<input class="editableColumnsStyle form-control" type="text" />');
                input.val(html);
                $(this).html(input);
            });
            $(this).parents('tr').find('.update-row').show();
            $(this).hide();
        } else if ($(this).hasClass('update-row')) {
            $(this).parents('tr').find('td.editableColumns').each(function(index) {
                var input = $(this).find('.editableColumnsStyle').val();
                $(this).html(input);
                dataPost[index] = input;
            });
            dataCallback = {};
            objectswal = {
                isconfirm : true
            };
            url = $(this).attr('data-url-update');
            myswalalert(objectswal, url, dataPost, dataCallback);
            $(this).parents('tr').find('.edit-row').show();
            $(this).hide();
        } else if ($(this).hasClass('remove-row')) {
            var id;
            $(this).parents('tr').find('td.id-preview').each(function() {
                id = $(this).text();
            });
            var objectdata= $(this).parents('tr:last');
            url = $(this).attr('data-url-remove');
            objectswal = {
                isconfirm : true,
                confirmbutton : "Yes, delete it!"
            };
            dataPost = {};
            dataCallback.selectordata = objectdata;
            dataCallback.functionCallback = "hideRowOnList";
            myswalalert(objectswal, url, dataPost, dataCallback);
        }
    });
    var hideRowOnList = function (success) {
        if (success == 1) {
            var objectdata = dataCallback.selectordata;
            $(objectdata).remove();
        }
    }

    // change status
    $('.toggle').toggles({text : {on : 'ACTIVE',off : 'INACTIVE'}});
    $('.toggle').on('toggle', function(e, active) {
        var status = 0;
        if (active) {
            var status = 1;
        } 
        dataCallback = {};
        objectswal = {};
        url = $(this).attr('data-url-changestatus')+'/'+status;
        myswalalert(objectswal, url, dataPost, dataCallback);
        status = undefined; 
    });

    var myswalalert = function(objectswal, url, dataPost, dataCallback) {
        console.log(objectswal);
        if (typeof(objectswal.isconfirm)!='undefined' && objectswal.isconfirm != null) {
            swal({
              title: (typeof(objectswal.title)=='undefined' || objectswal.title == null)?"Are you sure?":objectswal.title,
              text: (typeof(objectswal.text)=='undefined' ||objectswal.text == null)?"":objectswal.text,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: (typeof(objectswal.confirmbutton)=='undefined' ||objectswal.confirmbutton == null)?"Ok":objectswal.confirmbutton,
              closeOnConfirm: false
            },
            function(isConfirmAlert){
                if (isConfirmAlert) {
                    callajax(objectswal,url, dataPost, dataCallback);
                } else {
                    resetObjectDefault();
                }
            });
        } else {
            callajax(objectswal, url, dataPost, dataCallback);
        } 
    }

    var callajax = function (objectswal, url, dataPost, dataCallback) {
        $.ajax({
            type: 'post',
            url: url,
            data: {dataPost},
            success: function(response) {
                var res = $.parseJSON(response);
                if (objectswal.isconfirm) {
                    swal((res.error!=0)?"Error":"Done",res.message, (res.error!=0)?"error":"success");
                } else {
                    if (typeof(objectswal.haserror)!='undefined' && objectswal.haserror != null) {
                        swal((res.error!=0)?"Error":"Done",res.message, (res.error!=0)?"error":"success");
                    }
                }

                if (typeof(dataCallback.functionCallback)!='undefined' && dataCallback.functionCallback != null) {
                    if (res.error != 0) {
                        eval(dataCallback.functionCallback+"(0)");
                    } else {
                        eval(dataCallback.functionCallback+"(1)");
                    }
                    
                }
                resetObjectDefault();
            }
        });
    }

    var resetObjectDefault = function() {
        objectswal = {};
        dataPost = {};
        dataCallback = {};
        url = null;
    }
});
