<?php 
App::uses('BaseapiController', 'Controller');
class ClassapiController extends BaseapiController {
    public $components = array('Class', 'Response', 'User');
    public function beforeFilter() {
        parent::beforeFilter();
    }

    /**
     * register method as endpoint
     * @return [type]
     */
    public function index($method_name=null, $params=null) {
        $this->autoRender = false;
        $this->Class->connectModule($method_name);
        http_response_code(204);
    }
} 
