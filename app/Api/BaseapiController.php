<?php
App::uses('Controller', 'Controller');
class BaseapiController extends Controller {
	public $allow_url;
	public $allow_method;
	public $access_token = null;
	public $gets = array();
	public $posts = array();
	public $deleteds = array();
	public $send_request = array();
	public $userinfo = null;

	public function beforeFilter() { 
		// parent::beforeFilter();
		$this->allow_url = DOMAIN.'/'.$this->request->controller.(($this->request->shortcode !=null)?'/'.$this->request->shortcode:'').'/'.$this->request->func;
		$this->access_token = $this->request->query('_token');
		$this->access_token = isset($this->request->data['_token'])?$this->request->data['_token']:$this->access_token;
		$this->_verifyApiAvaiable();
		$this->_verifyToken();
		$this->allow_method = $_SERVER['REQUEST_METHOD'];
		$this->gets = $this->request->query;
		$this->posts = $this->request->data; 
		$this->_urlValidMethod();
		if ($this->allow_method == 'GET') {
			$this->send_request = $this->gets;
		} else {
			$this->send_request = array_merge($this->gets, $this->posts);
		}		
	}

	protected function _verifyApiAvaiable() {
		if (!in_array($this->allow_url, Configure::read('apis_avaiable'))) {
			if (!$this->access_token){
				$this->Response->responseApi(TOKEN_INVALID);
			}
		}
	}

	protected function _verifyToken() {
		if (!in_array($this->allow_url, Configure::read('urls_pass_token'))) {
			if (!$this->access_token){
				$this->Response->responseApi(API_NOT_AVAIABLE);
			} else {
				$this->_checkTokenValid();
			}
		}
	}

	protected function _urlValidMethod() {
		if (isset(Configure::read('urls_pass_method')[$this->allow_url])) {
			$methods = Configure::read('urls_pass_method')[$this->allow_url];
			$this->_validParamsRequire($methods);
		} else {
			$this->Response->responseApi(METHOD_NOT_ALLOW);
		}
	}

	private function _validParamsRequire($methods=array()) {
		if (!empty($methods)) {
			if (!in_array($this->allow_method, array_keys($methods))) {
				$this->Response->responseApi(METHOD_NOT_ALLOW, 'METHOD_NOT_ALLOW : method '.$this->allow_method.' not allow');
			}
			foreach ($methods as $key => $value) {
				if ($this->allow_method != $key || empty($value)) {
					continue;
				}
				$params = [];
				switch ($key) {
					case 'GET':
						$params = $this->gets;
						break;
					case 'POST':
						$params = $this->posts;
						break;
					case 'DELETE':
						$params = $this->deleteds;
						break;
					default:
						$this->Response->responseApi(METHOD_NOT_ALLOW, 'METHOD_NOT_ALLOW : method is invalid');
						break;
				}
				
				$keys_param = array_keys($params);
				$keys_require = array_keys($value);
				
				
				$intersect = array_intersect($keys_param, $keys_require);
				$missings = array_diff($keys_require,$intersect);
				if (!empty($missings)) {
					$this->Response->responseApi(MISSING_PARAMATERS, 'MISSING_PARAMATERS : Missing '.implode(',', $missings));
				}
				$this->_verifyInputType($params, $value);
			}
		} else {
			$this->Response->responseApi(METHOD_NOT_ALLOW, 'METHOD_NOT_ALLOW : all method is deny');
		}
	}

	private function _verifyInputType($inputs=[], $requires=[]) {
		if (!empty($requires)) {
			foreach ($requires as $key => $value) {
				if (isset($inputs[$key]) && gettype($inputs[$key]) != $value ) {
					switch ($value) {
						case 'integer':
							$str_convert = (string)((int) $inputs[$key]);
							break;
						case 'float':
							$str_convert = (string)((float) $inputs[$key]);
							break;
						case 'double':
							$str_convert = (string)((double) $inputs[$key]);
							break;
						
						default:
							break;
					}
					if (in_array($value, array('integer', 'float', 'double'))) {
						if ($inputs[$key] != $str_convert) {
							$this->Response->responseApi(TYPE_OF_PARAMATE_INVALID, 'TYPE_OF_PARAMATE_INVALID : '.$key.' must be '.$value);
						}
					} else {
						$this->Response->responseApi(TYPE_OF_PARAMATE_INVALID, 'TYPE_OF_PARAMATE_INVALID : '.$key.' must be '.$value);
					}
				}
			}
		} 
	}

	protected function _checkTokenValid() {
	    $this->log("_checkTokenValid: ".$this->access_token);
	    $user = $this->get_user_info_by_token($this->access_token);
        if (empty($user)) {
            $this->Response->responseApi(TOKEN_INVALID);
        }

        $this->userinfo = $user;
	}
	
	private function get_user_info_by_token($access_token) {
	    $link_search = OPENSMART_USER_API.'/userInfor/'.$access_token;
	    $data = $this->getdata_openfire($link_search);
	    $data = $this->conver_xml($data);
	    
	    if (empty($data))
	        return null;
	        $data['user'] = $data;
	        
	        return $this->filter_user_info_for_return($data, $access_token);
	}
	
	private function conver_xml($data){
	    $xml = simplexml_load_string ( $data, "SimpleXMLElement", LIBXML_NOCDATA );
	    $json = json_encode ( $xml );
	    $array = json_decode ( $json, TRUE );
	    return $array;
	}
	
	private function getdata_openfire($link) {
	    $ch = curl_init ();
	    curl_setopt_array ( $ch, [
	        CURLOPT_RETURNTRANSFER => 1,
	        CURLOPT_URL => OPENFIRE_HOST . '/' . $link,
	        CURLOPT_HTTPHEADER => [
	            'Content-Type:application/json',
	            'Authorization:3QaqHOlLCF2tOG5D'
	        ]
	    ] );
	    $response = curl_exec ( $ch );
	    curl_close ( $ch );
	    $jsonDecoded = json_decode ( $response ); // Returns an array
	    return $response;
	}
	
	private function filter_user_info_for_return($openFireData, $token) {
	    $userData = array (
	        'username' => $openFireData ['user']['username'],
	        'email' => isset($openFireData ['user']['email'])?$openFireData['user']['email']:'' ,
	        'name' => isset($openFireData['user']['name'])?$openFireData['user']['name']:'',
	        'team_mems' => isset($openFireData['user']['teamMems'])?$openFireData['user']['teamMems']:'',
	        'team_id' => isset($openFireData['user']['roomID'])?$openFireData['user']['roomID']:'',
	    );
	    $property = $openFireData ['user']['properties']['property'];
	    foreach ( $property as $value ) {
	        if ($value ['name'] == 'toeic_level_id') {
	            $userData ['toeic_level_id'] = $value ['value'];
	        }
	        if ($value ['name'] == 'birthday') {
	            $userData ['birthday'] = $value ['value'];
	        }
	        if ($value ['name'] == 'gender') {
	            $userData['gender'] = $value ['value'];
	        }
	        if ($value ['name'] == 'token') {
	            $userData ['token'] = $token;
	        }
	        if ($value ['name'] == 'claps') {
	            $userData ['claps'] = $value ['value'];
	        }
	        if ($value ['name'] == 'goal') {
	            $userData ['goal'] = $value ['value'];
	        }
	        if ($value ['name'] == 'quote') {
	            $userData ['quote'] = $value ['value'];
	        }
	        if ($value ['name'] == 'nick_name') {
	            $userData ['nick_name'] = $value ['value'];
	        }
	        //if ($value ['name'] == 'team_id'){
	        //	$userData['team_id'] = $value['value'];
	        //}
	        if ($value ['name'] == 'together_id'){
	            $userData['together_id'] = $value['value'];
	        }
	        if ($value ['name'] == 'native'){
	            $userData['native'] = $value['value'];
	        }
	        if ($value ['name'] == 'description'){
	            $userData['description'] = $value['value'];
	        }
	        if ($value ['name'] == 'full_name'){
	            $userData['full_name'] = $value['value'];
	        }
	        if ($value ['name'] == 'facebook_id'){
	            $userData['facebook_id'] = $value['value'];
	        }
	    }
	    // Open Fire config (Client will use this for connecting to XMPP)
	    $userData ['openfire_server'] = XMPP_HOST;
	    $userData ['openfire_port'] = XMPP_PORT;
	    
	    return $userData;
	}
}