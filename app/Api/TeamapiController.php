<?php 
App::uses('BaseapiController', 'Controller');
class TeamapiController extends BaseapiController {
    public $components = array('Team', 'Response', 'User');
    public function beforeFilter() {
        parent::beforeFilter();
    }

    /**
     * register method as endpoint
     * @return [type]
     */
    public function index($method_name=null, $params=null) {
        $this->autoRender = false;
        $this->Team->connectModule($method_name);
        http_response_code(204);
    }
} 
