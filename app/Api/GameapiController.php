<?php 
App::uses('BaseapiController', 'Controller');
class GameapiController extends BaseapiController {
    public $time_today;
    public $components = array('Game', 'Response', 'SentenceGame', 'PronounceGame', 'Team', 'User');
    public function beforeFilter() {
        parent::beforeFilter();
        $this->getTimeToday();
    }

    /**
     * register method as endpoint
     * @return [type]
     */
    public function index($short_code=null, $method=null) {
        $game_module = ucfirst($short_code).'Game';
        $this->{$game_module}->connectModule($method);
        http_response_code(204);
    }

    public function getTimeToday() {
        $time = time();
        $now_time = $time;
        $date_now = date('Y-m-d', $now_time);
        $array_date_now = explode('-', $date_now);
        $int_day_now = date('w', $now_time);
        $obj_date = new DateTime($date_now);
        $week = $obj_date->format("W");
        $days = ['monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday', 'sunday']; 
        $day_now = $days[$int_day_now-1];

        $this->time_today = array(
            'year' => $array_date_now[0],
            'month' => (int) $array_date_now[1],
            'day' => (int) $array_date_now[2],
            'week' => (int) $week,
            'day_in_week' => $int_day_now,
            'date' => $day_now,
            'time' => $time 
        );
        $this->time_today;
    }
} 
