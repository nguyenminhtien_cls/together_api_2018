<?php
App::uses('BaseapiController', 'Controller');
class UploadapiController extends BaseapiController {
    public $components = array('User', 'Upload', 'Response');
    public function beforeFilter() {
        parent::beforeFilter();
    }

    /**
     * register method as endpoint
     * @return [type]
     */
    public function index($method_name=null, $params=null) {
        $this->autoRender = false;
        $this->Upload->connectModule($method_name);
        http_response_code(204);
    }
}
