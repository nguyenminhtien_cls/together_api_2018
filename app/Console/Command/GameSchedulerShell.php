<?php
class GameSchedulerShell extends AppShell {
	public $uses = array('Game', 'Teams', 'GameScheduler', 'GameDataSentence', 'GameDataPronounce', 'WordCollection');

	public function main() {
		//run shell
		file_put_contents('/tmp/crontabtogether', date('Y-m-d H:is').PHP_EOL, FILE_APPEND);
		$this->out('====> Starting shell game scheduler ====='.PHP_EOL);
		$times = $this->getTomorowTime();
		// check point setting
		$points = $this->GameScheduler->find('all', array(
			'conditions' => array(
				'day' => $times['date'],
				'status_bash_timepoints' => true,
				'bash_timepoints != ' => '[]' 
			)
		));
		if (!empty($points)) {
			foreach ($points as $k_point => $v_point) {
				$gameShortCode = $v_point['Game']['short_code'];
				$gameClass = ucfirst($gameShortCode.'Game');
				App::uses($gameClass, 'Console/Command/Task'); 
				$params = [
					'times' => $times,
					'point' => $v_point
				];
				$gameClass = new $gameClass($params); 
				call_user_func(array($gameClass, 'process')); 
			} 
		}
		$this->out('====> Finished shell game scheduler ====='.PHP_EOL);
	}

	private function getTomorowTime() {
		$time = time();
		$tomorrow_time = $time + 86400;
		$date_tomorrow = date('Y-m-d', $tomorrow_time);
		$array_date_tomorrow = explode('-', $date_tomorrow);
		$int_day_tomorrow = date('w', $tomorrow_time);
		$obj_date = new DateTime($date_tomorrow);
		$week = $obj_date->format("W");
		$days = ['monday', 'tuesday', 'wednesday','thursday', 'friday', 'saturday', 'sunday']; 
		$day_tomorrow = $days[$int_day_tomorrow-1];

		$data = array(
			'year' => $array_date_tomorrow[0],
			'month' => (int) $array_date_tomorrow[1],
			'day' => (int) $array_date_tomorrow[2],
			'week' => (int) $week,
			'day_in_week' => $int_day_tomorrow,
			'date' => $day_tomorrow 
		);
		
		return $data;
	}
}