<?php

/**
 * Created by PhpStorm.
 * User: SonLT
 * Date: 8/2/2016
 * Time: 9:16 AM
 */
class MakeGameDataByWeekShell extends AppShell {

    public $uses = array('Game1', 'Team', 'GameData');

    public function main() {
        $this->out('Start make game data by week');
        $teams = $this->Team->getAllTeamsInfo();
        foreach ($teams as $key => $team) {
            $team_id = $team["Team"]["id"];
            $category_id = $team["Team"]['category_id'];
            $member = array();
            $member[1] = $team["Team"]['member1_id'];
            $member[2] = $team["Team"]['member2_id'];
            $member[3] = $team["Team"]['member3_id'];

            $info['team_id'] = $team_id;
            $info['words_category'] = $category_id;
            $info['week'] = date('W', strtotime("+1 week"));
            $info['month'] = date('m', strtotime("+1 week"));
            $info['year'] = date('Y', strtotime("+1 week"));
            for ($i = 1; $i < 4; $i++) {
                $info['user_id'] = (!empty($member[$i])) ? $member[$i] : 0;
                $info['game_type'] = 'pronounce';
                if (!$this->check_question_in_week($info)) {
                    for ($day = 1; $day < 6; $day++) {
                        $info['day_in_week'] = $day;
                        $this->Game1->GetAllWordsCollection($category_id);
                        $game_data = $this->Game1->GetRamdom5Words();
                        $info['word1'] = $game_data['words1'];
                        $info['word2'] = $game_data['words2'];
                        $info['word3'] = $game_data['words3'];
                        $info['word4'] = $game_data['words4'];
                        $info['word5'] = $game_data['words5'];
                        $this->GameData->save($info);
                        $this->GameData->clear();
                    }
                };
                $info['game_type'] = 'make_sentence';
                if (!$this->check_question_in_week($info)) {
                    for ($day = 1; $day < 6; $day++) {
                        $info['day_in_week'] = $day;
                        $this->Game1->GetAllWordsCollection($category_id);
                        $game_data = $this->Game1->GetRamdom5Words();
                        $info['word1'] = $game_data['words1'];
                        $info['word2'] = $game_data['words2'];
                        $info['word3'] = $game_data['words3'];
                        $info['word4'] = $game_data['words4'];
                        $info['word5'] = $game_data['words5'];
                        $this->GameData->save($info);
                        $this->GameData->clear();
                    }
                };
            }
        }
        $this->out('Done make game data by week');

        // backup table
//        if (date('d') == "01") {
//            $this->Game1->BackupGameData();
//            $this->Game1->DeleteOldGameData();
//        }
    }

    public function check_question_in_week($condition) {
        $check = $this->GameData->find('all', array(
            'conditions' => array(
                'GameData.game_type' => $condition['game_type'],
                'GameData.week' => $condition['week'],
                'GameData.month' => $condition['month'],
                'GameData.year' => $condition['year'],
                'GameData.words_category' => $condition['words_category'],
                'GameData.team_id' => $condition['team_id'],
                'GameData.user_id' => $condition['user_id'],
            )
        ));
        return $check;
    }

}
