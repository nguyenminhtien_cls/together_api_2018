<?php
/**
 *
 * @author Admin
 *        
 */
class MakePronounceGameDataShell extends AppShell
{
    // TODO - Insert your code here
    public $uses = array('Game', 'OfMucRoom', 'OfUser', 'GameWords', 'GameTimes', 'GameDates', 'GameStubs', 'Word');
    
    public function main() {
        
        $this->out(PHP_EOL.'====================Start make game data by week!===================='.PHP_EOL);
        $this->out(PHP_EOL.'===================='.date('d M Y H:i:s').'===================='.PHP_EOL);
        
        $users = $this->OfMucRoom->getAllTeamsInfo();
        
        $game_id = $this->getGameID('pronounce');
        if($game_id == null) return;
        
        $gameDates = $this->getGameDates($game_id);
        if($gameDates== null) return;
        
        $gameTimes = $this->getGameTimes($game_id);
        if($gameTimes== null) return;
        
        foreach($users as $key => $user) { 
            $team_id = $user['ofMucRoom']['roomID'];
            $username = $user['ofMucMember']['nickname'];
            
            $this->out('====Account: '.$username);
            
            foreach($gameDates as $keyDate=>$date)
            {
                $this->out('=======Date: '.$this->genGameDate($date['GameDates']['date']));
                foreach($gameTimes as $keyTime=>$time)
                {
                    //Tao ban ghi stub
                    $stub['game_id'] = $game_id;
                    $stub['game_date_id'] = $date['GameDates']['id'];
                    $stub['game_time_id'] = $time['GameTimes']['id'];
                    $stub['username'] = $username;
                    $stub['team_id'] = $team_id;
                    $stub['game_date'] = $this->genGameDate($date['GameDates']['date']);
                    $stub['create_date'] = date('Ymd');
                    $stub['status'] = true;
                    
                    $condition = array(
                        'game_id' => $game_id,
                        'game_date_id' => $date['GameDates']['id'],
                        'game_time_id' => $time['GameTimes']['id'],
                        'username' => $username,
                        'team_id' => $team_id,
                        'game_date' => $this->genGameDate($date['GameDates']['date'])
                    );
                    
                    //Xoa cac stub cu
                    $this->GameStubs->deleteAll($condition , false);
                    
                    //Them ban ghi stub
                    $this->GameStubs->saveAssociated($stub);
                    
                    $stub_id = $this->GameStubs->find('first', array(
                        'conditions' => $condition,
                        'fields' => array('id') 
                    ))['GameStubs']['id'];
                    
                    $words = $this->Word->getRandanWord($username);
                    
                    for($i = 1; $i <= 5; $i++)
                    {
                        //Tao du lieu word cho game
                        $word = $words[array_rand($words)]["words"]['id'];
                        $this->out('============Word Id: '.$word);
                        
                        $gameWord['stub_id'] = $stub_id;
                        $gameWord['word_id'] = $word;
                        $gameWord['create_time'] = date('Ymd');
                        $this->GameWords->saveAssociated($gameWord);
                    }
                }
            }
        }
        $this->out(PHP_EOL.'===================='.date('d M Y H:i:s').'===================='.PHP_EOL);
        $this->out(PHP_EOL.'====================Done make game data by week!===================='.PHP_EOL);
    }
    
    public function check_question_in_week($condition) {
        $check = $this->GameData->find('all', array(
            'conditions' => array(
                'GameData.game_type' => $condition['game_type'],
                'GameData.week' => $condition['week'],
                'GameData.month' => $condition['month'],
                'GameData.year' => $condition['year'],
                'GameData.words_category' => $condition['words_category'],
                'GameData.team_id' => $condition['team_id'],
                'GameData.user_id' => $condition['user_id'],
            )
        ));
        return $check;
    }
    
    /**
     * Lay thong tin game id
     * @param String code of game
     * @return int id of game
     */
    private function getGameID($game_code)
    {
        $gameEnt = $this->Game->find('first', array(
            'conditions' => array(
                'short_code' => $game_code
            )
        ));
        
        if(!$gameEnt)
        {
            return null;
        }
        
        return $gameEnt['Game']['id'];
    }
    
    private function getGameTimes($game_id)
    {
        $gameTimesEnt = $this->GameTimes->find('all', array(
            'conditions' => array(
                'game_id' => $game_id
            )
        ));
        
        if(!$gameTimesEnt)
        {
            return null;
        }
        return $gameTimesEnt;
    }
    
    private function getGameDates($game_id)
    {
        $gameDatesEnt = $this->GameDates->find('all', array(
            'conditions' => array(
                'game_id' => $game_id
            )
        ));
        
        if(!$gameDatesEnt)
        {
            return null;
        }
        return $gameDatesEnt;
    }
    
    private function genGameDate($day)
    {
        $year = date('Y', strtotime("+1 week"));
        $week = date('W', strtotime("+0 week"));
        $game_date = date('Ymd', strtotime( $year . "W". $week . $day));
        return $game_date;
    }
}

