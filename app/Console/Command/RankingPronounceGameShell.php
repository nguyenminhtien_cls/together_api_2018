<?php

App::uses('Xmpp', 'Lib');
App::uses('PushNotifications', 'Lib/Notification');

class RankingPronounceGameShell extends AppShell {

    public $uses = array('Ranking', 'User');

    public function main() {
        $this->out('Start ranking pronounce game');
        $year = date('Y');
        $week = date('W');
        $month = date('m');
        $sql = "select * from (SELECT team_id, name as roomName, naturalName as teamName, SUM(score) AS total_score, SUM(first_times) AS first_times, SUM(second_times) AS second_times, SUM(third_times) AS third_times,"
            ." sum(total_time_first) as total_time_first,"
                ." sum(total_time_second) as total_time_second,"
                    ." sum(total_time_thirt) as total_time_thirt"
                        ." FROM game_result_temp INNER JOIN ofMucRoom ON ofMucRoom.roomID = game_result_temp.team_id"
                            ." WHERE game_result_temp.year = " . $year . " and game_result_temp.week= " . $week . " and status = 1 "
                                ." GROUP BY team_id, name, naturalName order by total_score desc, first_times desc, second_times desc, third_times desc, total_time_first asc, total_time_second asc, total_time_thirt asc) DATA";
        
        $datas = $this->Ranking->query($sql);
        $rank = 1;
        foreach($datas as $key=>$data)
        {
            //array_push($results, $data['DATA']);
            $ranking['week'] = $week;
            $ranking['team_id'] = $data['DATA']['team_id'];
            $ranking['month'] = $month;
            $ranking['year'] = $year;
            $ranking['score'] = $data['DATA']['total_score'];
            $ranking['first_times'] = $data['DATA']['first_times'];
            $ranking['second_times'] = $data['DATA']['second_times'];
            $ranking['third_times'] = $data['DATA']['third_times'];
            $ranking['total_time_first'] = $data['DATA']['total_time_first'];
            $ranking['total_time_second'] = $data['DATA']['total_time_second'];
            $ranking['total_time_thirt'] = $data['DATA']['total_time_thirt'];
            $ranking['rank'] = $rank;
            $rank ++;
            
            $this->Ranking->saveAssociated($ranking);
        }
        
        $this->out('Done ranking pronounce game');
    }

    public function send_push($user_id) {
        try {
            // Message payload
            $msg_payload = array(
                'mtitle' => 'Game ranking',
                'mdesc' => 'Ranking for together game this week'
            );
            $push_notification = new PushNotifications();
            $user_data = $this->User->find('first', array(
                'fields' => array('User.push_token', 'User.device_type'),
                'conditions' => array(
                    'User.id' => $user_id
                )
            ));
            switch ($user_data['User']['device_type']) {
                case 'ios':
                    $mess = $push_notification->iOS($msg_payload, $user_data['User']['push_token'], array('ranking' => 'true'));
                    break;
                case 'android':
                    $mess = $push_notification->android($msg_payload, $user_data['User']['push_token']);
                    break;
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}
