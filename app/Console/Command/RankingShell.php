<?php

App::uses('Xmpp', 'Lib');
App::uses('PushNotifications', 'Lib/Notification');

class RankingShell extends AppShell {

    public $uses = array('Game1', 'Team', 'GameData', 'Ranking', 'User');

    public function main() {
        $this->out('Start ranking');
        $teams = $this->Team->getAllTeamsInfo();
        foreach ($teams as $key => $team) {
            $team_id = $team["Team"]["id"];
            $category_id = !empty($team["Team"]['category_id']) ? $team["Team"]['category_id'] : '';
            $member = array();
            $member[1] = $team["Team"]['member1_id'];
            $member[2] = $team["Team"]['member2_id'];
            $member[3] = $team["Team"]['member3_id'];
            $info['team_id'] = $team_id;
            $info['words_category'] = $category_id;
            $info['week'] = date('W');
            $info['month'] = date('m');
            $info['year'] = date('Y');
            $sum_clap = 0;
            for ($i = 1; $i < 4; $i++) {
                $info['user_id'] = $member[$i];
                $sum = $this->get_sum($info);
                $info['member_score_' . $i] = $sum['sum_score'];
                $sum_clap = $sum_clap + $sum['sum_clap'];
                $this->out($member[$i]);
                $this->send_push($member[$i]);
            }
            $info['score'] = $info['member_score_1'] + $info['member_score_2'] + $info['member_score_3'];
            $info['clap'] = $sum_clap;
            unset($info['user_id']);
            if ($this->Ranking->save($info)) {
                
            };
            $this->Ranking->clear();
        }
        $this->out('Done ranking');
    }

    public function get_sum($condition) {
        $check = $this->GameData->find('all', array(
            'fields' => array('sum(GameData.score) as sum_score', 'sum(GameData.clap) as sum_clap'),
            'conditions' => array(
                'GameData.week' => $condition['week'],
                'GameData.month' => $condition['month'],
                'GameData.year' => $condition['year'],
                'GameData.words_category' => $condition['words_category'],
                'GameData.team_id' => $condition['team_id'],
                'GameData.user_id' => $condition['user_id'],
            )
        ));
        if ($check) {
            return $check[0][0];
        } else {
            return array('sum_score' => 0, 'sum_clap' => 0);
        }
    }

    public function send_push($user_id) {
        try {
            // Message payload
            $msg_payload = array(
                'mtitle' => 'Game ranking',
                'mdesc' => 'Ranking for together game this week'
            );
            $push_notification = new PushNotifications();
            $user_data = $this->User->find('first', array(
                'fields' => array('User.push_token', 'User.device_type'),
                'conditions' => array(
                    'User.id' => $user_id
                )
            ));
            switch ($user_data['User']['device_type']) {
                case 'ios':
                    $mess = $push_notification->iOS($msg_payload, $user_data['User']['push_token'], array('ranking' => 'true'));
                    break;
                case 'android':
                    $mess = $push_notification->android($msg_payload, $user_data['User']['push_token']);
                    break;
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}
