<?php

App::uses('PushNotifications', 'Lib/Notification');

class SendReservedPushShell extends AppShell {

    public $uses = array('Game1', 'Team', 'GameData', 'Ranking', 'User');

    public function main() {
        $this->out('Send Reserved Push');
        $teams = $this->Team->getAllTeamsInfo();
        foreach ($teams as $key => $team) {
            $team_id = $team["Team"]["id"];
            $category_id = $team["Team"]['category_id'];
            $member = array();
            $member[1] = $team["Team"]['member1_id'];
            $member[2] = $team["Team"]['member2_id'];
            $member[3] = $team["Team"]['member3_id'];
            $info['team_id'] = $team_id;
            $info['words_category'] = $category_id;
            $info['week'] = date('W');
            $info['month'] = date('m');
            $info['year'] = date('Y');
            $days = array('Sunday' => 0, 'Monday' => 1, 'Tuesday' => 2, 'Wednesday' => 3, 'Thursday' => 4, 'Friday' => 5, 'Saturday' => 6);
            $day_str = date('l');
            $info['day_in_week'] = $days[$day_str];
            for ($i = 1; $i < 4; $i++) {
                $info['user_id'] = $member[$i];
                $data_question = $this->get_question($info);
                if ($data_question) {
                    $user_data = $this->User->find('first', array(
                        'fields' => array('User.push_token', 'User.device_type'),
                        'conditions' => array(
                            'User.id' => $member[$i]
                        )
                    ));
                    if ($user_data) {
                        $this->send_push($user_data['User'], $data_question['GameData']);
                    }
                }
            }
        }
        $this->out('Done Send Reserved Push');
    }

    public function get_question($condition) {
        $check = $this->GameData->find('first', array(
            'fields' => array('word1', 'word2', 'word3', 'word4', 'word5', 'game_type'),
            'conditions' => array(
                'GameData.day_in_week' => $condition['day_in_week'],
                'GameData.week' => $condition['week'],
                'GameData.month' => $condition['month'],
                'GameData.year' => $condition['year'],
                'GameData.words_category' => $condition['words_category'],
                'GameData.team_id' => $condition['team_id'],
                'GameData.user_id' => $condition['user_id'],
            )
        ));
        return $check;
    }

    public function send_push($user_data, $game_data) {
        try {
            // Message payload
            $msg_payload = array(
                'mtitle' => 'Game today',
                'mdesc' => 'Question for game ' . $game_data['game_type'],
            );
            $push_notification = new PushNotifications();
            switch ($user_data['device_type']) {
                case 'ios':
                    $mess = $push_notification->iOS($msg_payload, $user_data['push_token'], $game_data);
                    break;
                case 'android':
                    $mess = $push_notification->android($msg_payload, $user_data['User']['push_token']);
                    break;
            }
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}
