<?php
/**
* 
*/
class PronounceGame extends AppShell {
	public $uses = array('Game', 'Teams', 'GameScheduler', 'GameDataPronounce', 'WordCollection');
	public $params;
	public function __construct($params=array())
	{
		parent::__construct();
		$this->params = $params;
	}

	function process() {
		$v_point = $this->params['point'];
		$times = $this->params['times'];
		// get teams
		$page = 1;
		$limit = 20;
		while ($page<=1000) {
			// abstract class 
			$teams = $this->Teams->find('all', array(
				'conditions' => array(
					'member1_id != ' => NULL, 
					'member2_id != ' => NULL, 
					'member3_id != ' => NULL,
					'is_published' => 1 
				),
				'page' => $page,
				'limit' => $limit
			));
			if (empty($teams)) {
				break;
			}
			$page++;
			foreach ($teams as $k_team => $v_team) {
				// check data insert
				$gameDataModel = 'GameData'.ucfirst($v_point['Game']['short_code']);
				$exist_data = $this->{$gameDataModel}->find('first', array(
					'conditions' => array(
						'team_id' => $v_team["Teams"]['id'],
						'day_in_week' => $times['day_in_week'],
						'week' => $times['week'],
						'year' => $times['year']
					)
				));	
				if (empty($exist_data)) {
					// generate data for user in team
					$records[$gameDataModel] = array();
					for($i=1; $i<=3; $i++) {
						// get words in collections
						$dataGame = $this->GetRamdom5Words($this->getAllWordsCollection($v_team['Teams']['collection_id']));
						if (count($dataGame) == 5) {
							$insert = array(
								'team_id' => $v_team["Teams"]['id'],
								'day_in_week' => $times['day_in_week'],
								'week' => $times['week'],
								'year' => $times['year'],
								'month' => $times['month'],
								'collection_id' => $v_team['Teams']['collection_id'],
								'user_id' => $v_team['Teams']["member{$i}_id"]
							);
							$insert = array_merge($insert, $dataGame);
							$records[$gameDataModel][][$gameDataModel] = $insert;
						} else {
							$data_not_enough = true;
						}
					}
					if(!isset($data_not_enough)) {
						$this->{$gameDataModel}->saveAll($records[$gameDataModel]);
                		$this->{$gameDataModel}->clear();
					}
					unset($data_not_enough);
				}
			}
		}
	}

	/**
	 * get words in collection
	 */
	private function  getAllWordsCollection($word_collection_id)
    {
        $sql = 'select * from ';
        $sql .= '(SELECT word FROM word_collection';
        $sql .= ' inner join words';
        $sql .= ' where word_collection.word_id = words.id';
        $sql .= ' and collection_id=' . $word_collection_id . ') AS A';
        $sql .= ' ORDER BY RAND( )';

        $result = $this->WordCollection->query($sql);
        return $result;
    }

    /**
     * ramdom 5 words in list words
     */
    private function GetRamdom5Words($list_words_random=array()) {
    	$arrWord = array();
    	if (count($list_words_random)>=5) {
    		for($i=1; $i<=5; $i++) {
    			$arrWord["word{$i}"] = $list_words_random[array_rand($list_words_random)]["A"]["word"];
    		}
    	} 
        return $arrWord;
    }
}