<?php

/**
 * Created by PhpStorm.
 * User: SonLT
 * Date: 8/2/2016
 * Time: 9:16 AM
 */
class MakeGameDataByMonthShell extends AppShell {

    public $uses = array('Game1', 'Team');

    public function main() {
        $this->out('start crontab');
        // create table
        $this->Game1->CreateTableGameByMonth();

        // insert to table
        // get team info
        $teams = $this->Team->getAllTeamsInfo();

        //
        foreach ($teams as $key => $team) {

            $team_id = $team["Team"]["id"];
            $category_id = $team["Team"]['category_id'];

            $member1_id = $team["Team"]['member1_id'];
            $member2_id = $team["Team"]['member2_id'];
            $member3_id = $team["Team"]['member3_id'];

            $info['team_id'] = $team_id;
            $info['words_category'] = $category_id;


            for ($week = 1; $week < 5; $week++) {
                $info['week'] = $week;
                for ($day = 1; $day < 6; $day++) {
                    $game['game_type'] = 'pronounce';
                    $this->Game1->GetAllWordsCollection($category_id);

                    $game['mem1_data'] = $this->Game1->GetRamdom5Words();
                    $game['mem2_data'] = $this->Game1->GetRamdom5Words();
                    $game['mem3_data'] = $this->Game1->GetRamdom5Words();

                    $info['day' . $day] = $game;
                }
                // insert to table
                $this->Game1->InsertDataToTableGame($info);
            }
        }
    }

}
