<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/gameapi/:shortcode/:func', array('controller' => 'gameapi', 'action' => 'index'), array('pass' => array('shortcode', 'func')));
Router::connect('/userapi/:func/*', array('controller' => 'userapi', 'action' => 'index'), array('pass' => array('func')));
Router::connect('/teamapi/:func/*', array('controller' => 'teamapi', 'action' => 'index'), array('pass' => array('func')));
Router::connect('/classapi/:func/*', array('controller' => 'classapi', 'action' => 'index'), array('pass' => array('func')));
Router::connect('/uploadapi/:func/*', array('controller' => 'uploadapi', 'action' => 'index'), array('pass' => array('func')));
Router::connect('/pushNotifications/:func/*', array('controller' => 'pushNotifications', 'action' => 'index'), array('pass' => array('func')));

Router::connect('/', array('controller' => 'home', 'action' => 'index'));

Router::connect('/forgot-password', array('controller' => 'ForgotPassword', 'action' => 'index'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
// Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
// $api_list = array('user', 'community', 'word', 'conversation', 'message', 'upload', 'team', 'favorite', 'game', 'test');
// foreach ($api_list as $controller_name) {
//     $controller_name = strtolower($controller_name);
//     Router::connect('/api/' . $controller_name, array('controller' => $controller_name . 'Api', 'action' => 'index'));
//     Router::connect('/api/' . $controller_name . '/:action', array('controller' => $controller_name . 'Api', 'action' => ':action'));
// }


/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
