<?php

define('REWARD', 10);
// url is allow
$config['apis_avaiable'] = array(
    // user
    DOMAIN . '/userapi/normal_register',
    DOMAIN . '/userapi/get_all_user',
    DOMAIN . '/userapi/register',
    DOMAIN . '/userapi/normal_login',
    DOMAIN . '/userapi/facebook_login',
    DOMAIN . '/userapi/google_login',
    DOMAIN . '/userapi/twitter_login',
    DOMAIN . '/userapi/logout',
    DOMAIN . '/userapi/check_username',
    DOMAIN . '/userapi/userinfo',
    DOMAIN . '/userapi/update_profile',
    DOMAIN . '/userapi/check_user',
    DOMAIN . '/userapi/get_toeic',
    DOMAIN . '/userapi/get_user_info',
    DOMAIN . '/userapi/forgot_password',
    DOMAIN . '/userapi/get_users_participants',
    DOMAIN . '/userapi/search_users_participants',
    DOMAIN . '/userapi/search_user',
    DOMAIN . '/userapi/get_info_other',
    DOMAIN . '/userapi/update_push_token',
    DOMAIN . '/userapi/leave_team',
    DOMAIN . '/userapi/update_favorite',
    DOMAIN . '/userapi/search_individuals',
    DOMAIN . '/userapi/get_iq_messages',
    DOMAIN . '/userapi/get_last_chat_messages',
    DOMAIN . '/userapi/update_privacy',
    DOMAIN . '/userapi/get_privacies',
    DOMAIN . '/userapi/get_facebookid',
    DOMAIN . '/userapi/get_chat_type',
    DOMAIN . '/userapi/get_list_pairchat',
    DOMAIN . '/userapi/get_list_teamchat',
    DOMAIN . '/userapi/get_chatmessages',
    DOMAIN . '/userapi/get_teamchatmessages',
    // team
    DOMAIN . '/teamapi/create_team',
    DOMAIN . '/teamapi/get_team',
    DOMAIN . '/teamapi/join_team',
    DOMAIN . '/teamapi/update_team',
    DOMAIN . '/teamapi/leave_team',
    DOMAIN . '/teamapi/get_users',
    DOMAIN . '/teamapi/get_results_of_team',
    DOMAIN . '/teamapi/get_ranking_team',
    DOMAIN . '/teamapi/get_all_team',
    DOMAIN . '/teamapi/get_detail_team',
    DOMAIN . '/teamapi/search_team',
    // game
    DOMAIN . '/gameapi/sentence/today_game',
    DOMAIN . '/gameapi/pronounce/today_game',
    DOMAIN . '/gameapi/sentence/submit_answer',
    DOMAIN . '/gameapi/pronounce/submit_answer',
    DOMAIN . '/gameapi/sentence/get_ranking',
    DOMAIN . '/gameapi/pronounce/get_ranking',
    DOMAIN . '/gameapi/sentence/add_clap',
    DOMAIN . '/gameapi/pronounce/add_clap',
    DOMAIN . '/gameapi/sentence/scoring_make_sentence',
    DOMAIN . '/gameapi/pronounce/get_pronounce_data',
    DOMAIN . '/gameapi/pronounce/submit_pronounce_file',
    DOMAIN . '/gameapi/pronounce/create_game_data',
    DOMAIN . '/gameapi/pronounce/get_pronounce_file',
    DOMAIN . '/gameapi/pronounce/get_result_this_week',
    DOMAIN . '/gameapi/pronounce/get_ranking_last_week',
    DOMAIN . '/gameapi/pronounce/get_current_stubs',
    DOMAIN . '/gameapi/pronounce/get_ranking',
    DOMAIN . '/gameapi/pronounce/get_speech2text_file',
    //upload
    DOMAIN . '/uploadapi/upload_avatar_users',
    DOMAIN . '/uploadapi/upload_avatar_groups',
    DOMAIN . '/uploadapi/get_avatar_users',
    DOMAIN . '/uploadapi/get_avatar_groups',
    DOMAIN . '/uploadapi/upload_chat_images',
    DOMAIN . '/uploadapi/get_chat_images',
    DOMAIN . '/uploadapi/upload_chat_videos',
    DOMAIN . '/uploadapi/get_chat_videos',
    DOMAIN . '/uploadapi/get_chat_thumbnail',
    //push notifications
    DOMAIN . '/pushNotifications/android',
    DOMAIN . '/pushNotifications/iOS',
    //class
    DOMAIN . '/classapi/create_class',
    DOMAIN . '/classapi/get_all_class'
);

// url is pass access_token
$config['urls_pass_token'] = array(
    //user
    DOMAIN . '/userapi/normal_register',
    DOMAIN . '/userapi/normal_login',
    DOMAIN . '/userapi/facebook_login',
    DOMAIN . '/userapi/google_login',
    DOMAIN . '/userapi/twitter_login',
    DOMAIN . '/userapi/logout',
    //DOMAIN.'/userapi/get_all_user',
    DOMAIN . '/userapi/check_user',
    DOMAIN . '/userapi/get_toeic',
    DOMAIN . '/userapi/get_user_info',
    DOMAIN . '/userapi/forgot_password',
    DOMAIN . '/userapi/get_facebookid',
    DOMAIN . '/userapi/get_list_pairchat',
    DOMAIN . '/userapi/get_list_teamchat',
    DOMAIN . '/userapi/get_chatmessages',
    DOMAIN . '/userapi/get_teamchatmessages',
    //game
    DOMAIN . '/gameapi/pronounce/get_pronounce_data',
    DOMAIN . '/gameapi/pronounce/submit_pronounce_file',
    DOMAIN . '/gameapi/pronounce/create_game_data',
    DOMAIN . '/gameapi/pronounce/get_pronounce_file',
    DOMAIN . '/gameapi/pronounce/get_result_this_week',
    DOMAIN . '/gameapi/pronounce/get_ranking_last_week',
    DOMAIN . '/gameapi/pronounce/get_current_stubs',
    DOMAIN . '/gameapi/pronounce/get_ranking',
    DOMAIN . '/gameapi/pronounce/get_speech2text_file',
    //upload
    //DOMAIN.'/uploadapi/upload_avatar_users',
    //DOMAIN.'/uploadapi/upload_avatar_groups',
    //DOMAIN.'/uploadapi/get_avatar_users',
    //DOMAIN.'/uploadapi/get_avatar_groups',
    DOMAIN . '/uploadapi/upload_chat_images',
    DOMAIN . '/uploadapi/get_chat_images',
    DOMAIN . '/uploadapi/upload_chat_videos',
    DOMAIN . '/uploadapi/get_chat_videos',
    DOMAIN . '/uploadapi/get_chat_thumbnail',
    //push notifications
    DOMAIN . '/pushNotifications/android',
    DOMAIN . '/pushNotifications/iOS',
    //DOMAIN . '/classapi/create_class',
    //DOMAIN . '/classapi/get_all_class'
);

// url using method
$config['urls_pass_method'] = array(
    //USER
    DOMAIN . '/userapi/normal_register' => [
        'POST' => array(
            'email' => 'string',
            'password' => 'string',
        )
    ],
    DOMAIN . '/userapi/normal_login' => [
        'POST' => array(
            'username' => 'string',
            'password' => 'string'
        )
    ],
    DOMAIN . '/userapi/logout' => [
        'POST' => array(
            'email' => 'string'
        )
    ],
    DOMAIN . '/userapi/get_user_info' => [
        'POST' => array(
            'email' => 'string'
        )
    ],
    DOMAIN . '/userapi/get_info_other' => [
        'POST' => array(
            'username' => 'string'
        )
    ],
    DOMAIN . '/userapi/get_users_participants' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/search_users_participants' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/search_user' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/get_toeic' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/userinfo' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/check_user' => [
        'POST' => array()
    ],
    DOMAIN . '/userapi/forgot_password' => [
        'POST' => array(
            'email' => 'string',
        )
    ],
    DOMAIN . '/userapi/get_all_user' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/update_profile' => [
        'POST' => array(
            'email' => 'string'
        //,
        //'pass_word' => 'string',
        )
    ],
    DOMAIN . '/userapi/update_push_token' => [
        'POST' => array(
            'email' => 'string',
            'push_token' => 'string'
        )
    ],
    DOMAIN . '/userapi/leave_team' => [
        'POST' => array(
            'email' => 'string',
            'team_id' => 'string'
        )
    ],
    DOMAIN . '/userapi/update_favorite' => [
        'POST' => array(
            'username' => 'string',
            'favoriteduser' => 'string',
            'status' => 'integer'
        )
    ],
    DOMAIN . '/userapi/search_individuals' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/get_iq_messages' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/get_last_chat_messages' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/update_privacy' => [
        'POST' => array(
            'username' => 'string',
            'name' => 'string',
            'show' => 'string',
            '_token' => 'string'
        )
    ],
    DOMAIN . '/userapi/get_privacies' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/get_facebookid' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/get_chat_type' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/get_list_pairchat' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/get_list_teamchat' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/get_chatmessages' => [
        'GET' => array()
    ],
    DOMAIN . '/userapi/get_teamchatmessages' => [
        'GET' => array()
    ],
    //TEAM	
    DOMAIN . '/classapi/create_class' => [
        'POST' => array(
            'class_name' => 'string',
            'description' => 'string',
        )
    ],
    DOMAIN . '/classapi/get_all_class' => [
        'GET' => array()
    ],
    DOMAIN . '/teamapi/get_all_team' => [
        'GET' => array()
    ],
    DOMAIN . '/teamapi/search_team' => [
        'GET' => array()
    ],
    DOMAIN . '/teamapi/get_detail_team' => [
        'GET' => array()
    ],
    DOMAIN . '/teamapi/get_users' => [
        'GET' => array()
    ],
    DOMAIN . '/teamapi/get_team' => [
        'GET' => array()
    ],
    DOMAIN . '/teamapi/create_team' => [
        'POST' => array(
            'team_name' => 'string',
        )
    ],
    DOMAIN . '/teamapi/join_team' => [
        'POST' => array(
            'team_id' => 'integer',
        )
    ],
    DOMAIN . '/teamapi/leave_team' => [
        'POST' => array(
            'team_id' => 'integer'
        )
    ],
    DOMAIN . '/teamapi/update_team' => [
        'POST' => array(
            'room_id' => 'integer'
        )
    ],
    DOMAIN . '/teamapi/ranking' => [
        'GET' => array(
        )
    ],
    DOMAIN . '/teamapi/results_of_team' => [
        'GET' => array(
            'timestamp' => 'integer',
            'team_id' => 'integer'
        )
    ],
    //GAME
    DOMAIN . '/gameapi/sentence/today_game' => [
        'GET' => array(
            'team_id' => 'integer'
        )
    ],
    DOMAIN . '/gameapi/pronounce/today_game' => [
        'GET' => array(
        )
    ],
    DOMAIN . '/gameapi/sentence/submit_answer' => [
        'POST' => array(
            'team_id' => 'integer',
            'answer_num' => 'integer',
            'answer' => 'string'
        )
    ],
    DOMAIN . '/gameapi/pronounce/submit_answer' => [
        'POST' => array(
            'team_id' => 'integer',
            'answer_num' => 'integer',
            'answer' => 'string'
        )
    ],
    DOMAIN . '/gameapi/sentence/add_clap' => [
        'POST' => array(
            'team_id' => 'integer',
            'answer_num' => 'integer',
            'answer' => 'string'
        )
    ],
    DOMAIN . '/gameapi/pronounce/add_clap' => [
        'POST' => array(
            'team_id' => 'integer',
            'answer_num' => 'integer',
            'answer' => 'string'
        )
    ],
    DOMAIN . '/gameapi/pronounce/get_pronounce_data' => [
        'GET' => array(
            'team_id' => 'integer',
            'user_name' => 'string',
            'game_code' => 'string'
        )
    ],
    DOMAIN . '/gameapi/pronounce/submit_pronounce_file' => [
        'POST' => array(
            'user_name' => 'string',
        )
    ],
    DOMAIN . '/gameapi/pronounce/create_game_data' => [
        'GET' => array()
    ],
    DOMAIN . '/gameapi/pronounce/get_pronounce_file' => [
        'GET' => array()
    ],
    DOMAIN . '/gameapi/pronounce/get_result_this_week' => [
        'GET' => array()
    ],
    DOMAIN . '/gameapi/pronounce/get_ranking_last_week' => [
        'GET' => array()
    ],
    DOMAIN . '/gameapi/pronounce/get_current_stubs' => [
        'GET' => array()
    ],
    DOMAIN . '/gameapi/pronounce/get_ranking' => [
        'GET' => array()
    ],
    DOMAIN . '/gameapi/pronounce/get_speech2text_file' => [
        'GET' => array()
    ],
    //UPLOAD
    DOMAIN . '/uploadapi/upload_avatar_users' => [
        'POST' => array(
            'user_name' => 'string',
//            'file'=>array("image/jpg","image/png")
        )
    ],
    DOMAIN . '/uploadapi/upload_avatar_groups' => [
        'POST' => array()
    ],
    DOMAIN . '/uploadapi/get_avatar_groups' => [
        'GET' => array()
    ],
    DOMAIN . '/uploadapi/get_avatar_users' => [
        'GET' => array()
    ],
    DOMAIN . '/uploadapi/upload_chat_images' => [
        'POST' => array(
            'user_name' => 'string',
        )
    ],
    DOMAIN . '/uploadapi/get_chat_images' => [
        'GET' => array()
    ],
    DOMAIN . '/uploadapi/upload_chat_videos' => [
        'POST' => array(
            'user_name' => 'string',
        )
    ],
    DOMAIN . '/uploadapi/get_chat_videos' => [
        'GET' => array()
    ],
    DOMAIN . '/uploadapi/get_chat_thumbnail' => [
        'GET' => array()
    ],
    //PUSH
    DOMAIN . '/pushNotifications/android' => [
        'POST' => array(
        )
    ],
    DOMAIN . '/pushNotifications/iOS' => [
        'POST' => array(
        )
    ],
);
return $config;
