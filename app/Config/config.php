<?php

$config['project_title'] = "Together";
$config['version'] = '1.0';
$base_folder = '@' . str_replace(APP_DIR . '/' . WEBROOT_DIR . '/' . 'index.php', '', $_SERVER['PHP_SELF']);
$base_folder = str_replace('@/', '', $base_folder);
define('BASE_URL', Router::url('/', true) . $base_folder);
define('DOMAIN', rtrim(Router::url('/', true), '/'));

include 'configValidURL.php';
include 'configResponseERROR.php';

// upload config dir
$config['upload_path'] = '/uploads/images/';
$config['image_dir'] = WWW_ROOT . 'uploads/images/';
$config['image_url'] = BASE_URL . 'uploads/images/';
$config['image_default'] = BASE_URL . 'uploads/images/default.jpg';

$config['file_dir'] = WWW_ROOT . 'uploads/files/';
$config['upload_file_path'] = 'uploads/files/';

$config['upload_image_dir'] = WWW_ROOT . 'assets/uploads/files/';
$config['upload_image_path'] = DOMAIN . '/assets/uploads/files/';
$config['upload_chat_images_dir'] = WWW_ROOT . 'assets/uploads/files/chat/images/';
$config['upload_chat_images_path'] = DOMAIN . '/assets/uploads/files/chat/images/';

$config['upload_pronounce_audio_dir'] = WWW_ROOT . 'assets/uploads/folder/games/pronounce/mp3/';
$config['upload_pronounce_audio_path'] = DOMAIN . '/assets/uploads/folder/games/pronounce/mp3/';
$config['upload_pronounce_audio_wav_dir'] = WWW_ROOT . 'assets/uploads/folder/games/pronounce/wav/';
$config['upload_pronounce_audio_wav_path'] = DOMAIN . '/assets/uploads/folder/games/pronounce/wav/';

$config['upload_chat_videos_dir'] = WWW_ROOT . 'assets/uploads/files/chat/videos/';
$config['upload_chat_videos_path'] = DOMAIN . '/assets/uploads/files/chat/videos/';
$config['upload_chat_thumbnail_dir'] = WWW_ROOT . 'assets/uploads/files/chat/thumbnail/';
$config['upload_chat_thumbnail_path'] = DOMAIN . '/assets/uploads/files/chat/thumbnail/';

$config['upload_question_bank'] = WWW_ROOT . 'assets/uploads/files/question_bank/';
$config['upload_question_bank_path'] = DOMAIN . '/assets/uploads/files/question_bank/';


// $config['openfire']['server'] = '18.224.6.138';
$config['openfire']['server'] = '18.221.219.0';
$config['openfire']['port'] = 5222;
$config['openfire']['admin_id'] = 1;

$config['user_fields'] = array('User.id', 'User.username', 'User.word_category', 'User.display', 'User.email', 'User.nickname', 'User.birthday', 'User.gender', 'User.avatar', 'User.quote', 'User.goal', 'User.toeic_level_id', 'User.team_id', 'User.account_status');
$config['toeic_fields'] = array('level', 'title', 'point_from', 'point_to');

//define message type to show
define('MESS_TYPE', 'text');
// API host
define('OPENFIRE_HOST', 'http://ec2-18-224-6-138.us-east-2.compute.amazonaws.com:9090');
//define('XMPP_HOST', 'ip-172-31-17-210.us-east-2.compute.internal');
define('XMPP_HOST', 'ec2-18-224-6-138.us-east-2.compute.amazonaws.com');
//define('OPENFIRE_HOST', 'http://localhost:9090');
//define('XMPP_HOST', 'localhost');
define('XMPP_PORT', '5222');
//define('XMPP_PORT', '9090');
//config mail sever
/* user Link API */

define('API_TEAM', 'plugins/opensmart_api/v1/chatrooms');
define('API_GET_ALL_USER', 'plugins/opensmart_api/v1/users');
define('API_GET_USER_NOTEAM', 'plugins/opensmart_api/v1/users/filter');
define('API_GAME', 'plugins/opensmart_api/v1/games');

define('AUTHOR', 'msirYgswy42RT8wU');
define('SEND_EMAIL', 'soncnpmk53@gmail.com');


define('OPENFIRE_USER_API', 'plugins/restapi/v1/users');
define('OPENSMART_USER_API', 'plugins/opensmart_api/v1/users');
define('OPENSMART_MESSAGE_API', 'plugins/opensmart_api/v1/messages');

//config push
define('PEM_KEY', '/home/ubuntu/www/together-project/together-api/app/Controller/Component/ios_push_certificate.pem');
define('API_ACCESS_KEY', 'AIzaSyAWxKphZbNN2yFOOxkvDXqOtIQ_nVkcoz4');
define('DEV_ENV_PUSH', 1);
define('DIS_ENV_PUSH', 0);

$config['left_menu'] = array(
    array(
        'icon' => 'fa  fa-dashboard',
        'title' => 'Dashboad',
        'url' => 'home/',
        'childrens' => false
    ),
    array(
        'icon' => 'fa fa-cogs',
        'title' => 'Community',
        'url' => '#',
        'childrens' => array(
            array(
                'icon' => 'fa fa-user',
                'title' => 'Leaner',
                'url' => 'users/'
            ),
            array(
                'icon' => 'fa fa-users',
                'title' => 'Class',
                'url' => '/teams'
            ),
//            array(
//                'icon' => 'fa fa-line-chart',
//                'title' => 'Ranking',
//                'url' => '/ranking'
//            ),
            array(
                'icon' => 'fa fa-comments',
                'title' => 'Conversation',
                'url' => '/chat/conversation'
            ),
//            array(
//                'icon' => 'fa fa-bell',
//                'title' => 'Notification',
//                'url' => 'javascript:;'
//            ),
        )
    ),
    array(
        'icon' => 'fa fa-book',
        'title' => 'Mentor',
        'url' => 'home/',
        'childrens' => array(
            array(
                'icon' => 'fa fa-list',
                'title' => 'List class',
                'url' => 'mentor/list_class'
            ),
            array(
                'icon' => 'fa fa-list',
                'title' => 'Questions',
                'url' => 'mentor/questions'
            ),
//            array(
//                'icon' => 'fa fa-list',
//                'title' => 'Words',
//                'url' => 'words/'
//            ),
//            array(
//                'icon' => 'fa fa-list',
//                'title' => 'Collections',
//                'url' => 'collection/'
//            ),
//            array(
//                'icon' => 'fa fa-list',
//                'title' => 'Toeic levels',
//                'url' => 'ToeicLevel/'
//            ),
        )
    ),
//    array(
//        'icon' => 'fa fa-align-justify',
//        'title' => 'Game manager',
//        'url' => '',
//        'childrens' => array(
//            array(
//                'icon' => 'fa fa-gamepad',
//                'title' => 'Games',
//                'url' => 'games/'
//            )
//        )
//    ),
//    array(
//        'icon' => 'fa fa-book',
//        'title' => 'Dictionary',
//        'url' => 'home/',
//        'childrens' => array(
//            array(
//                'icon' => 'fa fa-list',
//                'title' => 'Words',
//                'url' => 'words/'
//            ),
//            array(
//                'icon' => 'fa fa-list',
//                'title' => 'Collections',
//                'url' => 'collection/'
//            ),
//            array(
//                'icon' => 'fa fa-list',
//                'title' => 'Toeic levels',
//                'url' => 'ToeicLevel/'
//            ),
//        )
//    ),
//    array(
//        'icon' => 'fa fa-question-circle',
//        'title' => 'Question bank',
//        'url' => 'question/',
//        'childrens' => array(
//        //    array(
//        //        'icon' => 'fa fa-bar-chart-o',
//        //        'title' => 'Question',
//        //        'url' => 'question/'
//        //    ),
//        //    array(
//        //        'icon' => 'fa fa-bar-chart-o',
//        //        'title' => 'Add',
//        //        'url' => 'question/add'
//        //    )
//        )
//    ),
//    array(
//        'icon' => 'fa  fa-building-o',
//        'title' => 'Privacy',
//        'url' => 'privacy/',
//        'childrens' => false
//    ),
//    array(
//        'icon' => 'fa  fa-building-o',
//        'title' => 'Report',
//        'url' => 'javascript:;',
//        'childrens' => false
//    ),
//    array(
//        'icon' => 'fa fa-wikipedia-w',
//        'title' => 'Master tool',
//        'url' => 'javascript:;',
//        'childrens' => false
//    ),
//    array(
//        'icon' => 'fa fa-sliders',
//        'title' => 'Settings',
//        'url' => '#',
//        'childrens' => array(
//            array(
//                'icon' => 'fa fa-cogs',
//                'title' => 'General',
//                'url' => 'setting'
//            ),
//            array(
//                'icon' => 'fa fa-cogs',
//                'title' => 'Admin Users',
//                'url' => 'admin'
//            ),
//            array(
//                'icon' => 'fa fa-cogs',
//                'title' => 'Static Pages',
//                'url' => 'setting/staticpage'
//            ),
//        )
//    ),
//    array(
//        'icon' => 'fa fa-address-book',
//        'title' => 'My account',
//        'url' => 'home/',
//        'childrens' => array(
//            array(
//                'icon' => 'fa fa-key',
//                'title' => 'Change Password',
//                'url' => 'admin/changepassword'
//            ),
//            array(
//                'icon' => 'fa fa-power-off',
//                'title' => 'Logout',
//                'url' => 'auth/logout'
//            ),
//        )
//    ),
);

$config['test_bar'] = array(
    array(
        'icon' => 'fa fa-user',
        'title' => 'Create user',
        'url' => 'autotest/index',
        'childrens' => array(
            array(
                'icon' => 'fa fa-list-ul',
                'title' => 'List user test',
                'url' => 'autotest/index'
            ),
            array(
                'title' => 'Create user',
                'url' => 'autotest/create_user',
                'icon' => ' fa fa-comments-o'
            )
        )
    ),
    array(
        'icon' => 'fa fa-users',
        'title' => 'Create team',
        'url' => 'autotest/team',
        'childrens' => array(
            array(
                'icon' => 'fa fa-list-ul',
                'title' => 'Team 2 people',
                'url' => 'autotest/create_team_2'
            ),
            array(
                'title' => 'Team 3 people',
                'url' => 'autotest/create_team_3',
                'icon' => ' fa fa-comments-o'
            )
        )
    ),
);
