<?php
define('SUCCESS',0);
//Common code
define('TOKEN_INVALID',100001);
define('MISSING_PARAMATERS',100002);
define('MISSING_CONFIG_REQUIRE',100003);
define('TYPE_OF_PARAMATE_INVALID',100004);
define('SAVE_DATA_FALSE',100005);
define('SYSTEM_ERROR',100006);
define('UNKNOWN_ERROR',100007);
define('API_NOT_AVAIABLE',100008);
define('METHOD_NOT_ALLOW',100009);

//Utility code
define('PUSH_MESSAGE_FAIL',100101);

//User API code
define('ACCOUNT_ALREADY_EXIST',100201);
define('EMAIL_ALREADY_EXIST',100202);
define('USER_OR_EMAIL_INVALID',100203);
define('EMAIL_INVALID',100204);
define('USER_DATA_INVALID',100205);
define('PASSWORD_WRONG',100206);
define('USER_NOT_AVAI',100207);
define('UPDATE_FAVORITE_FAIL',100208);
define('USERNAME_OR_FAVORITEDUSER_EMPTY',100209);
define('LEAVE_TEAM_FAIL',100210);
define('TOGETHER_ID_WAS_EXIST',100211);
define('TOGETHER_ID_WAS_SET',100212);
define('UPDATE_PRIVACY_FAIL',100213);
define('FACEBOOK_ID_EMPTY',100214);
define('FACEBOOK_ID_EXIST',100215);
define('FACEBOOK_ID_NOT_EXIST',100216);

//Team API code
define('TEAM_INVALID',100301);
define('TEAM_IS_EXIST',100302);
define('TEAM_IS_FULL',100303);
define('TEAM_NOT_EXIST',100304);
define('YOU_JOINING_TEAM_ANOTHER',100305);
define('YOU_ARE_THIS_TEAM',100306);
define('YOU_NOT_BELONG_TEAM',100307);
define('YOU_ARE_ALREADY_ANSWER',100308);
define('TEAM_ID_EMPTY',100309);


//Game API code
define('GAME_DATA_EMPTY',100401);
define('GAME_OVER_SUBMIT',100402);
define('GAME_STUB_EMPTY',100403);


$config['api_error'] = array(
	0	   => 'SUCCESS',
    //Common
	100001 => 'TOKEN_INVALID: Account is invalid or disable',
	100002 => 'MISSING_PARAMATERS: missing paramaters',
    100003 => 'MISSING_CONFIG_REQUIRE: missing config require param',
    100004 => 'TYPE_OF_PARAMATE_INVALID: type of data invalid ',
    100005 => 'SAVE_DATA_FALSE: Cannot save data',
    100006 => 'SYSTEM_ERROR: System operation errors',
    100007 => 'UNKNOWN_ERROR: Can not detect error',
    100008 => 'API_NOT_AVAIABLE: Api not avaliable',
    100009 => 'METHOD_NOT_ALLOW: This method not allow',
    
    //Utility
    100101 => 'PUSH_MESSAGE_FAIL: Message not delivered',
    
    //User    
	100201 => 'ACCOUNT_ALREADY_EXIST: Account already exists',
	100202 => 'EMAIL_ALREADY_EXIST: Email already exists',
    100203 => 'USER_OR_EMAIL_INVALID: Username or password is invalid',
    100204 => 'EMAIL_INVALID: Email is invalid',
    100205 => 'USER_DATA_INVALID: User data is invalid',
    100206 => 'PASSWORD_WRONG: Password is wrong',
    100207 => 'USER_NOT_AVAI: User not exists',
    100208 => 'UPDATE_FAVORITE_FAIL: Cannot update favorite',
    100209 => 'USERNAME_OR_FAVORITEDUSER_EMPTY: Username or favorited user is empty',
    100210 => 'LEAVE_TEAM_FAIL: Cannot leave team',
    100211 => 'TOGETHER_ID_WAS_EXIST: Together id was exist',
    100212 => 'TOGETHER_ID_WAS_SET: Together id was set',
    100213 => 'UPDATE_PRIVACY_FAIL: Update privacy was fail',
    100214 => 'FACEBOOK_ID_EMPTY: Facebook id is empty',
    100215 => 'FACEBOOK_ID_EXIST: Facebook id was exist',
    100216 => 'FACEBOOK_ID_NOT_EXIST: Facebook id was not exist',
    
    //Team
	100301 => 'TEAM_INVALID: Invalid team',
	100302 => 'TEAM_IS_EXIST: Team is exist',
    100303 => 'TEAM_IS_FULL: Team is full',
	100304 => 'TEAM_NOT_EXIST: Team is not exist',
    100305 => 'YOU_JOINING_TEAM_ANOTHER: You are joining another team',
    100306 => 'YOU_ARE_THIS_TEAM: You are here',
	100307 => 'YOU_NOT_BELONG_TEAM: You do not belong to this team',
	100308 => 'YOU_ARE_ALREADY_ANSWER: You are already answer',
    100309 => 'TEAM_ID_EMPTY: Team id is empty',

    //Game
	100401 => 'GAME_DATA_EMPTY: Game data empty',
    100402 => 'GAME_OVER_SUBMIT: Game over submit',
    100403 => 'GAME_STUB_EMPTY: Game stub empty'
);
return $config;
