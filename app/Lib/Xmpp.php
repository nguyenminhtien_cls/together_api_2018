<?php

include 'XMPPHP/XMPP.php';

class Xmpp {
	public $conn;

	public function __construct($username, $password)
	{
		$this->conn = new XMPPHP_XMPP(Configure::read('openfire.server'), Configure::read('openfire.port'), $username, $password, 'admincp');
	}

	public function connect()
	{
		try {
		    $this->conn->connect();
		    $this->conn->processUntil('session_start');
		    return TRUE;
		} catch(XMPPHP_Exception $e) {
		    return FALSE;
		}
	}

	public function send_message($to, $type, $data)
	{
		try {
			$to = $to . '@' . Configure::read('openfire.server');
		    $this->conn->message($to, '{"type":"' . $type . '","data":' . $data . '}');
		    return TRUE;
		} catch(XMPPHP_Exception $e) {
		    return FALSE;
		}
	}

	public function disconnect()
	{
		$this->conn->disconnect();
	}
}